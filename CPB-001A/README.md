# CPB-001A Reverse Engineering

![CPB-001A Board](doc/CPB-001A.png)

## PDF Render

[Click here to explore the schematics online](https://petitl.fr/cps2/CPB-001A/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/CPB-001A.pdf).

[![CPB-001A](https://petitl.fr/cps2/CPB-001A/CPB-001A.svg)](https://petitl.fr/cps2/CPB-001A/)

## Notes
* This was done using image material only. It's been checked by community efforts but if you spot a mistake please contact me.
* The CPB-001A image comes from the [Arcade Otaku Wiki](https://wiki.arcadeotaku.com/w/Capcom_QSound_Amplifier).

## Authors & Licence
See main README.
