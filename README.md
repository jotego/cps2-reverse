# CPS2 Reverse Engineering Project
The dream goal of this project is to lay out the full schematics of all the CPS2 boards. For years, CPS2 held its secrets but it has prevented arcade collectors to preserve their board in their prime glory. This knowledge repository should help repairs and even complete reproduction if we are able to fully understand each custom chip.

![CPS2](https://wiki.arcadeotaku.com/images/d/d3/CPS2.gif)

## Format
This project is fully written in KiCad.

One particularity that may trouble you: KiCad does not like when references do not end with a number while Capcom referenced their ICs using `10H` for instance. I've used `UH10` to point to the aforementioned reference. Also, bypass capacitors are not numbered
but KiCad wants it numbered so the prefix are conserved (CX and CCX) but a number has been added.

All the unknown pins are noted with the prefix `UK`.

## Progress
| Board | Description | Reference pictures | Traces | Schematics |
| ------ | ------ | ------ | ------ | ------ |
| [93646A](./93646A/README.md) | Main motherboard | Missing DL decap | 100% | 95% (cleaning TBD) |
| [93646B](./93646B/README.md) | Game board | Missing DL decap | 100% | 100% |
| 93646C | Complete ROM extension | See undamned | 0% | 0% |
| 93646D | Network extension | See undamned | 0% | 0% |
| [93646E](./93646E/README.md) | QSound ROM extension | See undamned | 100% | 100% |
| 93661G | Complete ROM extension | See undamned | 0% | 0% |
| 00716C | Flash ROM extension | See undamned | 0% | 0% |
| [CPB-001A](./CPB-001A/README.md) | QSound Amplifier |  | 100% | 90% |
| [(DL-xxxx)](./DLs/README.md) | Custom DL chips |  | 40% | 40% |

It goes without saying but: this is a project where traces were followed by hand and there may be issues!

## Custom lib
There is a custom lib that contains all the symbols that are not part of the standard library of KiCad. Feel free to exploit this but it's still very WIP.

## Help needed
Any knowledge or any pictures that shows any of the aforementioned board internals is definitely welcome. Please ping me on twitter.

## Licence
Unless mentioned otherwise, for reference pictures for instance, this work is distributed under the terms of the licence [CC-BY](http://creativecommons.org/licenses/by/4.0/).

## Authors
* [Loïc _WydD_ Petit](https://twitter.com/WydD)

Additional knowledge has been provided by:
* [Undamned](https://twitter.com/therealundamned/)
* [Eduardo Cruz](http://arcadehacker.blogspot.com/)
* Repair logs that can be found on various forums.
