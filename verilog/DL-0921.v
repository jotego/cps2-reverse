SolderJumper_2_Bridged JP1(
    .a         ( Net__JP1_Pad1_           ) /* pin 1*/ ,
    .b         ( Net__JP1_Pad2_           ) /* pin 2*/ 
);

SolderJumper_2_Bridged JP2(
    .a         ( Net__JP1_Pad2_           ) /* pin 1*/ ,
    .b         ( Net__JP2_Pad2_           ) /* pin 2*/ 
);

PIN P1(
    .p         ( _XSEL                    ) /* pin 0*/ 
);

PIN P10(
    .p         ( __TD1                    ) /* pin 0*/ 
);

PIN P100(
    .p         ( _LUT11                   ) /* pin 0*/ 
);

PIN P101(
    .p         ( Net__P101_Pad~_          ) /* pin 0*/ 
);

PIN P102(
    .p         ( _palette_control_~LUT_WR ) /* pin 0*/ 
);

PIN P103(
    .p         ( _main_signals_~RESETI    ) /* pin 0*/ 
);

PIN P104(
    .p         ( _main_signals_~CLK8M     ) /* pin 0*/ 
);

PIN P105(

);

PIN P106(
    .p         ( _bus_signal_control_~CK10 ) /* pin 0*/ 
);

PIN P107(
    .p         ( _TI0                     ) /* pin 0*/ 
);

PIN P108(
    .p         ( _bus_signal_control_~CK500 ) /* pin 0*/ 
);

PIN P109(
    .p         ( _TI1                     ) /* pin 0*/ 
);

PIN P11(
    .p         ( __TD2                    ) /* pin 0*/ 
);

PIN P110(
    .p         ( _main_signals_CLK16M     ) /* pin 0*/ 
);

PIN P111(
    .p         ( _TI2                     ) /* pin 0*/ 
);

PIN P112(
    .p         ( _TI3                     ) /* pin 0*/ 
);

PIN P113(
    .p         ( _FI                      ) /* pin 0*/ 
);

PIN P114(
    .p         ( _LI                      ) /* pin 0*/ 
);

PIN P115(
    .p         ( _FLIP                    ) /* pin 0*/ 
);

PIN P116(
    .p         ( _HSYNC                   ) /* pin 0*/ 
);

PIN P117(
    .p         ( _~HBLANK                 ) /* pin 0*/ 
);

PIN P118(
    .p         ( _VSYNC                   ) /* pin 0*/ 
);

PIN P119(
    .p         ( _~VBLANK                 ) /* pin 0*/ 
);

PIN P12(
    .p         ( __TD3                    ) /* pin 0*/ 
);

PIN P120(

);

PIN P121(
    .p         ( _APD0                    ) /* pin 0*/ 
);

PIN P122(
    .p         ( _APD1                    ) /* pin 0*/ 
);

PIN P123(
    .p         ( _APD2                    ) /* pin 0*/ 
);

PIN P124(
    .p         ( _APD3                    ) /* pin 0*/ 
);

PIN P125(
    .p         ( _APD4                    ) /* pin 0*/ 
);

PIN P126(
    .p         ( _APD5                    ) /* pin 0*/ 
);

PIN P127(
    .p         ( _APD6                    ) /* pin 0*/ 
);

PIN P128(
    .p         ( _APD7                    ) /* pin 0*/ 
);

PIN P129(
    .p         ( _BPD0                    ) /* pin 0*/ 
);

PIN P13(
    .p         ( __TD4                    ) /* pin 0*/ 
);

PIN P130(
    .p         ( _BPD1                    ) /* pin 0*/ 
);

PIN P131(
    .p         ( _BPD2                    ) /* pin 0*/ 
);

PIN P132(
    .p         ( _BPD3                    ) /* pin 0*/ 
);

PIN P133(
    .p         ( _BPD4                    ) /* pin 0*/ 
);

PIN P134(
    .p         ( _BPD5                    ) /* pin 0*/ 
);

PIN P135(
    .p         ( _BPD6                    ) /* pin 0*/ 
);

PIN P136(
    .p         ( _BPD7                    ) /* pin 0*/ 
);

PIN P137(
    .p         ( _CPD0                    ) /* pin 0*/ 
);

PIN P138(
    .p         ( _CPD1                    ) /* pin 0*/ 
);

PIN P139(
    .p         ( _CPD2                    ) /* pin 0*/ 
);

PIN P14(
    .p         ( __TD5                    ) /* pin 0*/ 
);

PIN P140(
    .p         ( _CPD3                    ) /* pin 0*/ 
);

PIN P141(
    .p         ( _CPD4                    ) /* pin 0*/ 
);

PIN P142(
    .p         ( _CPD5                    ) /* pin 0*/ 
);

PIN P143(
    .p         ( _CPD6                    ) /* pin 0*/ 
);

PIN P144(
    .p         ( _CPD7                    ) /* pin 0*/ 
);

PIN P145(
    .p         ( _DPD0                    ) /* pin 0*/ 
);

PIN P146(
    .p         ( _DPD1                    ) /* pin 0*/ 
);

PIN P147(
    .p         ( _DPD2                    ) /* pin 0*/ 
);

PIN P148(
    .p         ( _DPD3                    ) /* pin 0*/ 
);

PIN P149(
    .p         ( _DPD4                    ) /* pin 0*/ 
);

PIN P15(
    .p         ( __TD6                    ) /* pin 0*/ 
);

PIN P150(
    .p         ( _DPD5                    ) /* pin 0*/ 
);

PIN P151(
    .p         ( _DPD6                    ) /* pin 0*/ 
);

PIN P152(
    .p         ( _DPD7                    ) /* pin 0*/ 
);

PIN P153(
    .p         ( _CB0                     ) /* pin 0*/ 
);

PIN P154(
    .p         ( _CB1                     ) /* pin 0*/ 
);

PIN P155(
    .p         ( _CB2                     ) /* pin 0*/ 
);

PIN P156(
    .p         ( _CB3                     ) /* pin 0*/ 
);

PIN P157(
    .p         ( _CB4                     ) /* pin 0*/ 
);

PIN P158(
    .p         ( _CPC0                    ) /* pin 0*/ 
);

PIN P159(
    .p         ( _CPC1                    ) /* pin 0*/ 
);

PIN P16(
    .p         ( __TD7                    ) /* pin 0*/ 
);

PIN P160(
    .p         ( _CDEN                    ) /* pin 0*/ 
);

PIN P17(
    .p         ( __TD8                    ) /* pin 0*/ 
);

PIN P18(
    .p         ( __TD9                    ) /* pin 0*/ 
);

PIN P19(
    .p         ( __TD10                   ) /* pin 0*/ 
);

PIN P2(
    .p         ( _XS0                     ) /* pin 0*/ 
);

PIN P20(
    .p         ( __TD11                   ) /* pin 0*/ 
);

PIN P21(

);

PIN P22(
    .p         ( _ASD0                    ) /* pin 0*/ 
);

PIN P23(
    .p         ( _ASD1                    ) /* pin 0*/ 
);

PIN P24(
    .p         ( _ASD2                    ) /* pin 0*/ 
);

PIN P25(
    .p         ( _ASD3                    ) /* pin 0*/ 
);

PIN P26(
    .p         ( _ASD4                    ) /* pin 0*/ 
);

PIN P27(
    .p         ( _ASD5                    ) /* pin 0*/ 
);

PIN P28(
    .p         ( _ASD6                    ) /* pin 0*/ 
);

PIN P29(
    .p         ( _ASD7                    ) /* pin 0*/ 
);

PIN P3(
    .p         ( _XS1                     ) /* pin 0*/ 
);

PIN P30(
    .p         ( _ASD8                    ) /* pin 0*/ 
);

PIN P31(
    .p         ( _BSD0                    ) /* pin 0*/ 
);

PIN P32(
    .p         ( _BSD1                    ) /* pin 0*/ 
);

PIN P33(
    .p         ( _BSD2                    ) /* pin 0*/ 
);

PIN P34(
    .p         ( _BSD3                    ) /* pin 0*/ 
);

PIN P35(
    .p         ( _BSD4                    ) /* pin 0*/ 
);

PIN P36(
    .p         ( _BSD5                    ) /* pin 0*/ 
);

PIN P37(
    .p         ( _BSD6                    ) /* pin 0*/ 
);

PIN P38(
    .p         ( _BSD7                    ) /* pin 0*/ 
);

PIN P39(
    .p         ( _BSD8                    ) /* pin 0*/ 
);

PIN P4(
    .p         ( _XS2                     ) /* pin 0*/ 
);

PIN P40(
    .p         ( _security_management_VBAT ) /* pin 0*/ 
);

PIN P41(

);

PIN P42(
    .p         ( _security_management_SEC_STROBE ) /* pin 0*/ 
);

PIN P43(
    .p         ( _security_management_~SEC_E1 ) /* pin 0*/ 
);

PIN P44(
    .p         ( _security_management_SEC_E2 ) /* pin 0*/ 
);

PIN P45(
    .p         ( _main_signals_SEC_MODE0  ) /* pin 0*/ 
);

PIN P46(
    .p         ( _main_signals_SEC_MODE1  ) /* pin 0*/ 
);

PIN P47(

);

PIN P48(
    .p         ( _FAD0                    ) /* pin 0*/ 
);

PIN P49(
    .p         ( _FAD1                    ) /* pin 0*/ 
);

PIN P5(
    .p         ( _XS3                     ) /* pin 0*/ 
);

PIN P50(
    .p         ( _FAD2                    ) /* pin 0*/ 
);

PIN P51(
    .p         ( _FAD3                    ) /* pin 0*/ 
);

PIN P52(
    .p         ( _FBD0                    ) /* pin 0*/ 
);

PIN P53(
    .p         ( _FBD1                    ) /* pin 0*/ 
);

PIN P54(
    .p         ( _FBD2                    ) /* pin 0*/ 
);

PIN P55(
    .p         ( _FBD3                    ) /* pin 0*/ 
);

PIN P56(
    .p         ( _obj_manager_OBJEO       ) /* pin 0*/ 
);

PIN P57(
    .p         ( _raster_interrupts_~OBJUP ) /* pin 0*/ 
);

PIN P58(
    .p         ( _obj_manager_FRAME       ) /* pin 0*/ 
);

PIN P59(
    .p         ( _obj_manager_~REWEN      ) /* pin 0*/ 
);

PIN P6(
    .p         ( _XS4                     ) /* pin 0*/ 
);

PIN P60(

);

PIN P61(
    .p         ( _CD0                     ) /* pin 0*/ 
);

PIN P62(
    .p         ( _CD1                     ) /* pin 0*/ 
);

PIN P63(
    .p         ( _CD2                     ) /* pin 0*/ 
);

PIN P64(
    .p         ( _CD3                     ) /* pin 0*/ 
);

PIN P65(
    .p         ( _CD4                     ) /* pin 0*/ 
);

PIN P66(
    .p         ( _CD5                     ) /* pin 0*/ 
);

PIN P67(
    .p         ( _CD6                     ) /* pin 0*/ 
);

PIN P68(
    .p         ( _CD7                     ) /* pin 0*/ 
);

PIN P69(
    .p         ( _CD8                     ) /* pin 0*/ 
);

PIN P7(
    .p         ( _xpd_out_~ROMOE          ) /* pin 0*/ 
);

PIN P70(
    .p         ( _CD9                     ) /* pin 0*/ 
);

PIN P71(
    .p         ( _CD10                    ) /* pin 0*/ 
);

PIN P72(
    .p         ( _CD11                    ) /* pin 0*/ 
);

PIN P73(
    .p         ( _CD12                    ) /* pin 0*/ 
);

PIN P74(
    .p         ( _CD13                    ) /* pin 0*/ 
);

PIN P75(
    .p         ( _CD14                    ) /* pin 0*/ 
);

PIN P76(
    .p         ( _CD15                    ) /* pin 0*/ 
);

PIN P77(

);

PIN P78(
    .p         ( _CA1                     ) /* pin 0*/ 
);

PIN P79(
    .p         ( _CA2                     ) /* pin 0*/ 
);

PIN P8(

);

PIN P80(
    .p         ( _CA3                     ) /* pin 0*/ 
);

PIN P81(
    .p         ( _CA4                     ) /* pin 0*/ 
);

PIN P82(
    .p         ( _CA5                     ) /* pin 0*/ 
);

PIN P83(
    .p         ( _bus_signal_control_~CS  ) /* pin 0*/ 
);

PIN P84(
    .p         ( _bus_signal_control_~RD  ) /* pin 0*/ 
);

PIN P85(
    .p         ( _bus_signal_control_~WR  ) /* pin 0*/ 
);

PIN P86(
    .p         ( _bus_signal_control_LUTPRO ) /* pin 0*/ 
);

PIN P87(
    .p         ( _palette_control_~WSTROM ) /* pin 0*/ 
);

PIN P88(
    .p         ( _palette_control_CPY_LUT ) /* pin 0*/ 
);

PIN P89(
    .p         ( _LUT0                    ) /* pin 0*/ 
);

PIN P9(
    .p         ( __TD0                    ) /* pin 0*/ 
);

PIN P90(
    .p         ( _LUT1                    ) /* pin 0*/ 
);

PIN P91(
    .p         ( _LUT2                    ) /* pin 0*/ 
);

PIN P92(
    .p         ( _LUT3                    ) /* pin 0*/ 
);

PIN P93(
    .p         ( _LUT4                    ) /* pin 0*/ 
);

PIN P94(
    .p         ( _LUT5                    ) /* pin 0*/ 
);

PIN P95(
    .p         ( _LUT6                    ) /* pin 0*/ 
);

PIN P96(
    .p         ( _LUT7                    ) /* pin 0*/ 
);

PIN P97(
    .p         ( _LUT8                    ) /* pin 0*/ 
);

PIN P98(
    .p         ( _LUT9                    ) /* pin 0*/ 
);

PIN P99(
    .p         ( _LUT10                   ) /* pin 0*/ 
);

NBUF03 UAA162(
    .i         ( Net__UAA162_Pad1_        ) /* pin 1*/ ,
    .o         ( _xpd_out_~ROMOE          ) /* pin 2*/ 
);

OUTTRI UAA164(
    .i1        ( _xpd_out_OUT6            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD6                    ) /* pin 3*/ 
);

OUTTRI UAA168(
    .i1        ( _xpd_out_OUT4            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD4                    ) /* pin 3*/ 
);

OUTTRI UAA182(
    .i1        ( _xpd_out_OUT3            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD3                    ) /* pin 3*/ 
);

OUTTRI UAA186(
    .i1        ( _xpd_out_OUT5            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD5                    ) /* pin 3*/ 
);

OUTTRI UAA191(
    .i1        ( _xpd_out_OUT7            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD7                    ) /* pin 3*/ 
);

OUTTRI UAA201(
    .i1        ( _xpd_out_OUT1            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD1                    ) /* pin 3*/ 
);

OUTTRI UAA216(
    .i1        ( _xpd_out_OUT7            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD7                    ) /* pin 3*/ 
);

OUTTRI UAA227(
    .i1        ( _xpd_out_OUT6            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD6                    ) /* pin 3*/ 
);

OUTTRI UAA231(
    .i1        ( _xpd_out_OUT2            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD2                    ) /* pin 3*/ 
);

OUTTRI UAA241(
    .i1        ( _xpd_out_OUT5            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD5                    ) /* pin 3*/ 
);

OUTTRI UAA245(
    .i1        ( _xpd_out_OUT0            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _DPD0                    ) /* pin 3*/ 
);

BUF12 UAA254(
    .i         ( Net__UAA254_Pad1_        ) /* pin 1*/ ,
    .o         ( _tilemaps_SERIAL_LOAD    ) /* pin 2*/ 
);

OUTTRI UAA256(
    .i1        ( _xpd_out_OUT4            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD4                    ) /* pin 3*/ 
);

OUTTRI UAA269(
    .i1        ( _xpd_out_OUT3            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD3                    ) /* pin 3*/ 
);

OUTTRI UAA291(
    .i1        ( _xpd_out_OUT2            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD2                    ) /* pin 3*/ 
);

OUTTRI UAA295(
    .i1        ( _xpd_out_OUT1            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD1                    ) /* pin 3*/ 
);

OUTTRI UAA300(
    .i1        ( _xpd_out_OUT0            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _CPD0                    ) /* pin 3*/ 
);

OUTTRI UAA328(
    .i1        ( _xpd_out_OUT6            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD6                    ) /* pin 3*/ 
);

OUTTRI UAA337(
    .i1        ( _xpd_out_OUT4            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD4                    ) /* pin 3*/ 
);

OUTTRI UAA346(
    .i1        ( _xpd_out_OUT7            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD7                    ) /* pin 3*/ 
);

OUTTRI UAA351(
    .i1        ( _xpd_out_OUT3            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD3                    ) /* pin 3*/ 
);

OUTTRI UAA356(
    .i1        ( _xpd_out_OUT5            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD5                    ) /* pin 3*/ 
);

OUTTRI UAA383(
    .i1        ( _xpd_out_OUT7            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD7                    ) /* pin 3*/ 
);

OUTTRI UAA387(
    .i1        ( _xpd_out_OUT2            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD2                    ) /* pin 3*/ 
);

BUF12 UAA392(
    .i         ( _xpd_out_~OE             ) /* pin 1*/ ,
    .o         ( Net__UAA162_Pad1_        ) /* pin 2*/ 
);

BUF12 UAA394(
    .i         ( _xpd_out_~OE             ) /* pin 1*/ ,
    .o         ( Net__UAA162_Pad1_        ) /* pin 2*/ 
);

OUTTRI UAA396(
    .i1        ( _xpd_out_OUT5            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD5                    ) /* pin 3*/ 
);

OUTTRI UAA401(
    .i1        ( _xpd_out_OUT1            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD1                    ) /* pin 3*/ 
);

OUTTRI UAA407(
    .i1        ( _xpd_out_OUT0            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _BPD0                    ) /* pin 3*/ 
);

OUTTRI UAA412(
    .i1        ( _xpd_out_OUT2            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD2                    ) /* pin 3*/ 
);

OUTTRI UAA416(
    .i1        ( _xpd_out_OUT4            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD4                    ) /* pin 3*/ 
);

M273C #(.MiscRef(UAA421;UAA428;UAA432;UAA437;UAA441;UAA446;UAA450;UAA454;UAA459) 
) UAA423(
    .r_n       ( _~RESETI2                ) /* pin 1*/ ,
    .q0        ( _xpd_out_OUT0            ) /* pin 2*/ ,
    .d0        ( Net__UAA423_Pad3_        ) /* pin 3*/ ,
    .d1        ( Net__UAA423_Pad4_        ) /* pin 4*/ ,
    .q1        ( _xpd_out_OUT1            ) /* pin 5*/ ,
    .q2        ( _xpd_out_OUT2            ) /* pin 6*/ ,
    .d2        ( Net__UAA423_Pad7_        ) /* pin 7*/ ,
    .d3        ( Net__UAA423_Pad8_        ) /* pin 8*/ ,
    .q3        ( _xpd_out_OUT3            ) /* pin 9*/ ,
    .clk       ( Net__UAA423_Pad11_       ) /* pin 11*/ ,
    .q4        ( _xpd_out_OUT4            ) /* pin 12*/ ,
    .d4        ( Net__UAA423_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAA423_Pad14_       ) /* pin 14*/ ,
    .q5        ( _xpd_out_OUT5            ) /* pin 15*/ ,
    .q6        ( _xpd_out_OUT6            ) /* pin 16*/ ,
    .d6        ( Net__UAA423_Pad17_       ) /* pin 17*/ ,
    .d7        ( Net__UAA423_Pad18_       ) /* pin 18*/ ,
    .q7        ( _xpd_out_OUT7            ) /* pin 19*/ 
);

OUTTRI UAA463(
    .i1        ( _xpd_out_OUT1            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD1                    ) /* pin 3*/ 
);

BUF12 UAA467(
    .i         ( _xpd_out_~OE             ) /* pin 1*/ ,
    .o         ( Net__UAA162_Pad1_        ) /* pin 2*/ 
);

BUF12 UAA482(
    .i         ( Net__UAA482_Pad1_        ) /* pin 1*/ ,
    .o         ( _VSYNC                   ) /* pin 2*/ 
);

INV01 UAA484(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UAA484_Pad2_        ) /* pin 2*/ 
);

BUF12 UAA485(
    .i         ( Net__UAA485_Pad1_        ) /* pin 1*/ ,
    .o         ( _HSYNC                   ) /* pin 2*/ 
);

OUTTRI UAA488(
    .i1        ( _xpd_out_OUT3            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD3                    ) /* pin 3*/ 
);

OUTTRI UAA494(
    .i1        ( _xpd_out_OUT0            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD0                    ) /* pin 3*/ 
);

BUF12 UAA506(
    .i         ( Net__UAA506_Pad1_        ) /* pin 1*/ ,
    .o         ( _~VBLANK                 ) /* pin 2*/ 
);

BUF12 UAA511(
    .i         ( Net__UAA511_Pad1_        ) /* pin 1*/ ,
    .o         ( _~HBLANK                 ) /* pin 2*/ 
);

L8G UAE130(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE130_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE130_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE130_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE130_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE130_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE130_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE130_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE130_Pad17_       ) /* pin 17*/ 
);

NAND03 UAE166(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD2      ) /* pin 2*/ ,
    .i3        ( _bus_signal_control_GFX_CHNL ) /* pin 3*/ ,
    .o         ( Net__UAE130_Pad1_        ) /* pin 4*/ 
);

L8G UAE168(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE168_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE168_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE168_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE168_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE168_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE168_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE168_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE168_Pad17_       ) /* pin 17*/ 
);

L8G UAE2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE2_Pad3_          ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE2_Pad5_          ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE2_Pad7_          ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE2_Pad9_          ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE2_Pad11_         ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE2_Pad13_         ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE2_Pad15_         ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE2_Pad17_         ) /* pin 17*/ 
);

INV01 UAE204(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr2_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAE208(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr3_sync_~CLK8M ) /* pin 2*/ 
);

L8G UAE209(
    .le        ( Net__UAE209_Pad1_        ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE209_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE209_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE209_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE209_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE209_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE209_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE209_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE209_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAE209_Pad18_       ) /* pin 18*/ 
);

INV01 UAE245(
    .i         ( _tilemaps_cb_bus3_LOAD_CHNLA ) /* pin 1*/ ,
    .o         ( Net__UAE209_Pad1_        ) /* pin 2*/ 
);

NAND03 UAE246(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD3      ) /* pin 2*/ ,
    .i3        ( _bus_signal_control_GFX_CHNL ) /* pin 3*/ ,
    .o         ( _tilemaps_cb_bus3_LOAD_CHNLA ) /* pin 4*/ 
);

L8G UAE248(
    .le        ( Net__UAE209_Pad1_        ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE248_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE248_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE248_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE248_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE248_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE248_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE248_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE248_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAE209_Pad18_       ) /* pin 18*/ 
);

L8G UAE288(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE288_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE288_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE288_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE288_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE288_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE288_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE288_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE288_Pad17_       ) /* pin 17*/ 
);

NAND03 UAE325(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _obj_manager_LOAD_XPD4   ) /* pin 2*/ ,
    .i3        ( _bus_signal_control_GFX_CHNL ) /* pin 3*/ ,
    .o         ( Net__UAE288_Pad1_        ) /* pin 4*/ 
);

L8G UAE327(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE327_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE327_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE327_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE327_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE327_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE327_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE327_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE327_Pad17_       ) /* pin 17*/ 
);

INV01 UAE362(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( Net__UAE362_Pad2_        ) /* pin 2*/ 
);

NAND03 UAE38(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD1      ) /* pin 2*/ ,
    .i3        ( _bus_signal_control_GFX_CHNL ) /* pin 3*/ ,
    .o         ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 4*/ 
);

L8G UAE41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAE41_Pad3_         ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAE41_Pad5_         ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAE41_Pad7_         ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAE41_Pad9_         ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAE41_Pad11_        ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAE41_Pad13_        ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAE41_Pad15_        ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAE41_Pad17_        ) /* pin 17*/ 
);

M273C UAE79(
    .q0        ( Net__UAE79_Pad2_         ) /* pin 2*/ ,
    .d0        ( _CB0                     ) /* pin 3*/ ,
    .d1        ( _CB1                     ) /* pin 4*/ ,
    .q1        ( Net__UAE79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAE79_Pad6_         ) /* pin 6*/ ,
    .d2        ( _CB2                     ) /* pin 7*/ ,
    .d3        ( _CB3                     ) /* pin 8*/ ,
    .q3        ( Net__UAE79_Pad9_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 11*/ ,
    .q4        ( Net__UAE79_Pad12_        ) /* pin 12*/ ,
    .d4        ( _CB4                     ) /* pin 13*/ ,
    .d5        ( _CPC0                    ) /* pin 14*/ ,
    .q5        ( Net__UAE79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAE79_Pad16_        ) /* pin 16*/ ,
    .d6        ( _CPC1                    ) /* pin 17*/ ,
    .d7        ( _CDEN                    ) /* pin 18*/ ,
    .q7        ( Net__UAE79_Pad19_        ) /* pin 19*/ 
);

INV01 UAF1(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_sync_~CLK8M ) /* pin 2*/ 
);

L8G UAF130(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAE130_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF130_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAE130_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF130_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAE130_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF130_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAE130_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF130_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAE130_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF130_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAE130_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF130_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAE130_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF130_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAE130_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF130_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF130_Pad18_       ) /* pin 18*/ 
);

NAND03 UAF166(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD2      ) /* pin 2*/ ,
    .i3        ( Net__UAF166_Pad3_        ) /* pin 3*/ ,
    .o         ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 4*/ 
);

L8G UAF168(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAE168_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF168_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAE168_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF168_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAE168_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF168_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAE168_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF168_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAE168_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF168_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAE168_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF168_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAE168_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF168_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAE168_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF168_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF130_Pad18_       ) /* pin 18*/ 
);

L8G UAF2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( Net__UAE2_Pad3_          ) /* pin 2*/ ,
    .q8        ( Net__UAF2_Pad3_          ) /* pin 3*/ ,
    .d7        ( Net__UAE2_Pad5_          ) /* pin 4*/ ,
    .q7        ( Net__UAF2_Pad5_          ) /* pin 5*/ ,
    .d6        ( Net__UAE2_Pad7_          ) /* pin 6*/ ,
    .q6        ( Net__UAF2_Pad7_          ) /* pin 7*/ ,
    .d5        ( Net__UAE2_Pad9_          ) /* pin 8*/ ,
    .q5        ( Net__UAF2_Pad9_          ) /* pin 9*/ ,
    .d4        ( Net__UAE2_Pad11_         ) /* pin 10*/ ,
    .q4        ( Net__UAF2_Pad11_         ) /* pin 11*/ ,
    .d3        ( Net__UAE2_Pad13_         ) /* pin 12*/ ,
    .q3        ( Net__UAF2_Pad13_         ) /* pin 13*/ ,
    .d2        ( Net__UAE2_Pad15_         ) /* pin 14*/ ,
    .q2        ( Net__UAF2_Pad15_         ) /* pin 15*/ ,
    .d1        ( Net__UAE2_Pad17_         ) /* pin 16*/ ,
    .q1        ( Net__UAF2_Pad17_         ) /* pin 17*/ ,
    .g_n       ( Net__UAF2_Pad18_         ) /* pin 18*/ 
);

NAND03 UAF245(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD3      ) /* pin 2*/ ,
    .i3        ( Net__UAF245_Pad3_        ) /* pin 3*/ ,
    .o         ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 4*/ 
);

NAND02 UAF249(
    .i1        ( _tilemaps_LOAD_XPD3      ) /* pin 1*/ ,
    .i2        ( Net__UAF245_Pad3_        ) /* pin 2*/ ,
    .o         ( _tilemaps_sr3_sync_~LOAD ) /* pin 3*/ 
);

L8G UAF288(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAE288_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF288_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAE288_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF288_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAE288_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF288_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAE288_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF288_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAE288_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF288_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAE288_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF288_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAE288_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF288_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAE288_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF288_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF288_Pad18_       ) /* pin 18*/ 
);

NAND03 UAF325(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _obj_manager_LOAD_XPD4   ) /* pin 2*/ ,
    .i3        ( Net__UAF325_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UAF325_Pad4_        ) /* pin 4*/ 
);

L8G UAF327(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAE327_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF327_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAE327_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF327_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAE327_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF327_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAE327_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF327_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAE327_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF327_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAE327_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF327_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAE327_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF327_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAE327_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF327_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF288_Pad18_       ) /* pin 18*/ 
);

MULT UAF386(
    .x0        ( _bus_io_BUS_DATA0        ) /* pin 1*/ ,
    .x1        ( _bus_io_BUS_DATA1        ) /* pin 2*/ ,
    .x2        ( _bus_io_BUS_DATA2        ) /* pin 3*/ ,
    .x3        ( _bus_io_BUS_DATA3        ) /* pin 4*/ ,
    .x4        ( _bus_io_BUS_DATA4        ) /* pin 5*/ ,
    .x5        ( _bus_io_BUS_DATA5        ) /* pin 6*/ ,
    .x6        ( _bus_io_BUS_DATA6        ) /* pin 7*/ ,
    .x7        ( _bus_io_BUS_DATA7        ) /* pin 8*/ ,
    .x8        ( _bus_io_BUS_DATA8        ) /* pin 9*/ ,
    .x9        ( _bus_io_BUS_DATA9        ) /* pin 10*/ ,
    .x10       ( _bus_io_BUS_DATA10       ) /* pin 11*/ ,
    .x11       ( _bus_io_BUS_DATA11       ) /* pin 12*/ ,
    .x12       ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .x13       ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .x14       ( _bus_io_BUS_DATA14       ) /* pin 15*/ ,
    .x15       ( _bus_io_BUS_DATA15       ) /* pin 16*/ ,
    .y0        ( _bus_io_BUS_DATA0        ) /* pin 17*/ ,
    .y1        ( _bus_io_BUS_DATA1        ) /* pin 18*/ ,
    .y2        ( _bus_io_BUS_DATA2        ) /* pin 19*/ ,
    .y3        ( _bus_io_BUS_DATA3        ) /* pin 20*/ ,
    .y4        ( _bus_io_BUS_DATA4        ) /* pin 21*/ ,
    .y5        ( _bus_io_BUS_DATA5        ) /* pin 22*/ ,
    .y6        ( _bus_io_BUS_DATA6        ) /* pin 23*/ ,
    .y7        ( _bus_io_BUS_DATA7        ) /* pin 24*/ ,
    .y8        ( _bus_io_BUS_DATA8        ) /* pin 25*/ ,
    .y9        ( _bus_io_BUS_DATA9        ) /* pin 26*/ ,
    .y10       ( _bus_io_BUS_DATA10       ) /* pin 27*/ ,
    .y11       ( _bus_io_BUS_DATA11       ) /* pin 28*/ ,
    .y12       ( _bus_io_BUS_DATA12       ) /* pin 29*/ ,
    .y13       ( _bus_io_BUS_DATA13       ) /* pin 30*/ ,
    .y14       ( _bus_io_BUS_DATA14       ) /* pin 31*/ ,
    .y15       ( _bus_io_BUS_DATA15       ) /* pin 32*/ ,
    .tcx       ( Net__UAF386_Pad33_       ) /* pin 33*/ ,
    .tcy       ( Net__UAF386_Pad33_       ) /* pin 34*/ ,
    .ckx       ( Net__UAF386_Pad35_       ) /* pin 35*/ ,
    .cky       ( Net__UAF386_Pad36_       ) /* pin 36*/ ,
    .ckm       ( _CLK8M                   ) /* pin 37*/ ,
    .lsb0      ( _bus_io_BUS_DATA0        ) /* pin 38*/ ,
    .lsb1      ( _bus_io_BUS_DATA1        ) /* pin 39*/ ,
    .lsb2      ( _bus_io_BUS_DATA2        ) /* pin 40*/ ,
    .lsb3      ( _bus_io_BUS_DATA3        ) /* pin 41*/ ,
    .lsb4      ( _bus_io_BUS_DATA4        ) /* pin 42*/ ,
    .lsb5      ( _bus_io_BUS_DATA5        ) /* pin 43*/ ,
    .lsb6      ( _bus_io_BUS_DATA6        ) /* pin 44*/ ,
    .lsb7      ( _bus_io_BUS_DATA7        ) /* pin 45*/ ,
    .lsb8      ( _bus_io_BUS_DATA8        ) /* pin 46*/ ,
    .lsb9      ( _bus_io_BUS_DATA9        ) /* pin 47*/ ,
    .lsb10     ( _bus_io_BUS_DATA10       ) /* pin 48*/ ,
    .lsb11     ( _bus_io_BUS_DATA11       ) /* pin 49*/ ,
    .lsb12     ( _bus_io_BUS_DATA12       ) /* pin 50*/ ,
    .lsb13     ( _bus_io_BUS_DATA13       ) /* pin 51*/ ,
    .lsb14     ( _bus_io_BUS_DATA14       ) /* pin 52*/ ,
    .lsb15     ( _bus_io_BUS_DATA15       ) /* pin 53*/ ,
    .msb0      ( _bus_io_BUS_DATA0        ) /* pin 54*/ ,
    .msb1      ( _bus_io_BUS_DATA1        ) /* pin 55*/ ,
    .msb2      ( _bus_io_BUS_DATA2        ) /* pin 56*/ ,
    .msb3      ( _bus_io_BUS_DATA3        ) /* pin 57*/ ,
    .msb4      ( _bus_io_BUS_DATA4        ) /* pin 58*/ ,
    .msb5      ( _bus_io_BUS_DATA5        ) /* pin 59*/ ,
    .msb6      ( _bus_io_BUS_DATA6        ) /* pin 60*/ ,
    .msb7      ( _bus_io_BUS_DATA7        ) /* pin 61*/ ,
    .msb8      ( _bus_io_BUS_DATA8        ) /* pin 62*/ ,
    .msb9      ( _bus_io_BUS_DATA9        ) /* pin 63*/ ,
    .msb10     ( _bus_io_BUS_DATA10       ) /* pin 64*/ ,
    .msb11     ( _bus_io_BUS_DATA11       ) /* pin 65*/ ,
    .msb12     ( _bus_io_BUS_DATA12       ) /* pin 66*/ ,
    .msb13     ( _bus_io_BUS_DATA13       ) /* pin 67*/ ,
    .msb14     ( _bus_io_BUS_DATA14       ) /* pin 68*/ ,
    .msb15     ( _bus_io_BUS_DATA15       ) /* pin 69*/ ,
    .tsl_n     ( Net__UAF386_Pad70_       ) /* pin 70*/ ,
    .tsm_n     ( Net__UAF386_Pad71_       ) /* pin 71*/ ,
    .ckl       ( _CLK8M                   ) /* pin 72*/ 
);

L8G UAF41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( Net__UAE41_Pad3_         ) /* pin 2*/ ,
    .q8        ( Net__UAF41_Pad3_         ) /* pin 3*/ ,
    .d7        ( Net__UAE41_Pad5_         ) /* pin 4*/ ,
    .q7        ( Net__UAF41_Pad5_         ) /* pin 5*/ ,
    .d6        ( Net__UAE41_Pad7_         ) /* pin 6*/ ,
    .q6        ( Net__UAF41_Pad7_         ) /* pin 7*/ ,
    .d5        ( Net__UAE41_Pad9_         ) /* pin 8*/ ,
    .q5        ( Net__UAF41_Pad9_         ) /* pin 9*/ ,
    .d4        ( Net__UAE41_Pad11_        ) /* pin 10*/ ,
    .q4        ( Net__UAF41_Pad11_        ) /* pin 11*/ ,
    .d3        ( Net__UAE41_Pad13_        ) /* pin 12*/ ,
    .q3        ( Net__UAF41_Pad13_        ) /* pin 13*/ ,
    .d2        ( Net__UAE41_Pad15_        ) /* pin 14*/ ,
    .q2        ( Net__UAF41_Pad15_        ) /* pin 15*/ ,
    .d1        ( Net__UAE41_Pad17_        ) /* pin 16*/ ,
    .q1        ( Net__UAF41_Pad17_        ) /* pin 17*/ ,
    .g_n       ( Net__UAF2_Pad18_         ) /* pin 18*/ 
);

M273C UAF79(
    .q0        ( _tilemaps_col3_assemble_CDEN ) /* pin 2*/ ,
    .d0        ( Net__UAE79_Pad19_        ) /* pin 3*/ ,
    .d1        ( Net__UAE79_Pad16_        ) /* pin 4*/ ,
    .q1        ( _tilemaps_cb_bus2_CPC_OUT1 ) /* pin 5*/ ,
    .q2        ( _tilemaps_cb_bus2_CPC_OUT0 ) /* pin 6*/ ,
    .d2        ( Net__UAE79_Pad15_        ) /* pin 7*/ ,
    .d3        ( Net__UAE79_Pad12_        ) /* pin 8*/ ,
    .q3        ( _tilemaps_cb_bus2_CB_OUT4 ) /* pin 9*/ ,
    .clk       ( Net__UAF79_Pad11_        ) /* pin 11*/ ,
    .q4        ( _tilemaps_cb_bus2_CB_OUT3 ) /* pin 12*/ ,
    .d4        ( Net__UAE79_Pad9_         ) /* pin 13*/ ,
    .d5        ( Net__UAE79_Pad6_         ) /* pin 14*/ ,
    .q5        ( _tilemaps_cb_bus2_CB_OUT2 ) /* pin 15*/ ,
    .q6        ( _tilemaps_cb_bus2_CB_OUT1 ) /* pin 16*/ ,
    .d6        ( Net__UAE79_Pad5_         ) /* pin 17*/ ,
    .d7        ( Net__UAE79_Pad2_         ) /* pin 18*/ ,
    .q7        ( _tilemaps_cb_bus2_CB_OUT0 ) /* pin 19*/ 
);

INV01 UAG1(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_sync_~CLK8M ) /* pin 2*/ 
);

XOR02 UAG124(
    .i1        ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .i2        ( Net__UAG124_Pad2_        ) /* pin 2*/ ,
    .o         ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 3*/ 
);

L8G UAG130(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG130_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG130_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG130_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG130_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG130_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG130_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG130_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG130_Pad17_       ) /* pin 17*/ 
);

INV01 UAG165(
    .i         ( _bus_signal_control_GFX_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAF166_Pad3_        ) /* pin 2*/ 
);

OR02 UAG166(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( _tilemaps_xpd_bus2_XSEL  ) /* pin 2*/ ,
    .o         ( Net__UAG166_Pad3_        ) /* pin 3*/ 
);

L8G UAG168(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG168_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG168_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG168_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG168_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG168_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG168_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG168_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG168_Pad17_       ) /* pin 17*/ 
);

INV01 UAG204(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr2_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAG208(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr3_sync_~CLK8M ) /* pin 2*/ 
);

L8G UAG209(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG209_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG209_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG209_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG209_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG209_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG209_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG209_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG209_Pad17_       ) /* pin 17*/ 
);

OR02 UAG245(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( _tilemaps_xpd_bus3_XSEL  ) /* pin 2*/ ,
    .o         ( Net__UAG245_Pad3_        ) /* pin 3*/ 
);

INV01 UAG247(
    .i         ( _bus_signal_control_GFX_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAF245_Pad3_        ) /* pin 2*/ 
);

L8G UAG248(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG248_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG248_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG248_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG248_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG248_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG248_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG248_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG248_Pad17_       ) /* pin 17*/ 
);

L8G UAG288(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG288_Pad3_        ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG288_Pad5_        ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG288_Pad7_        ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG288_Pad9_        ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG288_Pad11_       ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG288_Pad13_       ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG288_Pad15_       ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG288_Pad17_       ) /* pin 17*/ 
);

INV01 UAG324(
    .i         ( _bus_signal_control_GFX_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAF325_Pad3_        ) /* pin 2*/ 
);

OR02 UAG325(
    .i1        ( _obj_manager_SERIAL_LOAD ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_XSEL_SYNC ) /* pin 2*/ ,
    .o         ( Net__UAG325_Pad3_        ) /* pin 3*/ 
);

L8G UAG327(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAG327_Pad3_        ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAG327_Pad5_        ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAG327_Pad7_        ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAG327_Pad9_        ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAG327_Pad11_       ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAG327_Pad13_       ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAG327_Pad15_       ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAG327_Pad17_       ) /* pin 17*/ 
);

NAND03 UAG38(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_LOAD_XPD1      ) /* pin 2*/ ,
    .i3        ( Net__UAG38_Pad3_         ) /* pin 3*/ ,
    .o         ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 4*/ 
);

INV01 UAG40(
    .i         ( _bus_signal_control_GFX_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAG38_Pad3_         ) /* pin 2*/ 
);

M273C UAG79(
    .q0        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 2*/ ,
    .d0        ( _tilemaps_cb_bus2_CB_OUT0 ) /* pin 3*/ ,
    .d1        ( _tilemaps_cb_bus2_CB_OUT1 ) /* pin 4*/ ,
    .q1        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 5*/ ,
    .q2        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 6*/ ,
    .d2        ( _tilemaps_cb_bus2_CB_OUT2 ) /* pin 7*/ ,
    .d3        ( _tilemaps_cb_bus2_CB_OUT3 ) /* pin 8*/ ,
    .q3        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 9*/ ,
    .clk       ( Net__UAG79_Pad11_        ) /* pin 11*/ ,
    .q4        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 12*/ ,
    .d4        ( _tilemaps_cb_bus2_CB_OUT4 ) /* pin 13*/ ,
    .d5        ( _tilemaps_cb_bus2_CPC_OUT0 ) /* pin 14*/ ,
    .q5        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 15*/ ,
    .q6        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 16*/ ,
    .d6        ( _tilemaps_cb_bus2_CPC_OUT1 ) /* pin 17*/ ,
    .d7        ( _tilemaps_col3_assemble_CDEN ) /* pin 18*/ ,
    .q7        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 19*/ 
);

INV01 UAH1(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAH165(
    .i         ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAF130_Pad18_       ) /* pin 2*/ 
);

L8G UAH2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _DPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAH2_Pad3_          ) /* pin 3*/ ,
    .d7        ( _DPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAH2_Pad5_          ) /* pin 5*/ ,
    .d6        ( _DPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAH2_Pad7_          ) /* pin 7*/ ,
    .d5        ( _DPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAH2_Pad9_          ) /* pin 9*/ ,
    .d4        ( _DPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAH2_Pad11_         ) /* pin 11*/ ,
    .d3        ( _DPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAH2_Pad13_         ) /* pin 13*/ ,
    .d2        ( _DPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAH2_Pad15_         ) /* pin 15*/ ,
    .d1        ( _DPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAH2_Pad17_         ) /* pin 17*/ 
);

INV01 UAH204(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr2_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAH208(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr3_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAH247(
    .i         ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAE209_Pad18_       ) /* pin 2*/ 
);

INV01 UAH324(
    .i         ( Net__UAH324_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UAF288_Pad18_       ) /* pin 2*/ 
);

OR02 UAH38(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( _tilemaps_xpd_bus1_XSEL  ) /* pin 2*/ ,
    .o         ( Net__UAH38_Pad3_         ) /* pin 3*/ 
);

INV01 UAH40(
    .i         ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAF2_Pad18_         ) /* pin 2*/ 
);

L8G UAH41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _CPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAH41_Pad3_         ) /* pin 3*/ ,
    .d7        ( _CPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAH41_Pad5_         ) /* pin 5*/ ,
    .d6        ( _CPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAH41_Pad7_         ) /* pin 7*/ ,
    .d5        ( _CPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAH41_Pad9_         ) /* pin 9*/ ,
    .d4        ( _CPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAH41_Pad11_        ) /* pin 11*/ ,
    .d3        ( _CPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAH41_Pad13_        ) /* pin 13*/ ,
    .d2        ( _CPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAH41_Pad15_        ) /* pin 15*/ ,
    .d1        ( _CPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAH41_Pad17_        ) /* pin 17*/ 
);

INV01 UAI1(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_sync_~CLK8M ) /* pin 2*/ 
);

L8G UAI130(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAG130_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF130_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG130_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF130_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG130_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF130_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG130_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF130_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG130_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF130_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG130_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF130_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG130_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF130_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG130_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF130_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 18*/ 
);

OR02 UAI165(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( Net__UAI165_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAI165_Pad3_        ) /* pin 3*/ 
);

INV01 UAI167(
    .i         ( _tilemaps_xpd_bus2_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAI165_Pad2_        ) /* pin 2*/ 
);

L8G UAI168(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAG168_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF168_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG168_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF168_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG168_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF168_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG168_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF168_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG168_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF168_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG168_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF168_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG168_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF168_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG168_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF168_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 18*/ 
);

L8G UAI2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAH2_Pad3_          ) /* pin 2*/ ,
    .q8        ( Net__UAF2_Pad3_          ) /* pin 3*/ ,
    .d7        ( Net__UAH2_Pad5_          ) /* pin 4*/ ,
    .q7        ( Net__UAF2_Pad5_          ) /* pin 5*/ ,
    .d6        ( Net__UAH2_Pad7_          ) /* pin 6*/ ,
    .q6        ( Net__UAF2_Pad7_          ) /* pin 7*/ ,
    .d5        ( Net__UAH2_Pad9_          ) /* pin 8*/ ,
    .q5        ( Net__UAF2_Pad9_          ) /* pin 9*/ ,
    .d4        ( Net__UAH2_Pad11_         ) /* pin 10*/ ,
    .q4        ( Net__UAF2_Pad11_         ) /* pin 11*/ ,
    .d3        ( Net__UAH2_Pad13_         ) /* pin 12*/ ,
    .q3        ( Net__UAF2_Pad13_         ) /* pin 13*/ ,
    .d2        ( Net__UAH2_Pad15_         ) /* pin 14*/ ,
    .q2        ( Net__UAF2_Pad15_         ) /* pin 15*/ ,
    .d1        ( Net__UAH2_Pad17_         ) /* pin 16*/ ,
    .q1        ( Net__UAF2_Pad17_         ) /* pin 17*/ ,
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 18*/ 
);

INV01 UAI204(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr2_sync_~CLK8M ) /* pin 2*/ 
);

INV01 UAI208(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _tilemaps_sr3_sync_~CLK8M ) /* pin 2*/ 
);

L8G UAI209(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAG209_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAE209_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG209_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAE209_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG209_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAE209_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG209_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAE209_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG209_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAE209_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG209_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAE209_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG209_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAE209_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG209_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAE209_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 18*/ 
);

INV01 UAI245(
    .i         ( _tilemaps_xpd_bus3_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAI245_Pad2_        ) /* pin 2*/ 
);

OR02 UAI246(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( Net__UAI245_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAI246_Pad3_        ) /* pin 3*/ 
);

L8G UAI248(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAG248_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAE248_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG248_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAE248_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG248_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAE248_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG248_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAE248_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG248_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAE248_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG248_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAE248_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG248_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAE248_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG248_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAE248_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 18*/ 
);

M198C UAI287(
    .l0        ( Net__UAF288_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAF288_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAF288_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAF288_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAF288_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAF288_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAF288_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAF288_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAI287_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAI287_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAI287_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAI287_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAI287_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAI287_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAI287_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAI287_Pad17_       ) /* pin 17*/ ,
    .clk       ( Net__UAE362_Pad2_        ) /* pin 19*/ ,
    .load_n    ( Net__UAI287_Pad20_       ) /* pin 20*/ ,
    .lnr       ( Net__UAG325_Pad3_        ) /* pin 21*/ ,
    .r_n       ( Net__UAI287_Pad22_       ) /* pin 22*/ 
);

L8G UAI288(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( Net__UAG288_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF288_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG288_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF288_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG288_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF288_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG288_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF288_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG288_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF288_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG288_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF288_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG288_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF288_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG288_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF288_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAH324_Pad1_        ) /* pin 18*/ 
);

OR02 UAI324(
    .i1        ( _obj_manager_SERIAL_LOAD ) /* pin 1*/ ,
    .i2        ( Net__UAI324_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAI287_Pad20_       ) /* pin 3*/ 
);

M198C UAI325(
    .l0        ( Net__UAF327_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAF327_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAF327_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAF327_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAF327_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAF327_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAF327_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAF327_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAI325_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAI325_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAI325_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAI325_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAI325_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAI325_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAI325_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAI325_Pad17_       ) /* pin 17*/ ,
    .clk       ( Net__UAE362_Pad2_        ) /* pin 19*/ ,
    .load_n    ( Net__UAI287_Pad20_       ) /* pin 20*/ ,
    .lnr       ( Net__UAG325_Pad3_        ) /* pin 21*/ ,
    .r_n       ( Net__UAI287_Pad22_       ) /* pin 22*/ 
);

INV01 UAI326(
    .i         ( _obj_manager_xpd_bus4_XSEL_SYNC ) /* pin 1*/ ,
    .o         ( Net__UAI324_Pad2_        ) /* pin 2*/ 
);

L8G UAI327(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( Net__UAG327_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAF327_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAG327_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAF327_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAG327_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAF327_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAG327_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAF327_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAG327_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAF327_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAG327_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAF327_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAG327_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAF327_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAG327_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAF327_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAH324_Pad1_        ) /* pin 18*/ 
);

INV01 UAI37(
    .i         ( _tilemaps_xpd_bus1_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAI37_Pad2_         ) /* pin 2*/ 
);

OR02 UAI39(
    .i1        ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .i2        ( Net__UAI37_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UAI39_Pad3_         ) /* pin 3*/ 
);

L8G UAI41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAH41_Pad3_         ) /* pin 2*/ ,
    .q8        ( Net__UAF41_Pad3_         ) /* pin 3*/ ,
    .d7        ( Net__UAH41_Pad5_         ) /* pin 4*/ ,
    .q7        ( Net__UAF41_Pad5_         ) /* pin 5*/ ,
    .d6        ( Net__UAH41_Pad7_         ) /* pin 6*/ ,
    .q6        ( Net__UAF41_Pad7_         ) /* pin 7*/ ,
    .d5        ( Net__UAH41_Pad9_         ) /* pin 8*/ ,
    .q5        ( Net__UAF41_Pad9_         ) /* pin 9*/ ,
    .d4        ( Net__UAH41_Pad11_        ) /* pin 10*/ ,
    .q4        ( Net__UAF41_Pad11_        ) /* pin 11*/ ,
    .d3        ( Net__UAH41_Pad13_        ) /* pin 12*/ ,
    .q3        ( Net__UAF41_Pad13_        ) /* pin 13*/ ,
    .d2        ( Net__UAH41_Pad15_        ) /* pin 14*/ ,
    .q2        ( Net__UAF41_Pad15_        ) /* pin 15*/ ,
    .d1        ( Net__UAH41_Pad17_        ) /* pin 16*/ ,
    .q1        ( Net__UAF41_Pad17_        ) /* pin 17*/ ,
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 18*/ 
);

M273C #(.MiscRef(UAI79;UAI85;UAI90;UAI94;UAI98;UAI103;UAI107) 
) UAI81(
    .q0        ( Net__UAI81_Pad2_         ) /* pin 2*/ ,
    .q1        ( Net__UAI81_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAI81_Pad6_         ) /* pin 6*/ ,
    .d3        ( _XSEL                    ) /* pin 8*/ ,
    .q3        ( Net__UAG124_Pad2_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 11*/ ,
    .q4        ( _tilemaps_cb_bus2_T_XS0  ) /* pin 12*/ ,
    .d4        ( _XS0                     ) /* pin 13*/ ,
    .d5        ( _XS1                     ) /* pin 14*/ ,
    .q5        ( _tilemaps_cb_bus2_T_XS1  ) /* pin 15*/ ,
    .q6        ( _tilemaps_cb_bus2_T_XS2  ) /* pin 16*/ ,
    .d6        ( _XS2                     ) /* pin 17*/ ,
    .d7        ( _XS3                     ) /* pin 18*/ ,
    .q7        ( _tilemaps_cb_bus2_T_XS3  ) /* pin 19*/ 
);

M198C UAJ1(
    .l0        ( Net__UAF2_Pad17_         ) /* pin 1*/ ,
    .l1        ( Net__UAF2_Pad15_         ) /* pin 2*/ ,
    .l2        ( Net__UAF2_Pad13_         ) /* pin 3*/ ,
    .l3        ( Net__UAF2_Pad11_         ) /* pin 4*/ ,
    .l4        ( Net__UAF2_Pad9_          ) /* pin 5*/ ,
    .l5        ( Net__UAF2_Pad7_          ) /* pin 6*/ ,
    .l6        ( Net__UAF2_Pad5_          ) /* pin 7*/ ,
    .l7        ( Net__UAF2_Pad3_          ) /* pin 9*/ ,
    .q0        ( Net__UAJ1_Pad10_         ) /* pin 10*/ ,
    .q1        ( Net__UAJ1_Pad11_         ) /* pin 11*/ ,
    .q2        ( Net__UAJ1_Pad12_         ) /* pin 12*/ ,
    .q3        ( Net__UAJ1_Pad13_         ) /* pin 13*/ ,
    .q4        ( Net__UAJ1_Pad14_         ) /* pin 14*/ ,
    .q5        ( Net__UAJ1_Pad15_         ) /* pin 15*/ ,
    .q6        ( Net__UAJ1_Pad16_         ) /* pin 16*/ ,
    .q7        ( Net__UAJ1_Pad17_         ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI39_Pad3_         ) /* pin 20*/ ,
    .lnr       ( Net__UAH38_Pad3_         ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAJ128(
    .l0        ( Net__UAF130_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAF130_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAF130_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAF130_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAF130_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAF130_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAF130_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAF130_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAJ128_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAJ128_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAJ128_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAJ128_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAJ128_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAJ128_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAJ128_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAJ128_Pad17_       ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI165_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG166_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAJ167(
    .l0        ( Net__UAF168_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAF168_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAF168_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAF168_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAF168_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAF168_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAF168_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAF168_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAJ167_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAJ167_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAJ167_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAJ167_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAJ167_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAJ167_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAJ167_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAJ167_Pad17_       ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI165_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG166_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAJ208(
    .l0        ( Net__UAE209_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAE209_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAE209_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAE209_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAE209_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAE209_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAE209_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAE209_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAJ208_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAJ208_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAJ208_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAJ208_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAJ208_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAJ208_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAJ208_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAJ208_Pad17_       ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI246_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG245_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAJ247(
    .l0        ( Net__UAE248_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAE248_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAE248_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAE248_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAE248_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAE248_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAE248_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAE248_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAJ247_Pad10_       ) /* pin 10*/ ,
    .q1        ( Net__UAJ247_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAJ247_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAJ247_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAJ247_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAJ247_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAJ247_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAJ247_Pad17_       ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI246_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG245_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAJ39(
    .l0        ( Net__UAF41_Pad17_        ) /* pin 1*/ ,
    .l1        ( Net__UAF41_Pad15_        ) /* pin 2*/ ,
    .l2        ( Net__UAF41_Pad13_        ) /* pin 3*/ ,
    .l3        ( Net__UAF41_Pad11_        ) /* pin 4*/ ,
    .l4        ( Net__UAF41_Pad9_         ) /* pin 5*/ ,
    .l5        ( Net__UAF41_Pad7_         ) /* pin 6*/ ,
    .l6        ( Net__UAF41_Pad5_         ) /* pin 7*/ ,
    .l7        ( Net__UAF41_Pad3_         ) /* pin 9*/ ,
    .q0        ( Net__UAJ39_Pad10_        ) /* pin 10*/ ,
    .q1        ( Net__UAJ39_Pad11_        ) /* pin 11*/ ,
    .q2        ( Net__UAJ39_Pad12_        ) /* pin 12*/ ,
    .q3        ( Net__UAJ39_Pad13_        ) /* pin 13*/ ,
    .q4        ( Net__UAJ39_Pad14_        ) /* pin 14*/ ,
    .q5        ( Net__UAJ39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAJ39_Pad16_        ) /* pin 16*/ ,
    .q7        ( Net__UAJ39_Pad17_        ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI39_Pad3_         ) /* pin 20*/ ,
    .lnr       ( Net__UAH38_Pad3_         ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M273C #(.MiscRef(UAJ79;UAJ85;UAJ90;UAJ94;UAJ98;UAJ103;UAJ107) 
) UAJ81(
    .q0        ( _tilemaps_cb_bus2_XS_OUT3 ) /* pin 2*/ ,
    .d0        ( _tilemaps_cb_bus2_T_XS3  ) /* pin 3*/ ,
    .d1        ( _tilemaps_cb_bus2_T_XS2  ) /* pin 4*/ ,
    .q1        ( _tilemaps_cb_bus2_XS_OUT2 ) /* pin 5*/ ,
    .q2        ( _tilemaps_cb_bus2_XS_OUT1 ) /* pin 6*/ ,
    .d2        ( _tilemaps_cb_bus2_T_XS1  ) /* pin 7*/ ,
    .d3        ( _tilemaps_cb_bus2_T_XS0  ) /* pin 8*/ ,
    .q3        ( _tilemaps_cb_bus2_XS_OUT0 ) /* pin 9*/ ,
    .clk       ( Net__UAF79_Pad11_        ) /* pin 11*/ ,
    .q4        ( _tilemaps_xpd_bus2_XSEL  ) /* pin 12*/ ,
    .d4        ( Net__UAG124_Pad2_        ) /* pin 13*/ ,
    .d5        ( Net__UAI81_Pad6_         ) /* pin 14*/ ,
    .q5        ( Net__UAJ81_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAJ81_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAI81_Pad5_         ) /* pin 17*/ ,
    .d7        ( Net__UAI81_Pad2_         ) /* pin 18*/ ,
    .q7        ( Net__UAJ81_Pad19_        ) /* pin 19*/ 
);

BUF12 UAK103(
    .i         ( _tilemaps_cb_bus2_XS_OUT3 ) /* pin 1*/ ,
    .o         ( Net__UAK103_Pad2_        ) /* pin 2*/ 
);

NBUF02 UAK107(
    .i         ( _tilemaps_cb_bus2_XS_OUT3 ) /* pin 1*/ ,
    .o         ( Net__UAK107_Pad2_        ) /* pin 2*/ 
);

M367C #(.MiscRef(UAK118;UAK120;UAK121) 
) UAK116(
    .oe_n      ( _debug_~DEBUG4           ) /* pin 1*/ ,
    .a0        ( _tilemaps_col3_assemble_GFX_A ) /* pin 2*/ ,
    .a1        ( _tilemaps_col3_assemble_GFX_B ) /* pin 3*/ ,
    .a2        ( _tilemaps_col3_assemble_GFX_C ) /* pin 4*/ ,
    .a3        ( _tilemaps_col3_assemble_GFX_D ) /* pin 5*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

M541C #(.MiscRef(UAK84;UAK88;UAK92;UAK96;UAK101;UAK105;UAK110;UAK115) 
) UAK81(
    .oe_n      ( _debug_~DEBUG4           ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 8*/ ,
    .a7        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

BUF12 UAK90(
    .i         ( _tilemaps_cb_bus2_XS_OUT0 ) /* pin 1*/ ,
    .o         ( Net__UAK90_Pad2_         ) /* pin 2*/ 
);

BUF12 UAK94(
    .i         ( _tilemaps_cb_bus2_XS_OUT1 ) /* pin 1*/ ,
    .o         ( Net__UAK94_Pad2_         ) /* pin 2*/ 
);

BUF12 UAK99(
    .i         ( _tilemaps_cb_bus2_XS_OUT2 ) /* pin 1*/ ,
    .o         ( Net__UAK99_Pad2_         ) /* pin 2*/ 
);

DFFCOO UAL102(
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAL102_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAL102_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UAL102_Pad5_        ) /* pin 5*/ 
);

NOR02 UAL106(
    .i1        ( Net__UAL102_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UAL106_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAL106_Pad3_        ) /* pin 3*/ 
);

XNOR02 UAL109(
    .Unknown_pin( Net__UAL109_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAL106_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAL109_Pad3_        ) /* pin 3*/ 
);

AOI22 UAL111(
    .i1        ( Net__UAL111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAL109_Pad3_        ) /* pin 3*/ ,
    .i4        ( _tilemaps_cb_bus2_T_XS1  ) /* pin 4*/ ,
    .o5        ( Net__UAL111_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAL114(
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAL111_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAL109_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAL114_Pad5_        ) /* pin 5*/ 
);

AND02 UAL119(
    .i1        ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .i2        ( _tilemaps_xpd_bus2_XSEL  ) /* pin 2*/ ,
    .o         ( Net__UAL111_Pad1_        ) /* pin 3*/ 
);

INV01 UAL121(
    .i         ( _tilemaps_xpd_bus2_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAL121_Pad2_        ) /* pin 2*/ 
);

AND02 UAL122(
    .i1        ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .i2        ( Net__UAL121_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAL111_Pad2_        ) /* pin 3*/ 
);

L8G UAL130(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL130_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL130_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL130_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL130_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL130_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL130_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL130_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL130_Pad17_       ) /* pin 17*/ 
);

AO22 UAL165(
    .i1        ( Net__UAL165_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL165_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAJ167_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAJ167_Pad10_       ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr2_D          ) /* pin 5*/ 
);

L8G UAL168(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL168_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL168_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL168_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL168_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL168_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL168_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL168_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL168_Pad17_       ) /* pin 17*/ 
);

L8G UAL2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL2_Pad3_          ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL2_Pad5_          ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL2_Pad7_          ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL2_Pad9_          ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL2_Pad11_         ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL2_Pad13_         ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL2_Pad15_         ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL2_Pad17_         ) /* pin 17*/ 
);

L8G UAL209(
    .le        ( Net__UAE209_Pad1_        ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL209_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL209_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL209_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL209_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL209_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL209_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL209_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL209_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAE209_Pad18_       ) /* pin 18*/ 
);

AO22 UAL245(
    .i1        ( Net__UAL245_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL245_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAJ208_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAJ208_Pad10_       ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr3_D          ) /* pin 5*/ 
);

L8G UAL248(
    .le        ( Net__UAE209_Pad1_        ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL248_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL248_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL248_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL248_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL248_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL248_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL248_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL248_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAE209_Pad18_       ) /* pin 18*/ 
);

L8G UAL288(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL288_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL288_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL288_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL288_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL288_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL288_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL288_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL288_Pad17_       ) /* pin 17*/ 
);

AO22 UAL324(
    .i1        ( Net__UAL324_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL324_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAI325_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAI325_Pad10_       ) /* pin 4*/ ,
    .o5        ( _obj_manager_xpd_bus4_D  ) /* pin 5*/ 
);

L8G UAL327(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL327_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL327_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL327_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL327_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL327_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL327_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL327_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL327_Pad17_       ) /* pin 17*/ 
);

AO22 UAL38(
    .i1        ( Net__UAL38_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UAL38_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UAJ1_Pad17_         ) /* pin 3*/ ,
    .i4        ( Net__UAJ1_Pad10_         ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr1_D          ) /* pin 5*/ 
);

L8G UAL41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAL41_Pad3_         ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAL41_Pad5_         ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAL41_Pad7_         ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAL41_Pad9_         ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAL41_Pad11_        ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAL41_Pad13_        ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAL41_Pad15_        ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAL41_Pad17_        ) /* pin 17*/ 
);

INV01 UAL93(
    .o         ( Net__UAL93_Pad2_         ) /* pin 2*/ 
);

NAND02 UAL94(
    .o         ( Net__UAL106_Pad2_        ) /* pin 3*/ 
);

XOR02 UAL96(
    .i1        ( Net__UAL102_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UAL106_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAL96_Pad3_         ) /* pin 3*/ 
);

AOI22 UAL99(
    .i1        ( Net__UAL111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAL96_Pad3_         ) /* pin 3*/ ,
    .i4        ( _tilemaps_cb_bus2_T_XS0  ) /* pin 4*/ ,
    .o5        ( Net__UAL102_Pad2_        ) /* pin 5*/ 
);

L8G UAM130(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAL130_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM130_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAL130_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM130_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAL130_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM130_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAL130_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM130_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAL130_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM130_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAL130_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM130_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAL130_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM130_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAL130_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM130_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF130_Pad18_       ) /* pin 18*/ 
);

AO22 UAM165(
    .i1        ( Net__UAL165_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL165_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAJ128_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAJ128_Pad10_       ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr2_C          ) /* pin 5*/ 
);

L8G UAM168(
    .le        ( Net__UAE130_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAL168_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM168_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAL168_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM168_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAL168_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM168_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAL168_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM168_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAL168_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM168_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAL168_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM168_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAL168_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM168_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAL168_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM168_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF130_Pad18_       ) /* pin 18*/ 
);

L8G UAM2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( Net__UAL2_Pad3_          ) /* pin 2*/ ,
    .q8        ( Net__UAM2_Pad3_          ) /* pin 3*/ ,
    .d7        ( Net__UAL2_Pad5_          ) /* pin 4*/ ,
    .q7        ( Net__UAM2_Pad5_          ) /* pin 5*/ ,
    .d6        ( Net__UAL2_Pad7_          ) /* pin 6*/ ,
    .q6        ( Net__UAM2_Pad7_          ) /* pin 7*/ ,
    .d5        ( Net__UAL2_Pad9_          ) /* pin 8*/ ,
    .q5        ( Net__UAM2_Pad9_          ) /* pin 9*/ ,
    .d4        ( Net__UAL2_Pad11_         ) /* pin 10*/ ,
    .q4        ( Net__UAM2_Pad11_         ) /* pin 11*/ ,
    .d3        ( Net__UAL2_Pad13_         ) /* pin 12*/ ,
    .q3        ( Net__UAM2_Pad13_         ) /* pin 13*/ ,
    .d2        ( Net__UAL2_Pad15_         ) /* pin 14*/ ,
    .q2        ( Net__UAM2_Pad15_         ) /* pin 15*/ ,
    .d1        ( Net__UAL2_Pad17_         ) /* pin 16*/ ,
    .q1        ( Net__UAM2_Pad17_         ) /* pin 17*/ ,
    .g_n       ( Net__UAF2_Pad18_         ) /* pin 18*/ 
);

AO22 UAM245(
    .i1        ( Net__UAL245_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL245_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAJ247_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAJ247_Pad10_       ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr3_C          ) /* pin 5*/ 
);

L8G UAM288(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAL288_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM288_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAL288_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM288_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAL288_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM288_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAL288_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM288_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAL288_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM288_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAL288_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM288_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAL288_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM288_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAL288_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM288_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF288_Pad18_       ) /* pin 18*/ 
);

AO22 UAM324(
    .i1        ( Net__UAL324_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL324_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAI287_Pad17_       ) /* pin 3*/ ,
    .i4        ( Net__UAI287_Pad10_       ) /* pin 4*/ ,
    .o5        ( _obj_manager_xpd_bus4_C  ) /* pin 5*/ 
);

L8G UAM327(
    .le        ( Net__UAE288_Pad1_        ) /* pin 1*/ ,
    .d8        ( Net__UAL327_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM327_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAL327_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM327_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAL327_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM327_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAL327_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM327_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAL327_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM327_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAL327_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM327_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAL327_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM327_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAL327_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM327_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAF288_Pad18_       ) /* pin 18*/ 
);

AO22 UAM38(
    .i1        ( Net__UAL38_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UAL38_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UAJ39_Pad17_        ) /* pin 3*/ ,
    .i4        ( Net__UAJ39_Pad10_        ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr1_C          ) /* pin 5*/ 
);

L8G UAM41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 1*/ ,
    .d8        ( Net__UAL41_Pad3_         ) /* pin 2*/ ,
    .q8        ( Net__UAM41_Pad3_         ) /* pin 3*/ ,
    .d7        ( Net__UAL41_Pad5_         ) /* pin 4*/ ,
    .q7        ( Net__UAM41_Pad5_         ) /* pin 5*/ ,
    .d6        ( Net__UAL41_Pad7_         ) /* pin 6*/ ,
    .q6        ( Net__UAM41_Pad7_         ) /* pin 7*/ ,
    .d5        ( Net__UAL41_Pad9_         ) /* pin 8*/ ,
    .q5        ( Net__UAM41_Pad9_         ) /* pin 9*/ ,
    .d4        ( Net__UAL41_Pad11_        ) /* pin 10*/ ,
    .q4        ( Net__UAM41_Pad11_        ) /* pin 11*/ ,
    .d3        ( Net__UAL41_Pad13_        ) /* pin 12*/ ,
    .q3        ( Net__UAM41_Pad13_        ) /* pin 13*/ ,
    .d2        ( Net__UAL41_Pad15_        ) /* pin 14*/ ,
    .q2        ( Net__UAM41_Pad15_        ) /* pin 15*/ ,
    .d1        ( Net__UAL41_Pad17_        ) /* pin 16*/ ,
    .q1        ( Net__UAM41_Pad17_        ) /* pin 17*/ ,
    .g_n       ( Net__UAF2_Pad18_         ) /* pin 18*/ 
);

AOI22 UAN103(
    .i1        ( Net__UAL111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAN103_Pad3_        ) /* pin 3*/ ,
    .i4        ( _tilemaps_cb_bus2_T_XS3  ) /* pin 4*/ ,
    .o5        ( Net__UAN103_Pad5_        ) /* pin 5*/ 
);

XNOR02 UAN105(
    .Unknown_pin( Net__UAN105_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAN105_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAN103_Pad3_        ) /* pin 3*/ 
);

NOR04 UAN108(
    .o1        ( Net__UAN105_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAL106_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAL102_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAL109_Pad1_        ) /* pin 4*/ ,
    .i5        ( Net__UAN108_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAN111(
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAN111_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAN108_Pad5_        ) /* pin 3*/ ,
    .q_n       ( Net__UAN111_Pad5_        ) /* pin 5*/ 
);

AOI22 UAN116(
    .i1        ( Net__UAL111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAN116_Pad3_        ) /* pin 3*/ ,
    .i4        ( _tilemaps_cb_bus2_T_XS2  ) /* pin 4*/ ,
    .o5        ( Net__UAN111_Pad2_        ) /* pin 5*/ 
);

XNOR02 UAN118(
    .Unknown_pin( Net__UAN108_Pad5_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAN118_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAN116_Pad3_        ) /* pin 3*/ 
);

NOR03 UAN122(
    .i1        ( Net__UAL106_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAL102_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UAL109_Pad1_        ) /* pin 3*/ ,
    .o4        ( Net__UAN118_Pad2_        ) /* pin 4*/ 
);

NAND02 UAN124(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_SERIAL_LOAD    ) /* pin 2*/ ,
    .o         ( Net__UAF79_Pad11_        ) /* pin 3*/ 
);

INV01 UAN129(
    .i         ( Net__UAL165_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UAL165_Pad1_        ) /* pin 2*/ 
);

L8G UAN130(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN130_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN130_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN130_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN130_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN130_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN130_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN130_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN130_Pad17_       ) /* pin 17*/ 
);

AO22 UAN165(
    .i1        ( Net__UAL165_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL165_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAN165_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAN165_Pad4_        ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr2_B          ) /* pin 5*/ 
);

L8G UAN168(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN168_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN168_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN168_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN168_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN168_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN168_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN168_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN168_Pad17_       ) /* pin 17*/ 
);

L8G UAN2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN2_Pad3_          ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN2_Pad5_          ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN2_Pad7_          ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN2_Pad9_          ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN2_Pad11_         ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN2_Pad13_         ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN2_Pad15_         ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN2_Pad17_         ) /* pin 17*/ 
);

L8G UAN209(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN209_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN209_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN209_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN209_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN209_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN209_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN209_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN209_Pad17_       ) /* pin 17*/ 
);

AO22 UAN245(
    .i1        ( Net__UAL245_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL245_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAN245_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAN245_Pad4_        ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr3_B          ) /* pin 5*/ 
);

L8G UAN248(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN248_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN248_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN248_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN248_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN248_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN248_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN248_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN248_Pad17_       ) /* pin 17*/ 
);

INV01 UAN284(
    .i         ( Net__UAL245_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UAL245_Pad1_        ) /* pin 2*/ 
);

INV01 UAN287(
    .i         ( Net__UAL324_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UAL324_Pad1_        ) /* pin 2*/ 
);

L8G UAN288(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN288_Pad3_        ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN288_Pad5_        ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN288_Pad7_        ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN288_Pad9_        ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN288_Pad11_       ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN288_Pad13_       ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN288_Pad15_       ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN288_Pad17_       ) /* pin 17*/ 
);

AO22 UAN324(
    .i1        ( Net__UAL324_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL324_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAN324_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAN324_Pad4_        ) /* pin 4*/ ,
    .o5        ( _obj_manager_xpd_bus4_B  ) /* pin 5*/ 
);

L8G UAN327(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( _BPD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN327_Pad3_        ) /* pin 3*/ ,
    .d7        ( _BPD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN327_Pad5_        ) /* pin 5*/ ,
    .d6        ( _BPD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN327_Pad7_        ) /* pin 7*/ ,
    .d5        ( _BPD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN327_Pad9_        ) /* pin 9*/ ,
    .d4        ( _BPD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN327_Pad11_       ) /* pin 11*/ ,
    .d3        ( _BPD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN327_Pad13_       ) /* pin 13*/ ,
    .d2        ( _BPD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN327_Pad15_       ) /* pin 15*/ ,
    .d1        ( _BPD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN327_Pad17_       ) /* pin 17*/ 
);

AO22 UAN38(
    .i1        ( Net__UAL38_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UAL38_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UAN38_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UAN38_Pad4_         ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr1_B          ) /* pin 5*/ 
);

L8G UAN41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( _APD7                    ) /* pin 2*/ ,
    .q8        ( Net__UAN41_Pad3_         ) /* pin 3*/ ,
    .d7        ( _APD6                    ) /* pin 4*/ ,
    .q7        ( Net__UAN41_Pad5_         ) /* pin 5*/ ,
    .d6        ( _APD5                    ) /* pin 6*/ ,
    .q6        ( Net__UAN41_Pad7_         ) /* pin 7*/ ,
    .d5        ( _APD4                    ) /* pin 8*/ ,
    .q5        ( Net__UAN41_Pad9_         ) /* pin 9*/ ,
    .d4        ( _APD3                    ) /* pin 10*/ ,
    .q4        ( Net__UAN41_Pad11_        ) /* pin 11*/ ,
    .d3        ( _APD2                    ) /* pin 12*/ ,
    .q3        ( Net__UAN41_Pad13_        ) /* pin 13*/ ,
    .d2        ( _APD1                    ) /* pin 14*/ ,
    .q2        ( Net__UAN41_Pad15_        ) /* pin 15*/ ,
    .d1        ( _APD0                    ) /* pin 16*/ ,
    .q1        ( Net__UAN41_Pad17_        ) /* pin 17*/ 
);

INV01 UAN76(
    .i         ( Net__UAL38_Pad2_         ) /* pin 1*/ ,
    .o         ( Net__UAL38_Pad1_         ) /* pin 2*/ 
);

NAND02 UAN81(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_SERIAL_LOAD    ) /* pin 2*/ ,
    .o         ( Net__UAN81_Pad3_         ) /* pin 3*/ 
);

NAND02 UAN91(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_col3_assemble_SERIAL_END ) /* pin 2*/ ,
    .o         ( Net__UAG79_Pad11_        ) /* pin 3*/ 
);

NOR02 UAN93(
    .i1        ( Net__UAL93_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UAN93_Pad2_         ) /* pin 2*/ ,
    .o         ( _tilemaps_col3_assemble_SERIAL_END ) /* pin 3*/ 
);

OR04 UAN95(
    .i1        ( Net__UAN105_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAN108_Pad5_        ) /* pin 2*/ ,
    .i3        ( Net__UAL109_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UAL102_Pad3_        ) /* pin 4*/ ,
    .o         ( Net__UAN93_Pad2_         ) /* pin 5*/ 
);

DFFCOO UAN98(
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAN103_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAN105_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAN98_Pad5_         ) /* pin 5*/ 
);

BUF12 UAO0(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_sync_~RESET ) /* pin 2*/ 
);

INV01 UAO129(
    .i         ( _tilemaps_xpd_bus2_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAL165_Pad2_        ) /* pin 2*/ 
);

L8G UAO130(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN130_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM130_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN130_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM130_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN130_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM130_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN130_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM130_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN130_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM130_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN130_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM130_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN130_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM130_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN130_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM130_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 18*/ 
);

AO22 UAO165(
    .i1        ( Net__UAL165_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL165_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAO165_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAO165_Pad4_        ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr2_A          ) /* pin 5*/ 
);

L8G UAO168(
    .le        ( _tilemaps_cb_bus2_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN168_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM168_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN168_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM168_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN168_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM168_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN168_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM168_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN168_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM168_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN168_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM168_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN168_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM168_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN168_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM168_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus2_SERIAL_CHNL ) /* pin 18*/ 
);

L8G UAO2(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN2_Pad3_          ) /* pin 2*/ ,
    .q8        ( Net__UAM2_Pad3_          ) /* pin 3*/ ,
    .d7        ( Net__UAN2_Pad5_          ) /* pin 4*/ ,
    .q7        ( Net__UAM2_Pad5_          ) /* pin 5*/ ,
    .d6        ( Net__UAN2_Pad7_          ) /* pin 6*/ ,
    .q6        ( Net__UAM2_Pad7_          ) /* pin 7*/ ,
    .d5        ( Net__UAN2_Pad9_          ) /* pin 8*/ ,
    .q5        ( Net__UAM2_Pad9_          ) /* pin 9*/ ,
    .d4        ( Net__UAN2_Pad11_         ) /* pin 10*/ ,
    .q4        ( Net__UAM2_Pad11_         ) /* pin 11*/ ,
    .d3        ( Net__UAN2_Pad13_         ) /* pin 12*/ ,
    .q3        ( Net__UAM2_Pad13_         ) /* pin 13*/ ,
    .d2        ( Net__UAN2_Pad15_         ) /* pin 14*/ ,
    .q2        ( Net__UAM2_Pad15_         ) /* pin 15*/ ,
    .d1        ( Net__UAN2_Pad17_         ) /* pin 16*/ ,
    .q1        ( Net__UAM2_Pad17_         ) /* pin 17*/ ,
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 18*/ 
);

BUF12 UAO204(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( _tilemaps_sr2_sync_~RESET ) /* pin 2*/ 
);

BUF12 UAO208(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( _tilemaps_sr3_sync_~RESET ) /* pin 2*/ 
);

L8G UAO209(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN209_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAL209_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN209_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAL209_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN209_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAL209_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN209_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAL209_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN209_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAL209_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN209_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAL209_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN209_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAL209_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN209_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAL209_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 18*/ 
);

AO22 UAO245(
    .i1        ( Net__UAL245_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL245_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAO245_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAO245_Pad4_        ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr3_A          ) /* pin 5*/ 
);

L8G UAO248(
    .le        ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN248_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAL248_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN248_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAL248_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN248_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAL248_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN248_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAL248_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN248_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAL248_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN248_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAL248_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN248_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAL248_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN248_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAL248_Pad17_       ) /* pin 17*/ ,
    .g_n       ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 18*/ 
);

INV01 UAO284(
    .i         ( _tilemaps_xpd_bus3_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAL245_Pad2_        ) /* pin 2*/ 
);

INV01 UAO287(
    .i         ( _obj_manager_xpd_bus4_XSEL_SYNC ) /* pin 1*/ ,
    .o         ( Net__UAL324_Pad2_        ) /* pin 2*/ 
);

L8G UAO288(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( Net__UAN288_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM288_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN288_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM288_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN288_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM288_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN288_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM288_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN288_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM288_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN288_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM288_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN288_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM288_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN288_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM288_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAH324_Pad1_        ) /* pin 18*/ 
);

AO22 UAO324(
    .i1        ( Net__UAL324_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAL324_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAO324_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAO324_Pad4_        ) /* pin 4*/ ,
    .o5        ( _obj_manager_xpd_bus4_A  ) /* pin 5*/ 
);

L8G UAO327(
    .le        ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d8        ( Net__UAN327_Pad3_        ) /* pin 2*/ ,
    .q8        ( Net__UAM327_Pad3_        ) /* pin 3*/ ,
    .d7        ( Net__UAN327_Pad5_        ) /* pin 4*/ ,
    .q7        ( Net__UAM327_Pad5_        ) /* pin 5*/ ,
    .d6        ( Net__UAN327_Pad7_        ) /* pin 6*/ ,
    .q6        ( Net__UAM327_Pad7_        ) /* pin 7*/ ,
    .d5        ( Net__UAN327_Pad9_        ) /* pin 8*/ ,
    .q5        ( Net__UAM327_Pad9_        ) /* pin 9*/ ,
    .d4        ( Net__UAN327_Pad11_       ) /* pin 10*/ ,
    .q4        ( Net__UAM327_Pad11_       ) /* pin 11*/ ,
    .d3        ( Net__UAN327_Pad13_       ) /* pin 12*/ ,
    .q3        ( Net__UAM327_Pad13_       ) /* pin 13*/ ,
    .d2        ( Net__UAN327_Pad15_       ) /* pin 14*/ ,
    .q2        ( Net__UAM327_Pad15_       ) /* pin 15*/ ,
    .d1        ( Net__UAN327_Pad17_       ) /* pin 16*/ ,
    .q1        ( Net__UAM327_Pad17_       ) /* pin 17*/ ,
    .g_n       ( Net__UAH324_Pad1_        ) /* pin 18*/ 
);

AO22 UAO38(
    .i1        ( Net__UAL38_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UAL38_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UAO38_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UAO38_Pad4_         ) /* pin 4*/ ,
    .o5        ( _tilemaps_sr1_A          ) /* pin 5*/ 
);

L8G UAO41(
    .le        ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 1*/ ,
    .d8        ( Net__UAN41_Pad3_         ) /* pin 2*/ ,
    .q8        ( Net__UAM41_Pad3_         ) /* pin 3*/ ,
    .d7        ( Net__UAN41_Pad5_         ) /* pin 4*/ ,
    .q7        ( Net__UAM41_Pad5_         ) /* pin 5*/ ,
    .d6        ( Net__UAN41_Pad7_         ) /* pin 6*/ ,
    .q6        ( Net__UAM41_Pad7_         ) /* pin 7*/ ,
    .d5        ( Net__UAN41_Pad9_         ) /* pin 8*/ ,
    .q5        ( Net__UAM41_Pad9_         ) /* pin 9*/ ,
    .d4        ( Net__UAN41_Pad11_        ) /* pin 10*/ ,
    .q4        ( Net__UAM41_Pad11_        ) /* pin 11*/ ,
    .d3        ( Net__UAN41_Pad13_        ) /* pin 12*/ ,
    .q3        ( Net__UAM41_Pad13_        ) /* pin 13*/ ,
    .d2        ( Net__UAN41_Pad15_        ) /* pin 14*/ ,
    .q2        ( Net__UAM41_Pad15_        ) /* pin 15*/ ,
    .d1        ( Net__UAN41_Pad17_        ) /* pin 16*/ ,
    .q1        ( Net__UAM41_Pad17_        ) /* pin 17*/ ,
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 18*/ 
);

INV01 UAO76(
    .i         ( _tilemaps_xpd_bus1_XSEL  ) /* pin 1*/ ,
    .o         ( Net__UAL38_Pad2_         ) /* pin 2*/ 
);

M273CG UAO79(
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .q0        ( Net__UAO79_Pad2_         ) /* pin 2*/ ,
    .d0        ( _CB0                     ) /* pin 3*/ ,
    .d1        ( _CB1                     ) /* pin 4*/ ,
    .q1        ( Net__UAO79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAO79_Pad6_         ) /* pin 6*/ ,
    .d2        ( _CB2                     ) /* pin 7*/ ,
    .d3        ( _CB3                     ) /* pin 8*/ ,
    .q3        ( Net__UAO79_Pad9_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 11*/ ,
    .q4        ( Net__UAO79_Pad12_        ) /* pin 12*/ ,
    .d4        ( _CB4                     ) /* pin 13*/ ,
    .d5        ( _CPC0                    ) /* pin 14*/ ,
    .q5        ( Net__UAO79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAO79_Pad16_        ) /* pin 16*/ ,
    .d6        ( _CPC1                    ) /* pin 17*/ ,
    .d7        ( _CDEN                    ) /* pin 18*/ ,
    .q7        ( Net__UAO79_Pad19_        ) /* pin 19*/ 
);

M198C UAP1(
    .l0        ( Net__UAM2_Pad17_         ) /* pin 1*/ ,
    .l1        ( Net__UAM2_Pad15_         ) /* pin 2*/ ,
    .l2        ( Net__UAM2_Pad13_         ) /* pin 3*/ ,
    .l3        ( Net__UAM2_Pad11_         ) /* pin 4*/ ,
    .l4        ( Net__UAM2_Pad9_          ) /* pin 5*/ ,
    .l5        ( Net__UAM2_Pad7_          ) /* pin 6*/ ,
    .l6        ( Net__UAM2_Pad5_          ) /* pin 7*/ ,
    .l7        ( Net__UAM2_Pad3_          ) /* pin 9*/ ,
    .q0        ( Net__UAN38_Pad4_         ) /* pin 10*/ ,
    .q1        ( Net__UAP1_Pad11_         ) /* pin 11*/ ,
    .q2        ( Net__UAP1_Pad12_         ) /* pin 12*/ ,
    .q3        ( Net__UAP1_Pad13_         ) /* pin 13*/ ,
    .q4        ( Net__UAP1_Pad14_         ) /* pin 14*/ ,
    .q5        ( Net__UAP1_Pad15_         ) /* pin 15*/ ,
    .q6        ( Net__UAP1_Pad16_         ) /* pin 16*/ ,
    .q7        ( Net__UAN38_Pad3_         ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI39_Pad3_         ) /* pin 20*/ ,
    .lnr       ( Net__UAH38_Pad3_         ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAP128(
    .l0        ( Net__UAM130_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAM130_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAM130_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAM130_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAM130_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAM130_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAM130_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAM130_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAO165_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP128_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP128_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP128_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP128_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP128_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP128_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAO165_Pad3_        ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI165_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG166_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAP167(
    .l0        ( Net__UAM168_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAM168_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAM168_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAM168_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAM168_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAM168_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAM168_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAM168_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAN165_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP167_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP167_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP167_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP167_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP167_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP167_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAN165_Pad3_        ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI165_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG166_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAP208(
    .l0        ( Net__UAL209_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAL209_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAL209_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAL209_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAL209_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAL209_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAL209_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAL209_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAN245_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP208_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP208_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP208_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP208_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP208_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP208_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAN245_Pad3_        ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI246_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG245_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAP247(
    .l0        ( Net__UAL248_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAL248_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAL248_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAL248_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAL248_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAL248_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAL248_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAL248_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAO245_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP247_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP247_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP247_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP247_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP247_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP247_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAO245_Pad3_        ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI246_Pad3_        ) /* pin 20*/ ,
    .lnr       ( Net__UAG245_Pad3_        ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M198C UAP287(
    .l0        ( Net__UAM288_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAM288_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAM288_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAM288_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAM288_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAM288_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAM288_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAM288_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAO324_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP287_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP287_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP287_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP287_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP287_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP287_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAO324_Pad3_        ) /* pin 17*/ ,
    .clk       ( Net__UAE362_Pad2_        ) /* pin 19*/ ,
    .load_n    ( Net__UAI287_Pad20_       ) /* pin 20*/ ,
    .lnr       ( Net__UAG325_Pad3_        ) /* pin 21*/ ,
    .r_n       ( Net__UAI287_Pad22_       ) /* pin 22*/ 
);

M198C UAP325(
    .l0        ( Net__UAM327_Pad17_       ) /* pin 1*/ ,
    .l1        ( Net__UAM327_Pad15_       ) /* pin 2*/ ,
    .l2        ( Net__UAM327_Pad13_       ) /* pin 3*/ ,
    .l3        ( Net__UAM327_Pad11_       ) /* pin 4*/ ,
    .l4        ( Net__UAM327_Pad9_        ) /* pin 5*/ ,
    .l5        ( Net__UAM327_Pad7_        ) /* pin 6*/ ,
    .l6        ( Net__UAM327_Pad5_        ) /* pin 7*/ ,
    .l7        ( Net__UAM327_Pad3_        ) /* pin 9*/ ,
    .q0        ( Net__UAN324_Pad4_        ) /* pin 10*/ ,
    .q1        ( Net__UAP325_Pad11_       ) /* pin 11*/ ,
    .q2        ( Net__UAP325_Pad12_       ) /* pin 12*/ ,
    .q3        ( Net__UAP325_Pad13_       ) /* pin 13*/ ,
    .q4        ( Net__UAP325_Pad14_       ) /* pin 14*/ ,
    .q5        ( Net__UAP325_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAP325_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAN324_Pad3_        ) /* pin 17*/ ,
    .clk       ( Net__UAE362_Pad2_        ) /* pin 19*/ ,
    .load_n    ( Net__UAI287_Pad20_       ) /* pin 20*/ ,
    .lnr       ( Net__UAG325_Pad3_        ) /* pin 21*/ ,
    .r_n       ( Net__UAI287_Pad22_       ) /* pin 22*/ 
);

M198C UAP39(
    .l0        ( Net__UAM41_Pad17_        ) /* pin 1*/ ,
    .l1        ( Net__UAM41_Pad15_        ) /* pin 2*/ ,
    .l2        ( Net__UAM41_Pad13_        ) /* pin 3*/ ,
    .l3        ( Net__UAM41_Pad11_        ) /* pin 4*/ ,
    .l4        ( Net__UAM41_Pad9_         ) /* pin 5*/ ,
    .l5        ( Net__UAM41_Pad7_         ) /* pin 6*/ ,
    .l6        ( Net__UAM41_Pad5_         ) /* pin 7*/ ,
    .l7        ( Net__UAM41_Pad3_         ) /* pin 9*/ ,
    .q0        ( Net__UAO38_Pad4_         ) /* pin 10*/ ,
    .q1        ( Net__UAP39_Pad11_        ) /* pin 11*/ ,
    .q2        ( Net__UAP39_Pad12_        ) /* pin 12*/ ,
    .q3        ( Net__UAP39_Pad13_        ) /* pin 13*/ ,
    .q4        ( Net__UAP39_Pad14_        ) /* pin 14*/ ,
    .q5        ( Net__UAP39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAP39_Pad16_        ) /* pin 16*/ ,
    .q7        ( Net__UAO38_Pad3_         ) /* pin 17*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 19*/ ,
    .load_n    ( Net__UAI39_Pad3_         ) /* pin 20*/ ,
    .lnr       ( Net__UAH38_Pad3_         ) /* pin 21*/ ,
    .r_n       ( _~RESET                  ) /* pin 22*/ 
);

M273CG UAP79(
    .g_n       ( Net__UAP79_Pad1_         ) /* pin 1*/ ,
    .q0        ( Net__UAO79_Pad2_         ) /* pin 2*/ ,
    .d0        ( _CB0                     ) /* pin 3*/ ,
    .d1        ( _CB1                     ) /* pin 4*/ ,
    .q1        ( Net__UAO79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAO79_Pad6_         ) /* pin 6*/ ,
    .d2        ( _CB2                     ) /* pin 7*/ ,
    .d3        ( _CB3                     ) /* pin 8*/ ,
    .q3        ( Net__UAO79_Pad9_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 11*/ ,
    .q4        ( Net__UAO79_Pad12_        ) /* pin 12*/ ,
    .d4        ( _CB4                     ) /* pin 13*/ ,
    .d5        ( _CPC0                    ) /* pin 14*/ ,
    .q5        ( Net__UAO79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAO79_Pad16_        ) /* pin 16*/ ,
    .d6        ( _CPC1                    ) /* pin 17*/ ,
    .d7        ( _CDEN                    ) /* pin 18*/ ,
    .q7        ( Net__UAO79_Pad19_        ) /* pin 19*/ 
);

M273CG UAQ79(
    .g_n       ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .q0        ( Net__UAQ79_Pad2_         ) /* pin 2*/ ,
    .q1        ( Net__UAQ79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAQ79_Pad6_         ) /* pin 6*/ ,
    .d2        ( _XSEL                    ) /* pin 7*/ ,
    .d3        ( _XS0                     ) /* pin 8*/ ,
    .q3        ( Net__UAQ79_Pad9_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus1_LOAD_CHNLB ) /* pin 11*/ ,
    .q4        ( Net__UAQ79_Pad12_        ) /* pin 12*/ ,
    .d4        ( _XS1                     ) /* pin 13*/ ,
    .d5        ( _XS2                     ) /* pin 14*/ ,
    .q5        ( Net__UAQ79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAQ79_Pad16_        ) /* pin 16*/ ,
    .q7        ( Net__UAQ79_Pad19_        ) /* pin 19*/ 
);

MUX8GLH UAR1(
    .q         ( Net__UAR1_Pad5_          ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_B ) /* pin 6*/ 
);

M273C UAR127(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAR127_Pad2_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr2_A          ) /* pin 3*/ ,
    .d1        ( Net__UAR127_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAR127_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAR127_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAR127_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAR127_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAR127_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAR127_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAR127_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAR127_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAR127_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAR127_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAR127_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAR127_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAR127_Pad19_       ) /* pin 19*/ 
);

MUX8GLH UAR167(
    .i3        ( Net__UAR167_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAR167_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAR167_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAR167_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAR167_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_B ) /* pin 6*/ ,
    .e_n       ( Net__UAK103_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAR167_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAR167_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAR167_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAR167_Pad15_       ) /* pin 15*/ 
);

MUX8GLH UAR208(
    .i3        ( Net__UAR208_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAR208_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAR208_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAR208_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAR208_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_B ) /* pin 6*/ ,
    .e_n       ( Net__UAR208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAR208_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAR208_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAR208_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAR208_Pad15_       ) /* pin 15*/ 
);

M273C UAR246(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAR246_Pad2_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr3_A          ) /* pin 3*/ ,
    .d1        ( Net__UAR246_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAR246_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAR246_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAR246_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAR246_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAR246_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAR246_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAR246_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAR246_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAR246_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAR246_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAR246_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAR246_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAR246_Pad19_       ) /* pin 19*/ 
);

M273C UAR287(
    .q0        ( _tilemaps_cb_bus3_CB_OUT0 ) /* pin 2*/ ,
    .d0        ( _CB0                     ) /* pin 3*/ ,
    .d1        ( _CB1                     ) /* pin 4*/ ,
    .q1        ( _tilemaps_cb_bus3_CB_OUT1 ) /* pin 5*/ ,
    .q2        ( _tilemaps_cb_bus3_CB_OUT2 ) /* pin 6*/ ,
    .d2        ( _CB2                     ) /* pin 7*/ ,
    .d3        ( _CB3                     ) /* pin 8*/ ,
    .q3        ( _tilemaps_cb_bus3_CB_OUT3 ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus3_LOAD_CHNLA ) /* pin 11*/ ,
    .q4        ( _tilemaps_cb_bus3_CB_OUT4 ) /* pin 12*/ ,
    .d4        ( _CB4                     ) /* pin 13*/ ,
    .d5        ( _CPC0                    ) /* pin 14*/ ,
    .q5        ( _tilemaps_cb_bus3_CPC_OUT0 ) /* pin 15*/ ,
    .q6        ( _tilemaps_cb_bus3_CPC_OUT1 ) /* pin 16*/ ,
    .d6        ( _CPC1                    ) /* pin 17*/ ,
    .d7        ( _CDEN                    ) /* pin 18*/ ,
    .q7        ( _tilemaps_col4_assemble_CDEN ) /* pin 19*/ 
);

BUF12 UAR327(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UAI287_Pad22_       ) /* pin 2*/ 
);

DFFCOO UAR341(
    .clk       ( _tilemaps_cb_bus3_LOAD_CHNLB ) /* pin 1*/ ,
    .d         ( _XSEL                    ) /* pin 2*/ ,
    .q         ( Net__UAR341_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UAR341_Pad5_        ) /* pin 5*/ 
);

XOR02 UAR346(
    .i1        ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .i2        ( Net__UAR341_Pad3_        ) /* pin 2*/ ,
    .o         ( _tilemaps_xpd_bus3_SERIAL_CHNL ) /* pin 3*/ 
);

XOR02 UAR349(
    .i1        ( Net__UAR349_Pad1_        ) /* pin 1*/ ,
    .i2        ( _obj_manager_SERIAL_CHNL ) /* pin 2*/ ,
    .o         ( Net__UAH324_Pad1_        ) /* pin 3*/ 
);

DFFCOO UAR352(
    .clk       ( Net__UAR352_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UAR349_Pad1_        ) /* pin 2*/ ,
    .q         ( _obj_manager_xpd_bus4_XSEL_SYNC ) /* pin 3*/ ,
    .q_n       ( Net__UAR352_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAR357(
    .clk       ( Net__UAF325_Pad4_        ) /* pin 1*/ ,
    .d         ( _XSEL                    ) /* pin 2*/ ,
    .q         ( Net__UAR349_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAR357_Pad5_        ) /* pin 5*/ 
);

NAND02 UAR361(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _obj_manager_SERIAL_LOAD ) /* pin 2*/ ,
    .o         ( Net__UAR352_Pad1_        ) /* pin 3*/ 
);

M273C UAR39(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAR39_Pad2_         ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr1_A          ) /* pin 3*/ ,
    .d1        ( Net__UAR39_Pad2_         ) /* pin 4*/ ,
    .q1        ( Net__UAR39_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAR39_Pad6_         ) /* pin 6*/ ,
    .d2        ( Net__UAR39_Pad5_         ) /* pin 7*/ ,
    .d3        ( Net__UAR39_Pad6_         ) /* pin 8*/ ,
    .q3        ( Net__UAR39_Pad13_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAR39_Pad12_        ) /* pin 12*/ ,
    .d4        ( Net__UAR39_Pad13_        ) /* pin 13*/ ,
    .d5        ( Net__UAR39_Pad12_        ) /* pin 14*/ ,
    .q5        ( Net__UAR39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAR39_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAR39_Pad15_        ) /* pin 17*/ ,
    .d7        ( Net__UAR39_Pad16_        ) /* pin 18*/ ,
    .q7        ( Net__UAR39_Pad19_        ) /* pin 19*/ 
);

M273CG UAR79(
    .g_n       ( Net__UAP79_Pad1_         ) /* pin 1*/ ,
    .q0        ( Net__UAQ79_Pad2_         ) /* pin 2*/ ,
    .q1        ( Net__UAQ79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAQ79_Pad6_         ) /* pin 6*/ ,
    .d2        ( _XSEL                    ) /* pin 7*/ ,
    .d3        ( _XS0                     ) /* pin 8*/ ,
    .q3        ( Net__UAQ79_Pad9_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus1_LOAD_CHNLA ) /* pin 11*/ ,
    .q4        ( Net__UAQ79_Pad12_        ) /* pin 12*/ ,
    .d4        ( _XS1                     ) /* pin 13*/ ,
    .d5        ( _XS2                     ) /* pin 14*/ ,
    .q5        ( Net__UAQ79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAQ79_Pad16_        ) /* pin 16*/ ,
    .q7        ( Net__UAQ79_Pad19_        ) /* pin 19*/ 
);

M273C UAS0(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAS0_Pad2_          ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr1_B          ) /* pin 3*/ ,
    .d1        ( Net__UAS0_Pad2_          ) /* pin 4*/ ,
    .q1        ( Net__UAS0_Pad5_          ) /* pin 5*/ ,
    .q2        ( Net__UAS0_Pad6_          ) /* pin 6*/ ,
    .d2        ( Net__UAS0_Pad5_          ) /* pin 7*/ ,
    .d3        ( Net__UAS0_Pad6_          ) /* pin 8*/ ,
    .q3        ( Net__UAS0_Pad13_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAS0_Pad12_         ) /* pin 12*/ ,
    .d4        ( Net__UAS0_Pad13_         ) /* pin 13*/ ,
    .d5        ( Net__UAS0_Pad12_         ) /* pin 14*/ ,
    .q5        ( Net__UAS0_Pad15_         ) /* pin 15*/ ,
    .q6        ( Net__UAS0_Pad16_         ) /* pin 16*/ ,
    .d6        ( Net__UAS0_Pad15_         ) /* pin 17*/ ,
    .d7        ( Net__UAS0_Pad16_         ) /* pin 18*/ ,
    .q7        ( Net__UAS0_Pad19_         ) /* pin 19*/ 
);

INV01 UAS126(
    .i         ( _obj_manager_SERIAL_CHNL ) /* pin 1*/ ,
    .o         ( Net__UAP79_Pad1_         ) /* pin 2*/ 
);

MUX8GLH UAS128(
    .i3        ( Net__UAR127_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAR127_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAR127_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAR127_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAS128_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_A ) /* pin 6*/ ,
    .e_n       ( Net__UAK103_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAR127_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAR127_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAR127_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAR127_Pad12_       ) /* pin 15*/ 
);

M273C UAS166(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAR167_Pad4_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr2_B          ) /* pin 3*/ ,
    .d1        ( Net__UAR167_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAR167_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAR167_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAR167_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAR167_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAR167_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAR167_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAR167_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAR167_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAR167_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAR167_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAR167_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAR167_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAR167_Pad12_       ) /* pin 19*/ 
);

M273C UAS207(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAR208_Pad4_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr3_B          ) /* pin 3*/ ,
    .d1        ( Net__UAR208_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAR208_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAR208_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAR208_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAR208_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAR208_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAR208_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAR208_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAR208_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAR208_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAR208_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAR208_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAR208_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAR208_Pad12_       ) /* pin 19*/ 
);

MUX8GLH UAS247(
    .i3        ( Net__UAR246_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAR246_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAR246_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAR246_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAS247_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_A ) /* pin 6*/ ,
    .e_n       ( Net__UAR208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAR246_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAR246_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAR246_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAR246_Pad12_       ) /* pin 15*/ 
);

M273C UAS287(
    .q0        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 2*/ ,
    .d0        ( _tilemaps_cb_bus3_CB_OUT0 ) /* pin 3*/ ,
    .d1        ( _tilemaps_cb_bus3_CB_OUT1 ) /* pin 4*/ ,
    .q1        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 5*/ ,
    .q2        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 6*/ ,
    .d2        ( _tilemaps_cb_bus3_CB_OUT2 ) /* pin 7*/ ,
    .d3        ( _tilemaps_cb_bus3_CB_OUT3 ) /* pin 8*/ ,
    .q3        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 9*/ ,
    .clk       ( Net__UAS287_Pad11_       ) /* pin 11*/ ,
    .q4        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 12*/ ,
    .d4        ( _tilemaps_cb_bus3_CB_OUT4 ) /* pin 13*/ ,
    .d5        ( _tilemaps_cb_bus3_CPC_OUT0 ) /* pin 14*/ ,
    .q5        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 15*/ ,
    .q6        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 16*/ ,
    .d6        ( _tilemaps_cb_bus3_CPC_OUT1 ) /* pin 17*/ ,
    .d7        ( _tilemaps_col4_assemble_CDEN ) /* pin 18*/ ,
    .q7        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 19*/ 
);

MUX8GLH UAS40(
    .q         ( Net__UAS40_Pad5_         ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_A ) /* pin 6*/ 
);

M273C UAS79(
    .q0        ( Net__UAS79_Pad2_         ) /* pin 2*/ ,
    .d0        ( Net__UAO79_Pad2_         ) /* pin 3*/ ,
    .d1        ( Net__UAO79_Pad5_         ) /* pin 4*/ ,
    .q1        ( Net__UAS79_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAS79_Pad6_         ) /* pin 6*/ ,
    .d2        ( Net__UAO79_Pad6_         ) /* pin 7*/ ,
    .d3        ( Net__UAO79_Pad9_         ) /* pin 8*/ ,
    .q3        ( Net__UAS79_Pad9_         ) /* pin 9*/ ,
    .clk       ( Net__UAN81_Pad3_         ) /* pin 11*/ ,
    .q4        ( Net__UAS79_Pad12_        ) /* pin 12*/ ,
    .d4        ( Net__UAO79_Pad12_        ) /* pin 13*/ ,
    .d5        ( Net__UAO79_Pad15_        ) /* pin 14*/ ,
    .q5        ( Net__UAS79_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAS79_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAO79_Pad16_        ) /* pin 17*/ ,
    .d7        ( Net__UAO79_Pad19_        ) /* pin 18*/ ,
    .q7        ( Net__UAS79_Pad19_        ) /* pin 19*/ 
);

MUX8GLH UAT1(
    .i3        ( Net__UAT1_Pad1_          ) /* pin 1*/ ,
    .i2        ( Net__UAT1_Pad2_          ) /* pin 2*/ ,
    .i1        ( Net__UAT1_Pad3_          ) /* pin 3*/ ,
    .i0        ( Net__UAT1_Pad4_          ) /* pin 4*/ ,
    .q         ( Net__UAT1_Pad5_          ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_B ) /* pin 6*/ ,
    .s2        ( _tilemaps_sr1_XS_BUF2    ) /* pin 9*/ ,
    .s1        ( _tilemaps_sr1_XS_BUF1    ) /* pin 10*/ ,
    .s0        ( _tilemaps_sr1_XS_BUF0    ) /* pin 11*/ ,
    .i7        ( Net__UAT1_Pad12_         ) /* pin 12*/ ,
    .i6        ( Net__UAT1_Pad13_         ) /* pin 13*/ ,
    .i5        ( Net__UAT1_Pad14_         ) /* pin 14*/ ,
    .i4        ( Net__UAT1_Pad15_         ) /* pin 15*/ 
);

M273C UAT127(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT127_Pad2_        ) /* pin 2*/ ,
    .d0        ( Net__UAR127_Pad19_       ) /* pin 3*/ ,
    .d1        ( Net__UAT127_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAT127_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAT127_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAT127_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAT127_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAT127_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT127_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAT127_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAT127_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAT127_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAT127_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAT127_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAT127_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAT127_Pad19_       ) /* pin 19*/ 
);

MUX8GLH UAT167(
    .i3        ( Net__UAT167_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT167_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAT167_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAT167_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAT167_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_B ) /* pin 6*/ ,
    .e_n       ( Net__UAK107_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAT167_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAT167_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAT167_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAT167_Pad15_       ) /* pin 15*/ 
);

MUX8GLH UAT208(
    .i3        ( Net__UAT208_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT208_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAT208_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAT208_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAT208_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_B ) /* pin 6*/ ,
    .e_n       ( Net__UAT208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAT208_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAT208_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAT208_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAT208_Pad15_       ) /* pin 15*/ 
);

M273C UAT246(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT246_Pad2_        ) /* pin 2*/ ,
    .d0        ( Net__UAR246_Pad19_       ) /* pin 3*/ ,
    .d1        ( Net__UAT246_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAT246_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAT246_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAT246_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAT246_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAT246_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT246_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAT246_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAT246_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAT246_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAT246_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAT246_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAT246_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAT246_Pad19_       ) /* pin 19*/ 
);

M367C #(.MiscRef(UAT287;UAT292;UAT294;UAT296) 
) UAT290(
    .oe_n      ( _debug_~DEBUG5           ) /* pin 1*/ ,
    .a0        ( _tilemaps_col4_assemble_GFX_A ) /* pin 2*/ ,
    .a1        ( _tilemaps_col4_assemble_GFX_B ) /* pin 3*/ ,
    .a2        ( _tilemaps_col4_assemble_GFX_C ) /* pin 4*/ ,
    .a3        ( _tilemaps_col4_assemble_GFX_D ) /* pin 5*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

M541C #(.MiscRef(UAT301;UAT303;UAT305;UAT310;UAT314;UAT319;UAT323) 
) UAT299(
    .oe_n      ( _debug_~DEBUG5           ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 8*/ ,
    .a7        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

INV01 UAT329(
    .o         ( Net__UAT329_Pad2_        ) /* pin 2*/ 
);

NAND02 UAT330(
    .o         ( Net__UAT330_Pad3_        ) /* pin 3*/ 
);

XOR02 UAT332(
    .i1        ( Net__UAT332_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT330_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UAT332_Pad3_        ) /* pin 3*/ 
);

AOI22 UAT335(
    .i1        ( Net__UAT335_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT335_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAT332_Pad3_        ) /* pin 3*/ ,
    .i4        ( _XS0                     ) /* pin 4*/ ,
    .o5        ( Net__UAT335_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAT338(
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAT335_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAT332_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAT338_Pad5_        ) /* pin 5*/ 
);

NOR02 UAT342(
    .i1        ( Net__UAT332_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT330_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UAT342_Pad3_        ) /* pin 3*/ 
);

XNOR02 UAT345(
    .Unknown_pin( Net__UAT345_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAT342_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAT345_Pad3_        ) /* pin 3*/ 
);

AOI22 UAT347(
    .i1        ( Net__UAT335_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT335_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAT345_Pad3_        ) /* pin 3*/ ,
    .i4        ( _XS1                     ) /* pin 4*/ ,
    .o5        ( Net__UAT347_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAT350(
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAT347_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAT345_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAT350_Pad5_        ) /* pin 5*/ 
);

AND02 UAT355(
    .i1        ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .i2        ( _tilemaps_sr3_sync_~LOAD ) /* pin 2*/ ,
    .o         ( Net__UAT335_Pad1_        ) /* pin 3*/ 
);

INV01 UAT357(
    .i         ( _tilemaps_sr3_sync_~LOAD ) /* pin 1*/ ,
    .o         ( Net__UAT357_Pad2_        ) /* pin 2*/ 
);

AND02 UAT358(
    .i1        ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .i2        ( Net__UAT357_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAT335_Pad2_        ) /* pin 3*/ 
);

M273C UAT39(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT39_Pad2_         ) /* pin 2*/ ,
    .d0        ( Net__UAR39_Pad19_        ) /* pin 3*/ ,
    .d1        ( Net__UAT39_Pad2_         ) /* pin 4*/ ,
    .q1        ( Net__UAT39_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAT39_Pad6_         ) /* pin 6*/ ,
    .d2        ( Net__UAT39_Pad5_         ) /* pin 7*/ ,
    .d3        ( Net__UAT39_Pad6_         ) /* pin 8*/ ,
    .q3        ( Net__UAT39_Pad13_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT39_Pad12_        ) /* pin 12*/ ,
    .d4        ( Net__UAT39_Pad13_        ) /* pin 13*/ ,
    .d5        ( Net__UAT39_Pad12_        ) /* pin 14*/ ,
    .q5        ( Net__UAT39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAT39_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAT39_Pad15_        ) /* pin 17*/ ,
    .d7        ( Net__UAT39_Pad16_        ) /* pin 18*/ ,
    .q7        ( Net__UAT39_Pad19_        ) /* pin 19*/ 
);

M273C UAT79(
    .q0        ( _tilemaps_col2_assemble_CDEN ) /* pin 2*/ ,
    .d0        ( Net__UAS79_Pad19_        ) /* pin 3*/ ,
    .d1        ( Net__UAS79_Pad16_        ) /* pin 4*/ ,
    .q1        ( _tilemaps_cb_bus1_CPC_OUT1 ) /* pin 5*/ ,
    .q2        ( _tilemaps_cb_bus1_CPC_OUT0 ) /* pin 6*/ ,
    .d2        ( Net__UAS79_Pad15_        ) /* pin 7*/ ,
    .d3        ( Net__UAS79_Pad12_        ) /* pin 8*/ ,
    .q3        ( _tilemaps_cb_bus1_CB_OUT4 ) /* pin 9*/ ,
    .clk       ( Net__UAN81_Pad3_         ) /* pin 11*/ ,
    .q4        ( _tilemaps_cb_bus1_CB_OUT3 ) /* pin 12*/ ,
    .d4        ( Net__UAS79_Pad9_         ) /* pin 13*/ ,
    .d5        ( Net__UAS79_Pad6_         ) /* pin 14*/ ,
    .q5        ( _tilemaps_cb_bus1_CB_OUT2 ) /* pin 15*/ ,
    .q6        ( _tilemaps_cb_bus1_CB_OUT1 ) /* pin 16*/ ,
    .d6        ( Net__UAS79_Pad5_         ) /* pin 17*/ ,
    .d7        ( Net__UAS79_Pad2_         ) /* pin 18*/ ,
    .q7        ( _tilemaps_cb_bus1_CB_OUT0 ) /* pin 19*/ 
);

M273C UAU0(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT1_Pad4_          ) /* pin 2*/ ,
    .d0        ( Net__UAS0_Pad19_         ) /* pin 3*/ ,
    .d1        ( Net__UAT1_Pad4_          ) /* pin 4*/ ,
    .q1        ( Net__UAT1_Pad3_          ) /* pin 5*/ ,
    .q2        ( Net__UAT1_Pad2_          ) /* pin 6*/ ,
    .d2        ( Net__UAT1_Pad3_          ) /* pin 7*/ ,
    .d3        ( Net__UAT1_Pad2_          ) /* pin 8*/ ,
    .q3        ( Net__UAT1_Pad1_          ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT1_Pad15_         ) /* pin 12*/ ,
    .d4        ( Net__UAT1_Pad1_          ) /* pin 13*/ ,
    .d5        ( Net__UAT1_Pad15_         ) /* pin 14*/ ,
    .q5        ( Net__UAT1_Pad14_         ) /* pin 15*/ ,
    .q6        ( Net__UAT1_Pad13_         ) /* pin 16*/ ,
    .d6        ( Net__UAT1_Pad14_         ) /* pin 17*/ ,
    .d7        ( Net__UAT1_Pad13_         ) /* pin 18*/ ,
    .q7        ( Net__UAT1_Pad12_         ) /* pin 19*/ 
);

MUX8GLH UAU128(
    .i3        ( Net__UAT127_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAT127_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAT127_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAT127_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAU128_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_A ) /* pin 6*/ ,
    .e_n       ( Net__UAK107_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAT127_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAT127_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAT127_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAT127_Pad12_       ) /* pin 15*/ 
);

M273C UAU166(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT167_Pad4_        ) /* pin 2*/ ,
    .d0        ( Net__UAR167_Pad12_       ) /* pin 3*/ ,
    .d1        ( Net__UAT167_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAT167_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAT167_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAT167_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAT167_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAT167_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT167_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAT167_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAT167_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAT167_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAT167_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAT167_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAT167_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAT167_Pad12_       ) /* pin 19*/ 
);

M273C UAU207(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAT208_Pad4_        ) /* pin 2*/ ,
    .d0        ( Net__UAR208_Pad12_       ) /* pin 3*/ ,
    .d1        ( Net__UAT208_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAT208_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAT208_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAT208_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAT208_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAT208_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAT208_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAT208_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAT208_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAT208_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAT208_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAT208_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAT208_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAT208_Pad12_       ) /* pin 19*/ 
);

MUX8GLH UAU247(
    .i3        ( Net__UAT246_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAT246_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAT246_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAT246_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAU247_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_A ) /* pin 6*/ ,
    .e_n       ( Net__UAT208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAT246_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAT246_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAT246_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAT246_Pad12_       ) /* pin 15*/ 
);

M273C #(.MiscRef(UAU287;UAU294;UAU298;UAU302;UAU307;UAU311;UAU316) 
) UAU289(
    .q0        ( Net__UAU289_Pad2_        ) /* pin 2*/ ,
    .d1        ( _XS3                     ) /* pin 4*/ ,
    .q1        ( _tilemaps_cb_bus3_XS_OUT3 ) /* pin 5*/ ,
    .q2        ( _tilemaps_cb_bus3_XS_OUT2 ) /* pin 6*/ ,
    .d2        ( _XS2                     ) /* pin 7*/ ,
    .d3        ( _XS1                     ) /* pin 8*/ ,
    .q3        ( _tilemaps_cb_bus3_XS_OUT1 ) /* pin 9*/ ,
    .clk       ( _tilemaps_cb_bus3_LOAD_CHNLA ) /* pin 11*/ ,
    .q4        ( _tilemaps_cb_bus3_XS_OUT0 ) /* pin 12*/ ,
    .d4        ( _XS0                     ) /* pin 13*/ ,
    .d5        ( Net__UAR341_Pad3_        ) /* pin 14*/ ,
    .q5        ( _tilemaps_xpd_bus3_XSEL  ) /* pin 15*/ ,
    .q6        ( Net__UAU289_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UAU289_Pad19_       ) /* pin 19*/ 
);

NAND02 UAU327(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_col4_assemble_SERIAL_END ) /* pin 2*/ ,
    .o         ( Net__UAS287_Pad11_       ) /* pin 3*/ 
);

NOR02 UAU329(
    .i1        ( Net__UAT329_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAU329_Pad2_        ) /* pin 2*/ ,
    .o         ( _tilemaps_col4_assemble_SERIAL_END ) /* pin 3*/ 
);

OR04 UAU331(
    .i1        ( Net__UAU331_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAU331_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAT345_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UAT332_Pad1_        ) /* pin 4*/ ,
    .o         ( Net__UAU329_Pad2_        ) /* pin 5*/ 
);

DFFCOO UAU334(
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAU334_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAU331_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAU334_Pad5_        ) /* pin 5*/ 
);

AOI22 UAU339(
    .i1        ( Net__UAT335_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT335_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAU339_Pad3_        ) /* pin 3*/ ,
    .i4        ( _XS3                     ) /* pin 4*/ ,
    .o5        ( Net__UAU334_Pad2_        ) /* pin 5*/ 
);

XNOR02 UAU341(
    .Unknown_pin( Net__UAU331_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAU341_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAU339_Pad3_        ) /* pin 3*/ 
);

NOR04 UAU344(
    .o1        ( Net__UAU341_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAT330_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UAT332_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UAT345_Pad1_        ) /* pin 4*/ ,
    .i5        ( Net__UAU331_Pad2_        ) /* pin 5*/ 
);

DFFCOO UAU347(
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAU347_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAU331_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UAU347_Pad5_        ) /* pin 5*/ 
);

AOI22 UAU352(
    .i1        ( Net__UAT335_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAT335_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAU352_Pad3_        ) /* pin 3*/ ,
    .i4        ( _XS2                     ) /* pin 4*/ ,
    .o5        ( Net__UAU347_Pad2_        ) /* pin 5*/ 
);

XNOR02 UAU355(
    .Unknown_pin( Net__UAU331_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAU355_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAU352_Pad3_        ) /* pin 3*/ 
);

NOR03 UAU358(
    .i1        ( Net__UAT330_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UAT332_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UAT345_Pad1_        ) /* pin 3*/ ,
    .o4        ( Net__UAU355_Pad2_        ) /* pin 4*/ 
);

MUX8GLH UAU40(
    .i3        ( Net__UAT39_Pad13_        ) /* pin 1*/ ,
    .i2        ( Net__UAT39_Pad6_         ) /* pin 2*/ ,
    .i1        ( Net__UAT39_Pad5_         ) /* pin 3*/ ,
    .i0        ( Net__UAT39_Pad2_         ) /* pin 4*/ ,
    .q         ( Net__UAU40_Pad5_         ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_A ) /* pin 6*/ ,
    .s2        ( _tilemaps_sr1_XS_BUF2    ) /* pin 9*/ ,
    .s1        ( _tilemaps_sr1_XS_BUF1    ) /* pin 10*/ ,
    .s0        ( _tilemaps_sr1_XS_BUF0    ) /* pin 11*/ ,
    .i7        ( Net__UAT39_Pad19_        ) /* pin 12*/ ,
    .i6        ( Net__UAT39_Pad16_        ) /* pin 13*/ ,
    .i5        ( Net__UAT39_Pad15_        ) /* pin 14*/ ,
    .i4        ( Net__UAT39_Pad12_        ) /* pin 15*/ 
);

M273C UAU79(
    .q0        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 2*/ ,
    .d0        ( _tilemaps_cb_bus1_CB_OUT0 ) /* pin 3*/ ,
    .d1        ( _tilemaps_cb_bus1_CB_OUT1 ) /* pin 4*/ ,
    .q1        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 5*/ ,
    .q2        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 6*/ ,
    .d2        ( _tilemaps_cb_bus1_CB_OUT2 ) /* pin 7*/ ,
    .d3        ( _tilemaps_cb_bus1_CB_OUT3 ) /* pin 8*/ ,
    .q3        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 9*/ ,
    .clk       ( Net__UAU79_Pad11_        ) /* pin 11*/ ,
    .q4        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 12*/ ,
    .d4        ( _tilemaps_cb_bus1_CB_OUT4 ) /* pin 13*/ ,
    .d5        ( _tilemaps_cb_bus1_CPC_OUT0 ) /* pin 14*/ ,
    .q5        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 15*/ ,
    .q6        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 16*/ ,
    .d6        ( _tilemaps_cb_bus1_CPC_OUT1 ) /* pin 17*/ ,
    .d7        ( _tilemaps_col2_assemble_CDEN ) /* pin 18*/ ,
    .q7        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 19*/ 
);

MUX8GLH UAV1(
    .q         ( Net__UAV1_Pad5_          ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_D ) /* pin 6*/ 
);

M367C #(.MiscRef(UAV117;UAV119;UAV121) 
) UAV116(
    .oe_n      ( _debug_~DEBUG3           ) /* pin 1*/ ,
    .a0        ( _tilemaps_col2_assemble_GFX_A ) /* pin 2*/ ,
    .a1        ( _tilemaps_col2_assemble_GFX_B ) /* pin 3*/ ,
    .a2        ( _tilemaps_col2_assemble_GFX_C ) /* pin 4*/ ,
    .a3        ( _tilemaps_col2_assemble_GFX_D ) /* pin 5*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

M273C UAV127(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAV127_Pad2_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr2_C          ) /* pin 3*/ ,
    .d1        ( Net__UAV127_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAV127_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAV127_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAV127_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAV127_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAV127_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAV127_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAV127_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAV127_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAV127_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAV127_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAV127_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAV127_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAV127_Pad19_       ) /* pin 19*/ 
);

MUX8GLH UAV167(
    .i3        ( Net__UAV167_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAV167_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAV167_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAV167_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAV167_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_D ) /* pin 6*/ ,
    .e_n       ( Net__UAK103_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAV167_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAV167_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAV167_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAV167_Pad15_       ) /* pin 15*/ 
);

MUX8GLH UAV208(
    .i3        ( Net__UAV208_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAV208_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAV208_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAV208_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAV208_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_D ) /* pin 6*/ ,
    .e_n       ( Net__UAR208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAV208_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAV208_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAV208_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAV208_Pad15_       ) /* pin 15*/ 
);

M273C UAV246(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAV246_Pad2_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr3_C          ) /* pin 3*/ ,
    .d1        ( Net__UAV246_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAV246_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAV246_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAV246_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAV246_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAV246_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAV246_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAV246_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAV246_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAV246_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAV246_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAV246_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAV246_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAV246_Pad19_       ) /* pin 19*/ 
);

BUF12 UAV294(
    .i         ( _tilemaps_cb_bus3_XS_OUT0 ) /* pin 1*/ ,
    .o         ( Net__UAR208_Pad11_       ) /* pin 2*/ 
);

BUF12 UAV298(
    .i         ( _tilemaps_cb_bus3_XS_OUT1 ) /* pin 1*/ ,
    .o         ( Net__UAR208_Pad10_       ) /* pin 2*/ 
);

BUF12 UAV303(
    .i         ( _tilemaps_cb_bus3_XS_OUT2 ) /* pin 1*/ ,
    .o         ( Net__UAR208_Pad9_        ) /* pin 2*/ 
);

BUF12 UAV306(
    .i         ( _tilemaps_cb_bus3_XS_OUT3 ) /* pin 1*/ ,
    .o         ( Net__UAR208_Pad7_        ) /* pin 2*/ 
);

NBUF02 UAV308(
    .i         ( _tilemaps_cb_bus3_XS_OUT3 ) /* pin 1*/ ,
    .o         ( Net__UAT208_Pad7_        ) /* pin 2*/ 
);

M273C UAV39(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAV39_Pad2_         ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr1_C          ) /* pin 3*/ ,
    .d1        ( Net__UAV39_Pad2_         ) /* pin 4*/ ,
    .q1        ( Net__UAV39_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAV39_Pad6_         ) /* pin 6*/ ,
    .d2        ( Net__UAV39_Pad5_         ) /* pin 7*/ ,
    .d3        ( Net__UAV39_Pad6_         ) /* pin 8*/ ,
    .q3        ( Net__UAV39_Pad13_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAV39_Pad12_        ) /* pin 12*/ ,
    .d4        ( Net__UAV39_Pad13_        ) /* pin 13*/ ,
    .d5        ( Net__UAV39_Pad12_        ) /* pin 14*/ ,
    .q5        ( Net__UAV39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAV39_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAV39_Pad15_        ) /* pin 17*/ ,
    .d7        ( Net__UAV39_Pad16_        ) /* pin 18*/ ,
    .q7        ( Net__UAV39_Pad19_        ) /* pin 19*/ 
);

M541C #(.MiscRef(UAV81;UAV85;UAV90;UAV94;UAV99;UAV103;UAV107;UAV112) 
) UAV79(
    .oe_n      ( _debug_~DEBUG3           ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 8*/ ,
    .a7        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

BUF12 UAV87(
    .i         ( _tilemaps_cb_bus1_XS_OUT0 ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_XS_BUF0    ) /* pin 2*/ 
);

BUF12 UAV92(
    .i         ( _tilemaps_cb_bus1_XS_OUT1 ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_XS_BUF1    ) /* pin 2*/ 
);

BUF12 UAV96(
    .i         ( _tilemaps_cb_bus1_XS_OUT2 ) /* pin 1*/ ,
    .o         ( _tilemaps_sr1_XS_BUF2    ) /* pin 2*/ 
);

M273C UAX0(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAX0_Pad2_          ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr1_D          ) /* pin 3*/ ,
    .d1        ( Net__UAX0_Pad2_          ) /* pin 4*/ ,
    .q1        ( Net__UAX0_Pad5_          ) /* pin 5*/ ,
    .q2        ( Net__UAX0_Pad6_          ) /* pin 6*/ ,
    .d2        ( Net__UAX0_Pad5_          ) /* pin 7*/ ,
    .d3        ( Net__UAX0_Pad6_          ) /* pin 8*/ ,
    .q3        ( Net__UAX0_Pad13_         ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAX0_Pad12_         ) /* pin 12*/ ,
    .d4        ( Net__UAX0_Pad13_         ) /* pin 13*/ ,
    .d5        ( Net__UAX0_Pad12_         ) /* pin 14*/ ,
    .q5        ( Net__UAX0_Pad15_         ) /* pin 15*/ ,
    .q6        ( Net__UAX0_Pad16_         ) /* pin 16*/ ,
    .d6        ( Net__UAX0_Pad15_         ) /* pin 17*/ ,
    .d7        ( Net__UAX0_Pad16_         ) /* pin 18*/ ,
    .q7        ( Net__UAX0_Pad19_         ) /* pin 19*/ 
);

INV01 UAX120(
    .i         ( _tilemaps_SERIAL_LOAD    ) /* pin 1*/ ,
    .o         ( Net__UAX120_Pad2_        ) /* pin 2*/ 
);

MUX8GLH UAX128(
    .i3        ( Net__UAV127_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAV127_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAV127_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAV127_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAX128_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_C ) /* pin 6*/ ,
    .e_n       ( Net__UAK103_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAV127_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAV127_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAV127_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAV127_Pad12_       ) /* pin 15*/ 
);

M273C UAX166(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAV167_Pad4_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr2_D          ) /* pin 3*/ ,
    .d1        ( Net__UAV167_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAV167_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAV167_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAV167_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAV167_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAV167_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAV167_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAV167_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAV167_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAV167_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAV167_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAV167_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAV167_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAV167_Pad12_       ) /* pin 19*/ 
);

M273C UAX207(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAV208_Pad4_        ) /* pin 2*/ ,
    .d0        ( _tilemaps_sr3_D          ) /* pin 3*/ ,
    .d1        ( Net__UAV208_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAV208_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAV208_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAV208_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAV208_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAV208_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAV208_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAV208_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAV208_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAV208_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAV208_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAV208_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAV208_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAV208_Pad12_       ) /* pin 19*/ 
);

MUX8GLH UAX247(
    .i3        ( Net__UAV246_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAV246_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAV246_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAV246_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAX247_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_C ) /* pin 6*/ ,
    .e_n       ( Net__UAR208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAV246_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAV246_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAV246_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAV246_Pad12_       ) /* pin 15*/ 
);

MUX8GLH UAX40(
    .q         ( Net__UAX40_Pad5_         ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_C ) /* pin 6*/ 
);

DFFCOR UAX81(
    .clk       ( Net__UAN81_Pad3_         ) /* pin 1*/ ,
    .d         ( Net__UAQ79_Pad6_         ) /* pin 2*/ ,
    .q         ( _tilemaps_xpd_bus1_XSEL  ) /* pin 3*/ ,
    .q_n       ( Net__UAX81_Pad5_         ) /* pin 5*/ 
);

DFFCOR #(.MiscRef(UAX79;UAX99) 
) UAX85(
    .clk       ( Net__UAN81_Pad3_         ) /* pin 1*/ ,
    .d         ( Net__UAQ79_Pad9_         ) /* pin 2*/ ,
    .q         ( _tilemaps_cb_bus1_XS_OUT0 ) /* pin 3*/ ,
    .q_n       ( Net__UAX85_Pad5_         ) /* pin 5*/ 
);

DFFCOR UAX90(
    .clk       ( Net__UAN81_Pad3_         ) /* pin 1*/ ,
    .d         ( Net__UAQ79_Pad12_        ) /* pin 2*/ ,
    .q         ( _tilemaps_cb_bus1_XS_OUT1 ) /* pin 3*/ ,
    .q_n       ( Net__UAX90_Pad5_         ) /* pin 5*/ 
);

DFFCOR UAX94(
    .clk       ( Net__UAN81_Pad3_         ) /* pin 1*/ ,
    .d         ( Net__UAQ79_Pad15_        ) /* pin 2*/ ,
    .q         ( _tilemaps_cb_bus1_XS_OUT2 ) /* pin 3*/ ,
    .q_n       ( Net__UAX94_Pad5_         ) /* pin 5*/ 
);

MUX8GLH UAY1(
    .i3        ( Net__UAY1_Pad1_          ) /* pin 1*/ ,
    .i2        ( Net__UAY1_Pad2_          ) /* pin 2*/ ,
    .i1        ( Net__UAY1_Pad3_          ) /* pin 3*/ ,
    .i0        ( Net__UAY1_Pad4_          ) /* pin 4*/ ,
    .q         ( Net__UAY1_Pad5_          ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_D ) /* pin 6*/ ,
    .s2        ( _tilemaps_sr1_XS_BUF2    ) /* pin 9*/ ,
    .s1        ( _tilemaps_sr1_XS_BUF1    ) /* pin 10*/ ,
    .s0        ( _tilemaps_sr1_XS_BUF0    ) /* pin 11*/ ,
    .i7        ( Net__UAY1_Pad12_         ) /* pin 12*/ ,
    .i6        ( Net__UAY1_Pad13_         ) /* pin 13*/ ,
    .i5        ( Net__UAY1_Pad14_         ) /* pin 14*/ ,
    .i4        ( Net__UAY1_Pad15_         ) /* pin 15*/ 
);

DFFCOO UAY102(
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAY102_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAY102_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UAY102_Pad5_        ) /* pin 5*/ 
);

NOR02 UAY106(
    .i1        ( Net__UAY102_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UAY106_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAY106_Pad3_        ) /* pin 3*/ 
);

XNOR02 UAY109(
    .Unknown_pin( Net__UAY109_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAY106_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAY109_Pad3_        ) /* pin 3*/ 
);

AOI22 UAY111(
    .i1        ( Net__UAY111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAY109_Pad3_        ) /* pin 3*/ ,
    .i4        ( _tilemaps_sr1_XS_BUF1    ) /* pin 4*/ ,
    .o5        ( Net__UAY111_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAY114(
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAY111_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAY109_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAY114_Pad5_        ) /* pin 5*/ 
);

AND02 UAY119(
    .i1        ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .i2        ( Net__UAX120_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAY111_Pad1_        ) /* pin 3*/ 
);

INV01 UAY121(
    .i         ( Net__UAX120_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UAY121_Pad2_        ) /* pin 2*/ 
);

AND02 UAY122(
    .i1        ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .i2        ( Net__UAY121_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAY111_Pad2_        ) /* pin 3*/ 
);

M273C UAY127(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY127_Pad2_        ) /* pin 2*/ ,
    .d0        ( Net__UAV127_Pad19_       ) /* pin 3*/ ,
    .d1        ( Net__UAY127_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAY127_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAY127_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAY127_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAY127_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAY127_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY127_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAY127_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAY127_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAY127_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAY127_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAY127_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAY127_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAY127_Pad19_       ) /* pin 19*/ 
);

MUX8GLH UAY167(
    .i3        ( Net__UAY167_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY167_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAY167_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAY167_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAY167_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_D ) /* pin 6*/ ,
    .e_n       ( Net__UAK107_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAY167_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAY167_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAY167_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAY167_Pad15_       ) /* pin 15*/ 
);

MUX8GLH UAY208(
    .i3        ( Net__UAY208_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY208_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UAY208_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UAY208_Pad4_        ) /* pin 4*/ ,
    .q         ( Net__UAY208_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_D ) /* pin 6*/ ,
    .e_n       ( Net__UAT208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAY208_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UAY208_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UAY208_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UAY208_Pad15_       ) /* pin 15*/ 
);

M273C UAY246(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY246_Pad2_        ) /* pin 2*/ ,
    .d0        ( Net__UAV246_Pad19_       ) /* pin 3*/ ,
    .d1        ( Net__UAY246_Pad2_        ) /* pin 4*/ ,
    .q1        ( Net__UAY246_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UAY246_Pad6_        ) /* pin 6*/ ,
    .d2        ( Net__UAY246_Pad5_        ) /* pin 7*/ ,
    .d3        ( Net__UAY246_Pad6_        ) /* pin 8*/ ,
    .q3        ( Net__UAY246_Pad13_       ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY246_Pad12_       ) /* pin 12*/ ,
    .d4        ( Net__UAY246_Pad13_       ) /* pin 13*/ ,
    .d5        ( Net__UAY246_Pad12_       ) /* pin 14*/ ,
    .q5        ( Net__UAY246_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UAY246_Pad16_       ) /* pin 16*/ ,
    .d6        ( Net__UAY246_Pad15_       ) /* pin 17*/ ,
    .d7        ( Net__UAY246_Pad16_       ) /* pin 18*/ ,
    .q7        ( Net__UAY246_Pad19_       ) /* pin 19*/ 
);

M273C UAY39(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY39_Pad2_         ) /* pin 2*/ ,
    .d0        ( Net__UAV39_Pad19_        ) /* pin 3*/ ,
    .d1        ( Net__UAY39_Pad2_         ) /* pin 4*/ ,
    .q1        ( Net__UAY39_Pad5_         ) /* pin 5*/ ,
    .q2        ( Net__UAY39_Pad6_         ) /* pin 6*/ ,
    .d2        ( Net__UAY39_Pad5_         ) /* pin 7*/ ,
    .d3        ( Net__UAY39_Pad6_         ) /* pin 8*/ ,
    .q3        ( Net__UAY39_Pad13_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY39_Pad12_        ) /* pin 12*/ ,
    .d4        ( Net__UAY39_Pad13_        ) /* pin 13*/ ,
    .d5        ( Net__UAY39_Pad12_        ) /* pin 14*/ ,
    .q5        ( Net__UAY39_Pad15_        ) /* pin 15*/ ,
    .q6        ( Net__UAY39_Pad16_        ) /* pin 16*/ ,
    .d6        ( Net__UAY39_Pad15_        ) /* pin 17*/ ,
    .d7        ( Net__UAY39_Pad16_        ) /* pin 18*/ ,
    .q7        ( Net__UAY39_Pad19_        ) /* pin 19*/ 
);

INV01 UAY93(
    .o         ( Net__UAY93_Pad2_         ) /* pin 2*/ 
);

NAND02 UAY94(
    .o         ( Net__UAY106_Pad2_        ) /* pin 3*/ 
);

XOR02 UAY96(
    .i1        ( Net__UAY102_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UAY106_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UAY96_Pad3_         ) /* pin 3*/ 
);

AOI22 UAY99(
    .i1        ( Net__UAY111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAY96_Pad3_         ) /* pin 3*/ ,
    .i4        ( _tilemaps_sr1_XS_BUF0    ) /* pin 4*/ ,
    .o5        ( Net__UAY102_Pad2_        ) /* pin 5*/ 
);

M273C UAZ0(
    .r_n       ( _tilemaps_sr1_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY1_Pad4_          ) /* pin 2*/ ,
    .d0        ( Net__UAX0_Pad19_         ) /* pin 3*/ ,
    .d1        ( Net__UAY1_Pad4_          ) /* pin 4*/ ,
    .q1        ( Net__UAY1_Pad3_          ) /* pin 5*/ ,
    .q2        ( Net__UAY1_Pad2_          ) /* pin 6*/ ,
    .d2        ( Net__UAY1_Pad3_          ) /* pin 7*/ ,
    .d3        ( Net__UAY1_Pad2_          ) /* pin 8*/ ,
    .q3        ( Net__UAY1_Pad1_          ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY1_Pad15_         ) /* pin 12*/ ,
    .d4        ( Net__UAY1_Pad1_          ) /* pin 13*/ ,
    .d5        ( Net__UAY1_Pad15_         ) /* pin 14*/ ,
    .q5        ( Net__UAY1_Pad14_         ) /* pin 15*/ ,
    .q6        ( Net__UAY1_Pad13_         ) /* pin 16*/ ,
    .d6        ( Net__UAY1_Pad14_         ) /* pin 17*/ ,
    .d7        ( Net__UAY1_Pad13_         ) /* pin 18*/ ,
    .q7        ( Net__UAY1_Pad12_         ) /* pin 19*/ 
);

AOI22 UAZ103(
    .i1        ( Net__UAY111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAZ103_Pad3_        ) /* pin 3*/ ,
    .o5        ( Net__UAZ103_Pad5_        ) /* pin 5*/ 
);

XNOR02 UAZ105(
    .Unknown_pin( Net__UAZ105_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAZ105_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAZ103_Pad3_        ) /* pin 3*/ 
);

NOR04 UAZ108(
    .o1        ( Net__UAZ105_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAY106_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAY102_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UAY109_Pad1_        ) /* pin 4*/ ,
    .i5        ( Net__UAZ108_Pad5_        ) /* pin 5*/ 
);

DFFCOO UAZ111(
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAZ111_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAZ108_Pad5_        ) /* pin 3*/ ,
    .q_n       ( Net__UAZ111_Pad5_        ) /* pin 5*/ 
);

AOI22 UAZ116(
    .i1        ( Net__UAY111_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAY111_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UAZ116_Pad3_        ) /* pin 3*/ ,
    .i4        ( _tilemaps_sr1_XS_BUF2    ) /* pin 4*/ ,
    .o5        ( Net__UAZ111_Pad2_        ) /* pin 5*/ 
);

XNOR02 UAZ118(
    .Unknown_pin( Net__UAZ108_Pad5_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UAZ118_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UAZ116_Pad3_        ) /* pin 3*/ 
);

NOR03 UAZ122(
    .i1        ( Net__UAY106_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UAY102_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UAY109_Pad1_        ) /* pin 3*/ ,
    .o4        ( Net__UAZ118_Pad2_        ) /* pin 4*/ 
);

MUX8GLH UAZ129(
    .i3        ( Net__UAY127_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAY127_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAY127_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAY127_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAZ129_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col3_assemble_GFX_C ) /* pin 6*/ ,
    .e_n       ( Net__UAK107_Pad2_        ) /* pin 7*/ ,
    .s2        ( Net__UAK99_Pad2_         ) /* pin 9*/ ,
    .s1        ( Net__UAK94_Pad2_         ) /* pin 10*/ ,
    .s0        ( Net__UAK90_Pad2_         ) /* pin 11*/ ,
    .i7        ( Net__UAY127_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAY127_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAY127_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAY127_Pad12_       ) /* pin 15*/ 
);

M273C UAZ166(
    .r_n       ( _tilemaps_sr2_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY167_Pad4_        ) /* pin 2*/ ,
    .d0        ( Net__UAV167_Pad12_       ) /* pin 3*/ ,
    .d1        ( Net__UAY167_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAY167_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAY167_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAY167_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAY167_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAY167_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr2_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY167_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAY167_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAY167_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAY167_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAY167_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAY167_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAY167_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAY167_Pad12_       ) /* pin 19*/ 
);

M273C UAZ207(
    .r_n       ( _tilemaps_sr3_sync_~RESET ) /* pin 1*/ ,
    .q0        ( Net__UAY208_Pad4_        ) /* pin 2*/ ,
    .d0        ( Net__UAV208_Pad12_       ) /* pin 3*/ ,
    .d1        ( Net__UAY208_Pad4_        ) /* pin 4*/ ,
    .q1        ( Net__UAY208_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UAY208_Pad2_        ) /* pin 6*/ ,
    .d2        ( Net__UAY208_Pad3_        ) /* pin 7*/ ,
    .d3        ( Net__UAY208_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UAY208_Pad1_        ) /* pin 9*/ ,
    .clk       ( _tilemaps_sr3_sync_~CLK8M ) /* pin 11*/ ,
    .q4        ( Net__UAY208_Pad15_       ) /* pin 12*/ ,
    .d4        ( Net__UAY208_Pad1_        ) /* pin 13*/ ,
    .d5        ( Net__UAY208_Pad15_       ) /* pin 14*/ ,
    .q5        ( Net__UAY208_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UAY208_Pad13_       ) /* pin 16*/ ,
    .d6        ( Net__UAY208_Pad14_       ) /* pin 17*/ ,
    .d7        ( Net__UAY208_Pad13_       ) /* pin 18*/ ,
    .q7        ( Net__UAY208_Pad12_       ) /* pin 19*/ 
);

MUX8GLH UAZ247(
    .i3        ( Net__UAY246_Pad13_       ) /* pin 1*/ ,
    .i2        ( Net__UAY246_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UAY246_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UAY246_Pad2_        ) /* pin 4*/ ,
    .q         ( Net__UAZ247_Pad5_        ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col4_assemble_GFX_C ) /* pin 6*/ ,
    .e_n       ( Net__UAT208_Pad7_        ) /* pin 7*/ ,
    .s2        ( Net__UAR208_Pad9_        ) /* pin 9*/ ,
    .s1        ( Net__UAR208_Pad10_       ) /* pin 10*/ ,
    .s0        ( Net__UAR208_Pad11_       ) /* pin 11*/ ,
    .i7        ( Net__UAY246_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UAY246_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UAY246_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UAY246_Pad12_       ) /* pin 15*/ 
);

MUX8GLH UAZ40(
    .i3        ( Net__UAY39_Pad13_        ) /* pin 1*/ ,
    .i2        ( Net__UAY39_Pad6_         ) /* pin 2*/ ,
    .i1        ( Net__UAY39_Pad5_         ) /* pin 3*/ ,
    .i0        ( Net__UAY39_Pad2_         ) /* pin 4*/ ,
    .q         ( Net__UAZ40_Pad5_         ) /* pin 5*/ ,
    .q_n       ( _tilemaps_col2_assemble_GFX_C ) /* pin 6*/ ,
    .s2        ( _tilemaps_sr1_XS_BUF2    ) /* pin 9*/ ,
    .s1        ( _tilemaps_sr1_XS_BUF1    ) /* pin 10*/ ,
    .s0        ( _tilemaps_sr1_XS_BUF0    ) /* pin 11*/ ,
    .i7        ( Net__UAY39_Pad19_        ) /* pin 12*/ ,
    .i6        ( Net__UAY39_Pad16_        ) /* pin 13*/ ,
    .i5        ( Net__UAY39_Pad15_        ) /* pin 14*/ ,
    .i4        ( Net__UAY39_Pad12_        ) /* pin 15*/ 
);

NAND02 UAZ91(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _tilemaps_col2_assemble_SERIAL_END ) /* pin 2*/ ,
    .o         ( Net__UAU79_Pad11_        ) /* pin 3*/ 
);

NOR02 UAZ93(
    .i1        ( Net__UAY93_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UAZ93_Pad2_         ) /* pin 2*/ ,
    .o         ( _tilemaps_col2_assemble_SERIAL_END ) /* pin 3*/ 
);

OR04 UAZ95(
    .i1        ( Net__UAZ105_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UAZ108_Pad5_        ) /* pin 2*/ ,
    .i3        ( Net__UAY109_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UAY102_Pad3_        ) /* pin 4*/ ,
    .o         ( Net__UAZ93_Pad2_         ) /* pin 5*/ 
);

DFFCOO UAZ98(
    .clk       ( _tilemaps_sr1_sync_~CLK8M ) /* pin 1*/ ,
    .d         ( Net__UAZ103_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UAZ105_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UAZ98_Pad5_         ) /* pin 5*/ 
);

M541C UBD192(
    .oe_n      ( _stars_row6_assemble_~DEBUG ) /* pin 1*/ ,
    .a0        ( _lut_assembly_lut_storage_ROW6_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_lut_storage_ROW6_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_lut_storage_ROW6_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_lut_storage_ROW6_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_lut_storage_ROW6_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_lut_storage_ROW6_D5 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_lut_storage_ROW6_D6 ) /* pin 8*/ ,
    .a7        ( Net__UBD192_Pad9_        ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

BUF12 UBD217(
    .i         ( _debug_td_io_IN4         ) /* pin 1*/ ,
    .o         ( __TD4                    ) /* pin 2*/ 
);

M273C #(.MiscRef(UBD219;UBD226;UBD230;UBD234;UBD238;UBD242;UBD245;UBD249) 
) UBD222(
    .q0        ( Net__UBD222_Pad2_        ) /* pin 2*/ ,
    .d0        ( _APD0                    ) /* pin 3*/ ,
    .d1        ( _APD1                    ) /* pin 4*/ ,
    .q1        ( Net__UBD222_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UBD222_Pad6_        ) /* pin 6*/ ,
    .d2        ( _APD2                    ) /* pin 7*/ ,
    .d3        ( _APD3                    ) /* pin 8*/ ,
    .q3        ( Net__UBD222_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBD222_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UBD222_Pad12_       ) /* pin 12*/ ,
    .d4        ( _APD4                    ) /* pin 13*/ ,
    .d5        ( _APD5                    ) /* pin 14*/ ,
    .q5        ( _stars_xpd_bus6_sync_PD5 ) /* pin 15*/ ,
    .q6        ( _stars_xpd_bus6_sync_PD6 ) /* pin 16*/ ,
    .d6        ( _APD6                    ) /* pin 17*/ ,
    .d7        ( _APD7                    ) /* pin 18*/ ,
    .q7        ( _stars_xpd_bus6_sync_PD7 ) /* pin 19*/ 
);

DFFCOO #(.MiscRef(UBD254) 
) UBD255(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBD255_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD255_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT1_Q0 ) /* pin 5*/ 
);

DFFCOO UBD259(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBD259_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD259_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT1_Q1 ) /* pin 5*/ 
);

DFFCOO UBD263(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBD263_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD263_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT1_Q2 ) /* pin 5*/ 
);

DFFCOO UBD267(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBD267_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD267_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT1_Q3 ) /* pin 5*/ 
);

NAND02 UBD272(
    .i1        ( _stars_row6_assemble_CNT0_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBD272_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBD272_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBD274(
    .Unknown_pin( _stars_row5_assemble_CNT1_Q3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD274_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBD274_Pad3_        ) /* pin 3*/ 
);

NAND02 UBD277(
    .i1        ( Net__UBD277_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD277_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBD255_Pad2_        ) /* pin 3*/ 
);

NAND02 UBD279(
    .i1        ( Net__UBD277_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD279_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBD259_Pad2_        ) /* pin 3*/ 
);

NAND02 UBD280(
    .i1        ( Net__UBD277_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD280_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBD263_Pad2_        ) /* pin 3*/ 
);

NAND02 UBD282(
    .i1        ( Net__UBD277_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD274_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBD267_Pad2_        ) /* pin 3*/ 
);

NAND04 UBD291(
    .i1        ( _stars_row5_assemble_CNT1_Q2 ) /* pin 1*/ ,
    .i2        ( _stars_row5_assemble_CNT1_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row5_assemble_CNT1_Q0 ) /* pin 3*/ ,
    .i4        ( Net__UBD291_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UBD274_Pad2_        ) /* pin 5*/ 
);

XNOR02 UBD294(
    .Unknown_pin( _stars_row5_assemble_CNT1_Q2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD294_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBD280_Pad2_        ) /* pin 3*/ 
);

NAND04 UBD296(
    .i1        ( _stars_row5_counter_IC   ) /* pin 1*/ ,
    .i2        ( _stars_row5_assemble_CNT1_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row5_assemble_CNT1_Q2 ) /* pin 3*/ ,
    .i4        ( _stars_row5_assemble_CNT1_Q3 ) /* pin 4*/ ,
    .o         ( Net__UBD296_Pad5_        ) /* pin 5*/ 
);

XNOR02 UBD299(
    .Unknown_pin( _stars_row5_assemble_CNT1_Q1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD299_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBD279_Pad2_        ) /* pin 3*/ 
);

AND02 UBD302(
    .i1        ( Net__UBD296_Pad5_        ) /* pin 1*/ ,
    .i2        ( _~RESET                  ) /* pin 2*/ ,
    .o         ( Net__UBD277_Pad1_        ) /* pin 3*/ 
);

NAND03 UBD304(
    .i1        ( _stars_row5_assemble_CNT1_Q1 ) /* pin 1*/ ,
    .i2        ( _stars_row5_assemble_CNT1_Q0 ) /* pin 2*/ ,
    .i3        ( Net__UBD291_Pad4_        ) /* pin 3*/ ,
    .o         ( Net__UBD294_Pad2_        ) /* pin 4*/ 
);

NAND02 UBD306(
    .i1        ( _stars_row5_assemble_CNT1_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBD291_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBD299_Pad2_        ) /* pin 3*/ 
);

BUF12 UBD308(
    .i         ( _stars_row5_counter_IC   ) /* pin 1*/ ,
    .o         ( Net__UBD291_Pad4_        ) /* pin 2*/ 
);

XOR02 UBD310(
    .i1        ( _stars_row5_assemble_CNT1_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBD291_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBD277_Pad2_        ) /* pin 3*/ 
);

BUF12 UBD316(
    .i         ( _debug_td_io_IN6         ) /* pin 1*/ ,
    .o         ( __TD6                    ) /* pin 2*/ 
);

AND03 UBD328(
    .i1        ( Net__UBD328_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_COL1 ) /* pin 2*/ ,
    .i3        ( Net__UBD328_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UAA254_Pad1_        ) /* pin 4*/ 
);

AND03 UBD330(
    .i1        ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .i2        ( Net__UBD328_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBD328_Pad3_        ) /* pin 3*/ ,
    .o         ( _tilemaps_LOAD_XPD1      ) /* pin 4*/ 
);

AND03 UBD333(
    .i1        ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_COL2 ) /* pin 2*/ ,
    .i3        ( Net__UBD328_Pad3_        ) /* pin 3*/ ,
    .o         ( _tilemaps_LOAD_XPD2      ) /* pin 4*/ 
);

BUF12 UBD337(
    .i         ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .o         ( _obj_manager_SERIAL_CHNL ) /* pin 2*/ 
);

DFFCOS #(.MiscRef(UBD339;UBD364) 
) UBD342(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD342_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD342_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBD346(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD346_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD346_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBD351(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD351_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD351_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBD355(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD355_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD355_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBD360(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD360_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBD360_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

INV01 UBD370(
    .i         ( _video_signals_video_counter_COL2 ) /* pin 1*/ ,
    .o         ( Net__UBD328_Pad1_        ) /* pin 2*/ 
);

OR03 UBD372(
    .i1        ( Net__UBD342_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBD346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBD351_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBD372_Pad4_        ) /* pin 4*/ 
);

BUF12 UBD383(
    .i         ( _video_signals_video_counter_COL1 ) /* pin 1*/ ,
    .o         ( _bus_signal_control_GFX_CHNL ) /* pin 2*/ 
);

XNOR02 UBD400(
    .Unknown_pin( Net__UBD355_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD372_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBD400_Pad3_        ) /* pin 3*/ 
);

INV01 UBD407(
    .i         ( Net__UBD407_Pad1_        ) /* pin 1*/ ,
    .o         ( _tilemaps_LOAD_XPD3      ) /* pin 2*/ 
);

OR04 UBD411(
    .i1        ( Net__UBD342_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBD346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBD351_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBD355_Pad3_        ) /* pin 4*/ ,
    .o         ( Net__UBD411_Pad5_        ) /* pin 5*/ 
);

MUX28H #(.MiscRef(UBD418;UBD421;UBD425;UBD428;UBD431;UBD435;UBD438;UBD441) 
) UBD415(
    .a0        ( _bus_io_BUS_DATA0        ) /* pin 1*/ ,
    .a1        ( _bus_io_BUS_DATA1        ) /* pin 2*/ ,
    .a2        ( _bus_io_BUS_DATA2        ) /* pin 3*/ ,
    .a3        ( _bus_io_BUS_DATA3        ) /* pin 4*/ ,
    .a4        ( _bus_io_BUS_DATA4        ) /* pin 5*/ ,
    .a7        ( _bus_io_BUS_DATA7        ) /* pin 6*/ ,
    .a6        ( _bus_io_BUS_DATA6        ) /* pin 7*/ ,
    .a5        ( _bus_io_BUS_DATA5        ) /* pin 8*/ ,
    .b0        ( _bus_io_BUS_DATA8        ) /* pin 9*/ ,
    .b1        ( _bus_io_BUS_DATA9        ) /* pin 10*/ ,
    .b2        ( _bus_io_BUS_DATA10       ) /* pin 11*/ ,
    .b3        ( _bus_io_BUS_DATA11       ) /* pin 12*/ ,
    .b6        ( _bus_io_BUS_DATA14       ) /* pin 13*/ ,
    .b4        ( _bus_io_BUS_DATA12       ) /* pin 14*/ ,
    .b7        ( _bus_io_BUS_DATA15       ) /* pin 15*/ ,
    .b5        ( _bus_io_BUS_DATA13       ) /* pin 16*/ ,
    .q0        ( Net__UAA423_Pad3_        ) /* pin 17*/ ,
    .q1        ( Net__UAA423_Pad4_        ) /* pin 18*/ ,
    .q2        ( Net__UAA423_Pad7_        ) /* pin 19*/ ,
    .q3        ( Net__UAA423_Pad8_        ) /* pin 20*/ ,
    .q6        ( Net__UAA423_Pad17_       ) /* pin 21*/ ,
    .q4        ( Net__UAA423_Pad13_       ) /* pin 22*/ ,
    .q5        ( Net__UAA423_Pad14_       ) /* pin 23*/ ,
    .q7        ( Net__UAA423_Pad18_       ) /* pin 24*/ ,
    .anb       ( Net__UBD415_Pad25_       ) /* pin 25*/ 
);

OUTTRI UBD444(
    .i1        ( _xpd_out_OUT6            ) /* pin 1*/ ,
    .i2        ( Net__UAA162_Pad1_        ) /* pin 2*/ ,
    .t3        ( _APD6                    ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBD449;UBD469) 
) UBD451(
    .clk       ( Net__UAA423_Pad11_       ) /* pin 1*/ ,
    .d         ( _bus_signal_control_LUTPRO ) /* pin 2*/ ,
    .q         ( Net__UBD451_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UBD451_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBD456(
    .clk       ( Net__UAA423_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UBD451_Pad3_        ) /* pin 2*/ ,
    .q         ( _xpd_out_~OE             ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UBD456_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBD460(
    .clk       ( Net__UAA423_Pad11_       ) /* pin 1*/ ,
    .d         ( _bus_signal_control_GFX_OUT_LMSB ) /* pin 2*/ ,
    .q         ( Net__UBD460_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UBD460_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBD464(
    .clk       ( Net__UAA423_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UBD460_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBD415_Pad25_       ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UBD464_Pad5_        ) /* pin 5*/ 
);

BUF12 UBD471(
    .i         ( _bus_signal_control_GFX_CHNL ) /* pin 1*/ ,
    .o         ( _bus_signal_control_~CK500 ) /* pin 2*/ 
);

BUF12 UBD474(
    .i         ( Net__UBD415_Pad25_       ) /* pin 1*/ ,
    .o         ( Net__UBD474_Pad2_        ) /* pin 2*/ 
);

DFGOO #(.MiscRef(UBD481;UBD513) 
) UBD484(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD342_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBD490(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD346_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBD495(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD351_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA2        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBD501(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD355_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA3        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBD507(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBD360_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA4        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

NBUF03 UBD516(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _main_signals_~CLK8M     ) /* pin 2*/ 
);

BUF12 UBD521(
    .i         ( Net__UBD474_Pad2_        ) /* pin 1*/ ,
    .o         ( _bus_signal_control_~CK10 ) /* pin 2*/ 
);

M125CX6 #(.MiscRef(UBE2;UBE3;UBE5;UBE7;UBE8) 
) UBE0(
    .oe_n      ( _obj_manager_~DEBUG2     ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 7*/ ,
    .y5        ( _debug_~DEBUG5           ) /* pin 13*/ ,
    .y4        ( _debug_~DEBUG4           ) /* pin 14*/ ,
    .y3        ( _debug_~DEBUG3           ) /* pin 15*/ ,
    .y2        ( _debug_~DEBUG2           ) /* pin 16*/ ,
    .y1        ( _debug_~DEBUG1           ) /* pin 17*/ ,
    .y0        ( _debug_~DEBUG0           ) /* pin 18*/ 
);

DFFCOR UBE104(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA12       ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW1_S0 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE104_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBE108(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA13       ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW1_S1 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE108_Pad5_        ) /* pin 5*/ 
);

M273CG #(.MiscRef(UBE116;UBE118;UBE122;UBE123;UBE127;UBE129;UBE133;UBE135;UBE139;UBE140;UBE144;UBE146;UBE148) 
) UBE112(
    .g_n       ( _lut_assembly_lut_storage_~E7 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBE112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBE112_Pad19_       ) /* pin 19*/ 
);

M125CX6 #(.MiscRef(UBE10;UBE14;UBE16;UBE17;UBE19;UBE20) 
) UBE12(
    .oe_n      ( _obj_manager_~DEBUG2     ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ ,
    .y5        ( _debug_~DEBUG11          ) /* pin 13*/ ,
    .y4        ( _debug_~DEBUG10          ) /* pin 14*/ ,
    .y3        ( _debug_~DEBUG9           ) /* pin 15*/ ,
    .y2        ( _debug_~DEBUG8           ) /* pin 16*/ ,
    .y1        ( _debug_~DEBUG7           ) /* pin 17*/ ,
    .y0        ( _debug_~DEBUG6           ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBE154;UBE156;UBE160;UBE161;UBE165;UBE167;UBE171;UBE172;UBE176;UBE178;UBE182) 
) UBE150(
    .g_n       ( _lut_assembly_lut_storage_~E7 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBE150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBE150_Pad19_       ) /* pin 19*/ 
);

NOR02 UBE46(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_~LAYER_CTRL ) /* pin 2*/ ,
    .o         ( Net__UBE46_Pad3_         ) /* pin 3*/ 
);

NAND02 UBE48(
    .i1        ( _~CLK4M                  ) /* pin 1*/ ,
    .i2        ( Net__UBE46_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UBE104_Pad4_        ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBE81;UBE84) 
) UBE50(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _lut_assembly_layer_control_BUS1 ) /* pin 2*/ ,
    .q         ( Net__UBE50_Pad3_         ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE50_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE55(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _lut_assembly_layer_control_BUS2 ) /* pin 2*/ ,
    .q         ( Net__UBE55_Pad3_         ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE55_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE59(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _lut_assembly_layer_control_BUS3 ) /* pin 2*/ ,
    .q         ( Net__UBE59_Pad3_         ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE59_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE64(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _lut_assembly_layer_control_BUS4 ) /* pin 2*/ ,
    .q         ( Net__UBE64_Pad3_         ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE64_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE68(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _lut_assembly_layer_control_BUS5 ) /* pin 2*/ ,
    .q         ( Net__UBE68_Pad3_         ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE68_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE72(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA6        ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW4_S0 ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE72_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE77(
    .clk       ( Net__UBE104_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA7        ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW4_S1 ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UBE77_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE86(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA8        ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW3_S0 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE86_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE90(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA9        ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW3_S1 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE90_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE95(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA10       ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW2_S0 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE95_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBE99(
    .clk       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA11       ) /* pin 2*/ ,
    .q         ( _lut_assembly_layer_control_ROW2_S1 ) /* pin 3*/ ,
    .i4        ( Net__UBE104_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBE99_Pad5_         ) /* pin 5*/ 
);

_8BUF11 #(.MiscRef(UBF103;UBF104;UBF106;UBF107;UBF109;UBF111) 
) UBF101(
    .a0        ( _lut_assembly_lut_storage_ROW6_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_lut_storage_ROW6_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_lut_storage_ROW6_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_lut_storage_ROW6_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_lut_storage_ROW6_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_lut_storage_ROW6_D5 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_lut_storage_ROW6_D6 ) /* pin 8*/ ,
    .y7        ( Net__UBF101_Pad11_       ) /* pin 11*/ ,
    .y6        ( Net__UBF101_Pad12_       ) /* pin 12*/ ,
    .y5        ( Net__UBF101_Pad13_       ) /* pin 13*/ ,
    .y4        ( Net__UBF101_Pad14_       ) /* pin 14*/ ,
    .y3        ( Net__UBF101_Pad15_       ) /* pin 15*/ ,
    .y2        ( Net__UBF101_Pad16_       ) /* pin 16*/ ,
    .y1        ( Net__UBF101_Pad17_       ) /* pin 17*/ ,
    .y0        ( Net__UBF101_Pad18_       ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBF116;UBF118;UBF122;UBF123;UBF127;UBF129;UBF133;UBF135;UBF139;UBF140;UBF144;UBF146;UBF148) 
) UBF112(
    .g_n       ( _lut_assembly_lut_storage_~E6 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( Net__UBF101_Pad18_       ) /* pin 3*/ ,
    .d1        ( Net__UBF101_Pad17_       ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( Net__UBF101_Pad16_       ) /* pin 7*/ ,
    .d3        ( Net__UBF101_Pad15_       ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( Net__UBF101_Pad14_       ) /* pin 13*/ ,
    .d5        ( Net__UBF101_Pad13_       ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBF112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBF112_Pad19_       ) /* pin 19*/ 
);

INV01 UBF12(
    .i         ( Net__UBE64_Pad3_         ) /* pin 1*/ ,
    .o         ( _stars_~LAYERS_E3        ) /* pin 2*/ 
);

OR02 UBF13(
    .i1        ( _stars_~LAYERS_E3        ) /* pin 1*/ ,
    .i2        ( Net__UBF13_Pad2_         ) /* pin 2*/ ,
    .o         ( _lut_assembly_~STARS1_E  ) /* pin 3*/ 
);

M273CG #(.MiscRef(UBF154;UBF156;UBF160;UBF161;UBF165;UBF167;UBF171;UBF172;UBF176;UBF178;UBF182) 
) UBF150(
    .g_n       ( _lut_assembly_lut_storage_~E6 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( Net__UBF101_Pad12_       ) /* pin 3*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBF150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBF150_Pad19_       ) /* pin 19*/ 
);

INV01 UBF16(
    .i         ( Net__UBE68_Pad3_         ) /* pin 1*/ ,
    .o         ( _stars_~LAYERS_E4        ) /* pin 2*/ 
);

OR02 UBF17(
    .i1        ( _stars_~LAYERS_E4        ) /* pin 1*/ ,
    .i2        ( Net__UBD192_Pad9_        ) /* pin 2*/ ,
    .o         ( _lut_assembly_~STARS2_E  ) /* pin 3*/ 
);

D24L UBF19(
    .sa        ( _lut_assembly_layer_control_ROW4_S0 ) /* pin 1*/ ,
    .sb        ( _lut_assembly_layer_control_ROW4_S1 ) /* pin 2*/ ,
    .o3_n      ( _lut_assembly_layer_draw_order_~R4C4 ) /* pin 3*/ ,
    .o2_n      ( _lut_assembly_layer_draw_order_~R4C3 ) /* pin 4*/ ,
    .o1_n      ( _lut_assembly_layer_draw_order_~R4C2 ) /* pin 5*/ ,
    .o0_n      ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 6*/ 
);

D24L UBF26(
    .sa        ( _lut_assembly_layer_control_ROW3_S0 ) /* pin 1*/ ,
    .sb        ( _lut_assembly_layer_control_ROW3_S1 ) /* pin 2*/ ,
    .o3_n      ( _lut_assembly_layer_draw_order_~R3C4 ) /* pin 3*/ ,
    .o2_n      ( _lut_assembly_layer_draw_order_~R3C3 ) /* pin 4*/ ,
    .o1_n      ( _lut_assembly_layer_draw_order_~R3C2 ) /* pin 5*/ ,
    .o0_n      ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 6*/ 
);

D24L UBF33(
    .sa        ( _lut_assembly_layer_control_ROW2_S0 ) /* pin 1*/ ,
    .sb        ( _lut_assembly_layer_control_ROW2_S1 ) /* pin 2*/ ,
    .o3_n      ( _lut_assembly_layer_draw_order_~R2C4 ) /* pin 3*/ ,
    .o2_n      ( _lut_assembly_layer_draw_order_~R2C3 ) /* pin 4*/ ,
    .o1_n      ( _lut_assembly_layer_draw_order_~R2C2 ) /* pin 5*/ ,
    .o0_n      ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 6*/ 
);

D24L UBF40(
    .sa        ( _lut_assembly_layer_control_ROW1_S0 ) /* pin 1*/ ,
    .sb        ( _lut_assembly_layer_control_ROW1_S1 ) /* pin 2*/ ,
    .o3_n      ( _lut_assembly_layer_draw_order_~R1C4 ) /* pin 3*/ ,
    .o2_n      ( _lut_assembly_layer_draw_order_~R1C3 ) /* pin 4*/ ,
    .o1_n      ( _lut_assembly_layer_draw_order_~R1C2 ) /* pin 5*/ ,
    .o0_n      ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 6*/ 
);

M273C UBF48(
    .q0        ( Net__UBF48_Pad2_         ) /* pin 2*/ ,
    .d1        ( Net__UBF48_Pad4_         ) /* pin 4*/ ,
    .q1        ( _lut_assembly_lut_storage_~E1 ) /* pin 5*/ ,
    .q2        ( _lut_assembly_lut_storage_~E2 ) /* pin 6*/ ,
    .d2        ( Net__UBF48_Pad7_         ) /* pin 7*/ ,
    .d3        ( Net__UBF48_Pad8_         ) /* pin 8*/ ,
    .q3        ( _lut_assembly_lut_storage_~E3 ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_assembly_lut_storage_~E4 ) /* pin 12*/ ,
    .d4        ( Net__UBF48_Pad13_        ) /* pin 13*/ ,
    .d5        ( Net__UBF48_Pad14_        ) /* pin 14*/ ,
    .q5        ( _lut_assembly_lut_storage_~E5 ) /* pin 15*/ ,
    .q6        ( _lut_assembly_lut_storage_~E6 ) /* pin 16*/ ,
    .d6        ( Net__UBF48_Pad17_        ) /* pin 17*/ ,
    .d7        ( Net__UBF48_Pad18_        ) /* pin 18*/ ,
    .q7        ( _lut_assembly_lut_storage_~E7 ) /* pin 19*/ 
);

INV01 UBF88(
    .i         ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 1*/ ,
    .o         ( Net__UBF88_Pad2_         ) /* pin 2*/ 
);

NAND04 UBF89(
    .i1        ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 3*/ ,
    .i4        ( Net__UBF88_Pad2_         ) /* pin 4*/ ,
    .o         ( _lut_assembly_priority_transparency_~SPRITE_R1 ) /* pin 5*/ 
);

INV01 UBF91(
    .i         ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 1*/ ,
    .o         ( Net__UBF91_Pad2_         ) /* pin 2*/ 
);

NAND04 UBF92(
    .i1        ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 3*/ ,
    .i4        ( Net__UBF91_Pad2_         ) /* pin 4*/ ,
    .o         ( _lut_assembly_priority_transparency_~SPRITE_R2 ) /* pin 5*/ 
);

INV01 UBF94(
    .i         ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 1*/ ,
    .o         ( Net__UBF94_Pad2_         ) /* pin 2*/ 
);

NAND04 UBF95(
    .i1        ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 3*/ ,
    .i4        ( Net__UBF94_Pad2_         ) /* pin 4*/ ,
    .o         ( _lut_assembly_priority_transparency_~SPRITE_R3 ) /* pin 5*/ 
);

INV01 UBF98(
    .i         ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .o         ( Net__UBF98_Pad2_         ) /* pin 2*/ 
);

NAND04 UBF99(
    .i1        ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 3*/ ,
    .i4        ( Net__UBF98_Pad2_         ) /* pin 4*/ ,
    .o         ( _lut_assembly_priority_transparency_~SPRITE_R4 ) /* pin 5*/ 
);

_8BUF11 #(.MiscRef(UBH103;UBH104;UBH106;UBH107;UBH109;UBH111) 
) UBH101(
    .a0        ( _lut_assembly_lut_storage_ROW5_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_lut_storage_ROW5_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_lut_storage_ROW5_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_lut_storage_ROW5_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_lut_storage_ROW5_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_lut_storage_ROW5_D5 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_lut_storage_ROW5_D6 ) /* pin 8*/ ,
    .y7        ( Net__UBH101_Pad11_       ) /* pin 11*/ ,
    .y6        ( Net__UBH101_Pad12_       ) /* pin 12*/ ,
    .y5        ( Net__UBH101_Pad13_       ) /* pin 13*/ ,
    .y4        ( Net__UBH101_Pad14_       ) /* pin 14*/ ,
    .y3        ( Net__UBH101_Pad15_       ) /* pin 15*/ ,
    .y2        ( Net__UBH101_Pad16_       ) /* pin 16*/ ,
    .y1        ( Net__UBH101_Pad17_       ) /* pin 17*/ ,
    .y0        ( Net__UBH101_Pad18_       ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBH116;UBH118;UBH122;UBH123;UBH127;UBH129;UBH133;UBH135;UBH139;UBH140;UBH144;UBH146;UBH148) 
) UBH112(
    .g_n       ( _lut_assembly_lut_storage_~E5 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( Net__UBH101_Pad18_       ) /* pin 3*/ ,
    .d1        ( Net__UBH101_Pad17_       ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( Net__UBH101_Pad16_       ) /* pin 7*/ ,
    .d3        ( Net__UBH101_Pad15_       ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( Net__UBH101_Pad14_       ) /* pin 13*/ ,
    .d5        ( Net__UBH101_Pad13_       ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBH112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBH112_Pad19_       ) /* pin 19*/ 
);

M273CG #(.MiscRef(UBH154;UBH156;UBH160;UBH161;UBH165;UBH167;UBH171;UBH172;UBH176;UBH178;UBH182) 
) UBH150(
    .g_n       ( _lut_assembly_lut_storage_~E5 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( Net__UBH101_Pad12_       ) /* pin 3*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBH150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBH150_Pad19_       ) /* pin 19*/ 
);

INV01 UBH16(
    .i         ( Net__UBE50_Pad3_         ) /* pin 1*/ ,
    .o         ( _tilemaps_~LAYERS_E0     ) /* pin 2*/ 
);

OR02 UBH17(
    .i1        ( _tilemaps_~LAYERS_E0     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col2_assemble_GFX_A ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL2_D0 ) /* pin 3*/ 
);

OR02 UBH19(
    .i1        ( _tilemaps_~LAYERS_E0     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col2_assemble_GFX_B ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL2_D1 ) /* pin 3*/ 
);

BUF12 UBH193(
    .i         ( _obj_manager_~DEBUG2     ) /* pin 1*/ ,
    .o         ( __TD2                    ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBH195) 
) UBH196(
    .clk       ( Net__UBH196_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD_OUT5 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW6_D4 ) /* pin 3*/ ,
    .q_n       ( Net__UBH196_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH200(
    .clk       ( Net__UBH196_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD_OUT6 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW6_D5 ) /* pin 3*/ ,
    .q_n       ( Net__UBH200_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH204(
    .clk       ( Net__UBH196_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD_OUT7 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW6_D6 ) /* pin 3*/ ,
    .q_n       ( Net__UBH204_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH208(
    .clk       ( Net__UBH196_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD_OUT8 ) /* pin 2*/ ,
    .q         ( Net__UBH208_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBH208_Pad5_        ) /* pin 5*/ 
);

BUF12 UBH217(
    .i         ( _debug_td_io_IN9         ) /* pin 1*/ ,
    .o         ( __TD9                    ) /* pin 2*/ 
);

BUF12 UBH219(
    .i         ( _debug_td_io_IN3         ) /* pin 1*/ ,
    .o         ( __TD3                    ) /* pin 2*/ 
);

OR02 UBH22(
    .i1        ( _tilemaps_~LAYERS_E0     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col2_assemble_GFX_C ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL2_D2 ) /* pin 3*/ 
);

INV01 UBH221(
    .i         ( Net__UBD222_Pad12_       ) /* pin 1*/ ,
    .o         ( Net__UBH221_Pad2_        ) /* pin 2*/ 
);

NAND05 UBH222(
    .i0        ( Net__UBD222_Pad9_        ) /* pin 1*/ ,
    .i1        ( Net__UBD222_Pad6_        ) /* pin 2*/ ,
    .i2        ( Net__UBD222_Pad5_        ) /* pin 3*/ ,
    .i3        ( Net__UBD222_Pad2_        ) /* pin 4*/ ,
    .i4        ( Net__UBH221_Pad2_        ) /* pin 5*/ ,
    .o         ( _stars_xpd_bus6_sync_PD8 ) /* pin 6*/ 
);

DFFCOO #(.MiscRef(UBH226) 
) UBH227(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD5 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_sync_PD_OUT5 ) /* pin 3*/ ,
    .q_n       ( Net__UBH227_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH231(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD6 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_sync_PD_OUT6 ) /* pin 3*/ ,
    .q_n       ( Net__UBH231_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH235(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD7 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_sync_PD_OUT7 ) /* pin 3*/ ,
    .q_n       ( Net__UBH235_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBH239(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD8 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_sync_PD_OUT8 ) /* pin 3*/ ,
    .q_n       ( Net__UBH239_Pad5_        ) /* pin 5*/ 
);

OR02 UBH24(
    .i1        ( _tilemaps_~LAYERS_E0     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col2_assemble_GFX_D ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL2_D3 ) /* pin 3*/ 
);

MUX24H #(.MiscRef(UBH244;UBH248;UBH251;UBH254) 
) UBH243(
    .a3        ( _stars_row5_assemble_CNT0_Q3 ) /* pin 1*/ ,
    .a2        ( _stars_row5_assemble_CNT0_Q2 ) /* pin 2*/ ,
    .a1        ( _stars_row5_assemble_CNT0_Q1 ) /* pin 3*/ ,
    .a0        ( _stars_row5_assemble_CNT0_Q0 ) /* pin 4*/ ,
    .q0        ( _lut_assembly_lut_storage_ROW5_D0 ) /* pin 5*/ ,
    .q1        ( _lut_assembly_lut_storage_ROW5_D1 ) /* pin 6*/ ,
    .q2        ( _lut_assembly_lut_storage_ROW5_D2 ) /* pin 7*/ ,
    .q3        ( _lut_assembly_lut_storage_ROW5_D3 ) /* pin 8*/ ,
    .anb       ( _lut_assembly_lut_storage_ROW5_D6 ) /* pin 9*/ ,
    .b3        ( _stars_row5_assemble_CNT1_Q3 ) /* pin 10*/ ,
    .b2        ( _stars_row5_assemble_CNT1_Q2 ) /* pin 11*/ ,
    .b1        ( _stars_row5_assemble_CNT1_Q1 ) /* pin 12*/ ,
    .b0        ( _stars_row5_assemble_CNT1_Q0 ) /* pin 13*/ 
);

DFFCOO #(.MiscRef(UBH258) 
) UBH259(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBH259_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBH259_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT0_Q0 ) /* pin 5*/ 
);

DFFCOO UBH263(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBH263_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBH263_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT0_Q1 ) /* pin 5*/ 
);

DFFCOO UBH267(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBH267_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBH267_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT0_Q2 ) /* pin 5*/ 
);

DFFCOO UBH271(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBH271_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBH271_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row5_assemble_CNT0_Q3 ) /* pin 5*/ 
);

NAND02 UBH275(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBH275_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH259_Pad2_        ) /* pin 3*/ 
);

NAND02 UBH277(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBH277_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH263_Pad2_        ) /* pin 3*/ 
);

NAND02 UBH278(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBH278_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH267_Pad2_        ) /* pin 3*/ 
);

NAND02 UBH280(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBH280_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH271_Pad2_        ) /* pin 3*/ 
);

XOR02 UBH282(
    .i1        ( _stars_row5_assemble_CNT0_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBH282_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH275_Pad2_        ) /* pin 3*/ 
);

XNOR02 UBH291(
    .Unknown_pin( _stars_row5_assemble_CNT0_Q3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBH291_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH280_Pad2_        ) /* pin 3*/ 
);

NAND04 UBH294(
    .i1        ( _stars_row5_assemble_CNT0_Q2 ) /* pin 1*/ ,
    .i2        ( _stars_row5_assemble_CNT0_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row5_assemble_CNT0_Q0 ) /* pin 3*/ ,
    .i4        ( Net__UBH282_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UBH291_Pad2_        ) /* pin 5*/ 
);

XNOR02 UBH297(
    .Unknown_pin( _stars_row5_assemble_CNT0_Q2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBH297_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH278_Pad2_        ) /* pin 3*/ 
);

XNOR02 UBH299(
    .Unknown_pin( _stars_row5_assemble_CNT0_Q1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBH299_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH277_Pad2_        ) /* pin 3*/ 
);

NAND03 UBH302(
    .i1        ( _stars_row5_assemble_CNT0_Q1 ) /* pin 1*/ ,
    .i2        ( _stars_row5_assemble_CNT0_Q0 ) /* pin 2*/ ,
    .i3        ( Net__UBH282_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UBH297_Pad2_        ) /* pin 4*/ 
);

NAND02 UBH304(
    .i1        ( _stars_row5_assemble_CNT0_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBH282_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBH299_Pad2_        ) /* pin 3*/ 
);

BUF12 UBH306(
    .i         ( _stars_row5_counter_IC   ) /* pin 1*/ ,
    .o         ( Net__UBH282_Pad2_        ) /* pin 2*/ 
);

M367C UBH308(
    .oe_n      ( _stars_row6_assemble_~DEBUG ) /* pin 1*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

XNOR02 UBH318(
    .Unknown_pin( Net__UBD342_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD346_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH318_Pad3_        ) /* pin 3*/ 
);

OR02 UBH320(
    .i1        ( Net__UBD346_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBD342_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBH320_Pad3_        ) /* pin 3*/ 
);

AND04 UBH328(
    .i1        ( _video_signals_video_counter_COL4 ) /* pin 1*/ ,
    .i2        ( Net__UBD328_Pad1_        ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_COL1 ) /* pin 3*/ ,
    .i4        ( Net__UBD328_Pad3_        ) /* pin 4*/ ,
    .o         ( _obj_manager_SERIAL_LOAD ) /* pin 5*/ 
);

NBUF02 UBH331(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UBH331_Pad2_        ) /* pin 2*/ 
);

XNOR02 UBH333(
    .Unknown_pin( Net__UBD351_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBH320_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH333_Pad3_        ) /* pin 3*/ 
);

INV01 UBH335(
    .i         ( Net__UBD342_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UBH335_Pad2_        ) /* pin 2*/ 
);

MUX25H #(.MiscRef(UBH339;UBH343;UBH346;UBH349;UBH353) 
) UBH336(
    .a3        ( Net__UBH336_Pad1_        ) /* pin 1*/ ,
    .a4        ( Net__UBH336_Pad2_        ) /* pin 2*/ ,
    .a2        ( Net__UBH336_Pad3_        ) /* pin 3*/ ,
    .a1        ( Net__UBH336_Pad4_        ) /* pin 4*/ ,
    .a0        ( Net__UBH336_Pad5_        ) /* pin 5*/ ,
    .q0        ( Net__UBD342_Pad2_        ) /* pin 6*/ ,
    .q1        ( Net__UBD346_Pad2_        ) /* pin 7*/ ,
    .q2        ( Net__UBD351_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UBD355_Pad2_        ) /* pin 9*/ ,
    .q4        ( Net__UBD360_Pad2_        ) /* pin 10*/ ,
    .anb       ( Net__UBH336_Pad11_       ) /* pin 11*/ ,
    .b3        ( Net__UBD400_Pad3_        ) /* pin 12*/ ,
    .b4        ( Net__UBH336_Pad13_       ) /* pin 13*/ ,
    .b2        ( Net__UBH333_Pad3_        ) /* pin 14*/ ,
    .b1        ( Net__UBH318_Pad3_        ) /* pin 15*/ ,
    .b0        ( Net__UBH335_Pad2_        ) /* pin 16*/ 
);

M367C UBH356(
    .oe_n      ( _debug_~DEBUG0           ) /* pin 1*/ ,
    .a0        ( _video_signals_video_counter_COL2 ) /* pin 2*/ ,
    .a1        ( _video_signals_video_counter_COL1 ) /* pin 3*/ ,
    .a2        ( _~CLK4M                  ) /* pin 4*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

NAND05 UBH366(
    .i0        ( _video_signals_video_counter_COL4 ) /* pin 1*/ ,
    .i1        ( Net__UBH366_Pad2_        ) /* pin 2*/ ,
    .i2        ( _video_signals_video_counter_COL2 ) /* pin 3*/ ,
    .i3        ( Net__UBH366_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UBD328_Pad3_        ) /* pin 5*/ ,
    .o         ( Net__UBH366_Pad6_        ) /* pin 6*/ 
);

DFFCOO #(.MiscRef(UBH369) 
) UBH371(
    .clk       ( Net__UAF386_Pad36_       ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q         ( Net__UBH371_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBH371_Pad5_        ) /* pin 5*/ 
);

INV01 UBH375(
    .i         ( _video_signals_video_counter_COL1 ) /* pin 1*/ ,
    .o         ( Net__UBH366_Pad4_        ) /* pin 2*/ 
);

INV01 UBH377(
    .i         ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .o         ( Net__UBH366_Pad2_        ) /* pin 2*/ 
);

INV01 UBH38(
    .i         ( Net__UBE55_Pad3_         ) /* pin 1*/ ,
    .o         ( _tilemaps_~LAYERS_E1     ) /* pin 2*/ 
);

NAND05 UBH383(
    .i0        ( Net__UBH383_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UBH366_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UBD328_Pad1_        ) /* pin 3*/ ,
    .i3        ( _video_signals_video_counter_COL1 ) /* pin 4*/ ,
    .i4        ( Net__UBD328_Pad3_        ) /* pin 5*/ ,
    .o         ( Net__UBH383_Pad6_        ) /* pin 6*/ 
);

DFFCOO #(.MiscRef(UBH387) 
) UBH388(
    .clk       ( Net__UAF386_Pad35_       ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q         ( Net__UBH388_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBH388_Pad5_        ) /* pin 5*/ 
);

OR02 UBH39(
    .i1        ( _tilemaps_~LAYERS_E1     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col3_assemble_GFX_A ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL3_D0 ) /* pin 3*/ 
);

AND04 UBH393(
    .i1        ( Net__UBH383_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBH366_Pad2_        ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_COL2 ) /* pin 3*/ ,
    .i4        ( Net__UBD328_Pad3_        ) /* pin 4*/ ,
    .o         ( _obj_manager_LOAD_XPD4   ) /* pin 5*/ 
);

D38GL UBH396(
    .sa        ( _TI0                     ) /* pin 1*/ ,
    .sb        ( _TI1                     ) /* pin 2*/ ,
    .sc        ( _TI2                     ) /* pin 3*/ ,
    .g1        ( Net__UBH396_Pad4_        ) /* pin 4*/ ,
    .o7_n      ( _debug_~DEBUG7           ) /* pin 7*/ ,
    .o6_n      ( _debug_~DEBUG6           ) /* pin 9*/ ,
    .o5_n      ( _debug_~DEBUG5           ) /* pin 10*/ ,
    .o4_n      ( _debug_~DEBUG4           ) /* pin 11*/ ,
    .o3_n      ( _debug_~DEBUG3           ) /* pin 12*/ ,
    .o2_n      ( _debug_~DEBUG2           ) /* pin 13*/ ,
    .o1_n      ( _debug_~DEBUG1           ) /* pin 14*/ ,
    .o0_n      ( _debug_~DEBUG0           ) /* pin 15*/ 
);

OR02 UBH42(
    .i1        ( _tilemaps_~LAYERS_E1     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col3_assemble_GFX_B ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL3_D1 ) /* pin 3*/ 
);

INV01 UBH423(
    .i         ( _TI3                     ) /* pin 1*/ ,
    .o         ( Net__UBH396_Pad4_        ) /* pin 2*/ 
);

XNOR02 UBH424(
    .Unknown_pin( Net__UBD360_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD411_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH336_Pad13_       ) /* pin 3*/ 
);

BUF12 UBH427(
    .i         ( Net__UBH427_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBH427_Pad2_        ) /* pin 2*/ 
);

BUF12 UBH428(
    .i         ( Net__UBH427_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBH428_Pad2_        ) /* pin 2*/ 
);

NAND05 UBH430(
    .i0        ( _video_signals_video_counter_COL4 ) /* pin 1*/ ,
    .i1        ( Net__UBH366_Pad2_        ) /* pin 2*/ ,
    .i2        ( _video_signals_video_counter_COL2 ) /* pin 3*/ ,
    .i3        ( _video_signals_video_counter_COL1 ) /* pin 4*/ ,
    .i4        ( Net__UBD328_Pad3_        ) /* pin 5*/ ,
    .o         ( Net__UBH430_Pad6_        ) /* pin 6*/ 
);

DFFCOR #(.MiscRef(UBH433;UBH458) 
) UBH436(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBH436_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAA482_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBH436_Pad5_        ) /* pin 5*/ 
);

OR02 UBH44(
    .i1        ( _tilemaps_~LAYERS_E1     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col3_assemble_GFX_C ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL3_D2 ) /* pin 3*/ 
);

DFFCOR UBH441(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBH441_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAA506_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBH441_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBH445(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBH445_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAA485_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBH445_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBH450(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBH450_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UAA511_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBH450_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBH454(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBH454_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBH454_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBH454_Pad5_        ) /* pin 5*/ 
);

OR02 UBH46(
    .i1        ( _tilemaps_~LAYERS_E1     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col3_assemble_GFX_D ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL3_D3 ) /* pin 3*/ 
);

XOR02 UBH463(
    .i1        ( Net__UBH371_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBH388_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBH463_Pad3_        ) /* pin 3*/ 
);

NAND02 UBH474(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_STARS_LOAD2       ) /* pin 2*/ ,
    .o         ( Net__UBD222_Pad11_       ) /* pin 3*/ 
);

INV01 UBH476(
    .i         ( Net__UBH430_Pad6_        ) /* pin 1*/ ,
    .o         ( _stars_STARS_LOAD2       ) /* pin 2*/ 
);

NOR05 UBH481(
    .o1        ( Net__UBH481_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD342_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBD346_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBD355_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBD351_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UBD360_Pad3_        ) /* pin 6*/ 
);

NAND02 UBH485(
    .i1        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBD484_Pad1_        ) /* pin 3*/ 
);

BUF12 UBH490(
    .i         ( _main_signals_~RESETI    ) /* pin 1*/ ,
    .o         ( _~RESET                  ) /* pin 2*/ 
);

BUF12 UBH492(
    .i         ( _main_signals_~RESETI    ) /* pin 1*/ ,
    .o         ( _~RESET                  ) /* pin 2*/ 
);

BUF12 UBH500(
    .i         ( Net__UBH428_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT10                   ) /* pin 2*/ 
);

BUF12 UBH503(
    .i         ( Net__UBH503_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBH503_Pad2_        ) /* pin 2*/ 
);

BUF12 UBH505(
    .i         ( Net__UBH505_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBH505_Pad2_        ) /* pin 2*/ 
);

BUF12 UBH512(
    .i         ( Net__UBH505_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBH512_Pad2_        ) /* pin 2*/ 
);

BUF12 UBH519(
    .i         ( Net__UBH503_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT0                    ) /* pin 2*/ 
);

INV01 UBH60(
    .i         ( Net__UBE59_Pad3_         ) /* pin 1*/ ,
    .o         ( _tilemaps_~LAYERS_E2     ) /* pin 2*/ 
);

OR02 UBH62(
    .i1        ( _tilemaps_~LAYERS_E2     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col4_assemble_GFX_A ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL4_D0 ) /* pin 3*/ 
);

OR02 UBH64(
    .i1        ( _tilemaps_~LAYERS_E2     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col4_assemble_GFX_B ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL4_D1 ) /* pin 3*/ 
);

OR02 UBH66(
    .i1        ( _tilemaps_~LAYERS_E2     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col4_assemble_GFX_C ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL4_D2 ) /* pin 3*/ 
);

OR02 UBH68(
    .i1        ( _tilemaps_~LAYERS_E2     ) /* pin 1*/ ,
    .i2        ( _tilemaps_col4_assemble_GFX_D ) /* pin 2*/ ,
    .o         ( _lut_assembly_layer_draw_order_COL4_D3 ) /* pin 3*/ 
);

PRIO UBH70(
    .x0_n      ( Net__UBH70_Pad1_         ) /* pin 1*/ ,
    .x1_n      ( Net__UBH70_Pad2_         ) /* pin 2*/ ,
    .x2_n      ( Net__UBH70_Pad3_         ) /* pin 3*/ ,
    .x3_n      ( Net__UBH70_Pad4_         ) /* pin 4*/ ,
    .x4_n      ( _lut_assembly_~STARS1_E  ) /* pin 5*/ ,
    .x5_n      ( _lut_assembly_~STARS2_E  ) /* pin 6*/ ,
    .xe_n      ( _lut_assembly_BLANK      ) /* pin 7*/ ,
    .q0_n      ( Net__UBF48_Pad4_         ) /* pin 8*/ ,
    .q1_n      ( Net__UBF48_Pad7_         ) /* pin 9*/ ,
    .q2_n      ( Net__UBF48_Pad8_         ) /* pin 10*/ ,
    .q3_n      ( Net__UBF48_Pad13_        ) /* pin 11*/ ,
    .q4_n      ( Net__UBF48_Pad14_        ) /* pin 12*/ ,
    .q5_n      ( Net__UBF48_Pad17_        ) /* pin 13*/ ,
    .qx_n      ( Net__UBF48_Pad18_        ) /* pin 14*/ 
);

M125CX6 #(.MiscRef(UBI1;UBI2;UBI3;UBI4;UBI5;UBI6;UBI7;UBI8;UBI9;UBI10;UBI11;UBI13;UBI14;UBI15;UBI16;UBI17;UBI18;UBI19;UBI20;UBI22;UBI23;UBI24;UBI25;UBI26;UBI27;UBI28;UBI29;UBI30;UBI31;UBI32;UBI34;UBI35;UBI36;UBI37;UBI38;UBI39;UBI40;UBI41;UBI42;UBI44;UBI45;UBI46;UBI47;UBI48;UBI49;UBI50;UBI51;UBI52;UBI53;UBI54;UBI56;UBI57;UBI58;UBI59;UBI60;UBI61;UBI62;UBI63;UBI64;UBI66;UBI67;UBI68;UBI69;UBI70;UBI71;UBI72;UBI73;UBI74;UBI75;UBI76;UBI78;UBI79;UBI80;UBI81;UBI82;UBI83;UBI84;UBI85;UBI86;UBI88;UBI89;UBI90;UBI91;UBI92;UBI93;UBI94;UBI95;UBI96;UBI97;UBI98;UBI100;UBI101;UBI102;UBI103;UBI104;UBI105;UBI106;UBI107;UBK1;UBK2;UBK3;UBK4;UBK5;UBK6;UBK7;UBK8;UBK9;UBK10;UBK11;UBK13;UBK14;UBK15;UBK16;UBK17;UBK18;UBK19;UBK20;UBK22;UBK23;UBK24;UBK25;UBK26;UBK27;UBK28;UBK29;UBK30;UBK31;UBK32;UBK34;UBK35;UBK36;UBK37;UBK38;UBK39;UBK40;UBK41;UBK42;UBK44;UBK45;UBK46;UBK47;UBK48;UBK49;UBK50;UBK51;UBK52;UBK53;UBK54;UBK56;UBK57;UBK58;UBK59;UBK60;UBK61;UBK62;UBK63;UBK64;UBK66;UBK67;UBK68;UBK69;UBK70;UBK71;UBK72;UBK73;UBK74;UBK75;UBK76;UBK78;UBK79;UBK80;UBK81;UBK82;UBK83;UBK84;UBK85;UBK86;UBK88;UBK89;UBK90;UBK91;UBK92;UBK93;UBK94;UBK95;UBK96;UBK97;UBK98;UBK100;UBK101;UBK102;UBK103;UBK104;UBK105;UBK106;UBK107;UBM1;UBM2;UBM3;UBM4;UBM5;UBM6;UBM7;UBM8;UBM9;UBM10;UBM11;UBM13;UBM14;UBM15;UBM16;UBM17;UBM18;UBM19;UBM20;UBM22;UBM23;UBM24;UBM25;UBM26;UBM27;UBM28;UBM29;UBM30;UBM31;UBM32;UBM34;UBM35;UBM36;UBM37;UBM38;UBM39;UBM40;UBM41;UBM42;UBM44;UBM45;UBM46;UBM47;UBM48;UBM49;UBM50;UBM51;UBM52;UBM53;UBM54;UBM56;UBM57;UBM58;UBM59;UBM60;UBM61;UBM62;UBM63;UBM64;UBM66;UBM67;UBM68;UBM69;UBM70;UBM71;UBM72;UBM73;UBM74;UBM75;UBM76;UBM78;UBM79;UBM80;UBM81;UBM82;UBM83;UBM84;UBM85;UBM86;UBM88;UBM89;UBM90;UBM91;UBM92;UBM93;UBM94;UBM95;UBM96;UBM97;UBM98;UBM100;UBM101;UBM102;UBM103;UBM104;UBM105;UBM106;UBM107;UBN1;UBN2;UBN3;UBN4;UBN5;UBN6;UBN7;UBN8;UBN9;UBN10;UBN11;UBN13;UBN14;UBN15;UBN16;UBN17;UBN18;UBN19;UBN20;UBN22;UBN23;UBN24;UBN25;UBN26;UBN27;UBN28;UBN29;UBN30;UBN31;UBN32;UBN34;UBN35;UBN36;UBN37;UBN38;UBN39;UBN40;UBN41;UBN42;UBN44;UBN45;UBN46;UBN47;UBN48;UBN49;UBN50;UBN51;UBN52;UBN53;UBN54;UBN56;UBN57;UBN58;UBN59;UBN60;UBN61;UBN62;UBN63;UBN64;UBN66;UBN67;UBN68;UBN69;UBN70;UBN71;UBN72;UBN73;UBN74;UBN75;UBN76;UBN78;UBN79;UBN80;UBN81;UBN82;UBN83;UBN84;UBN85;UBN86;UBN88;UBN89;UBN90;UBN91;UBN92;UBN93;UBN94;UBN95;UBN96;UBN97;UBN98;UBN100;UBN101;UBN102;UBN103;UBN104;UBN105;UBN106;UBN107) 
) UBI0(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 18*/ 
);

NAND04 UBI108(
    .i1        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 4*/ ,
    .o         ( Net__UBI108_Pad5_        ) /* pin 5*/ 
);

OAI21 UBI110(
    .i1        ( Net__UBI108_Pad5_        ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_~SPRITE_R4 ) /* pin 2*/ ,
    .i3        ( Net__UBI110_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UBH70_Pad4_         ) /* pin 4*/ 
);

M273CG #(.MiscRef(UBI116;UBI118;UBI122;UBI124;UBI127;UBI129;UBI133;UBI135;UBI139;UBI140;UBI144;UBI146;UBI148) 
) UBI112(
    .g_n       ( _lut_assembly_lut_storage_~E4 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_draw_order_ROW4_D4 ) /* pin 13*/ ,
    .d5        ( _lut_assembly_layer_draw_order_ROW4_D5 ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBI112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBI112_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBI12(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D6 ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBI154;UBI156;UBI160;UBI161;UBI165;UBI167;UBI171;UBI172;UBI176;UBI178;UBI182) 
) UBI150(
    .g_n       ( _lut_assembly_lut_storage_~E4 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW4_D6 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW4_D7 ) /* pin 4*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW4_D8 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_control_ROW4_S0 ) /* pin 8*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_control_ROW4_S1 ) /* pin 13*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBI150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBI150_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBI21(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 18*/ 
);

M125CX6 UBI33(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D6 ) /* pin 18*/ 
);

M125CX6 UBI43(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 18*/ 
);

M125CX6 UBI55(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D6 ) /* pin 18*/ 
);

M125CX6 UBI65(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 18*/ 
);

M125CX6 UBI77(
    .oe_n      ( _lut_assembly_layer_draw_order_~R4C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW4_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW4_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW4_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW4_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW4_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW4_D6 ) /* pin 18*/ 
);

M125CX6 UBI87(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R4 ) /* pin 1*/ ,
    .y5        ( _lut_assembly_priority_transparency_OUT5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 18*/ 
);

M125CX6 UBI99(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R4 ) /* pin 1*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 6*/ ,
    .a5        ( Net__UBI99_Pad7_         ) /* pin 7*/ ,
    .y5        ( Net__UBI99_Pad13_        ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT6 ) /* pin 18*/ 
);

NAND04 UBIX158(
    .i1        ( _lut_assembly_priority_transparency_OUT10 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_OUT9 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_priority_transparency_OUT8 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_priority_transparency_OUT7 ) /* pin 4*/ ,
    .o         ( Net__UBIX158_Pad5_       ) /* pin 5*/ 
);

AND02 UBIX164(
    .i1        ( _lut_assembly_priority_transparency_OUT6 ) /* pin 1*/ ,
    .i2        ( Net__UBIX164_Pad2_       ) /* pin 2*/ ,
    .o         ( Net__UBIX164_Pad3_       ) /* pin 3*/ 
);

M125CX6 UBK0(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 18*/ 
);

NAND04 UBK108(
    .i1        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 4*/ ,
    .o         ( Net__UBK108_Pad5_        ) /* pin 5*/ 
);

OAI21 UBK110(
    .i1        ( Net__UBK108_Pad5_        ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_~SPRITE_R3 ) /* pin 2*/ ,
    .i3        ( Net__UBI110_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UBH70_Pad3_         ) /* pin 4*/ 
);

M273CG #(.MiscRef(UBK116;UBK118;UBK122;UBK124;UBK128;UBK129;UBK133;UBK135;UBK139;UBK140;UBK144;UBK146;UBK148) 
) UBK112(
    .g_n       ( _lut_assembly_lut_storage_~E3 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_draw_order_ROW3_D4 ) /* pin 13*/ ,
    .d5        ( _lut_assembly_layer_draw_order_ROW3_D5 ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBK112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBK112_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBK12(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D6 ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBK154;UBK156;UBK160;UBK161;UBK165;UBK167;UBK171;UBK172;UBK176;UBK178;UBK182) 
) UBK150(
    .g_n       ( _lut_assembly_lut_storage_~E3 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW3_D6 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW3_D7 ) /* pin 4*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW3_D8 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_control_ROW3_S0 ) /* pin 8*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_control_ROW3_S1 ) /* pin 13*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBK150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBK150_Pad19_       ) /* pin 19*/ 
);

BUF12 UBK193(
    .i         ( _debug_td_io_IN7         ) /* pin 1*/ ,
    .o         ( __TD7                    ) /* pin 2*/ 
);

MUX24H #(.MiscRef(UBK197;UBK200;UBK204;UBK207) 
) UBK195(
    .a3        ( _stars_row6_assemble_CNT0_Q3 ) /* pin 1*/ ,
    .a2        ( _stars_row6_assemble_CNT0_Q2 ) /* pin 2*/ ,
    .a1        ( _stars_row6_assemble_CNT0_Q1 ) /* pin 3*/ ,
    .a0        ( _stars_row6_assemble_CNT0_Q0 ) /* pin 4*/ ,
    .q0        ( _lut_assembly_lut_storage_ROW6_D0 ) /* pin 5*/ ,
    .q1        ( _lut_assembly_lut_storage_ROW6_D1 ) /* pin 6*/ ,
    .q2        ( _lut_assembly_lut_storage_ROW6_D2 ) /* pin 7*/ ,
    .q3        ( _lut_assembly_lut_storage_ROW6_D3 ) /* pin 8*/ ,
    .anb       ( _lut_assembly_lut_storage_ROW6_D6 ) /* pin 9*/ ,
    .b3        ( _stars_row6_assemble_CNT1_Q3 ) /* pin 10*/ ,
    .b2        ( _stars_row6_assemble_CNT1_Q2 ) /* pin 11*/ ,
    .b1        ( _stars_row6_assemble_CNT1_Q1 ) /* pin 12*/ ,
    .b0        ( _stars_row6_assemble_CNT1_Q0 ) /* pin 13*/ 
);

M125CX6 UBK21(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 18*/ 
);

NAND02 UBK210(
    .i1        ( _stars_xpd6_sync_ZERO    ) /* pin 1*/ ,
    .i2        ( Net__UBH208_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBD192_Pad9_        ) /* pin 3*/ 
);

BUF12 UBK217(
    .i         ( _debug_td_io_IN10        ) /* pin 1*/ ,
    .o         ( __TD10                   ) /* pin 2*/ 
);

M541C UBK219(
    .oe_n      ( _stars_row5_assemble_~DEBUG ) /* pin 1*/ ,
    .a0        ( _lut_assembly_lut_storage_ROW5_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_lut_storage_ROW5_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_lut_storage_ROW5_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_lut_storage_ROW5_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_lut_storage_ROW5_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_lut_storage_ROW5_D5 ) /* pin 7*/ ,
    .a6        ( _lut_assembly_lut_storage_ROW5_D6 ) /* pin 8*/ ,
    .a7        ( Net__UBF13_Pad2_         ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

BUF12 UBK238(
    .i         ( _stars_row6_counter_IC   ) /* pin 1*/ ,
    .o         ( Net__UBD272_Pad2_        ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBK240) 
) UBK242(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBK242_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK242_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT0_Q0 ) /* pin 5*/ 
);

DFFCOO UBK245(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBK245_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK245_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT0_Q1 ) /* pin 5*/ 
);

DFFCOO UBK249(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBK249_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK249_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT0_Q2 ) /* pin 5*/ 
);

DFFCOO UBK253(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBK253_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK253_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT0_Q3 ) /* pin 5*/ 
);

XNOR02 UBK257(
    .Unknown_pin( _stars_row6_assemble_CNT0_Q3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBK257_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBK257_Pad3_        ) /* pin 3*/ 
);

NAND04 UBK260(
    .i1        ( _stars_row6_assemble_CNT0_Q2 ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_CNT0_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row6_assemble_CNT0_Q0 ) /* pin 3*/ ,
    .i4        ( Net__UBD272_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UBK257_Pad2_        ) /* pin 5*/ 
);

NAND03 UBK263(
    .i1        ( _stars_row6_assemble_CNT0_Q1 ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_CNT0_Q0 ) /* pin 2*/ ,
    .i3        ( Net__UBD272_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UBK263_Pad4_        ) /* pin 4*/ 
);

XNOR02 UBK265(
    .Unknown_pin( _stars_row6_assemble_CNT0_Q2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBK263_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBK265_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBK268(
    .Unknown_pin( _stars_row6_assemble_CNT0_Q1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBD272_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBK268_Pad3_        ) /* pin 3*/ 
);

NAND02 UBK270(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBK270_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBK242_Pad2_        ) /* pin 3*/ 
);

NAND02 UBK272(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBK268_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBK245_Pad2_        ) /* pin 3*/ 
);

NAND02 UBK274(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBK265_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBK249_Pad2_        ) /* pin 3*/ 
);

NAND02 UBK275(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UBK257_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBK253_Pad2_        ) /* pin 3*/ 
);

XOR02 UBK277(
    .i1        ( _stars_row6_assemble_CNT0_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBD272_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBK270_Pad2_        ) /* pin 3*/ 
);

OR02 UBK295(
    .i1        ( _stars_row5_counter_INC  ) /* pin 1*/ ,
    .i2        ( Net__UBK295_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_row5_counter_IC   ) /* pin 3*/ 
);

OR03 UBK298(
    .i1        ( Net__UBK298_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK298_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBK298_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBK298_Pad4_        ) /* pin 4*/ 
);

XNOR02 UBK301(
    .Unknown_pin( Net__UBK301_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBK298_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBK301_Pad3_        ) /* pin 3*/ 
);

NOR05 UBK304(
    .o1        ( Net__UBK304_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK298_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBK298_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBK301_Pad1_        ) /* pin 4*/ ,
    .i5        ( Net__UBK298_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UBK304_Pad6_        ) /* pin 6*/ 
);

INV01 UBK308(
    .i         ( Net__UBK304_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBK308_Pad2_        ) /* pin 2*/ 
);

OR02 UBK309(
    .i1        ( Net__UBK309_Pad1_        ) /* pin 1*/ ,
    .i2        ( _FI                      ) /* pin 2*/ ,
    .o         ( _raster_interrupts_counter1_SET_~INC ) /* pin 3*/ 
);

INV01 UBK311(
    .i         ( _debug_~DEBUG13          ) /* pin 1*/ ,
    .o         ( Net__UBK295_Pad2_        ) /* pin 2*/ 
);

OR04 UBK312(
    .i1        ( Net__UBK298_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK298_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBK298_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBK301_Pad1_        ) /* pin 4*/ ,
    .o         ( Net__UBK312_Pad5_        ) /* pin 5*/ 
);

DFFCOO #(.MiscRef(UBK315) 
) UBK317(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( _FI                      ) /* pin 2*/ ,
    .q         ( Net__UBK309_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UBK317_Pad5_        ) /* pin 5*/ 
);

INV01 UBK322(
    .i         ( Net__UBH366_Pad6_        ) /* pin 1*/ ,
    .o         ( _stars_STARS_LOAD1       ) /* pin 2*/ 
);

BUF12 UBK328(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( _~RESETI1                ) /* pin 2*/ 
);

M125CX6 UBK33(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D6 ) /* pin 18*/ 
);

INV01 UBK330(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UBD328_Pad3_        ) /* pin 2*/ 
);

M367C UBK331(
    .oe_n      ( _stars_row5_assemble_~DEBUG ) /* pin 1*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

DFGOO #(.MiscRef(UBK341;UBK373) 
) UBK344(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBK298_Pad1_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBK349(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBK298_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBK355(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBK298_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA2        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBK361(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBK301_Pad1_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA3        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBK367(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBK304_Pad6_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA4        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

OR02 UBK376(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _security_management_~MULT_X ) /* pin 2*/ ,
    .o         ( Net__UAF386_Pad35_       ) /* pin 3*/ 
);

INV01 UBK383(
    .i         ( _video_signals_video_counter_COL4 ) /* pin 1*/ ,
    .o         ( Net__UBH383_Pad1_        ) /* pin 2*/ 
);

D38GL UBK384(
    .sa        ( _TI0                     ) /* pin 1*/ ,
    .sb        ( _TI1                     ) /* pin 2*/ ,
    .sc        ( _TI2                     ) /* pin 3*/ ,
    .g1        ( _TI3                     ) /* pin 4*/ ,
    .o7_n      ( _debug_~DEBUG15          ) /* pin 7*/ ,
    .o6_n      ( _debug_~DEBUG14          ) /* pin 9*/ ,
    .o5_n      ( _debug_~DEBUG13          ) /* pin 10*/ ,
    .o4_n      ( _debug_~DEBUG12          ) /* pin 11*/ ,
    .o3_n      ( _debug_~DEBUG11          ) /* pin 12*/ ,
    .o2_n      ( _debug_~DEBUG10          ) /* pin 13*/ ,
    .o1_n      ( _debug_~DEBUG9           ) /* pin 14*/ ,
    .o0_n      ( _debug_~DEBUG8           ) /* pin 15*/ 
);

AND02 UBK411(
    .i1        ( _debug_~DEBUG7           ) /* pin 1*/ ,
    .i2        ( _debug_~DEBUG14          ) /* pin 2*/ ,
    .o         ( _stars_row6_assemble_~DEBUG ) /* pin 3*/ 
);

INV01 UBK414(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UBK414_Pad2_        ) /* pin 2*/ 
);

NAND03 UBK416(
    .i1        ( Net__UBK416_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK416_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBK414_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UBD407_Pad1_        ) /* pin 4*/ 
);

INV01 UBK419(
    .i         ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .o         ( Net__UBK416_Pad1_        ) /* pin 2*/ 
);

INV01 UBK420(
    .i         ( _video_signals_video_counter_COL2 ) /* pin 1*/ ,
    .o         ( Net__UBK416_Pad2_        ) /* pin 2*/ 
);

AND02 UBK428(
    .i1        ( _debug_~DEBUG6           ) /* pin 1*/ ,
    .i2        ( _debug_~DEBUG13          ) /* pin 2*/ ,
    .o         ( _stars_row5_assemble_~DEBUG ) /* pin 3*/ 
);

M125CX6 UBK43(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 18*/ 
);

BUF12 UBK433(
    .i         ( _video_signals_video_counter_COL2 ) /* pin 1*/ ,
    .o         ( _bus_signal_control_GFX_OUT_LMSB ) /* pin 2*/ 
);

BUF12 UBK435(
    .i         ( Net__UBH454_Pad3_        ) /* pin 1*/ ,
    .o         ( _lut_assembly_BLANK      ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UBK437;UBK444) 
) UBK439(
    .clk       ( Net__UAA423_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UBK439_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK439_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UBK439_Pad5_        ) /* pin 5*/ 
);

BUF12 UBK454(
    .i         ( Net__UBK454_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBH503_Pad1_        ) /* pin 2*/ 
);

M273C #(.MiscRef(UBK457;UBK464;UBK469;UBK473;UBK478;UBK482;UBK487;UBK491;UBK495) 
) UBK460(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UBK460_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA9        ) /* pin 4*/ ,
    .q1        ( Net__UBK460_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UBK460_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA10       ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA11       ) /* pin 8*/ ,
    .q3        ( Net__UBK460_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBK460_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UBK460_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .q5        ( Net__UBK460_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UBK460_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA14       ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA15       ) /* pin 18*/ ,
    .q7        ( Net__UBK460_Pad19_       ) /* pin 19*/ 
);

BUF12 UBK498(
    .i         ( Net__UBK498_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBK498_Pad2_        ) /* pin 2*/ 
);

M152C UBK499(
    .i3        ( Net__UBK460_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UBK460_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UBK460_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UBK460_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UBK499_Pad5_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UBK460_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UBK460_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UBK460_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UBK460_Pad12_       ) /* pin 15*/ 
);

BUF12 UBK521(
    .i         ( Net__UBK498_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT11                   ) /* pin 2*/ 
);

M125CX6 UBK55(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D6 ) /* pin 18*/ 
);

M125CX6 UBK65(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 18*/ 
);

M125CX6 UBK77(
    .oe_n      ( _lut_assembly_layer_draw_order_~R3C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW3_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW3_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW3_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW3_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW3_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW3_D6 ) /* pin 18*/ 
);

M125CX6 UBK87(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW4_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW4_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW4_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW4_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW4_D9 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_ROW4_D10 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_priority_transparency_OUT5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 18*/ 
);

M125CX6 UBK99(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW4_D11 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 6*/ ,
    .a5        ( Net__UBK99_Pad7_         ) /* pin 7*/ ,
    .y5        ( Net__UBK99_Pad13_        ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT6 ) /* pin 18*/ 
);

NAND04 UBKX158(
    .i1        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 4*/ ,
    .o         ( Net__UBIX164_Pad2_       ) /* pin 5*/ 
);

M125CX6 UBM0(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 18*/ 
);

NAND04 UBM108(
    .i1        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 4*/ ,
    .o         ( Net__UBM108_Pad5_        ) /* pin 5*/ 
);

OAI21 UBM110(
    .i1        ( Net__UBM108_Pad5_        ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_~SPRITE_R2 ) /* pin 2*/ ,
    .i3        ( Net__UBI110_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UBH70_Pad2_         ) /* pin 4*/ 
);

M273CG #(.MiscRef(UBM116;UBM118;UBM122;UBM124;UBM128;UBM129;UBM133;UBM135;UBM139;UBM140;UBM144;UBM146;UBM148) 
) UBM112(
    .g_n       ( _lut_assembly_lut_storage_~E2 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_draw_order_ROW2_D4 ) /* pin 13*/ ,
    .d5        ( _lut_assembly_layer_draw_order_ROW2_D5 ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBM112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBM112_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBM12(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D6 ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBM154;UBM156;UBM160;UBM161;UBM165;UBM167;UBM171;UBM172;UBM176;UBM178;UBM182) 
) UBM150(
    .g_n       ( _lut_assembly_lut_storage_~E2 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW2_D6 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW2_D7 ) /* pin 4*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW2_D8 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_control_ROW2_S0 ) /* pin 8*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_control_ROW2_S1 ) /* pin 13*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBM150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBM150_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBM21(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 18*/ 
);

M125CX6 UBM33(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D6 ) /* pin 18*/ 
);

M125CX6 UBM43(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 18*/ 
);

M125CX6 UBM55(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D6 ) /* pin 18*/ 
);

M125CX6 UBM65(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 18*/ 
);

M125CX6 UBM77(
    .oe_n      ( _lut_assembly_layer_draw_order_~R2C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW2_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW2_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW2_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW2_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW2_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW2_D6 ) /* pin 18*/ 
);

M125CX6 UBM87(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW3_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW3_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW3_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW3_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW3_D9 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_ROW3_D10 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_priority_transparency_OUT5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 18*/ 
);

M125CX6 UBM99(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW3_D11 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 6*/ ,
    .a5        ( Net__UBM99_Pad7_         ) /* pin 7*/ ,
    .y5        ( Net__UBM99_Pad13_        ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT6 ) /* pin 18*/ 
);

INV01 UBMX159(
    .i         ( _lut_assembly_priority_masks_OUT ) /* pin 1*/ ,
    .o         ( Net__UBI110_Pad3_        ) /* pin 2*/ 
);

INV01 UBMX160(
    .i         ( _lut_assembly_BLANK      ) /* pin 1*/ ,
    .o         ( Net__UBMX160_Pad2_       ) /* pin 2*/ 
);

AND02 UBMX164(
    .i1        ( Net__UBIX158_Pad5_       ) /* pin 1*/ ,
    .i2        ( Net__UBIX164_Pad3_       ) /* pin 2*/ ,
    .o         ( Net__UBMX164_Pad3_       ) /* pin 3*/ 
);

M125CX6 UBN0(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 18*/ 
);

NAND04 UBN108(
    .i1        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 2*/ ,
    .i3        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 3*/ ,
    .i4        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 4*/ ,
    .o         ( Net__UBN108_Pad5_        ) /* pin 5*/ 
);

OAI21 UBN110(
    .i1        ( Net__UBN108_Pad5_        ) /* pin 1*/ ,
    .i2        ( _lut_assembly_priority_transparency_~SPRITE_R1 ) /* pin 2*/ ,
    .i3        ( Net__UBI110_Pad3_        ) /* pin 3*/ ,
    .o         ( Net__UBH70_Pad1_         ) /* pin 4*/ 
);

M273CG #(.MiscRef(UBN116;UBN118;UBN122;UBN124;UBN128;UBN129;UBN133;UBN135;UBN139;UBN140;UBN144;UBN146;UBN148) 
) UBN112(
    .g_n       ( _lut_assembly_lut_storage_~E1 ) /* pin 1*/ ,
    .q0        ( _lut_io_A0               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 4*/ ,
    .q1        ( _lut_io_A1               ) /* pin 5*/ ,
    .q2        ( _lut_io_A2               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 8*/ ,
    .q3        ( _lut_io_A3               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A4               ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_draw_order_ROW1_D4 ) /* pin 13*/ ,
    .d5        ( _lut_assembly_layer_draw_order_ROW1_D5 ) /* pin 14*/ ,
    .q5        ( _lut_io_A5               ) /* pin 15*/ ,
    .q6        ( Net__UBN112_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBN112_Pad19_       ) /* pin 19*/ 
);

M125CX6 UBN12(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D6 ) /* pin 18*/ 
);

M273CG #(.MiscRef(UBN154;UBN156;UBN160;UBN161;UBN165;UBN167;UBN171;UBN172;UBN176;UBN178;UBN182) 
) UBN150(
    .g_n       ( _lut_assembly_lut_storage_~E1 ) /* pin 1*/ ,
    .q0        ( _lut_io_A6               ) /* pin 2*/ ,
    .d0        ( _lut_assembly_layer_draw_order_ROW1_D6 ) /* pin 3*/ ,
    .d1        ( _lut_assembly_layer_draw_order_ROW1_D7 ) /* pin 4*/ ,
    .q1        ( _lut_io_A7               ) /* pin 5*/ ,
    .q2        ( _lut_io_A8               ) /* pin 6*/ ,
    .d2        ( _lut_assembly_layer_draw_order_ROW1_D8 ) /* pin 7*/ ,
    .d3        ( _lut_assembly_layer_control_ROW1_S0 ) /* pin 8*/ ,
    .q3        ( _lut_io_A9               ) /* pin 9*/ ,
    .clk       ( _CLK8M                   ) /* pin 11*/ ,
    .q4        ( _lut_io_A10              ) /* pin 12*/ ,
    .d4        ( _lut_assembly_layer_control_ROW1_S1 ) /* pin 13*/ ,
    .q5        ( _lut_io_A11              ) /* pin 15*/ ,
    .q6        ( Net__UBN150_Pad16_       ) /* pin 16*/ ,
    .q7        ( Net__UBN150_Pad19_       ) /* pin 19*/ 
);

NAND02 UBN193(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_xpd5_sync_LOAD    ) /* pin 2*/ ,
    .o         ( Net__UBN193_Pad3_        ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBN195) 
) UBN196(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBN196_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBN196_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT1_Q0 ) /* pin 5*/ 
);

DFFCOO UBN200(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBN200_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBN200_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT1_Q1 ) /* pin 5*/ 
);

DFFCOO UBN204(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBN204_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBN204_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT1_Q2 ) /* pin 5*/ 
);

DFFCOO UBN208(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBN208_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBN208_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_row6_assemble_CNT1_Q3 ) /* pin 5*/ 
);

M125CX6 UBN21(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 18*/ 
);

AND02 UBN217(
    .i1        ( Net__UBN217_Pad1_        ) /* pin 1*/ ,
    .i2        ( _~RESET                  ) /* pin 2*/ ,
    .o         ( Net__UBN217_Pad3_        ) /* pin 3*/ 
);

BUF12 UBN219(
    .i         ( _stars_row6_counter_IC   ) /* pin 1*/ ,
    .o         ( Net__UBN219_Pad2_        ) /* pin 2*/ 
);

NAND04 UBN221(
    .i1        ( _stars_row6_counter_IC   ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_CNT1_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row6_assemble_CNT1_Q2 ) /* pin 3*/ ,
    .i4        ( _stars_row6_assemble_CNT1_Q3 ) /* pin 4*/ ,
    .o         ( Net__UBN217_Pad1_        ) /* pin 5*/ 
);

M273C #(.MiscRef(UBN223;UBN230;UBN234;UBN238;UBN242;UBN245;UBN249;UBN253) 
) UBN226(
    .q0        ( Net__UBN226_Pad2_        ) /* pin 2*/ ,
    .d0        ( _APD0                    ) /* pin 3*/ ,
    .d1        ( _APD1                    ) /* pin 4*/ ,
    .q1        ( Net__UBN226_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UBN226_Pad6_        ) /* pin 6*/ ,
    .d2        ( _APD2                    ) /* pin 7*/ ,
    .d3        ( _APD3                    ) /* pin 8*/ ,
    .q3        ( Net__UBN226_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBN226_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UBN226_Pad12_       ) /* pin 12*/ ,
    .d4        ( _APD4                    ) /* pin 13*/ ,
    .d5        ( _APD5                    ) /* pin 14*/ ,
    .q5        ( _stars_xpd_bus5_sync_PD5 ) /* pin 15*/ ,
    .q6        ( _stars_xpd_bus5_sync_PD6 ) /* pin 16*/ ,
    .d6        ( _APD6                    ) /* pin 17*/ ,
    .d7        ( _APD7                    ) /* pin 18*/ ,
    .q7        ( _stars_xpd_bus5_sync_PD7 ) /* pin 19*/ 
);

NAND02 UBN258(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_STARS_SERIAL      ) /* pin 2*/ ,
    .o         ( Net__UBH227_Pad1_        ) /* pin 3*/ 
);

INV01 UBN260(
    .i         ( Net__UBH383_Pad6_        ) /* pin 1*/ ,
    .o         ( _stars_STARS_SERIAL      ) /* pin 2*/ 
);

OR02 UBN261(
    .i1        ( _stars_row5_counter_INC  ) /* pin 1*/ ,
    .i2        ( Net__UBN261_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_row6_counter_IC   ) /* pin 3*/ 
);

DELAY #(.MiscRef(UBN265;UBN267;UBN269;UBN270;UBN272;UBN274;UBN275) 
) UBN264(
    .i         ( _~RESETI1                ) /* pin 1*/ ,
    .o         ( Net__UBN264_Pad2_        ) /* pin 2*/ 
);

NAND02 UBN277(
    .i1        ( Net__UBN277_Pad1_        ) /* pin 1*/ ,
    .i2        ( _FI                      ) /* pin 2*/ ,
    .o         ( Net__UBN277_Pad3_        ) /* pin 3*/ 
);

NAND02 UBN279(
    .i1        ( _lut_io_~E               ) /* pin 1*/ ,
    .i2        ( _~RESET                  ) /* pin 2*/ ,
    .o         ( Net__UBN279_Pad3_        ) /* pin 3*/ 
);

NAND02 UBN281(
    .i1        ( Net__UBN279_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBN277_Pad3_        ) /* pin 2*/ ,
    .o         ( _lut_io_~E               ) /* pin 3*/ 
);

NAND02 UBN282(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_STARS_LOAD1       ) /* pin 2*/ ,
    .o         ( Net__UBN226_Pad11_       ) /* pin 3*/ 
);

DFFCOS #(.MiscRef(UBN292;UBN317) 
) UBN295(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBN295_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK298_Pad1_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBN299(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBN299_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK298_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBN304(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBN304_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK298_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBN308(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBN308_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK301_Pad1_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBN313(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBN313_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBK304_Pad6_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

XNOR02 UBN319(
    .Unknown_pin( Net__UBK304_Pad6_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBK312_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBN319_Pad3_        ) /* pin 3*/ 
);

M157C UBN328(
    .a3        ( _lut_io_A3               ) /* pin 1*/ ,
    .a2        ( _lut_io_A2               ) /* pin 2*/ ,
    .a1        ( _lut_io_A1               ) /* pin 3*/ ,
    .a0        ( _lut_io_A0               ) /* pin 4*/ ,
    .q0        ( Net__UBK454_Pad1_        ) /* pin 5*/ ,
    .q1        ( Net__UBN328_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBN328_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UBH505_Pad1_        ) /* pin 8*/ ,
    .anb       ( _palette_control_CPY_LUT ) /* pin 9*/ ,
    .b3        ( _lut_io_B3               ) /* pin 10*/ ,
    .b2        ( _lut_io_B2               ) /* pin 11*/ ,
    .b1        ( _lut_io_B1               ) /* pin 12*/ ,
    .b0        ( _lut_io_B0               ) /* pin 13*/ ,
    .e_n       ( _lut_io_~E               ) /* pin 14*/ 
);

M125CX6 UBN33(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C2 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL2_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL2_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL2_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL2_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL2_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL2_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D6 ) /* pin 18*/ 
);

M367C UBN344(
    .oe_n      ( _video_signals_video_signal_generator_~DEBUG_HI ) /* pin 1*/ ,
    .a0        ( _video_signals_video_counter_LINE0 ) /* pin 2*/ ,
    .a1        ( _FI                      ) /* pin 3*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

BUF12 UBN355(
    .i         ( Net__UBN328_Pad7_        ) /* pin 1*/ ,
    .o         ( Net__UBN355_Pad2_        ) /* pin 2*/ 
);

OR02 UBN357(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _multiplier_~LSB_E       ) /* pin 2*/ ,
    .o         ( Net__UAF386_Pad70_       ) /* pin 3*/ 
);

DELAY #(.MiscRef(UBN364;UBN366;UBN367;UBN369;UBN371;UBN372;UBN374) 
) UBN362(
    .i         ( Net__UBN264_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__JP1_Pad1_           ) /* pin 2*/ 
);

NBUF03 UBN376(
    .i         ( Net__UBN376_Pad1_        ) /* pin 1*/ ,
    .o         ( _~BUS_RD                 ) /* pin 2*/ 
);

BUF12 UBN383(
    .i         ( Net__UBK439_Pad5_        ) /* pin 1*/ ,
    .o         ( _~BUS_WR                 ) /* pin 2*/ 
);

AND03 UBN385(
    .i1        ( _debug_~DEBUG1           ) /* pin 1*/ ,
    .i2        ( _debug_~DEBUG12          ) /* pin 2*/ ,
    .i3        ( _debug_~DEBUG11          ) /* pin 3*/ ,
    .o         ( _video_signals_video_signal_generator_~DEBUG_HI ) /* pin 4*/ 
);

OR03 UBN388(
    .i1        ( _~SCANLINE               ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .i3        ( Net__UBN388_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBN388_Pad4_        ) /* pin 4*/ 
);

DELAY #(.MiscRef(UBN392;UBN394;UBN396;UBN397;UBN399;UBN401;UBN402) 
) UBN391(
    .i         ( Net__UBK439_Pad5_        ) /* pin 1*/ ,
    .o         ( Net__UBN391_Pad2_        ) /* pin 2*/ 
);

INV01 UBN405(
    .i         ( _debug_~DEBUG14          ) /* pin 1*/ ,
    .o         ( Net__UBN261_Pad2_        ) /* pin 2*/ 
);

M367C UBN406(
    .oe_n      ( _debug_~DEBUG9           ) /* pin 1*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

BUF12 UBN416(
    .i         ( Net__UBN328_Pad6_        ) /* pin 1*/ ,
    .o         ( Net__UBN416_Pad2_        ) /* pin 2*/ 
);

DELAY #(.MiscRef(UBN420;UBN421;UBN423;UBN425;UBN426;UBN428;UBN430) 
) UBN418(
    .i         ( Net__UBN391_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBN418_Pad2_        ) /* pin 2*/ 
);

M125CX6 UBN43(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 18*/ 
);

AND03 UBN431(
    .i1        ( Net__UBN431_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK439_Pad5_        ) /* pin 2*/ ,
    .i3        ( Net__UBN418_Pad2_        ) /* pin 3*/ ,
    .o         ( _~IO_READ                ) /* pin 4*/ 
);

DFFCOR UBN434(
    .clk       ( Net__UBN434_Pad1_        ) /* pin 1*/ ,
    .q         ( Net__UBK439_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBN434_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBN431_Pad1_        ) /* pin 5*/ 
);

AND02 UBN439(
    .i1        ( _~RESETI2                ) /* pin 1*/ ,
    .i2        ( Net__UBK439_Pad5_        ) /* pin 2*/ ,
    .o         ( Net__UBN434_Pad4_        ) /* pin 3*/ 
);

BUF12 UBN441(
    .i         ( Net__UBN416_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBN441_Pad2_        ) /* pin 2*/ 
);

INV01 UBN445(
    .i         ( Net__UAF386_Pad71_       ) /* pin 1*/ ,
    .o         ( Net__UBN445_Pad2_        ) /* pin 2*/ 
);

BUF12 UBN446(
    .i         ( Net__UBN446_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBK498_Pad1_        ) /* pin 2*/ 
);

OR02 UBN451(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA14 ) /* pin 2*/ ,
    .o         ( Net__UBN451_Pad3_        ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBN453;UBN459) 
) UBN455(
    .clk       ( Net__UBN455_Pad1_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA0        ) /* pin 2*/ ,
    .q         ( Net__UAF386_Pad33_       ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBN455_Pad5_        ) /* pin 5*/ 
);

INV01 UBN461(
    .i         ( Net__UAF386_Pad33_       ) /* pin 1*/ ,
    .o         ( Net__UBN461_Pad2_        ) /* pin 2*/ 
);

AND02 UBN462(
    .i1        ( _bus_io_BUS_DATA15       ) /* pin 1*/ ,
    .i2        ( Net__UBN461_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBN462_Pad3_        ) /* pin 3*/ 
);

MUX2H #(.MiscRef(UBN465) 
) UBN467(
    .Unknown_pin( Net__UAF386_Pad33_       ) /* pin 1*/ ,
    .Unknown_pin( Net__UBH463_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBH463_Pad3_        ) /* pin 3*/ ,
    .Unknown_pin( Net__UBN467_Pad4_        ) /* pin 4*/ 
);

NOR04 UBN474(
    .o1        ( Net__UBN376_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBD451_Pad5_        ) /* pin 2*/ ,
    .i3        ( _~LUT_OE                 ) /* pin 3*/ ,
    .i4        ( _bus_signal_control_~CS  ) /* pin 4*/ ,
    .i5        ( _bus_signal_control_~RD  ) /* pin 5*/ 
);

BUF12 UBN477(
    .i         ( Net__UBN355_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBN477_Pad2_        ) /* pin 2*/ 
);

OR04 UBN479(
    .i1        ( Net__UBD451_Pad5_        ) /* pin 1*/ ,
    .i2        ( _~LUT_OE                 ) /* pin 2*/ ,
    .i3        ( _bus_signal_control_~CS  ) /* pin 3*/ ,
    .i4        ( _bus_signal_control_~WR  ) /* pin 4*/ ,
    .o         ( Net__UBN434_Pad1_        ) /* pin 5*/ 
);

M273C #(.MiscRef(UBN482;UBN489;UBN494;UBN498;UBN503;UBN507;UBN512;UBN516;UBN520) 
) UBN485(
    .r_n       ( _~RESETI1                ) /* pin 1*/ ,
    .q0        ( _video_signals_video_counter_DATA8 ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA9        ) /* pin 4*/ ,
    .q1        ( _video_signals_video_counter_DATA9 ) /* pin 5*/ ,
    .q2        ( _video_signals_video_counter_DATA10 ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA10       ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA11       ) /* pin 8*/ ,
    .q3        ( _video_signals_video_counter_DATA11 ) /* pin 9*/ ,
    .clk       ( Net__UBN388_Pad4_        ) /* pin 11*/ ,
    .q4        ( _video_signals_video_counter_DATA12 ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .q5        ( _video_signals_video_counter_DATA13 ) /* pin 15*/ ,
    .q6        ( _video_signals_video_counter_DATA14 ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA14       ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA15       ) /* pin 18*/ ,
    .q7        ( _video_signals_video_counter_DATA15 ) /* pin 19*/ 
);

M125CX6 UBN55(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C3 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL3_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL3_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL3_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL3_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL3_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL3_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D6 ) /* pin 18*/ 
);

M125CX6 UBN65(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D4 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D5 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 18*/ 
);

M125CX6 UBN77(
    .oe_n      ( _lut_assembly_layer_draw_order_~R1C4 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_COL4_D6 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_COL4_D7 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_COL4_D8 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_COL4_D9 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_COL4_D10 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_COL4_D11 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_layer_draw_order_ROW1_D11 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_layer_draw_order_ROW1_D10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_layer_draw_order_ROW1_D9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_layer_draw_order_ROW1_D8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_layer_draw_order_ROW1_D7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_layer_draw_order_ROW1_D6 ) /* pin 18*/ 
);

M125CX6 UBN87(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW2_D0 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW2_D1 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW2_D2 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW2_D3 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW2_D9 ) /* pin 6*/ ,
    .a5        ( _lut_assembly_layer_draw_order_ROW2_D10 ) /* pin 7*/ ,
    .y5        ( _lut_assembly_priority_transparency_OUT5 ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT4 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 18*/ 
);

M125CX6 UBN99(
    .oe_n      ( _lut_assembly_priority_transparency_~SPRITE_R1 ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_draw_order_ROW2_D11 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_draw_order_ROW1_D0 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_draw_order_ROW1_D1 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_draw_order_ROW1_D2 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_layer_draw_order_ROW1_D3 ) /* pin 6*/ ,
    .a5        ( Net__UBN99_Pad7_         ) /* pin 7*/ ,
    .y5        ( Net__UBN99_Pad13_        ) /* pin 13*/ ,
    .y4        ( _lut_assembly_priority_transparency_OUT10 ) /* pin 14*/ ,
    .y3        ( _lut_assembly_priority_transparency_OUT9 ) /* pin 15*/ ,
    .y2        ( _lut_assembly_priority_transparency_OUT8 ) /* pin 16*/ ,
    .y1        ( _lut_assembly_priority_transparency_OUT7 ) /* pin 17*/ ,
    .y0        ( _lut_assembly_priority_transparency_OUT6 ) /* pin 18*/ 
);

AND02 UBNX164(
    .i1        ( Net__UBMX160_Pad2_       ) /* pin 1*/ ,
    .i2        ( Net__UBMX164_Pad3_       ) /* pin 2*/ ,
    .o         ( Net__UBNX164_Pad3_       ) /* pin 3*/ 
);

BUF12 UBR1(
    .i         ( _debug_td_io_IN1         ) /* pin 1*/ ,
    .o         ( __TD1                    ) /* pin 2*/ 
);

XNOR02 UBR106(
    .Unknown_pin( Net__UBR106_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR106_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR106_Pad3_        ) /* pin 3*/ 
);

NAND02 UBR109(
    .i1        ( Net__UBR109_Pad1_        ) /* pin 1*/ ,
    .i2        ( _counter_fi_INC_E        ) /* pin 2*/ ,
    .o         ( Net__UBR109_Pad3_        ) /* pin 3*/ 
);

NAND04 UBR11(
    .i1        ( Net__UBR11_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBR11_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBR109_Pad1_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_INC_E        ) /* pin 4*/ ,
    .o         ( Net__UBR106_Pad2_        ) /* pin 5*/ 
);

NAND05 UBR111(
    .i0        ( Net__UBR106_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UBR11_Pad1_         ) /* pin 2*/ ,
    .i2        ( Net__UBR11_Pad2_         ) /* pin 3*/ ,
    .i3        ( Net__UBR109_Pad1_        ) /* pin 4*/ ,
    .i4        ( _counter_fi_INC_E        ) /* pin 5*/ ,
    .o         ( Net__UBR111_Pad6_        ) /* pin 6*/ 
);

XNOR02 UBR114(
    .Unknown_pin( Net__UBR11_Pad2_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR109_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR114_Pad3_        ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBR117) 
) UBR118(
    .clk       ( Net__UBN193_Pad3_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD_OUT5 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW5_D4 ) /* pin 3*/ ,
    .q_n       ( Net__UBR118_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBR122(
    .clk       ( Net__UBN193_Pad3_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD_OUT6 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW5_D5 ) /* pin 3*/ ,
    .q_n       ( Net__UBR122_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBR126(
    .clk       ( Net__UBN193_Pad3_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD_OUT7 ) /* pin 2*/ ,
    .q         ( _lut_assembly_lut_storage_ROW5_D6 ) /* pin 3*/ ,
    .q_n       ( Net__UBR126_Pad5_        ) /* pin 5*/ 
);

NAND03 UBR13(
    .i1        ( Net__UBR11_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBR109_Pad1_        ) /* pin 2*/ ,
    .i3        ( _counter_fi_INC_E        ) /* pin 3*/ ,
    .o         ( Net__UBR13_Pad4_         ) /* pin 4*/ 
);

DFFCOO UBR130(
    .clk       ( Net__UBN193_Pad3_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD_OUT8 ) /* pin 2*/ ,
    .q         ( Net__UBR130_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBR130_Pad5_        ) /* pin 5*/ 
);

NAND05 UBR134(
    .i0        ( Net__UBN226_Pad9_        ) /* pin 1*/ ,
    .i1        ( Net__UBN226_Pad6_        ) /* pin 2*/ ,
    .i2        ( Net__UBN226_Pad5_        ) /* pin 3*/ ,
    .i3        ( Net__UBN226_Pad2_        ) /* pin 4*/ ,
    .i4        ( Net__UBR134_Pad5_        ) /* pin 5*/ ,
    .o         ( _stars_xpd_bus5_sync_PD8 ) /* pin 6*/ 
);

XNOR02 UBR139(
    .Unknown_pin( Net__UBR139_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR111_Pad6_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR139_Pad3_        ) /* pin 3*/ 
);

NAND02 UBR141(
    .i1        ( _stars_xpd5_sync_ZERO    ) /* pin 1*/ ,
    .i2        ( Net__UBR130_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBF13_Pad2_         ) /* pin 3*/ 
);

NAND06 UBR143(
    .i1        ( _counter_fi_INC_E        ) /* pin 1*/ ,
    .i2        ( Net__UBR109_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBR11_Pad2_         ) /* pin 3*/ ,
    .i4        ( Net__UBR11_Pad1_         ) /* pin 4*/ ,
    .i5        ( Net__UBR106_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UBR139_Pad1_        ) /* pin 6*/ ,
    .o9        ( Net__UBR143_Pad9_        ) /* pin 9*/ 
);

XNOR02 UBR148(
    .Unknown_pin( Net__UBR148_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR143_Pad9_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR148_Pad3_        ) /* pin 3*/ 
);

NAND08 UBR151(
    .i1        ( _counter_fi_INC_E        ) /* pin 1*/ ,
    .i2        ( Net__UBR109_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBR11_Pad2_         ) /* pin 3*/ ,
    .i4        ( Net__UBR11_Pad1_         ) /* pin 4*/ ,
    .i5        ( Net__UBR106_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UBR139_Pad1_        ) /* pin 6*/ ,
    .i7        ( Net__UBR148_Pad1_        ) /* pin 7*/ ,
    .o9        ( Net__UBR151_Pad9_        ) /* pin 9*/ 
);

DFFCOR #(.MiscRef(UBR159;UBR188) 
) UBR161(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR161_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR109_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR161_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR166(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR114_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBR11_Pad2_         ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR166_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR170(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR170_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR11_Pad1_         ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR170_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR175(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR106_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBR106_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR175_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR179(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR139_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBR139_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR179_Pad5_        ) /* pin 5*/ 
);

DFFCOO #(.MiscRef(UBR16) 
) UBR18(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR18_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR18_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR18_Pad5_         ) /* pin 5*/ 
);

DFFCOR UBR184(
    .clk       ( Net__UBR161_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR148_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBR148_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBR184_Pad5_        ) /* pin 5*/ 
);

XNOR02 UBR192(
    .Unknown_pin( Net__UBR192_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR192_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR192_Pad3_        ) /* pin 3*/ 
);

M541C UBR194(
    .oe_n      ( _debug_~DEBUG15          ) /* pin 1*/ ,
    .y7        ( Net__UBR194_Pad11_       ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN11        ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN10        ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN9         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN8         ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN7         ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN6         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN5         ) /* pin 18*/ 
);

XOR02 UBR218(
    .i1        ( Net__UBD222_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR218_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus6_sync_PD0 ) /* pin 3*/ 
);

DFFCOO UBR22(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR22_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR22_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR22_Pad5_         ) /* pin 5*/ 
);

XOR02 UBR221(
    .i1        ( Net__UBD222_Pad5_        ) /* pin 1*/ ,
    .i2        ( Net__UBR218_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus6_sync_PD1 ) /* pin 3*/ 
);

XOR02 UBR224(
    .i1        ( Net__UBD222_Pad6_        ) /* pin 1*/ ,
    .i2        ( Net__UBR218_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus6_sync_PD2 ) /* pin 3*/ 
);

XOR02 UBR226(
    .i1        ( Net__UBD222_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UBR218_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus6_sync_PD3 ) /* pin 3*/ 
);

XOR02 UBR229(
    .i1        ( Net__UBD222_Pad12_       ) /* pin 1*/ ,
    .i2        ( Net__UBR218_Pad2_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus6_sync_PD4 ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBR232) 
) UBR233(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _FLIP                    ) /* pin 2*/ ,
    .q         ( Net__UBR233_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBR233_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBR240(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD0 ) /* pin 2*/ ,
    .q         ( Net__UBR240_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus6_sync_~PD_OUT0 ) /* pin 5*/ 
);

FFCOO #(.MiscRef(UBR237) 
) UBR244(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD1 ) /* pin 2*/ ,
    .q         ( Net__UBR244_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus6_sync_~PD_OUT1 ) /* pin 5*/ 
);

DFFCOO UBR248(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD2 ) /* pin 2*/ ,
    .q         ( Net__UBR248_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus6_sync_~PD_OUT2 ) /* pin 5*/ 
);

DFFCOO UBR25(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR25_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR25_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR25_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBR252(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD3 ) /* pin 2*/ ,
    .q         ( Net__UBR252_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus6_sync_~PD_OUT3 ) /* pin 5*/ 
);

DFFCOO UBR255(
    .clk       ( Net__UBH227_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus6_sync_PD4 ) /* pin 2*/ ,
    .q         ( Net__UBR255_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus6_sync_~PD_OUT4 ) /* pin 5*/ 
);

NBUF03 UBR271(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( Net__UBR271_Pad2_        ) /* pin 2*/ 
);

INV01 UBR274(
    .i         ( Net__UBR151_Pad9_        ) /* pin 1*/ ,
    .o         ( Net__UBN277_Pad1_        ) /* pin 2*/ 
);

DFFCOO UBR29(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR29_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR29_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR29_Pad5_         ) /* pin 5*/ 
);

XNOR02 UBR293(
    .Unknown_pin( Net__UBK298_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBK298_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR293_Pad3_        ) /* pin 3*/ 
);

OR02 UBR296(
    .i1        ( Net__UBK298_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBK298_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UBR296_Pad3_        ) /* pin 3*/ 
);

INV01 UBR298(
    .i         ( Net__UBK298_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBR298_Pad2_        ) /* pin 2*/ 
);

BUF12 UBR3(
    .i         ( _debug_td_io_IN5         ) /* pin 1*/ ,
    .o         ( __TD5                    ) /* pin 2*/ 
);

XNOR02 UBR300(
    .Unknown_pin( Net__UBK298_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR296_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR300_Pad3_        ) /* pin 3*/ 
);

MUX25H #(.MiscRef(UBR305;UBR308;UBR312;UBR315;UBR318) 
) UBR302(
    .a3        ( Net__UBR302_Pad1_        ) /* pin 1*/ ,
    .a4        ( Net__UBR302_Pad2_        ) /* pin 2*/ ,
    .a2        ( Net__UBR302_Pad3_        ) /* pin 3*/ ,
    .a1        ( Net__UBR302_Pad4_        ) /* pin 4*/ ,
    .a0        ( Net__UBR302_Pad5_        ) /* pin 5*/ ,
    .q0        ( Net__UBN295_Pad2_        ) /* pin 6*/ ,
    .q1        ( Net__UBN299_Pad2_        ) /* pin 7*/ ,
    .q2        ( Net__UBN304_Pad2_        ) /* pin 8*/ ,
    .q3        ( Net__UBN308_Pad2_        ) /* pin 9*/ ,
    .q4        ( Net__UBN313_Pad2_        ) /* pin 10*/ ,
    .anb       ( Net__UBR302_Pad11_       ) /* pin 11*/ ,
    .b3        ( Net__UBK301_Pad3_        ) /* pin 12*/ ,
    .b4        ( Net__UBN319_Pad3_        ) /* pin 13*/ ,
    .b2        ( Net__UBR300_Pad3_        ) /* pin 14*/ ,
    .b1        ( Net__UBR293_Pad3_        ) /* pin 15*/ ,
    .b0        ( Net__UBR298_Pad2_        ) /* pin 16*/ 
);

M541C UBR325(
    .oe_n      ( Net__UBR325_Pad1_        ) /* pin 1*/ ,
    .y7        ( _bus_io_BUS_DATA15       ) /* pin 11*/ ,
    .y6        ( _bus_io_BUS_DATA14       ) /* pin 12*/ ,
    .y5        ( _bus_io_BUS_DATA13       ) /* pin 13*/ ,
    .y4        ( _bus_io_BUS_DATA12       ) /* pin 14*/ ,
    .y3        ( _bus_io_BUS_DATA11       ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA10       ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA9        ) /* pin 17*/ ,
    .y0        ( Net__UBR325_Pad18_       ) /* pin 18*/ 
);

NOR05 UBR33(
    .o1        ( _stars_xpd5_sync_LOAD    ) /* pin 1*/ ,
    .i2        ( Net__UBR33_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBR18_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UBR25_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UBR22_Pad3_         ) /* pin 5*/ ,
    .i6        ( Net__UBR29_Pad3_         ) /* pin 6*/ 
);

M541C UBR345(
    .oe_n      ( _debug_~DEBUG9           ) /* pin 1*/ ,
    .a0        ( _raster_interrupts_~RASTER3 ) /* pin 2*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

NOR02 UBR365(
    .i1        ( _raster_interrupts_~RASTER3 ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .o         ( Net__UBR365_Pad3_        ) /* pin 3*/ 
);

OR02 UBR367(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _raster_interrupts_~RASTER3 ) /* pin 2*/ ,
    .o         ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 3*/ 
);

NOR02 UBR37(
    .i1        ( Net__UBR18_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBR33_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBR37_Pad3_         ) /* pin 3*/ 
);

AND03 UBR371(
    .i1        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 1*/ ,
    .i2        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 2*/ ,
    .i3        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 3*/ ,
    .o         ( Net__UBR325_Pad1_        ) /* pin 4*/ 
);

NOR02 UBR374(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_~PRIORITY3 ) /* pin 2*/ ,
    .o         ( Net__UBR374_Pad3_        ) /* pin 3*/ 
);

NBUF03 UBR376(
    .i         ( Net__UBN376_Pad1_        ) /* pin 1*/ ,
    .o         ( _~BUS_RD                 ) /* pin 2*/ 
);

M157C UBR383(
    .a3        ( _lut_io_A11              ) /* pin 1*/ ,
    .a2        ( _lut_io_A10              ) /* pin 2*/ ,
    .a1        ( _lut_io_A9               ) /* pin 3*/ ,
    .a0        ( _lut_io_A8               ) /* pin 4*/ ,
    .q0        ( Net__UBR383_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UBR383_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBH427_Pad1_        ) /* pin 7*/ ,
    .q3        ( Net__UBN446_Pad1_        ) /* pin 8*/ ,
    .anb       ( _palette_control_CPY_LUT ) /* pin 9*/ ,
    .b3        ( _lut_io_B11              ) /* pin 10*/ ,
    .b2        ( _lut_io_B10              ) /* pin 11*/ ,
    .b1        ( _lut_io_B9               ) /* pin 12*/ ,
    .b0        ( _lut_io_B8               ) /* pin 13*/ ,
    .e_n       ( _lut_io_~E               ) /* pin 14*/ 
);

XNOR02 UBR39(
    .Unknown_pin( Net__UBR22_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR37_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR39_Pad3_         ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBR399;UBR424) 
) UBR401(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR401_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR401_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR401_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR406(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR406_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR406_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR406_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR410(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR410_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR410_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR410_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR415(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR415_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR415_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR415_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR419(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR419_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR419_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR419_Pad5_        ) /* pin 5*/ 
);

INV01 UBR42(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBR42_Pad2_         ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UBR426;UBR451) 
) UBR429(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR401_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBH436_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR429_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR434(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR406_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBH441_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR434_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR438(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR410_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBH445_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR438_Pad5_        ) /* pin 5*/ 
);

BUF12 UBR44(
    .i         ( _debug_td_io_IN8         ) /* pin 1*/ ,
    .o         ( __TD8                    ) /* pin 2*/ 
);

DFFCOR UBR442(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR415_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBH450_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR442_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBR447(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UBR419_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBH454_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR447_Pad5_        ) /* pin 5*/ 
);

DFGOO #(.MiscRef(UBR453;UBR486) 
) UBR456(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( _raster_interrupts_counter3__OUT0 ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBR462(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR462_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBR468(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA2        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBR474(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR474_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA3        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBR479(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR479_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA4        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

BUF12 UBR487(
    .i         ( Net__UBR487_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBR487_Pad2_        ) /* pin 2*/ 
);

BUF12 UBR489(
    .i         ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ 
);

BUF12 UBR491(
    .i         ( Net__UBR491_Pad1_        ) /* pin 1*/ ,
    .o         ( _palette_control_~LUT_WR ) /* pin 2*/ 
);

BUF12 UBR494(
    .i         ( Net__UBR383_Pad6_        ) /* pin 1*/ ,
    .o         ( Net__UBR494_Pad2_        ) /* pin 2*/ 
);

OR02 UBR497(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBR497_Pad3_        ) /* pin 3*/ 
);

BUF12 UBR5(
    .i         ( _debug_td_io_IN11        ) /* pin 1*/ ,
    .o         ( __TD11                   ) /* pin 2*/ 
);

INV01 UBR50(
    .i         ( _stars_xpd5_sync_LOAD    ) /* pin 1*/ ,
    .o         ( Net__UBR50_Pad2_         ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UBR499;UBR505) 
) UBR501(
    .clk       ( _LI                      ) /* pin 1*/ ,
    .d         ( Net__UBR497_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBR501_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBR501_Pad5_        ) /* pin 5*/ 
);

BUF12 UBR508(
    .i         ( _~LUT_OE                 ) /* pin 1*/ ,
    .o         ( Net__P101_Pad~_          ) /* pin 2*/ 
);

BUF12 UBR509(
    .i         ( Net__UBR494_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBR509_Pad2_        ) /* pin 2*/ 
);

BUF12 UBR512(
    .i         ( Net__UBN441_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT1                    ) /* pin 2*/ 
);

BUF12 UBR515(
    .i         ( Net__UBH512_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT3                    ) /* pin 2*/ 
);

BUF12 UBR518(
    .i         ( Net__UBR509_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT9                    ) /* pin 2*/ 
);

XOR02 UBR52(
    .i1        ( Net__UBR52_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBR52_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBR52_Pad3_         ) /* pin 3*/ 
);

BUF12 UBR520(
    .i         ( Net__UBR487_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT8                    ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBR55) 
) UBR56(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR56_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR52_Pad1_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR56_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBR60(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR60_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR60_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR60_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBR64(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR64_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR64_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR64_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBR68(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBR68_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBR68_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBR68_Pad5_         ) /* pin 5*/ 
);

INV01 UBR72(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBR72_Pad2_         ) /* pin 2*/ 
);

NOR04 UBR73(
    .o1        ( Net__UBR73_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBR52_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBR52_Pad1_         ) /* pin 3*/ ,
    .i4        ( Net__UBR60_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UBR64_Pad3_         ) /* pin 5*/ 
);

NOR05 UBR76(
    .o1        ( _stars_xpd6_sync_ZERO    ) /* pin 1*/ ,
    .i2        ( Net__UBR52_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBR52_Pad1_         ) /* pin 3*/ ,
    .i4        ( Net__UBR64_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UBR60_Pad3_         ) /* pin 5*/ ,
    .i6        ( Net__UBR68_Pad3_         ) /* pin 6*/ 
);

XNOR02 UBR8(
    .Unknown_pin( Net__UBR11_Pad1_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR13_Pad4_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR170_Pad2_        ) /* pin 3*/ 
);

XNOR02 UBR81(
    .Unknown_pin( Net__UBR68_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR73_Pad1_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR81_Pad3_         ) /* pin 3*/ 
);

M258C UBR83(
    .a0        ( _stars_xpd_bus6_sync_~PD_OUT4 ) /* pin 4*/ ,
    .q0_n      ( Net__UBR56_Pad2_         ) /* pin 5*/ ,
    .q1_n      ( Net__UBR60_Pad2_         ) /* pin 6*/ ,
    .q2_n      ( Net__UBR64_Pad2_         ) /* pin 7*/ ,
    .q3_n      ( Net__UBR68_Pad2_         ) /* pin 8*/ ,
    .anb       ( Net__UBR83_Pad9_         ) /* pin 9*/ ,
    .b3        ( Net__UBR81_Pad3_         ) /* pin 10*/ ,
    .b2        ( Net__UBR83_Pad11_        ) /* pin 11*/ ,
    .b1        ( Net__UBR83_Pad12_        ) /* pin 12*/ ,
    .b0        ( Net__UBR52_Pad3_         ) /* pin 13*/ ,
    .e_n       ( Net__UBR72_Pad2_         ) /* pin 14*/ 
);

XOR02 UBR98(
    .i1        ( Net__UBR109_Pad1_        ) /* pin 1*/ ,
    .i2        ( _counter_fi_INC_E        ) /* pin 2*/ ,
    .o         ( Net__UBR161_Pad2_        ) /* pin 3*/ 
);

BUF12 UBU1(
    .i         ( _debug_td_io_IN0         ) /* pin 1*/ ,
    .o         ( __TD0                    ) /* pin 2*/ 
);

XOR02 UBU10(
    .i1        ( Net__UBR18_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBR33_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBU10_Pad3_         ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBU108) 
) UBU109(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU109_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU109_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU109_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU113(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU113_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU113_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU113_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU117(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU117_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU117_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU117_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU121(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU121_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU121_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU121_Pad5_        ) /* pin 5*/ 
);

M258C UBU125(
    .a0        ( _stars_xpd_bus5_sync_~PD_OUT4 ) /* pin 4*/ ,
    .q0_n      ( Net__UBU109_Pad2_        ) /* pin 5*/ ,
    .q1_n      ( Net__UBU113_Pad2_        ) /* pin 6*/ ,
    .q2_n      ( Net__UBU117_Pad2_        ) /* pin 7*/ ,
    .q3_n      ( Net__UBU121_Pad2_        ) /* pin 8*/ ,
    .anb       ( Net__UBR50_Pad2_         ) /* pin 9*/ ,
    .b3        ( Net__UBU125_Pad10_       ) /* pin 10*/ ,
    .b2        ( Net__UBU125_Pad11_       ) /* pin 11*/ ,
    .b1        ( Net__UBU125_Pad12_       ) /* pin 12*/ ,
    .b0        ( Net__UBU125_Pad13_       ) /* pin 13*/ ,
    .e_n       ( Net__UAA484_Pad2_        ) /* pin 14*/ 
);

XNOR02 UBU14(
    .Unknown_pin( Net__UBR29_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU14_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBU14_Pad3_         ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBU139) 
) UBU141(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD5 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_sync_PD_OUT5 ) /* pin 3*/ ,
    .q_n       ( Net__UBU141_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU145(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD6 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_sync_PD_OUT6 ) /* pin 3*/ ,
    .q_n       ( Net__UBU145_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU148(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD7 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_sync_PD_OUT7 ) /* pin 3*/ ,
    .q_n       ( Net__UBU148_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU152(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD8 ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_sync_PD_OUT8 ) /* pin 3*/ ,
    .q_n       ( Net__UBU152_Pad5_        ) /* pin 5*/ 
);

DFFCOS #(.MiscRef(UBU159;UBU179) 
) UBU161(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU161_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU161_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBU165(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU165_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU165_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

NOR04 UBU17(
    .o1        ( Net__UBU14_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBR33_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBR18_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UBR22_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UBR25_Pad3_         ) /* pin 5*/ 
);

DFFCOS UBU170(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU170_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU170_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UBU174(
    .clk       ( Net__UBN295_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU174_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBR192_Pad1_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

INV01 UBU180(
    .i         ( Net__UBU180_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBU180_Pad2_        ) /* pin 2*/ 
);

INV01 UBU181(
    .i         ( _stars_xpd6_sync_LOAD    ) /* pin 1*/ ,
    .o         ( Net__UBR83_Pad9_         ) /* pin 2*/ 
);

AND02 UBU182(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU180_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBU182_Pad3_        ) /* pin 3*/ 
);

NAND02 UBU185(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_ZERO_XS1 ) /* pin 2*/ ,
    .o         ( Net__UBH196_Pad1_        ) /* pin 3*/ 
);

MUX26H #(.MiscRef(UBU190;UBU193;UBU196;UBU200;UBU203;UBU206) 
) UBU187(
    .a0        ( _security_management_security_decoder_~K_MULT_X ) /* pin 1*/ ,
    .a1        ( _security_management_security_decoder_~K_MULT_Y ) /* pin 2*/ ,
    .a2        ( _security_management_security_decoder_~K_MULT_LSB ) /* pin 3*/ ,
    .a3        ( _security_management_security_decoder_~K_MULT_MSB ) /* pin 4*/ ,
    .a4        ( _security_management_security_decoder_~K_MULT_TC ) /* pin 5*/ ,
    .a5        ( _security_management_addr_decoder_registry_~CHECK1 ) /* pin 6*/ ,
    .b0        ( _security_management_addr_decoder_0x00 ) /* pin 7*/ ,
    .b1        ( _security_management_addr_decoder_0x02 ) /* pin 8*/ ,
    .b2        ( _security_management_addr_decoder_0x04 ) /* pin 9*/ ,
    .b3        ( _security_management_addr_decoder_0x06 ) /* pin 10*/ ,
    .b4        ( _security_management_addr_decoder_0x08 ) /* pin 11*/ ,
    .b5        ( _security_management_addr_decoder_0x0A ) /* pin 12*/ ,
    .q0        ( _security_management_~MULT_X ) /* pin 13*/ ,
    .q1        ( _security_management_~MULT_Y ) /* pin 14*/ ,
    .q2        ( _multiplier_~LSB_E       ) /* pin 15*/ ,
    .q3        ( _multiplier_~MSB_E       ) /* pin 16*/ ,
    .q4        ( _security_management_~MULT_TC ) /* pin 17*/ ,
    .q5        ( _~CHECK1                 ) /* pin 18*/ ,
    .anb       ( _main_signals_DEFAULT    ) /* pin 19*/ 
);

NAND04 UBU20(
    .i1        ( _stars_row6_assemble_CNT1_Q2 ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_CNT1_Q1 ) /* pin 2*/ ,
    .i3        ( _stars_row6_assemble_CNT1_Q0 ) /* pin 3*/ ,
    .i4        ( Net__UBN219_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UBU20_Pad5_         ) /* pin 5*/ 
);

NBUF03 UBU210(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( _stars_row6_assemble_ZERO_XS1 ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBU217) 
) UBU218(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU218_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU218_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU218_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU222(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU222_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU222_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU222_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBU226(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU226_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU226_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU226_Pad5_        ) /* pin 5*/ 
);

INV01 UBU23(
    .i         ( Net__UBU23_Pad1_         ) /* pin 1*/ ,
    .o         ( Net__UBR33_Pad2_         ) /* pin 2*/ 
);

DFFCOO UBU230(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU230_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU230_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU230_Pad5_        ) /* pin 5*/ 
);

M541C UBU234(
    .oe_n      ( _debug_~DEBUG8           ) /* pin 1*/ ,
    .a0        ( _security_management_~MULT_X ) /* pin 2*/ ,
    .a1        ( _security_management_~MULT_Y ) /* pin 3*/ ,
    .a2        ( _multiplier_~LSB_E       ) /* pin 4*/ ,
    .a3        ( _multiplier_~MSB_E       ) /* pin 5*/ ,
    .a4        ( _security_management_~MULT_TC ) /* pin 6*/ ,
    .a5        ( _~CHECK1                 ) /* pin 7*/ ,
    .a6        ( _~CHECK2                 ) /* pin 8*/ ,
    .a7        ( _raster_interrupts_~RASTER1 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

NOR04 UBU24(
    .o1        ( Net__UBU24_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBU24_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBU24_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UBU24_Pad4_         ) /* pin 4*/ ,
    .i5        ( Net__UBU24_Pad5_         ) /* pin 5*/ 
);

DFFCOO #(.MiscRef(UBU256) 
) UBU258(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _FLIP                    ) /* pin 2*/ ,
    .q         ( Net__UBU258_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBR218_Pad2_        ) /* pin 5*/ 
);

NAND02 UBU265(
    .i1        ( Net__UBU265_Pad1_        ) /* pin 1*/ ,
    .i2        ( _debug_~DEBUG11          ) /* pin 2*/ ,
    .o         ( Net__UBU265_Pad3_        ) /* pin 3*/ 
);

NOR03 UBU27(
    .i1        ( Net__UBR33_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBR18_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UBR22_Pad3_         ) /* pin 3*/ ,
    .o4        ( Net__UBU27_Pad4_         ) /* pin 4*/ 
);

DFFCOR #(.MiscRef(UBU269;UBU276) 
) UBU271(
    .clk       ( Net__UBU271_Pad1_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q         ( Net__UBU271_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UBU271_Pad5_        ) /* pin 5*/ 
);

DFFCOR #(.MiscRef(UBU277;UBU284) 
) UBU279(
    .clk       ( Net__UBU279_Pad1_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q         ( Net__UBU279_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UBU279_Pad5_        ) /* pin 5*/ 
);

AOI21 UBU291(
    .i1        ( Net__UBR365_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU271_Pad3_        ) /* pin 2*/ ,
    .i3        ( _LI                      ) /* pin 3*/ ,
    .o4        ( Net__UBN295_Pad1_        ) /* pin 4*/ 
);

AND02 UBU293(
    .i1        ( Net__UBU293_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBU265_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UBU180_Pad1_        ) /* pin 3*/ 
);

DFGOO #(.MiscRef(UBU296) 
) UBU298(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU161_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA5        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBU303(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU165_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA6        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBU309(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBU170_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA7        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

M258C UBU31(
    .a0        ( _stars_xpd_bus5_XS_OUT4  ) /* pin 4*/ ,
    .q0_n      ( Net__UBR18_Pad2_         ) /* pin 5*/ ,
    .q1_n      ( Net__UBR22_Pad2_         ) /* pin 6*/ ,
    .q2_n      ( Net__UBR25_Pad2_         ) /* pin 7*/ ,
    .q3_n      ( Net__UBR29_Pad2_         ) /* pin 8*/ ,
    .anb       ( Net__UBU31_Pad9_         ) /* pin 9*/ ,
    .b3        ( Net__UBU14_Pad3_         ) /* pin 10*/ ,
    .b2        ( Net__UBU31_Pad11_        ) /* pin 11*/ ,
    .b1        ( Net__UBR39_Pad3_         ) /* pin 12*/ ,
    .b0        ( Net__UBU10_Pad3_         ) /* pin 13*/ ,
    .e_n       ( Net__UBR42_Pad2_         ) /* pin 14*/ 
);

DFGOO UBU315(
    .clk       ( Net__UBK344_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR192_Pad1_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 5*/ 
);

INV01 UBU328(
    .i         ( _LI                      ) /* pin 1*/ ,
    .o         ( Net__UBU265_Pad1_        ) /* pin 2*/ 
);

R52 UBU329(
    .le        ( Net__UBR365_Pad3_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS0 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS1 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS2 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS3 ) /* pin 6*/ ,
    .d5_n      ( _raster_interrupts_counter1_~BUS4 ) /* pin 7*/ ,
    .q1        ( Net__UBR302_Pad5_        ) /* pin 8*/ ,
    .q2        ( Net__UBR302_Pad4_        ) /* pin 9*/ ,
    .q3        ( Net__UBR302_Pad3_        ) /* pin 10*/ ,
    .q4        ( Net__UBR302_Pad1_        ) /* pin 11*/ ,
    .q5        ( Net__UBR302_Pad2_        ) /* pin 12*/ 
);

OR02 UBU347(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _security_management_~MULT_Y ) /* pin 2*/ ,
    .o         ( Net__UAF386_Pad36_       ) /* pin 3*/ 
);

R42 UBU349(
    .le        ( Net__UBR365_Pad3_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS5 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS6 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS7 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS8 ) /* pin 6*/ ,
    .q1        ( Net__UBU349_Pad7_        ) /* pin 7*/ ,
    .q2        ( Net__UBU349_Pad8_        ) /* pin 8*/ ,
    .q3        ( Net__UBU349_Pad9_        ) /* pin 9*/ ,
    .q4        ( Net__UBU349_Pad10_       ) /* pin 10*/ 
);

INV01 UBU364(
    .i         ( _bus_io_BUS_DATA0        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS0 ) /* pin 2*/ 
);

INV01 UBU365(
    .i         ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS3 ) /* pin 2*/ 
);

INV01 UBU366(
    .i         ( _bus_io_BUS_DATA4        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS4 ) /* pin 2*/ 
);

NAND02 UBU367(
    .i1        ( _raster_interrupts_counter1_~OUT_ENABLE ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBK344_Pad1_        ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UBU369) 
) UBU370(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBU265_Pad1_        ) /* pin 2*/ ,
    .q         ( Net__UBU293_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU370_Pad5_        ) /* pin 5*/ 
);

NOR02 UBU376(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_~PRIORITY2 ) /* pin 2*/ ,
    .o         ( Net__UBU376_Pad3_        ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBU385;UBU410) 
) UBU388(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( _video_signals_video_signal_generator_VSYNC ) /* pin 2*/ ,
    .q         ( Net__UBR401_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBU388_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBU392(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( _video_signals_video_signal_generator_~VBLANK ) /* pin 2*/ ,
    .q         ( Net__UBR406_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBU392_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBU396(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( _video_signals_video_signal_generator_HSYNC ) /* pin 2*/ ,
    .q         ( Net__UBR410_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBU396_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBU401(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( _video_signals_video_signal_generator_~HBLANK ) /* pin 2*/ ,
    .q         ( Net__UBR415_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBU401_Pad5_        ) /* pin 5*/ 
);

DFFCOR UBU405(
    .clk       ( Net__UBH331_Pad2_        ) /* pin 1*/ ,
    .d         ( _video_signals_video_signal_generator_BLANK ) /* pin 2*/ ,
    .q         ( Net__UBR419_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UBU405_Pad5_        ) /* pin 5*/ 
);

BUF12 UBU412(
    .i         ( Net__UBR383_Pad5_        ) /* pin 1*/ ,
    .o         ( Net__UBR487_Pad1_        ) /* pin 2*/ 
);

DELAY #(.MiscRef(UBU415;UBU417;UBU418) 
) UBU414(
    .i         ( Net__JP1_Pad1_           ) /* pin 1*/ ,
    .o         ( Net__JP1_Pad2_           ) /* pin 2*/ 
);

DELAY #(.MiscRef(UBU422;UBU424;UBU425) 
) UBU420(
    .i         ( Net__JP1_Pad2_           ) /* pin 1*/ ,
    .o         ( Net__JP2_Pad2_           ) /* pin 2*/ 
);

M367C UBU427(
    .oe_n      ( _debug_~DEBUG8           ) /* pin 1*/ ,
    .a0        ( _raster_interrupts_~RASTER2 ) /* pin 2*/ ,
    .a1        ( _~BUS_RD                 ) /* pin 3*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

DFFCOO #(.MiscRef(UBU437) 
) UBU439(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( _LI                      ) /* pin 2*/ ,
    .q         ( Net__UBU439_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU439_Pad5_        ) /* pin 5*/ 
);

INV01 UBU443(
    .i         ( _FI                      ) /* pin 1*/ ,
    .o         ( Net__UBU443_Pad2_        ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBU444) 
) UBU446(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBU443_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBU446_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU446_Pad5_        ) /* pin 5*/ 
);

AND02 UBU451(
    .i1        ( Net__UBU446_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU443_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBU451_Pad3_        ) /* pin 3*/ 
);

OR02 UBU453(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA15 ) /* pin 2*/ ,
    .o         ( Net__UBU453_Pad3_        ) /* pin 3*/ 
);

M273C #(.MiscRef(UBU456;UBU463;UBU467;UBU472;UBU476;UBU481;UBU485;UBU489;UBU494) 
) UBU458(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UBU458_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA9        ) /* pin 4*/ ,
    .q1        ( Net__UBU458_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UBU458_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA10       ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA11       ) /* pin 8*/ ,
    .q3        ( Net__UBU458_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBU458_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UBU458_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .q5        ( Net__UBU458_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UBU458_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA14       ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA15       ) /* pin 18*/ ,
    .q7        ( Net__UBU458_Pad19_       ) /* pin 19*/ 
);

XNOR02 UBU48(
    .Unknown_pin( Net__UBR25_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU27_Pad4_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBU31_Pad11_        ) /* pin 3*/ 
);

OR02 UBU496(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA13 ) /* pin 2*/ ,
    .o         ( Net__UBU496_Pad3_        ) /* pin 3*/ 
);

M152C UBU498(
    .i3        ( Net__UBU458_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UBU458_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UBU458_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UBU458_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UBU498_Pad5_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UBU458_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UBU458_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UBU458_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UBU458_Pad12_       ) /* pin 15*/ 
);

BUF12 UBU519(
    .i         ( Net__UBU519_Pad1_        ) /* pin 1*/ ,
    .o         ( _LUT7                    ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBU50) 
) UBU52(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU52_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBU24_Pad2_         ) /* pin 3*/ ,
    .q_n       ( Net__UBU52_Pad5_         ) /* pin 5*/ 
);

BUF12 UBU521(
    .i         ( Net__UBN477_Pad2_        ) /* pin 1*/ ,
    .o         ( _LUT2                    ) /* pin 2*/ 
);

DFFCOO UBU56(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU56_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBU24_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UBU56_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBU60(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU60_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBU24_Pad4_         ) /* pin 3*/ ,
    .q_n       ( Net__UBU60_Pad5_         ) /* pin 5*/ 
);

DFFCOO UBU64(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBU64_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UBU24_Pad5_         ) /* pin 3*/ ,
    .q_n       ( Net__UBU64_Pad5_         ) /* pin 5*/ 
);

INV01 UBU68(
    .i         ( Net__UBU24_Pad1_         ) /* pin 1*/ ,
    .o         ( Net__UBU68_Pad2_         ) /* pin 2*/ 
);

NOR02 UBU70(
    .i1        ( Net__UBU109_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU68_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBU70_Pad3_         ) /* pin 3*/ 
);

XOR02 UBU73(
    .i1        ( Net__UBU109_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU68_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBU125_Pad13_       ) /* pin 3*/ 
);

XNOR02 UBU77(
    .Unknown_pin( Net__UBR64_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU77_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR83_Pad11_        ) /* pin 3*/ 
);

M125C UBU80(
    .i1        ( _debug_~DEBUG15          ) /* pin 1*/ ,
    .i2        ( _lut_assembly_layer_control_BUS5 ) /* pin 2*/ ,
    .t3        ( _debug_td_io_IN4         ) /* pin 3*/ 
);

M367C UBU83(
    .oe_n      ( _debug_~DEBUG15          ) /* pin 1*/ ,
    .a0        ( _lut_assembly_layer_control_BUS1 ) /* pin 2*/ ,
    .a1        ( _lut_assembly_layer_control_BUS2 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_layer_control_BUS3 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_layer_control_BUS4 ) /* pin 5*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

NOR03 UBU92(
    .i1        ( Net__UBU68_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBU109_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBU113_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBU92_Pad4_         ) /* pin 4*/ 
);

XNOR02 UBU96(
    .Unknown_pin( Net__UBU113_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU70_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBU125_Pad12_       ) /* pin 3*/ 
);

XNOR02 UBU98(
    .Unknown_pin( Net__UBU117_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU92_Pad4_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBU125_Pad11_       ) /* pin 3*/ 
);

NOR04 UBW107(
    .o1        ( Net__UBW107_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBU68_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBU109_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU113_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBU117_Pad3_        ) /* pin 5*/ 
);

OR02 UBW110(
    .i1        ( Net__UBU161_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBK308_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBW110_Pad3_        ) /* pin 3*/ 
);

NOR05 UBW113(
    .o1        ( _stars_xpd5_sync_ZERO    ) /* pin 1*/ ,
    .i2        ( Net__UBU68_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UBU109_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU117_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBU113_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UBU121_Pad3_        ) /* pin 6*/ 
);

XNOR02 UBW117(
    .Unknown_pin( Net__UBU121_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW107_Pad1_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBU125_Pad10_       ) /* pin 3*/ 
);

OR03 UBW119(
    .i1        ( Net__UBK308_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBU161_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBU165_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBW119_Pad4_        ) /* pin 4*/ 
);

XNOR02 UBW122(
    .Unknown_pin( Net__UBU170_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW119_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW122_Pad3_        ) /* pin 3*/ 
);

INV01 UBW125(
    .i         ( Net__UBN226_Pad12_       ) /* pin 1*/ ,
    .o         ( Net__UBR134_Pad5_        ) /* pin 2*/ 
);

XNOR02 UBW127(
    .Unknown_pin( Net__UBU165_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW110_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW127_Pad3_        ) /* pin 3*/ 
);

XOR02 UBW131(
    .i1        ( Net__UBN226_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR233_Pad5_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus5_sync_PD0 ) /* pin 3*/ 
);

XOR02 UBW134(
    .i1        ( Net__UBN226_Pad5_        ) /* pin 1*/ ,
    .i2        ( Net__UBR233_Pad5_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus5_sync_PD1 ) /* pin 3*/ 
);

XOR02 UBW137(
    .i1        ( Net__UBN226_Pad6_        ) /* pin 1*/ ,
    .i2        ( Net__UBR233_Pad5_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus5_sync_PD2 ) /* pin 3*/ 
);

XOR02 UBW139(
    .i1        ( Net__UBN226_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UBR233_Pad5_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus5_sync_PD3 ) /* pin 3*/ 
);

XOR02 UBW142(
    .i1        ( Net__UBN226_Pad12_       ) /* pin 1*/ ,
    .i2        ( Net__UBR233_Pad5_        ) /* pin 2*/ ,
    .o         ( _stars_xpd_bus5_sync_PD4 ) /* pin 3*/ 
);

XNOR02 UBW146(
    .Unknown_pin( Net__UBK308_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU161_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW146_Pad3_        ) /* pin 3*/ 
);

DFFCOO UBW150(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBW150_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW150_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBW150_Pad5_        ) /* pin 5*/ 
);

BUF12 UBW154(
    .i         ( Net__UBW150_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL1 ) /* pin 2*/ 
);

MUX24H #(.MiscRef(UBW162;UBW165;UBW169;UBW172) 
) UBW160(
    .a3        ( Net__UBU349_Pad10_       ) /* pin 1*/ ,
    .a2        ( Net__UBU349_Pad9_        ) /* pin 2*/ ,
    .a1        ( Net__UBU349_Pad8_        ) /* pin 3*/ ,
    .a0        ( Net__UBU349_Pad7_        ) /* pin 4*/ ,
    .q0        ( Net__UBU161_Pad2_        ) /* pin 5*/ ,
    .q1        ( Net__UBU165_Pad2_        ) /* pin 6*/ ,
    .q2        ( Net__UBU170_Pad2_        ) /* pin 7*/ ,
    .q3        ( Net__UBU174_Pad2_        ) /* pin 8*/ ,
    .anb       ( Net__UBR302_Pad11_       ) /* pin 9*/ ,
    .b3        ( Net__UBR192_Pad3_        ) /* pin 10*/ ,
    .b2        ( Net__UBW122_Pad3_        ) /* pin 11*/ ,
    .b1        ( Net__UBW127_Pad3_        ) /* pin 12*/ ,
    .b0        ( Net__UBW146_Pad3_        ) /* pin 13*/ 
);

AND02 UBW175(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU180_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UBW175_Pad3_        ) /* pin 3*/ 
);

AOI22 UBW177(
    .i1        ( Net__UBW175_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU182_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBW177_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW177_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UBW150_Pad2_        ) /* pin 5*/ 
);

AOI22 UBW180(
    .i1        ( Net__UBW175_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU182_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBW180_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW180_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UBW180_Pad5_        ) /* pin 5*/ 
);

AND02 UBW183(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU451_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBW183_Pad3_        ) /* pin 3*/ 
);

D38GL UBW185(
    .sa        ( _security_management_addr_decoder_registry_CA1 ) /* pin 1*/ ,
    .sb        ( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .sc        ( _security_management_addr_decoder_registry_CA3 ) /* pin 3*/ ,
    .g2_n      ( _security_management_addr_decoder_registry_CA2 ) /* pin 5*/ ,
    .g3_n      ( _security_management_addr_decoder_registry_CA1 ) /* pin 6*/ ,
    .o7_n      ( _security_management_addr_decoder_0x0E ) /* pin 7*/ ,
    .o6_n      ( _security_management_addr_decoder_0x0C ) /* pin 9*/ ,
    .o5_n      ( _security_management_addr_decoder_0x0A ) /* pin 10*/ ,
    .o4_n      ( _security_management_addr_decoder_0x08 ) /* pin 11*/ ,
    .o3_n      ( _security_management_addr_decoder_0x06 ) /* pin 12*/ ,
    .o2_n      ( _security_management_addr_decoder_0x04 ) /* pin 13*/ ,
    .o1_n      ( _security_management_addr_decoder_0x02 ) /* pin 14*/ ,
    .o0_n      ( _security_management_addr_decoder_0x00 ) /* pin 15*/ 
);

OR04 UBW217(
    .i1        ( Net__UBK308_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBU161_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBU165_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU170_Pad3_        ) /* pin 4*/ ,
    .o         ( Net__UBR192_Pad2_        ) /* pin 5*/ 
);

MUX26H #(.MiscRef(UBW223;UBW227;UBW230;UBW233;UBW237;UBW240) 
) UBW221(
    .a0        ( _security_management_addr_decoder_registry_~CHECK2 ) /* pin 1*/ ,
    .a1        ( _security_management_security_decoder_~K_RASTER1 ) /* pin 2*/ ,
    .a2        ( _security_management_security_decoder_~K_RASTER2 ) /* pin 3*/ ,
    .a3        ( _security_management_security_decoder_~K_RASTER3 ) /* pin 4*/ ,
    .a4        ( _security_management_security_decoder_~K_LAYER_CTRL ) /* pin 5*/ ,
    .a5        ( _security_management_security_decoder_~K_PRIORITY0 ) /* pin 6*/ ,
    .b0        ( _security_management_addr_decoder_0x0C ) /* pin 7*/ ,
    .b1        ( _security_management_addr_decoder_0x0E ) /* pin 8*/ ,
    .b2        ( _security_management_addr_decoder_0x10 ) /* pin 9*/ ,
    .b3        ( _security_management_addr_decoder_0x12 ) /* pin 10*/ ,
    .b4        ( _security_management_addr_decoder_0x26 ) /* pin 11*/ ,
    .b5        ( _security_management_addr_decoder_0x28 ) /* pin 12*/ ,
    .q0        ( _~CHECK2                 ) /* pin 13*/ ,
    .q1        ( _raster_interrupts_~RASTER1 ) /* pin 14*/ ,
    .q2        ( _raster_interrupts_~RASTER2 ) /* pin 15*/ ,
    .q3        ( _raster_interrupts_~RASTER3 ) /* pin 16*/ ,
    .q4        ( _lut_assembly_~LAYER_CTRL ) /* pin 17*/ ,
    .q5        ( _lut_assembly_~PRIORITY0 ) /* pin 18*/ ,
    .anb       ( _main_signals_DEFAULT    ) /* pin 19*/ 
);

XNOR02 UBW244(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY27 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW244_Pad3_        ) /* pin 3*/ 
);

D24GL UBW247(
    .sa        ( _security_management_addr_decoder_registry_CA1 ) /* pin 1*/ ,
    .sb        ( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .g_n       ( Net__UBW247_Pad3_        ) /* pin 3*/ ,
    .o3_n      ( _security_management_addr_decoder_0x32 ) /* pin 4*/ ,
    .o2_n      ( _security_management_addr_decoder_0x30 ) /* pin 5*/ ,
    .o1_n      ( _security_management_addr_decoder_0x12 ) /* pin 6*/ ,
    .o0_n      ( _security_management_addr_decoder_0x10 ) /* pin 7*/ 
);

XNOR02 UBW262(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY26 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW262_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBW265(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY25 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW265_Pad3_        ) /* pin 3*/ 
);

AOI21 UBW272(
    .i1        ( Net__UBW272_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBU279_Pad3_        ) /* pin 2*/ ,
    .i3        ( _LI                      ) /* pin 3*/ ,
    .o4        ( Net__UBD342_Pad1_        ) /* pin 4*/ 
);

INV01 UBW274(
    .i         ( Net__UBR365_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UBU271_Pad1_        ) /* pin 2*/ 
);

NOR02 UBW275(
    .i1        ( _lut_io_~E               ) /* pin 1*/ ,
    .i2        ( Net__UBW275_Pad2_        ) /* pin 2*/ ,
    .o         ( _counter_fi_~LUT_RESET   ) /* pin 3*/ 
);

BUF12 UBW278(
    .i         ( Net__UBW278_Pad1_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL4 ) /* pin 2*/ 
);

OR03 UBW280(
    .i1        ( _security_management_addr_decoder_registry_CA2 ) /* pin 1*/ ,
    .i2        ( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .i3        ( Net__UBW280_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBW247_Pad3_        ) /* pin 4*/ 
);

INV01 UBW283(
    .i         ( Net__UBW272_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBU279_Pad1_        ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UBW291) 
) UBW293(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UBW293_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW293_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B8               ) /* pin 5*/ 
);

DFFCOO UBW296(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UBW296_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW296_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B9               ) /* pin 5*/ 
);

DFFCOO UBW300(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UBW300_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW300_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B10              ) /* pin 5*/ 
);

DFFCOO UBW304(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UBW304_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW304_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B11              ) /* pin 5*/ 
);

AOI21 UBW308(
    .i1        ( Net__UBW308_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBW308_Pad2_        ) /* pin 2*/ ,
    .i3        ( _raster_interrupts_counter1_SET_~INC ) /* pin 3*/ ,
    .o4        ( Net__UBH336_Pad11_       ) /* pin 4*/ 
);

NAND02 UBW31(
    .i1        ( _stars_row6_assemble_CNT1_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBN219_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBW31_Pad3_         ) /* pin 3*/ 
);

AOI21 UBW310(
    .i1        ( Net__UBW310_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBW310_Pad2_        ) /* pin 2*/ ,
    .i3        ( _raster_interrupts_counter1_SET_~INC ) /* pin 3*/ ,
    .o4        ( Net__UBR302_Pad11_       ) /* pin 4*/ 
);

DFFCOO #(.MiscRef(UBW313) 
) UBW314(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBU271_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBW310_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UBW314_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBW318(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBR365_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBW310_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UBW318_Pad5_        ) /* pin 5*/ 
);

XNOR02 UBW32(
    .Unknown_pin( _stars_row6_assemble_CNT1_Q1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW31_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW32_Pad3_         ) /* pin 3*/ 
);

M541C UBW328(
    .oe_n      ( _debug_~DEBUG10          ) /* pin 1*/ ,
    .a0        ( _lut_assembly_~LAYER_CTRL ) /* pin 2*/ ,
    .a1        ( _lut_assembly_~PRIORITY0 ) /* pin 3*/ ,
    .a2        ( _lut_assembly_~PRIORITY1 ) /* pin 4*/ ,
    .a3        ( _lut_assembly_~PRIORITY2 ) /* pin 5*/ ,
    .a4        ( _lut_assembly_~PRIORITY3 ) /* pin 6*/ ,
    .a5        ( _~PAL_CTRL               ) /* pin 7*/ ,
    .a6        ( _security_management_~CPS_ID ) /* pin 8*/ ,
    .a7        ( _~SCANLINE               ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

R52 UBW347(
    .le        ( Net__UBW272_Pad1_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS0 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS1 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS2 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS3 ) /* pin 6*/ ,
    .d5_n      ( _raster_interrupts_counter1_~BUS4 ) /* pin 7*/ ,
    .q1        ( Net__UBH336_Pad5_        ) /* pin 8*/ ,
    .q2        ( Net__UBH336_Pad4_        ) /* pin 9*/ ,
    .q3        ( Net__UBH336_Pad3_        ) /* pin 10*/ ,
    .q4        ( Net__UBH336_Pad1_        ) /* pin 11*/ ,
    .q5        ( Net__UBH336_Pad2_        ) /* pin 12*/ 
);

NOR03 UBW35(
    .i1        ( Net__UBU24_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBU24_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UBU24_Pad4_         ) /* pin 3*/ ,
    .o4        ( Net__UBW35_Pad4_         ) /* pin 4*/ 
);

INV01 UBW364(
    .i         ( _bus_io_BUS_DATA1        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS1 ) /* pin 2*/ 
);

INV01 UBW365(
    .i         ( _bus_io_BUS_DATA2        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS2 ) /* pin 2*/ 
);

OR02 UBW366(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _raster_interrupts_~RASTER1 ) /* pin 2*/ ,
    .o         ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBW370;UBW376) 
) UBW371(
    .clk       ( _raster_interrupts_counter3__OUT0 ) /* pin 1*/ ,
    .d         ( Net__UBW371_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBW371_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UBW371_Pad5_        ) /* pin 5*/ 
);

XNOR02 UBW38(
    .Unknown_pin( Net__UBU24_Pad5_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW35_Pad4_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW38_Pad3_         ) /* pin 3*/ 
);

XNOR02 UBW384(
    .Unknown_pin( _~CLK4M                  ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW384_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( _raster_interrupts_counter3__OUT0 ) /* pin 3*/ 
);

OR02 UBW387(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _multiplier_~MSB_E       ) /* pin 2*/ ,
    .o         ( Net__UAF386_Pad71_       ) /* pin 3*/ 
);

NAND02 UBW389(
    .i1        ( Net__UBU376_Pad3_        ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBK460_Pad11_       ) /* pin 3*/ 
);

NAND05 UBW391(
    .i0        ( _security_management_addr_decoder_registry_CA1 ) /* pin 1*/ ,
    .i1        ( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .i2        ( _security_management_addr_decoder_registry_CA3 ) /* pin 3*/ ,
    .i3        ( _security_management_addr_decoder_registry_CA2 ) /* pin 4*/ ,
    .i4        ( _security_management_addr_decoder_registry_CA1 ) /* pin 5*/ ,
    .o         ( _~SCANLINE               ) /* pin 6*/ 
);

INV01 UBW395(
    .i         ( Net__UBW395_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBW280_Pad3_        ) /* pin 2*/ 
);

BUF12 UBW396(
    .i         ( Net__UBW395_Pad1_        ) /* pin 1*/ ,
    .o         ( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ 
);

OR02 UBW398(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _security_management_~MULT_TC ) /* pin 2*/ ,
    .o         ( Net__UBN455_Pad1_        ) /* pin 3*/ 
);

MUX4H UBW401(
    .x0        ( Net__UBW401_Pad1_        ) /* pin 1*/ ,
    .x1        ( Net__UBW401_Pad2_        ) /* pin 2*/ ,
    .x2        ( Net__UBW401_Pad3_        ) /* pin 3*/ ,
    .x3        ( Net__UBW401_Pad4_        ) /* pin 4*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT4 ) /* pin 5*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT5 ) /* pin 6*/ ,
    .q         ( _lut_assembly_priority_masks_OUT ) /* pin 7*/ 
);

XNOR02 UBW41(
    .Unknown_pin( _stars_row6_assemble_CNT1_Q2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW41_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW41_Pad3_         ) /* pin 3*/ 
);

OR02 UBW410(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _raster_interrupts_~RASTER2 ) /* pin 2*/ ,
    .o         ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 3*/ 
);

OR02 UBW412(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA11 ) /* pin 2*/ ,
    .o         ( Net__UBW412_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBW414(
    .Unknown_pin( Net__UBR479_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW414_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW414_Pad3_        ) /* pin 3*/ 
);

NBUF02 UBW417(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UAA423_Pad11_       ) /* pin 2*/ 
);

OR03 UBW419(
    .i1        ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .o4        ( Net__UBW414_Pad2_        ) /* pin 4*/ 
);

AND02 UBW421(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA2 ) /* pin 2*/ ,
    .o         ( Net__UBW180_Pad4_        ) /* pin 3*/ 
);

BUF12 UBW424(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( _~RESETI2                ) /* pin 2*/ 
);

M367C UBW426(
    .oe_n      ( _debug_~DEBUG10          ) /* pin 1*/ ,
    .a0        ( _~BUS_RD                 ) /* pin 2*/ ,
    .a1        ( _~BUS_WR                 ) /* pin 3*/ ,
    .y3        ( _debug_td_io_IN11        ) /* pin 15*/ ,
    .y2        ( _debug_td_io_IN10        ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN9         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN8         ) /* pin 18*/ 
);

NAND03 UBW43(
    .i1        ( _stars_row6_assemble_CNT1_Q1 ) /* pin 1*/ ,
    .i2        ( _stars_row6_assemble_CNT1_Q0 ) /* pin 2*/ ,
    .i3        ( Net__UBN219_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UBW41_Pad2_         ) /* pin 4*/ 
);

NBUF02 UBW436(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( _lut_counter_CLK4M       ) /* pin 2*/ 
);

AND02 UBW438(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA7 ) /* pin 2*/ ,
    .o         ( Net__UBW438_Pad3_        ) /* pin 3*/ 
);

NAND02 UBW440(
    .i1        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBR456_Pad1_        ) /* pin 3*/ 
);

AND02 UBW442(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA1 ) /* pin 2*/ ,
    .o         ( Net__UBW442_Pad3_        ) /* pin 3*/ 
);

AND02 UBW445(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA0 ) /* pin 2*/ ,
    .o         ( Net__UBW177_Pad4_        ) /* pin 3*/ 
);

OR02 UBW447(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA10 ) /* pin 2*/ ,
    .o         ( Net__UBW447_Pad3_        ) /* pin 3*/ 
);

AND02 UBW450(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA6 ) /* pin 2*/ ,
    .o         ( Net__UBW450_Pad3_        ) /* pin 3*/ 
);

BUF12 UBW453(
    .i         ( Net__UBW453_Pad1_        ) /* pin 1*/ ,
    .o         ( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ 
);

M273C #(.MiscRef(UBW455;UBW462;UBW466;UBW471;UBW475;UBW480;UBW484;UBW488;UBW493) 
) UBW457(
    .r_n       ( _~RESETI1                ) /* pin 1*/ ,
    .q0        ( _video_signals_video_counter_DATA0 ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA1        ) /* pin 4*/ ,
    .q1        ( _video_signals_video_counter_DATA1 ) /* pin 5*/ ,
    .q2        ( _video_signals_video_counter_DATA2 ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA2        ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA3        ) /* pin 8*/ ,
    .q3        ( _video_signals_video_counter_DATA3 ) /* pin 9*/ ,
    .clk       ( Net__UBN388_Pad4_        ) /* pin 11*/ ,
    .q4        ( _video_signals_video_counter_DATA4 ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA4        ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .q5        ( _video_signals_video_counter_DATA5 ) /* pin 15*/ ,
    .q6        ( _video_signals_video_counter_DATA6 ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA6        ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA7        ) /* pin 18*/ ,
    .q7        ( _video_signals_video_counter_DATA7 ) /* pin 19*/ 
);

M258C UBW48(
    .a3        ( _stars_xpd_bus5_sync_~PD_OUT3 ) /* pin 1*/ ,
    .a2        ( _stars_xpd_bus5_sync_~PD_OUT2 ) /* pin 2*/ ,
    .a1        ( _stars_xpd_bus5_sync_~PD_OUT1 ) /* pin 3*/ ,
    .a0        ( _stars_xpd_bus5_sync_~PD_OUT0 ) /* pin 4*/ ,
    .q0_n      ( Net__UBU52_Pad2_         ) /* pin 5*/ ,
    .q1_n      ( Net__UBU56_Pad2_         ) /* pin 6*/ ,
    .q2_n      ( Net__UBU60_Pad2_         ) /* pin 7*/ ,
    .q3_n      ( Net__UBU64_Pad2_         ) /* pin 8*/ ,
    .anb       ( Net__UBR50_Pad2_         ) /* pin 9*/ ,
    .b3        ( Net__UBW38_Pad3_         ) /* pin 10*/ ,
    .b2        ( Net__UBW48_Pad11_        ) /* pin 11*/ ,
    .b1        ( Net__UBW48_Pad12_        ) /* pin 12*/ ,
    .b0        ( Net__UBU24_Pad2_         ) /* pin 13*/ ,
    .e_n       ( Net__UBW48_Pad14_        ) /* pin 14*/ 
);

OR02 UBW496(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA12 ) /* pin 2*/ ,
    .o         ( Net__UBW496_Pad3_        ) /* pin 3*/ 
);

TINVBF UBW498(
    .i1        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .i2        ( Net__UBW498_Pad2_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad3_        ) /* pin 3*/ 
);

BUF12 UBW502(
    .i         ( Net__UBW502_Pad1_        ) /* pin 1*/ ,
    .o         ( _LUT6                    ) /* pin 2*/ 
);

BUF12 UBW504(
    .i         ( Net__UBW504_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBU519_Pad1_        ) /* pin 2*/ 
);

TINVBF UBW505(
    .i1        ( Net__UBW505_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK499_Pad5_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad3_        ) /* pin 3*/ 
);

TINVBF UBW509(
    .i1        ( Net__UBW505_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBU498_Pad5_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad4_        ) /* pin 3*/ 
);

BUF12 UBW515(
    .i         ( Net__UBW515_Pad1_        ) /* pin 1*/ ,
    .o         ( _LUT5                    ) /* pin 2*/ 
);

BUF12 UBW517(
    .i         ( Net__UBW517_Pad1_        ) /* pin 1*/ ,
    .o         ( _LUT4                    ) /* pin 2*/ 
);

NAND02 UBW522(
    .i1        ( Net__UBR374_Pad3_        ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBU458_Pad11_       ) /* pin 3*/ 
);

XOR02 UBW63(
    .i1        ( _stars_row6_assemble_CNT1_Q0 ) /* pin 1*/ ,
    .i2        ( Net__UBN219_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBW63_Pad3_         ) /* pin 3*/ 
);

DFFCOO UBW69(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD0 ) /* pin 2*/ ,
    .q         ( Net__UBW69_Pad3_         ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus5_sync_~PD_OUT0 ) /* pin 5*/ 
);

FFCOO #(.MiscRef(UBW66) 
) UBW73(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD1 ) /* pin 2*/ ,
    .q         ( Net__UBW73_Pad3_         ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus5_sync_~PD_OUT1 ) /* pin 5*/ 
);

DFFCOO UBW76(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD2 ) /* pin 2*/ ,
    .q         ( Net__UBW76_Pad3_         ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus5_sync_~PD_OUT2 ) /* pin 5*/ 
);

L8 #(.MiscRef(UBW7;UBW11;UBW14;UBW17;UBW20;UBW22;UBW25;UBW28) 
) UBW8(
    .le        ( Net__UBW8_Pad1_          ) /* pin 1*/ ,
    .d8        ( _bus_io_BUS_DATA7        ) /* pin 2*/ ,
    .q8        ( Net__UBW8_Pad3_          ) /* pin 3*/ ,
    .d7        ( _bus_io_BUS_DATA6        ) /* pin 4*/ ,
    .q7        ( Net__UBW8_Pad5_          ) /* pin 5*/ ,
    .d6        ( _bus_io_BUS_DATA5        ) /* pin 6*/ ,
    .q6        ( Net__UBW8_Pad7_          ) /* pin 7*/ ,
    .d5        ( _bus_io_BUS_DATA4        ) /* pin 8*/ ,
    .q5        ( Net__UBW8_Pad9_          ) /* pin 9*/ ,
    .d4        ( _bus_io_BUS_DATA3        ) /* pin 10*/ ,
    .q4        ( Net__UBW8_Pad11_         ) /* pin 11*/ ,
    .d3        ( _bus_io_BUS_DATA2        ) /* pin 12*/ ,
    .q3        ( Net__UBW8_Pad13_         ) /* pin 13*/ ,
    .d2        ( _bus_io_BUS_DATA1        ) /* pin 14*/ ,
    .q2        ( Net__UBW8_Pad15_         ) /* pin 15*/ ,
    .d1        ( _bus_io_BUS_DATA0        ) /* pin 16*/ ,
    .q1        ( Net__UBW8_Pad17_         ) /* pin 17*/ 
);

DFFCOO UBW80(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD3 ) /* pin 2*/ ,
    .q         ( Net__UBW80_Pad3_         ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus5_sync_~PD_OUT3 ) /* pin 5*/ 
);

DFFCOO UBW84(
    .clk       ( Net__UBU141_Pad1_        ) /* pin 1*/ ,
    .d         ( _stars_xpd_bus5_sync_PD4 ) /* pin 2*/ ,
    .q         ( Net__UBW84_Pad3_         ) /* pin 3*/ ,
    .q_n       ( _stars_xpd_bus5_sync_~PD_OUT4 ) /* pin 5*/ 
);

XNOR02 UBW88(
    .Unknown_pin( _stars_row6_assemble_CNT1_Q3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBU20_Pad5_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW88_Pad3_         ) /* pin 3*/ 
);

NAND02 UBW93(
    .i1        ( _CLK8M                   ) /* pin 1*/ ,
    .i2        ( _stars_STARS_SERIAL      ) /* pin 2*/ ,
    .o         ( Net__UBU141_Pad1_        ) /* pin 3*/ 
);

NAND02 UBW94(
    .i1        ( Net__UBN217_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW63_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UBN196_Pad2_        ) /* pin 3*/ 
);

NAND02 UBW96(
    .i1        ( Net__UBN217_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW32_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UBN200_Pad2_        ) /* pin 3*/ 
);

NAND02 UBW98(
    .i1        ( Net__UBN217_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW41_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UBN204_Pad2_        ) /* pin 3*/ 
);

NAND02 UBW99(
    .i1        ( Net__UBN217_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW88_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UBN208_Pad2_        ) /* pin 3*/ 
);

DFFCOO UBZ10(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS2                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_XS_OUT2  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ10_Pad5_         ) /* pin 5*/ 
);

MUX25H #(.MiscRef(UBZ109;UBZ112;UBZ115;UBZ119;UBZ122) 
) UBZ106(
    .a3        ( _security_management_security_decoder_K_LAYER_D4 ) /* pin 1*/ ,
    .a4        ( _security_management_security_decoder_K_LAYER_D5 ) /* pin 2*/ ,
    .a2        ( _security_management_security_decoder_K_LAYER_D3 ) /* pin 3*/ ,
    .a1        ( _security_management_security_decoder_K_LAYER_D2 ) /* pin 4*/ ,
    .a0        ( _security_management_security_decoder_K_LAYER_D1 ) /* pin 5*/ ,
    .q0        ( _lut_assembly_layer_control_BUS1 ) /* pin 6*/ ,
    .q1        ( _lut_assembly_layer_control_BUS2 ) /* pin 7*/ ,
    .q2        ( _lut_assembly_layer_control_BUS3 ) /* pin 8*/ ,
    .q3        ( _lut_assembly_layer_control_BUS4 ) /* pin 9*/ ,
    .q4        ( _lut_assembly_layer_control_BUS5 ) /* pin 10*/ ,
    .anb       ( _main_signals_DEFAULT    ) /* pin 11*/ ,
    .b3        ( _bus_io_BUS_DATA4        ) /* pin 12*/ ,
    .b4        ( _bus_io_BUS_DATA5        ) /* pin 13*/ ,
    .b2        ( _bus_io_BUS_DATA3        ) /* pin 14*/ ,
    .b1        ( _bus_io_BUS_DATA2        ) /* pin 15*/ ,
    .b0        ( _bus_io_BUS_DATA1        ) /* pin 16*/ 
);

DFFCOO UBZ126(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBW180_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ126_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ126_Pad5_        ) /* pin 5*/ 
);

BUF12 UBZ131(
    .i         ( Net__UBZ126_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL3 ) /* pin 2*/ 
);

INV01 UBZ133(
    .i         ( _FI                      ) /* pin 1*/ ,
    .o         ( Net__UBR161_Pad1_        ) /* pin 2*/ 
);

XNOR02 UBZ135(
    .Unknown_pin( Net__UBZ126_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ135_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW180_Pad3_        ) /* pin 3*/ 
);

NOR03 UBZ138(
    .i1        ( Net__UBZ138_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBW150_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ138_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBZ135_Pad2_        ) /* pin 4*/ 
);

DFFCOO UBZ14(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS3                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_XS_OUT3  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ14_Pad5_         ) /* pin 5*/ 
);

NOR04 UBZ141(
    .o1        ( Net__UBZ141_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ126_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ138_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UBW150_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ138_Pad3_        ) /* pin 5*/ 
);

XNOR02 UBZ144(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY123 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ144_Pad3_        ) /* pin 3*/ 
);

NAND05 UBZ147(
    .i0        ( Net__UBZ147_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UBZ147_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UBZ147_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UBZ144_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UBZ147_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_LAYER_CTRL ) /* pin 6*/ 
);

DFFCOO UBZ150(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ150_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UBZ150_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ150_Pad5_        ) /* pin 5*/ 
);

NAND02 UBZ155(
    .i1        ( _debug_~DEBUG11          ) /* pin 1*/ ,
    .i2        ( _debug_~DEBUG11          ) /* pin 2*/ ,
    .o         ( Net__UBZ138_Pad3_        ) /* pin 3*/ 
);

AOI22 UBZ159(
    .i1        ( Net__UBZ159_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ159_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ159_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW450_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ159_Pad5_        ) /* pin 5*/ 
);

AOI22 UBZ162(
    .i1        ( Net__UBZ159_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ159_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ162_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW438_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ162_Pad5_        ) /* pin 5*/ 
);

XOR02 UBZ165(
    .i1        ( Net__UBZ138_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW150_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBW177_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBZ168(
    .Unknown_pin( Net__UBZ150_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ168_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ168_Pad3_        ) /* pin 3*/ 
);

AOI22 UBZ171(
    .i1        ( Net__UBZ159_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ159_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ168_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ171_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ150_Pad2_        ) /* pin 5*/ 
);

NOR02 UBZ173(
    .i1        ( Net__UBZ138_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBW150_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBZ173_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBZ176(
    .Unknown_pin( Net__UBZ138_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ173_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ176_Pad3_        ) /* pin 3*/ 
);

AOI22 UBZ179(
    .i1        ( Net__UBW175_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU182_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ176_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW442_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ179_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBZ18(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS4                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_XS_OUT4  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ18_Pad5_         ) /* pin 5*/ 
);

XNOR02 UBZ182(
    .Unknown_pin( Net__UBZ182_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ141_Pad1_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ182_Pad3_        ) /* pin 3*/ 
);

AOI22 UBZ185(
    .i1        ( Net__UBW175_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU182_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ182_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ185_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ185_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBZ187(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ185_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ182_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UBW278_Pad1_        ) /* pin 5*/ 
);

INV01 UBZ192(
    .i         ( Net__UBZ192_Pad1_        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~ZERO ) /* pin 2*/ 
);

NOR05 UBZ193(
    .o1        ( Net__UBZ192_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBK308_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBU161_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU170_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBU165_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UBR192_Pad1_        ) /* pin 6*/ 
);

NAND02 UBZ197(
    .i1        ( Net__UBU265_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU265_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBZ197_Pad3_        ) /* pin 3*/ 
);

INV01 UBZ199(
    .i         ( Net__UBU265_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UBZ199_Pad2_        ) /* pin 2*/ 
);

NOR05 UBZ200(
    .o1        ( Net__UBZ200_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ200_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ200_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ200_Pad5_        ) /* pin 5*/ ,
    .i6        ( Net__UBZ199_Pad2_        ) /* pin 6*/ 
);

OR02 UBZ203(
    .i1        ( _~CHECK1                 ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .o         ( Net__UBZ203_Pad3_        ) /* pin 3*/ 
);

NOR04 UBZ205(
    .o1        ( Net__UBZ205_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ200_Pad5_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ200_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ197_Pad3_        ) /* pin 5*/ 
);

XNOR02 UBZ209(
    .Unknown_pin( Net__UBZ200_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ205_Pad1_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ209_Pad3_        ) /* pin 3*/ 
);

XOR02 UBZ217(
    .i1        ( Net__UBZ197_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBZ217_Pad3_        ) /* pin 3*/ 
);

NOR03 UBZ220(
    .i1        ( Net__UBZ200_Pad5_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad4_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ197_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UBZ220_Pad4_        ) /* pin 4*/ 
);

NOR02 UBZ223(
    .i1        ( Net__UBZ197_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBZ223_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBZ225(
    .Unknown_pin( Net__UBZ200_Pad5_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ223_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ225_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBZ229(
    .Unknown_pin( Net__UBZ200_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ220_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ229_Pad3_        ) /* pin 3*/ 
);

AOI22 UBZ232(
    .i1        ( Net__UBW183_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ232_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ225_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ232_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ232_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBZ234(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ232_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ200_Pad5_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ234_Pad5_        ) /* pin 5*/ 
);

AOI22 UBZ240(
    .i1        ( Net__UBW183_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ232_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ209_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW412_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ240_Pad5_        ) /* pin 5*/ 
);

INV01 UBZ242(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBW275_Pad2_        ) /* pin 2*/ 
);

DFFCOO UBZ243(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ240_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ200_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ243_Pad5_        ) /* pin 5*/ 
);

MUX25H #(.MiscRef(UBZ251;UBZ254;UBZ257;UBZ261;UBZ264) 
) UBZ248(
    .a3        ( _security_management_security_decoder_~K_PAL_CTRL ) /* pin 1*/ ,
    .a4        ( _security_management_addr_decoder_registry_~CPS_ID ) /* pin 2*/ ,
    .a2        ( _security_management_security_decoder_~K_PRIORITY3 ) /* pin 3*/ ,
    .a1        ( _security_management_security_decoder_~K_PRIORITY2 ) /* pin 4*/ ,
    .a0        ( _security_management_security_decoder_~K_PRIORITY1 ) /* pin 5*/ ,
    .q0        ( _lut_assembly_~PRIORITY1 ) /* pin 6*/ ,
    .q1        ( _lut_assembly_~PRIORITY2 ) /* pin 7*/ ,
    .q2        ( _lut_assembly_~PRIORITY3 ) /* pin 8*/ ,
    .q3        ( _~PAL_CTRL               ) /* pin 9*/ ,
    .q4        ( _security_management_~CPS_ID ) /* pin 10*/ ,
    .anb       ( _main_signals_DEFAULT    ) /* pin 11*/ ,
    .b3        ( _security_management_addr_decoder_0x30 ) /* pin 12*/ ,
    .b4        ( _security_management_addr_decoder_0x32 ) /* pin 13*/ ,
    .b2        ( _security_management_addr_decoder_0x2E ) /* pin 14*/ ,
    .b1        ( _security_management_addr_decoder_0x2C ) /* pin 15*/ ,
    .b0        ( _security_management_addr_decoder_0x2A ) /* pin 16*/ 
);

DFFCOO #(.MiscRef(UBZ23) 
) UBZ26(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS0                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_XS_OUT0  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ26_Pad5_         ) /* pin 5*/ 
);

AOI22 UBZ269(
    .i1        ( Net__UBW183_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ232_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ229_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW447_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UBZ269_Pad5_        ) /* pin 5*/ 
);

NAND05 UBZ272(
    .i0        ( Net__UBZ272_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UBW265_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UBW262_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UBW244_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UBZ272_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_PAL_CTRL ) /* pin 6*/ 
);

DFFCOO #(.MiscRef(UBZ276) 
) UBZ277(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBU279_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UBW308_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ277_Pad5_        ) /* pin 5*/ 
);

DFFCOO UBZ281(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBW272_Pad1_        ) /* pin 2*/ ,
    .q         ( Net__UBW308_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UBZ281_Pad5_        ) /* pin 5*/ 
);

XNOR02 UBZ292(
    .Unknown_pin( _lut_io_B10              ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ292_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ292_Pad3_        ) /* pin 3*/ 
);

XOR02 UBZ294(
    .i1        ( Net__UBZ294_Pad1_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B8               ) /* pin 2*/ ,
    .o         ( Net__UBZ294_Pad3_        ) /* pin 3*/ 
);

INV01 UBZ298(
    .i         ( Net__UBZ298_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBZ298_Pad2_        ) /* pin 2*/ 
);

M258C UBZ299(
    .a3        ( _palette_control_BASE11  ) /* pin 1*/ ,
    .a2        ( _palette_control_BASE10  ) /* pin 2*/ ,
    .a1        ( _palette_control_BASE9   ) /* pin 3*/ ,
    .q0_n      ( Net__UBW293_Pad2_        ) /* pin 5*/ ,
    .q1_n      ( Net__UBW296_Pad2_        ) /* pin 6*/ ,
    .q2_n      ( Net__UBW300_Pad2_        ) /* pin 7*/ ,
    .q3_n      ( Net__UBW304_Pad2_        ) /* pin 8*/ ,
    .anb       ( _~LUT_OE                 ) /* pin 9*/ ,
    .b3        ( Net__UBZ299_Pad10_       ) /* pin 10*/ ,
    .b2        ( Net__UBZ292_Pad3_        ) /* pin 11*/ ,
    .b1        ( Net__UBZ299_Pad12_       ) /* pin 12*/ ,
    .b0        ( Net__UBZ294_Pad3_        ) /* pin 13*/ ,
    .e_n       ( Net__UBZ298_Pad2_        ) /* pin 14*/ 
);

DFFCOO #(.MiscRef(UBZ0) 
) UBZ3(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS0                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_XS_OUT0  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ3_Pad5_          ) /* pin 5*/ 
);

DFFCOO UBZ30(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS1                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_XS_OUT1  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ30_Pad5_         ) /* pin 5*/ 
);

NOR02 UBZ313(
    .i1        ( _raster_interrupts_~RASTER2 ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .o         ( Net__UBW272_Pad1_        ) /* pin 3*/ 
);

NOR02 UBZ315(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_~PRIORITY0 ) /* pin 2*/ ,
    .o         ( Net__UBZ315_Pad3_        ) /* pin 3*/ 
);

NAND04 UBZ318(
    .i1        ( _lut_io_B2               ) /* pin 1*/ ,
    .i2        ( _lut_io_B1               ) /* pin 2*/ ,
    .i3        ( _lut_io_B0               ) /* pin 3*/ ,
    .i4        ( Net__UBZ318_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UBZ318_Pad5_        ) /* pin 5*/ 
);

BUF12 UBZ320(
    .i         ( _palette_control_CPY_LUT ) /* pin 1*/ ,
    .o         ( Net__UBZ318_Pad4_        ) /* pin 2*/ 
);

INV01 UBZ328(
    .i         ( Net__UBZ328_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBZ328_Pad2_        ) /* pin 2*/ 
);

NAND05 UBZ329(
    .i0        ( _lut_io_B0               ) /* pin 1*/ ,
    .i1        ( _lut_io_B1               ) /* pin 2*/ ,
    .i2        ( _lut_io_B2               ) /* pin 3*/ ,
    .i3        ( _lut_io_B3               ) /* pin 4*/ ,
    .i4        ( Net__UBZ318_Pad4_        ) /* pin 5*/ ,
    .o         ( Net__UBZ328_Pad1_        ) /* pin 6*/ 
);

NOR02 UBZ333(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _lut_assembly_~PRIORITY1 ) /* pin 2*/ ,
    .o         ( Net__UBZ333_Pad3_        ) /* pin 3*/ 
);

NAND02 UBZ335(
    .i1        ( Net__UBZ333_Pad3_        ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBZ335_Pad3_        ) /* pin 3*/ 
);

INV01 UBZ337(
    .i         ( Net__UBZ337_Pad1_        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter3_~ZERO ) /* pin 2*/ 
);

XNOR02 UBZ338(
    .Unknown_pin( Net__UBZ338_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ338_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ338_Pad3_        ) /* pin 3*/ 
);

DFFCOO UBZ34(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS2                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_XS_OUT2  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ34_Pad5_         ) /* pin 5*/ 
);

XNOR02 UBZ341(
    .Unknown_pin( Net__UBZ341_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ341_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ341_Pad3_        ) /* pin 3*/ 
);

NOR02 UBZ344(
    .i1        ( _raster_interrupts_~RASTER1 ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .o         ( Net__UBZ344_Pad3_        ) /* pin 3*/ 
);

NOR05 UBZ346(
    .o1        ( Net__UBZ337_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ346_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ346_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ341_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UBZ338_Pad1_        ) /* pin 6*/ 
);

OR04 UBZ349(
    .i1        ( Net__UBZ346_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ346_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UBZ338_Pad2_        ) /* pin 5*/ 
);

AOI21 UBZ353(
    .i1        ( _raster_interrupts_counter2_~ZERO ) /* pin 1*/ ,
    .i2        ( _raster_interrupts_counter1_~ZERO ) /* pin 2*/ ,
    .i3        ( _raster_interrupts_counter3_~ZERO ) /* pin 3*/ ,
    .o4        ( Net__UBW371_Pad2_        ) /* pin 4*/ 
);

OR02 UBZ355(
    .i1        ( Net__UBR468_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR462_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ355_Pad3_        ) /* pin 3*/ 
);

XNOR02 UBZ357(
    .Unknown_pin( Net__UBR474_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ355_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ357_Pad3_        ) /* pin 3*/ 
);

R52 UBZ360(
    .le        ( Net__UBZ344_Pad3_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS0 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS1 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS2 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS3 ) /* pin 6*/ ,
    .d5_n      ( _raster_interrupts_counter1_~BUS4 ) /* pin 7*/ ,
    .q1        ( Net__UBW384_Pad2_        ) /* pin 8*/ ,
    .q2        ( Net__UBZ360_Pad9_        ) /* pin 9*/ ,
    .q3        ( Net__UBZ360_Pad10_       ) /* pin 10*/ ,
    .q4        ( Net__UBZ360_Pad11_       ) /* pin 11*/ ,
    .q5        ( Net__UBZ360_Pad12_       ) /* pin 12*/ 
);

INV01 UBZ377(
    .i         ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBZ377_Pad2_        ) /* pin 2*/ 
);

DFFCOO UBZ38(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS3                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_XS_OUT3  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ38_Pad5_         ) /* pin 5*/ 
);

MUX24H #(.MiscRef(UBZ385;UBZ388;UBZ391;UBZ395) 
) UBZ383(
    .a3        ( Net__UBZ360_Pad12_       ) /* pin 1*/ ,
    .a2        ( Net__UBZ360_Pad11_       ) /* pin 2*/ ,
    .a1        ( Net__UBZ360_Pad10_       ) /* pin 3*/ ,
    .a0        ( Net__UBZ360_Pad9_        ) /* pin 4*/ ,
    .q0        ( Net__UBZ383_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UBZ383_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBZ383_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UBZ383_Pad8_        ) /* pin 8*/ ,
    .anb       ( Net__UBZ383_Pad9_        ) /* pin 9*/ ,
    .b3        ( Net__UBW414_Pad3_        ) /* pin 10*/ ,
    .b2        ( Net__UBZ357_Pad3_        ) /* pin 11*/ ,
    .b1        ( Net__UBZ383_Pad12_       ) /* pin 12*/ ,
    .b0        ( Net__UBZ377_Pad2_        ) /* pin 13*/ 
);

MUX24H #(.MiscRef(UBZ400;UBZ403;UBZ406;UBZ410) 
) UBZ398(
    .a3        ( Net__UBZ398_Pad1_        ) /* pin 1*/ ,
    .a2        ( Net__UBZ398_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UBZ398_Pad3_        ) /* pin 3*/ ,
    .a0        ( Net__UBZ398_Pad4_        ) /* pin 4*/ ,
    .q0        ( Net__UBZ398_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UBZ398_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBZ398_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UBZ398_Pad8_        ) /* pin 8*/ ,
    .anb       ( Net__UBZ383_Pad9_        ) /* pin 9*/ ,
    .b3        ( Net__UBZ338_Pad3_        ) /* pin 10*/ ,
    .b2        ( Net__UBZ398_Pad11_       ) /* pin 11*/ ,
    .b1        ( Net__UBZ341_Pad3_        ) /* pin 12*/ ,
    .b0        ( Net__UBZ398_Pad13_       ) /* pin 13*/ 
);

R42 UBZ413(
    .le        ( Net__UBZ344_Pad3_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS5 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS6 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS7 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS8 ) /* pin 6*/ ,
    .q1        ( Net__UBZ398_Pad4_        ) /* pin 7*/ ,
    .q2        ( Net__UBZ398_Pad3_        ) /* pin 8*/ ,
    .q3        ( Net__UBZ398_Pad2_        ) /* pin 9*/ ,
    .q4        ( Net__UBZ398_Pad1_        ) /* pin 10*/ 
);

DFFCOO UBZ42(
    .clk       ( Net__UBN226_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS4                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus5_XS_OUT4  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ42_Pad5_         ) /* pin 5*/ 
);

NOR02 UBZ428(
    .i1        ( _LI                      ) /* pin 1*/ ,
    .i2        ( Net__UBU439_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBZ383_Pad9_        ) /* pin 3*/ 
);

AND02 UBZ430(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA5 ) /* pin 2*/ ,
    .o         ( Net__UBZ171_Pad4_        ) /* pin 3*/ 
);

NBUF02 UBZ432(
    .i         ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .o         ( Net__UBW421_Pad1_        ) /* pin 2*/ 
);

BUF12 UBZ434(
    .i         ( Net__UBZ434_Pad1_        ) /* pin 1*/ ,
    .o         ( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ 
);

BUF12 UBZ437(
    .i         ( Net__UBZ437_Pad1_        ) /* pin 1*/ ,
    .o         ( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ 
);

DFGOO #(.MiscRef(UBZ438) 
) UBZ440(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA5        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBZ446(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ341_Pad1_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA6        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBZ452(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ346_Pad4_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA7        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBZ458(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ338_Pad1_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter3_~OUT_ENABLE ) /* pin 5*/ 
);

OR03 UBZ465(
    .i1        ( _~BUS_WR                 ) /* pin 1*/ ,
    .i2        ( _~PAL_CTRL               ) /* pin 2*/ ,
    .i3        ( _lut_counter_CLK4M       ) /* pin 3*/ ,
    .o4        ( Net__UBZ465_Pad4_        ) /* pin 4*/ 
);

AND02 UBZ467(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA4 ) /* pin 2*/ ,
    .o         ( Net__UBZ467_Pad3_        ) /* pin 3*/ 
);

AND02 UBZ470(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA3 ) /* pin 2*/ ,
    .o         ( Net__UBZ185_Pad4_        ) /* pin 3*/ 
);

OAI211 UBZ473(
    .i1        ( _~CLK4M                  ) /* pin 1*/ ,
    .i2        ( _palette_control_CPY_LUT ) /* pin 2*/ ,
    .i3        ( Net__UBZ473_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ473_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UBR491_Pad1_        ) /* pin 5*/ 
);

AND02 UBZ475(
    .i1        ( Net__UBW421_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA9 ) /* pin 2*/ ,
    .o         ( Net__UBZ232_Pad4_        ) /* pin 3*/ 
);

TINVBF UBZ477(
    .i1        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .i2        ( Net__UBZ477_Pad2_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad4_        ) /* pin 3*/ 
);

INV01 UBZ48(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBZ48_Pad2_         ) /* pin 2*/ 
);

L #(.MiscRef(UBZ481) 
) UBZ482(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d         ( _CA1                     ) /* pin 2*/ ,
    .q         ( Net__UBZ482_Pad3_        ) /* pin 3*/ 
);

L UBZ485(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d         ( _CA2                     ) /* pin 2*/ ,
    .q         ( Net__UBZ434_Pad1_        ) /* pin 3*/ 
);

L UBZ488(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d         ( _CA3                     ) /* pin 2*/ ,
    .q         ( Net__UBZ437_Pad1_        ) /* pin 3*/ 
);

INV01 UBZ49(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBW48_Pad14_        ) /* pin 2*/ 
);

L UBZ491(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d         ( _CA4                     ) /* pin 2*/ ,
    .q         ( Net__UBW395_Pad1_        ) /* pin 3*/ 
);

L UBZ493(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d         ( _CA5                     ) /* pin 2*/ ,
    .q         ( Net__UBW453_Pad1_        ) /* pin 3*/ 
);

DFGOO #(.MiscRef(UBZ496;UBZ521) 
) UBZ498(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ498_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA5        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBZ504(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ504_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA6        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

DFGOO UBZ509(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ509_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA7        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

INV01 UBZ51(
    .i         ( Net__UBZ51_Pad1_         ) /* pin 1*/ ,
    .o         ( Net__UBW8_Pad1_          ) /* pin 2*/ 
);

DFGOO UBZ515(
    .clk       ( Net__UBD484_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ515_Pad2_        ) /* pin 2*/ ,
    .q         ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .i5        ( _raster_interrupts_counter2_~OUT_ENABLE ) /* pin 5*/ 
);

XOR02 UBZ52(
    .i1        ( Net__UBU24_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBU24_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBW48_Pad12_        ) /* pin 3*/ 
);

NOR02 UBZ55(
    .i1        ( Net__UBU24_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBU24_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBZ55_Pad3_         ) /* pin 3*/ 
);

XNOR02 UBZ58(
    .Unknown_pin( Net__UBU24_Pad4_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ55_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBW48_Pad11_        ) /* pin 3*/ 
);

INV01 UBZ62(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UBZ62_Pad2_         ) /* pin 2*/ 
);

M258C UBZ66(
    .a3        ( _stars_xpd_bus5_XS_OUT3  ) /* pin 1*/ ,
    .a2        ( _stars_xpd_bus5_XS_OUT2  ) /* pin 2*/ ,
    .a1        ( _stars_xpd_bus5_XS_OUT1  ) /* pin 3*/ ,
    .a0        ( _stars_xpd_bus5_XS_OUT0  ) /* pin 4*/ ,
    .q0_n      ( Net__UBZ66_Pad5_         ) /* pin 5*/ ,
    .q1_n      ( Net__UBZ66_Pad6_         ) /* pin 6*/ ,
    .q2_n      ( Net__UBZ66_Pad7_         ) /* pin 7*/ ,
    .q3_n      ( Net__UBZ66_Pad8_         ) /* pin 8*/ ,
    .anb       ( Net__UBU31_Pad9_         ) /* pin 9*/ ,
    .b3        ( Net__UBZ66_Pad10_        ) /* pin 10*/ ,
    .b2        ( Net__UBZ66_Pad11_        ) /* pin 11*/ ,
    .b1        ( Net__UBZ66_Pad12_        ) /* pin 12*/ ,
    .b0        ( Net__UBZ66_Pad13_        ) /* pin 13*/ ,
    .e_n       ( Net__UBZ62_Pad2_         ) /* pin 14*/ 
);

DFFCOO UBZ7(
    .clk       ( Net__UBD222_Pad11_       ) /* pin 1*/ ,
    .d         ( _XS1                     ) /* pin 2*/ ,
    .q         ( _stars_xpd_bus6_XS_OUT1  ) /* pin 3*/ ,
    .q_n       ( Net__UBZ7_Pad5_          ) /* pin 5*/ 
);

NOR03 UBZ80(
    .i1        ( Net__UBR52_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UBR52_Pad1_         ) /* pin 2*/ ,
    .i3        ( Net__UBR60_Pad3_         ) /* pin 3*/ ,
    .o4        ( Net__UBU77_Pad2_         ) /* pin 4*/ 
);

NOR02 UBZ83(
    .i1        ( Net__UBR52_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBR52_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UBZ83_Pad3_         ) /* pin 3*/ 
);

INV01 UBZ86(
    .i         ( _stars_STARS_SERIAL      ) /* pin 1*/ ,
    .o         ( Net__UBU31_Pad9_         ) /* pin 2*/ 
);

XNOR02 UBZ87(
    .Unknown_pin( Net__UBR60_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ83_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBR83_Pad12_        ) /* pin 3*/ 
);

OR02 UBZ90(
    .i1        ( _~CHECK2                 ) /* pin 1*/ ,
    .i2        ( _~BUS_WR                 ) /* pin 2*/ ,
    .o         ( Net__UBZ51_Pad1_         ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UBZ93;UBZ99) 
) UBZ94(
    .clk       ( Net__UBZ51_Pad1_         ) /* pin 1*/ ,
    .d         ( Net__UBZ94_Pad2_         ) /* pin 2*/ ,
    .q         ( _counter_fi_INC_E        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UBZ94_Pad5_         ) /* pin 5*/ 
);

XNOR02 UCC107(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY116 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC107_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCC112(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY118 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC112_Pad3_        ) /* pin 3*/ 
);

NAND05 UCC115(
    .i0        ( Net__UCC115_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCC107_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCC115_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCC112_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCC115_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_RASTER1 ) /* pin 6*/ 
);

INV01 UCC119(
    .i         ( _stars_STARS_SERIAL      ) /* pin 1*/ ,
    .o         ( Net__UCC119_Pad2_        ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UCC10) 
) UCC12(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC12_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCC12_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC12_Pad5_         ) /* pin 5*/ 
);

AND02 UCC121(
    .i1        ( _~CLK4M                  ) /* pin 1*/ ,
    .i2        ( _FI                      ) /* pin 2*/ ,
    .o         ( Net__UCC121_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCC124(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY119 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC115_Pad5_        ) /* pin 3*/ 
);

INV01 UCC130(
    .i         ( _debug_~DEBUG11          ) /* pin 1*/ ,
    .o         ( Net__UCC130_Pad2_        ) /* pin 2*/ 
);

NOR05 UCC131(
    .o1        ( Net__UCC131_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ182_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ126_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW150_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ138_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UCC130_Pad2_        ) /* pin 6*/ 
);

XNOR02 UCC136(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY124 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ147_Pad5_        ) /* pin 3*/ 
);

XNOR02 UCC139(
    .Unknown_pin( Net__UCC139_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC139_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ159_Pad3_        ) /* pin 3*/ 
);

INV01 UCC141(
    .i         ( Net__UCC131_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCC141_Pad2_        ) /* pin 2*/ 
);

NAND02 UCC143(
    .i1        ( Net__UCC131_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC131_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UCC143_Pad3_        ) /* pin 3*/ 
);

NOR05 UCC144(
    .o1        ( Net__UCC144_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC144_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCC139_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UCC144_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ150_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UCC141_Pad2_        ) /* pin 6*/ 
);

NOR03 UCC147(
    .i1        ( Net__UBZ150_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC144_Pad4_        ) /* pin 2*/ ,
    .i3        ( Net__UCC143_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UCC139_Pad2_        ) /* pin 4*/ 
);

XNOR02 UCC150(
    .Unknown_pin( Net__UCC144_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC150_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ162_Pad3_        ) /* pin 3*/ 
);

NOR04 UCC153(
    .o1        ( Net__UCC150_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCC139_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ150_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCC144_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCC143_Pad3_        ) /* pin 5*/ 
);

XNOR02 UCC159(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY38 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC159_Pad3_        ) /* pin 3*/ 
);

DFFCOO UCC16(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC16_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCC16_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC16_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCC161(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ159_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCC139_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC161_Pad5_        ) /* pin 5*/ 
);

XOR02 UCC166(
    .i1        ( Net__UCC143_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC144_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UCC166_Pad3_        ) /* pin 3*/ 
);

NOR02 UCC169(
    .i1        ( Net__UCC143_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC144_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBZ168_Pad2_        ) /* pin 3*/ 
);

BUF12 UCC172(
    .i         ( Net__UBZ150_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL6 ) /* pin 2*/ 
);

AOI22 UCC174(
    .i1        ( Net__UBZ159_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ159_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCC166_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ467_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UCC174_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC176(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCC174_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCC144_Pad4_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC176_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC181(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ179_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ138_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC181_Pad5_        ) /* pin 5*/ 
);

AND02 UCC185(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU180_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UBZ159_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCC188(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY35 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC188_Pad3_        ) /* pin 3*/ 
);

DFFCOO UCC19(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC19_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCC19_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC19_Pad5_         ) /* pin 5*/ 
);

BUF12 UCC192(
    .i         ( Net__UCC176_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL5 ) /* pin 2*/ 
);

NAND05 UCC194(
    .i0        ( Net__UCC194_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCC188_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCC194_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCC194_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCC159_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_PRIORITY3 ) /* pin 6*/ 
);

BUF12 UCC198(
    .i         ( Net__UCC161_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL7 ) /* pin 2*/ 
);

XNOR02 UCC200(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY34 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC194_Pad1_        ) /* pin 3*/ 
);

INV01 UCC202(
    .i         ( Net__UBZ203_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCC202_Pad2_        ) /* pin 2*/ 
);

XOR02 UCC204(
    .i1        ( Net__UBU222_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU218_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCC204_Pad3_        ) /* pin 3*/ 
);

AND02 UCC207(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UCC207_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ232_Pad2_        ) /* pin 3*/ 
);

XNOR02 UCC209(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY56 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC209_Pad3_        ) /* pin 3*/ 
);

NAND05 UCC218(
    .i0        ( Net__UCC218_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCC218_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCC218_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCC218_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCC209_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_MULT_TC ) /* pin 6*/ 
);

XNOR02 UCC221(
    .Unknown_pin( Net__UBU226_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC221_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC221_Pad3_        ) /* pin 3*/ 
);

M258C UCC224(
    .a3        ( _stars_xpd_bus6_sync_~PD_OUT3 ) /* pin 1*/ ,
    .a2        ( _stars_xpd_bus6_sync_~PD_OUT2 ) /* pin 2*/ ,
    .a1        ( _stars_xpd_bus6_sync_~PD_OUT1 ) /* pin 3*/ ,
    .a0        ( _stars_xpd_bus6_sync_~PD_OUT0 ) /* pin 4*/ ,
    .q0_n      ( Net__UBU218_Pad2_        ) /* pin 5*/ ,
    .q1_n      ( Net__UBU222_Pad2_        ) /* pin 6*/ ,
    .q2_n      ( Net__UBU226_Pad2_        ) /* pin 7*/ ,
    .q3_n      ( Net__UBU230_Pad2_        ) /* pin 8*/ ,
    .anb       ( Net__UBR83_Pad9_         ) /* pin 9*/ ,
    .b3        ( Net__UCC224_Pad10_       ) /* pin 10*/ ,
    .b2        ( Net__UCC221_Pad3_        ) /* pin 11*/ ,
    .b1        ( Net__UCC204_Pad3_        ) /* pin 12*/ ,
    .b0        ( Net__UBU218_Pad3_        ) /* pin 13*/ ,
    .e_n       ( Net__UCC224_Pad14_       ) /* pin 14*/ 
);

DFFCOO UCC23(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC23_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCC23_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC23_Pad5_         ) /* pin 5*/ 
);

NOR02 UCC239(
    .i1        ( Net__UBU222_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU218_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCC221_Pad2_        ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UCC242) 
) UCC243(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC243_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC243_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC243_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC247(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC247_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC247_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC247_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC251(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC251_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC251_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC251_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC255(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UCC255_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC255_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC255_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCC260(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY28 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ272_Pad5_        ) /* pin 3*/ 
);

INV01 UCC263(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UCC224_Pad14_       ) /* pin 2*/ 
);

AOI22 UCC264(
    .i1        ( Net__UBW183_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ232_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ217_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCC264_Pad4_        ) /* pin 4*/ ,
    .o5        ( Net__UCC264_Pad5_        ) /* pin 5*/ 
);

DFFCOO UCC269(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ269_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ200_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC269_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCC27(
    .Unknown_pin( Net__UCC23_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC27_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC27_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCC275(
    .Unknown_pin( _lut_io_B1               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC275_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC275_Pad3_        ) /* pin 3*/ 
);

XOR02 UCC277(
    .i1        ( Net__UBZ318_Pad4_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B0               ) /* pin 2*/ ,
    .o         ( Net__UCC277_Pad3_        ) /* pin 3*/ 
);

NAND03 UCC280(
    .i1        ( _lut_io_B1               ) /* pin 1*/ ,
    .i2        ( Net__UBZ318_Pad4_        ) /* pin 2*/ ,
    .i3        ( _lut_io_B0               ) /* pin 3*/ ,
    .o         ( Net__UCC280_Pad4_        ) /* pin 4*/ 
);

NAND03 UCC283(
    .i1        ( _lut_io_B9               ) /* pin 1*/ ,
    .i2        ( Net__UBZ294_Pad1_        ) /* pin 2*/ ,
    .i3        ( _lut_io_B8               ) /* pin 3*/ ,
    .o         ( Net__UBZ292_Pad2_        ) /* pin 4*/ 
);

DFFCOO #(.MiscRef(UCC291) 
) UCC293(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCC293_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC293_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B0               ) /* pin 5*/ 
);

DFFCOO UCC296(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCC296_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC296_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B1               ) /* pin 5*/ 
);

DFFCOO UCC300(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCC300_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC300_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B2               ) /* pin 5*/ 
);

DFFCOO UCC304(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCC304_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCC304_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B3               ) /* pin 5*/ 
);

NAND02 UCC308(
    .i1        ( Net__UCC308_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC277_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCC293_Pad2_        ) /* pin 3*/ 
);

M258C UCC31(
    .a0        ( _stars_xpd_bus6_XS_OUT4  ) /* pin 4*/ ,
    .q0_n      ( Net__UCC12_Pad2_         ) /* pin 5*/ ,
    .q1_n      ( Net__UCC16_Pad2_         ) /* pin 6*/ ,
    .q2_n      ( Net__UCC19_Pad2_         ) /* pin 7*/ ,
    .q3_n      ( Net__UCC23_Pad2_         ) /* pin 8*/ ,
    .anb       ( Net__UCC119_Pad2_        ) /* pin 9*/ ,
    .b3        ( Net__UCC27_Pad3_         ) /* pin 10*/ ,
    .b2        ( Net__UCC31_Pad11_        ) /* pin 11*/ ,
    .b1        ( Net__UCC31_Pad12_        ) /* pin 12*/ ,
    .b0        ( Net__UCC31_Pad13_        ) /* pin 13*/ ,
    .e_n       ( Net__UBZ48_Pad2_         ) /* pin 14*/ 
);

NAND02 UCC310(
    .i1        ( Net__UCC308_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC275_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCC296_Pad2_        ) /* pin 3*/ 
);

NAND02 UCC312(
    .i1        ( Net__UCC308_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC312_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCC300_Pad2_        ) /* pin 3*/ 
);

NAND02 UCC313(
    .i1        ( Net__UCC308_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC313_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCC304_Pad2_        ) /* pin 3*/ 
);

INV01 UCC315(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UCC315_Pad2_        ) /* pin 2*/ 
);

XNOR02 UCC316(
    .Unknown_pin( _lut_io_B2               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC280_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC312_Pad2_        ) /* pin 3*/ 
);

XNOR02 UCC319(
    .Unknown_pin( _lut_io_B3               ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ318_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC313_Pad2_        ) /* pin 3*/ 
);

AND04 UCC329(
    .i1        ( _video_signals_video_counter_COL8 ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_COL7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_COL6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_COL5 ) /* pin 4*/ ,
    .o         ( Net__UCC329_Pad5_        ) /* pin 5*/ 
);

NOR02 UCC332(
    .i1        ( Net__UCC329_Pad5_        ) /* pin 1*/ ,
    .i2        ( Net__UCC332_Pad2_        ) /* pin 2*/ ,
    .o         ( _video_signals_video_signal_generator_HSYNC ) /* pin 3*/ 
);

M541C UCC334(
    .oe_n      ( _debug_~DEBUG0           ) /* pin 1*/ ,
    .a0        ( _LI                      ) /* pin 2*/ ,
    .a1        ( _FI                      ) /* pin 3*/ ,
    .a2        ( _video_signals_video_counter_COL8 ) /* pin 4*/ ,
    .a3        ( _video_signals_video_counter_COL7 ) /* pin 5*/ ,
    .a4        ( _video_signals_video_counter_COL6 ) /* pin 6*/ ,
    .a5        ( _video_signals_video_counter_COL5 ) /* pin 7*/ ,
    .a6        ( _video_signals_video_counter_COL4 ) /* pin 8*/ ,
    .a7        ( _video_signals_video_counter_COL3 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

OR02 UCC354(
    .i1        ( Net__UBZ346_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ341_Pad2_        ) /* pin 3*/ 
);

DFFCOS #(.MiscRef(UCC357;UCC377) 
) UCC359(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBR462_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC363(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UBR468_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC368(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad7_        ) /* pin 2*/ ,
    .q         ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC372(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad8_        ) /* pin 2*/ ,
    .q         ( Net__UBR479_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

INV01 UCC383(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UCC359_Pad1_        ) /* pin 2*/ 
);

OR04 UCC385(
    .i1        ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBR479_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UBZ346_Pad2_        ) /* pin 5*/ 
);

XNOR02 UCC388(
    .Unknown_pin( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ383_Pad12_       ) /* pin 3*/ 
);

INV01 UCC391(
    .i         ( _bus_io_BUS_DATA5        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS5 ) /* pin 2*/ 
);

OR03 UCC393(
    .i1        ( Net__UBZ346_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .o4        ( Net__UCC393_Pad4_        ) /* pin 4*/ 
);

XNOR02 UCC396(
    .Unknown_pin( Net__UBZ346_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ398_Pad13_       ) /* pin 3*/ 
);

DFFCOS #(.MiscRef(UCC399;UCC418) 
) UCC400(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ346_Pad3_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC405(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC409(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad7_        ) /* pin 2*/ ,
    .q         ( Net__UBZ346_Pad4_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCC414(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad8_        ) /* pin 2*/ ,
    .q         ( Net__UBZ338_Pad1_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

BUF12 UCC420(
    .i         ( Net__UCC420_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBW504_Pad1_        ) /* pin 2*/ 
);

XNOR02 UCC422(
    .Unknown_pin( Net__UBZ346_Pad4_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC393_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ398_Pad11_       ) /* pin 3*/ 
);

TINVBF UCC424(
    .i1        ( Net__UBW505_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC424_Pad2_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad1_        ) /* pin 3*/ 
);

OR02 UCC428(
    .i1        ( _debug_~DEBUG12          ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_DATA8 ) /* pin 2*/ ,
    .o         ( Net__UCC264_Pad4_        ) /* pin 3*/ 
);

OAI23 UCC430(
    .i1        ( Net__UCC430_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC430_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCC430_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCC430_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCC430_Pad5_        ) /* pin 5*/ ,
    .i6        ( Net__UCC430_Pad6_        ) /* pin 6*/ ,
    .o7        ( Net__UBZ473_Pad4_        ) /* pin 7*/ 
);

D38GL UCC434(
    .sa        ( _lut_io_B9               ) /* pin 1*/ ,
    .sb        ( _lut_io_B10              ) /* pin 2*/ ,
    .sc        ( _lut_io_B11              ) /* pin 3*/ ,
    .o7_n      ( Net__UCC434_Pad7_        ) /* pin 7*/ ,
    .o6_n      ( Net__UCC434_Pad9_        ) /* pin 9*/ ,
    .o5_n      ( Net__UCC434_Pad10_       ) /* pin 10*/ ,
    .o4_n      ( Net__UCC434_Pad11_       ) /* pin 11*/ ,
    .o3_n      ( Net__UCC434_Pad12_       ) /* pin 12*/ ,
    .o2_n      ( Net__UCC430_Pad2_        ) /* pin 13*/ ,
    .o1_n      ( Net__UCC430_Pad4_        ) /* pin 14*/ ,
    .o0_n      ( Net__UCC430_Pad6_        ) /* pin 15*/ 
);

M152C UCC461(
    .i3        ( Net__UCC461_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC461_Pad2_        ) /* pin 2*/ ,
    .i1        ( Net__UCC461_Pad3_        ) /* pin 3*/ ,
    .i0        ( Net__UCC461_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UBZ477_Pad2_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCC461_Pad12_       ) /* pin 12*/ ,
    .i6        ( Net__UCC461_Pad13_       ) /* pin 13*/ ,
    .i5        ( Net__UCC461_Pad14_       ) /* pin 14*/ ,
    .i4        ( Net__UCC461_Pad15_       ) /* pin 15*/ 
);

INV01 UCC479(
    .i         ( Net__UBH481_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCC479_Pad2_        ) /* pin 2*/ 
);

BUF12 UCC481(
    .i         ( Net__UBZ482_Pad3_        ) /* pin 1*/ ,
    .o         ( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ 
);

M273C #(.MiscRef(UCC483;UCC495;UCC499;UCC503;UCC508;UCC512;UCC517;UCC521) 
) UCC486(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCC461_Pad4_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA1        ) /* pin 4*/ ,
    .q1        ( Net__UCC461_Pad3_        ) /* pin 5*/ ,
    .q2        ( Net__UCC461_Pad2_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA2        ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA3        ) /* pin 8*/ ,
    .q3        ( Net__UCC461_Pad1_        ) /* pin 9*/ ,
    .clk       ( Net__UBU458_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UCC461_Pad15_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA4        ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .q5        ( Net__UCC461_Pad14_       ) /* pin 15*/ ,
    .q6        ( Net__UCC461_Pad13_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA6        ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA7        ) /* pin 18*/ ,
    .q7        ( Net__UCC461_Pad12_       ) /* pin 19*/ 
);

DFFCOO #(.MiscRef(UCC52) 
) UCC53(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBZ66_Pad5_         ) /* pin 2*/ ,
    .q         ( Net__UBZ66_Pad13_        ) /* pin 3*/ ,
    .q_n       ( Net__UCC53_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCC57(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBZ66_Pad6_         ) /* pin 2*/ ,
    .q         ( Net__UCC57_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC57_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCC61(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBZ66_Pad7_         ) /* pin 2*/ ,
    .q         ( Net__UCC61_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC61_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCC65(
    .clk       ( _stars_row6_assemble_ZERO_XS1 ) /* pin 1*/ ,
    .d         ( Net__UBZ66_Pad8_         ) /* pin 2*/ ,
    .q         ( Net__UCC65_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCC65_Pad5_         ) /* pin 5*/ 
);

NOR04 UCC69(
    .o1        ( Net__UBU23_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBZ66_Pad13_        ) /* pin 2*/ ,
    .i3        ( Net__UCC57_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCC61_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCC65_Pad3_         ) /* pin 5*/ 
);

XNOR02 UCC73(
    .Unknown_pin( Net__UCC65_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC73_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ66_Pad10_        ) /* pin 3*/ 
);

XNOR02 UCC76(
    .Unknown_pin( Net__UCC61_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCC76_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ66_Pad11_        ) /* pin 3*/ 
);

NOR03 UCC79(
    .i1        ( Net__UBZ66_Pad13_        ) /* pin 1*/ ,
    .i2        ( Net__UCC57_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UCC61_Pad3_         ) /* pin 3*/ ,
    .o4        ( Net__UCC73_Pad2_         ) /* pin 4*/ 
);

XNOR02 UCC82(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY115 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC115_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCC85(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY89 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC85_Pad3_         ) /* pin 3*/ 
);

NOR02 UCC87(
    .i1        ( Net__UCC57_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBZ66_Pad13_        ) /* pin 2*/ ,
    .o         ( Net__UCC76_Pad2_         ) /* pin 3*/ 
);

XOR02 UCC89(
    .i1        ( Net__UCC57_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UBZ66_Pad13_        ) /* pin 2*/ ,
    .o         ( Net__UBZ66_Pad12_        ) /* pin 3*/ 
);

NAND05 UCC93(
    .i0        ( Net__UCC93_Pad1_         ) /* pin 1*/ ,
    .i1        ( Net__UCC93_Pad2_         ) /* pin 2*/ ,
    .i2        ( Net__UCC93_Pad3_         ) /* pin 3*/ ,
    .i3        ( Net__UCC85_Pad3_         ) /* pin 4*/ ,
    .i4        ( Net__UCC93_Pad5_         ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_PRIORITY0 ) /* pin 6*/ 
);

INV01 UCC96(
    .i         ( Net__UCC96_Pad1_         ) /* pin 1*/ ,
    .o         ( Net__UBR52_Pad2_         ) /* pin 2*/ 
);

XNOR02 UCC98(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY86 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC93_Pad1_         ) /* pin 3*/ 
);

XNOR02 UCG106(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY109 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG106_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG109(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY82 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG109_Pad3_        ) /* pin 3*/ 
);

NOR03 UCG11(
    .i1        ( Net__UCG11_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UCC12_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UCC16_Pad3_         ) /* pin 3*/ ,
    .o4        ( Net__UCG11_Pad4_         ) /* pin 4*/ 
);

XNOR02 UCG112(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY85 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG112_Pad3_        ) /* pin 3*/ 
);

NAND05 UCG114(
    .i0        ( Net__UCG114_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCG109_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCG114_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCG114_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCG112_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_addr_decoder_registry_~CHECK1 ) /* pin 6*/ 
);

XNOR02 UCG118(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY107 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG118_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG121(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY108 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG121_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG124(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY129 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG124_Pad3_        ) /* pin 3*/ 
);

NAND05 UCG127(
    .i0        ( Net__UCG118_Pad3_        ) /* pin 1*/ ,
    .i1        ( Net__UCG121_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCG106_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCG127_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCG127_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_addr_decoder_registry_~CHECK2 ) /* pin 6*/ 
);

XNOR02 UCG131(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY128 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG131_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG134(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY127 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG134_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG137(
    .Unknown_pin( _security_check_CHECK1_D5 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG137_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG137_Pad3_        ) /* pin 3*/ 
);

NOR04 UCG14(
    .o1        ( Net__UCC27_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UCG11_Pad1_         ) /* pin 2*/ ,
    .i3        ( Net__UCC12_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCC16_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCC19_Pad3_         ) /* pin 5*/ 
);

XNOR02 UCG140(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY111 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG127_Pad5_        ) /* pin 3*/ 
);

NAND05 UCG143(
    .i0        ( Net__UCG143_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCG143_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCG134_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCG131_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCG124_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_RASTER2 ) /* pin 6*/ 
);

XNOR02 UCG148(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY125 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG143_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCG150(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY121 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ147_Pad2_        ) /* pin 3*/ 
);

XNOR02 UCG153(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY120 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ147_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCG159(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY37 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC194_Pad4_        ) /* pin 3*/ 
);

DFFCOO UCG161(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UBZ162_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCC144_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UCG161_Pad5_        ) /* pin 5*/ 
);

INV01 UCG166(
    .i         ( Net__UBU180_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG166_Pad2_        ) /* pin 2*/ 
);

AND02 UCG167(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UCG166_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ159_Pad2_        ) /* pin 3*/ 
);

NAND02 UCG169(
    .i1        ( Net__UBZ200_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ200_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UCG169_Pad3_        ) /* pin 3*/ 
);

NOR05 UCG17(
    .o1        ( _stars_xpd6_sync_LOAD    ) /* pin 1*/ ,
    .i2        ( Net__UCG11_Pad1_         ) /* pin 2*/ ,
    .i3        ( Net__UCC12_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCC19_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCC16_Pad3_         ) /* pin 5*/ ,
    .i6        ( Net__UCC23_Pad3_         ) /* pin 6*/ 
);

XNOR02 UCG171(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY22 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG171_Pad3_        ) /* pin 3*/ 
);

M368C UCG174(
    .oe_n      ( Net__UCG174_Pad1_        ) /* pin 1*/ ,
    .a0        ( Net__UCG174_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UCG174_Pad3_        ) /* pin 3*/ ,
    .a2        ( Net__UCG174_Pad4_        ) /* pin 4*/ ,
    .a3        ( Net__UCG174_Pad5_        ) /* pin 5*/ ,
    .y3        ( _bus_io_BUS_DATA3        ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA2        ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA1        ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA0        ) /* pin 18*/ 
);

NAND05 UCG191(
    .i0        ( Net__UCG191_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCG191_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCG191_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCG171_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCG191_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_MULT_Y ) /* pin 6*/ 
);

XNOR02 UCG194(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY19 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG191_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCG197(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY20 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG191_Pad2_        ) /* pin 3*/ 
);

AND02 UCG200(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU451_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCG200_Pad3_        ) /* pin 3*/ 
);

INV01 UCG202(
    .i         ( Net__UBU451_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCC207_Pad2_        ) /* pin 2*/ 
);

NOR04 UCG203(
    .o1        ( Net__UCC96_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UBU218_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBU222_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU226_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UBU230_Pad3_        ) /* pin 5*/ 
);

NOR03 UCG206(
    .i1        ( Net__UBU218_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBU222_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBU226_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UCG206_Pad4_        ) /* pin 4*/ 
);

XNOR02 UCG209(
    .Unknown_pin( Net__UBU230_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG206_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC224_Pad10_       ) /* pin 3*/ 
);

XNOR02 UCG21(
    .Unknown_pin( Net__UCC19_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG11_Pad4_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC31_Pad11_        ) /* pin 3*/ 
);

XNOR02 UCG217(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY55 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC218_Pad4_        ) /* pin 3*/ 
);

INV01 UCG220(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UCG220_Pad2_        ) /* pin 2*/ 
);

M258C UCG221(
    .a3        ( _stars_xpd_bus6_XS_OUT3  ) /* pin 1*/ ,
    .a2        ( _stars_xpd_bus6_XS_OUT2  ) /* pin 2*/ ,
    .a1        ( _stars_xpd_bus6_XS_OUT1  ) /* pin 3*/ ,
    .a0        ( _stars_xpd_bus6_XS_OUT0  ) /* pin 4*/ ,
    .q0_n      ( Net__UCC243_Pad2_        ) /* pin 5*/ ,
    .q1_n      ( Net__UCC247_Pad2_        ) /* pin 6*/ ,
    .q2_n      ( Net__UCC251_Pad2_        ) /* pin 7*/ ,
    .q3_n      ( Net__UCC255_Pad2_        ) /* pin 8*/ ,
    .anb       ( Net__UCC119_Pad2_        ) /* pin 9*/ ,
    .b3        ( Net__UCG221_Pad10_       ) /* pin 10*/ ,
    .b2        ( Net__UCG221_Pad11_       ) /* pin 11*/ ,
    .b1        ( Net__UCG221_Pad12_       ) /* pin 12*/ ,
    .b0        ( Net__UCC243_Pad3_        ) /* pin 13*/ ,
    .e_n       ( Net__UCG220_Pad2_        ) /* pin 14*/ 
);

XNOR02 UCG235(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY53 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC218_Pad2_        ) /* pin 3*/ 
);

M368C UCG239(
    .oe_n      ( Net__UCG174_Pad1_        ) /* pin 1*/ ,
    .a0        ( Net__UCG239_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UCG239_Pad3_        ) /* pin 3*/ ,
    .a2        ( Net__UCG239_Pad4_        ) /* pin 4*/ ,
    .a3        ( Net__UCG239_Pad5_        ) /* pin 5*/ ,
    .y3        ( _bus_io_BUS_DATA7        ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA6        ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA5        ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA4        ) /* pin 18*/ 
);

XNOR02 UCG25(
    .Unknown_pin( _security_check_CHECK2_D5 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad7_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG25_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCG256(
    .Unknown_pin( Net__UCC255_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG256_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG221_Pad10_       ) /* pin 3*/ 
);

NOR04 UCG259(
    .o1        ( Net__UCG259_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC243_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UCC247_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCC251_Pad3_        ) /* pin 4*/ ,
    .i5        ( Net__UCC255_Pad3_        ) /* pin 5*/ 
);

INV01 UCG262(
    .i         ( _video_signals_video_counter_COL7 ) /* pin 1*/ ,
    .o         ( Net__UCG262_Pad2_        ) /* pin 2*/ 
);

XOR02 UCG264(
    .i1        ( Net__UCC247_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC243_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCG221_Pad12_       ) /* pin 3*/ 
);

XNOR02 UCG27(
    .Unknown_pin( Net__UCG27_Pad1_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG27_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG27_Pad3_         ) /* pin 3*/ 
);

NOR03 UCG270(
    .i1        ( Net__UCC243_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC247_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UCC251_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UCG256_Pad2_        ) /* pin 4*/ 
);

XNOR02 UCG273(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY24 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ272_Pad1_        ) /* pin 3*/ 
);

DFFCOO UCG275(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCC264_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ200_Pad4_        ) /* pin 3*/ ,
    .q_n       ( Net__UCG275_Pad5_        ) /* pin 5*/ 
);

NAND02 UCG281(
    .i1        ( Net__UBZ318_Pad4_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B0               ) /* pin 2*/ ,
    .o         ( Net__UCC275_Pad2_        ) /* pin 3*/ 
);

BUF12 UCG283(
    .i         ( Net__UCC181_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL2 ) /* pin 2*/ 
);

INV01 UCG291(
    .i         ( _~BUS_RD                 ) /* pin 1*/ ,
    .o         ( Net__UCG174_Pad1_        ) /* pin 2*/ 
);

M157C UCG292(
    .a3        ( _lut_io_A7               ) /* pin 1*/ ,
    .a2        ( _lut_io_A6               ) /* pin 2*/ ,
    .a1        ( _lut_io_A5               ) /* pin 3*/ ,
    .a0        ( _lut_io_A4               ) /* pin 4*/ ,
    .q0        ( Net__UCG292_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UCG292_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UCG292_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UCC420_Pad1_        ) /* pin 8*/ ,
    .anb       ( _palette_control_CPY_LUT ) /* pin 9*/ ,
    .b3        ( _lut_io_B7               ) /* pin 10*/ ,
    .b2        ( _lut_io_B6               ) /* pin 11*/ ,
    .b1        ( _lut_io_B5               ) /* pin 12*/ ,
    .b0        ( _lut_io_B4               ) /* pin 13*/ ,
    .e_n       ( _lut_io_~E               ) /* pin 14*/ 
);

BUF12 UCG308(
    .i         ( Net__UCG308_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UBZ294_Pad1_        ) /* pin 2*/ 
);

AND02 UCG309(
    .i1        ( _~LUT_OE                 ) /* pin 1*/ ,
    .i2        ( Net__UBZ298_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UCC308_Pad1_        ) /* pin 3*/ 
);

NOR02 UCG31(
    .i1        ( Net__UCG31_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UCG31_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UCG31_Pad3_         ) /* pin 3*/ 
);

AOI41 UCG312(
    .i1        ( _lut_io_B8               ) /* pin 1*/ ,
    .o2        ( Net__UBZ298_Pad1_        ) /* pin 2*/ ,
    .i2        ( _lut_io_B9               ) /* pin 3*/ ,
    .i3        ( _lut_io_B11              ) /* pin 4*/ ,
    .i4        ( Net__UCG308_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UCC315_Pad2_        ) /* pin 6*/ 
);

NAND04 UCG315(
    .i1        ( _lut_io_B10              ) /* pin 1*/ ,
    .i2        ( _lut_io_B9               ) /* pin 2*/ ,
    .i3        ( _lut_io_B8               ) /* pin 3*/ ,
    .i4        ( Net__UBZ294_Pad1_        ) /* pin 4*/ ,
    .o         ( Net__UCG315_Pad5_        ) /* pin 5*/ 
);

NAND02 UCG318(
    .i1        ( Net__UBZ315_Pad3_        ) /* pin 1*/ ,
    .i2        ( _~CLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UCG318_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG320(
    .Unknown_pin( _lut_io_B11              ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG315_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ299_Pad10_       ) /* pin 3*/ 
);

BUF12 UCG328(
    .i         ( Net__UBZ328_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UCG328_Pad2_        ) /* pin 2*/ 
);

INV01 UCG329(
    .i         ( Net__UCG329_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG308_Pad1_        ) /* pin 2*/ 
);

NAND05 UCG330(
    .i0        ( _lut_io_B4               ) /* pin 1*/ ,
    .i1        ( _lut_io_B5               ) /* pin 2*/ ,
    .i2        ( _lut_io_B6               ) /* pin 3*/ ,
    .i3        ( _lut_io_B7               ) /* pin 4*/ ,
    .i4        ( Net__UCG328_Pad2_        ) /* pin 5*/ ,
    .o         ( Net__UCG329_Pad1_        ) /* pin 6*/ 
);

AND02 UCG334(
    .i1        ( _~LUT_OE                 ) /* pin 1*/ ,
    .i2        ( Net__UBZ298_Pad1_        ) /* pin 2*/ ,
    .o         ( Net__UCG334_Pad3_        ) /* pin 3*/ 
);

BUF12 UCG336(
    .i         ( Net__UCG292_Pad7_        ) /* pin 1*/ ,
    .o         ( Net__UCG336_Pad2_        ) /* pin 2*/ 
);

NOR03 UCG34(
    .i1        ( Net__UCG31_Pad2_         ) /* pin 1*/ ,
    .i2        ( Net__UCG31_Pad1_         ) /* pin 2*/ ,
    .i3        ( Net__UCG34_Pad3_         ) /* pin 3*/ ,
    .o4        ( Net__UCG27_Pad2_         ) /* pin 4*/ 
);

M273C #(.MiscRef(UCG338;UCG345;UCG349;UCG354;UCG358;UCG363;UCG367;UCG371;UCG376) 
) UCG340(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCG340_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA9        ) /* pin 4*/ ,
    .q1        ( Net__UCG340_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCG340_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA10       ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA11       ) /* pin 8*/ ,
    .q3        ( Net__UCG340_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UCG318_Pad3_        ) /* pin 11*/ ,
    .q4        ( Net__UCG340_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .q5        ( Net__UCG340_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCG340_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA14       ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA15       ) /* pin 18*/ ,
    .q7        ( Net__UCG340_Pad19_       ) /* pin 19*/ 
);

M152C UCG383(
    .i3        ( Net__UCG340_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCG340_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UCG340_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UCG340_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UCC424_Pad2_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCG340_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UCG340_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UCG340_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UCG340_Pad12_       ) /* pin 15*/ 
);

XNOR02 UCG39(
    .Unknown_pin( Net__UCG34_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG31_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG39_Pad3_         ) /* pin 3*/ 
);

INV01 UCG401(
    .i         ( _bus_io_BUS_DATA7        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS7 ) /* pin 2*/ 
);

INV01 UCG402(
    .i         ( Net__UCG402_Pad1_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_BLANK ) /* pin 2*/ 
);

AND02 UCG403(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UBU451_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCG403_Pad3_        ) /* pin 3*/ 
);

AOI22 UCG405(
    .i1        ( Net__UCG403_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCG405_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCG405_Pad3_        ) /* pin 3*/ ,
    .i4        ( _debug_~DEBUG12          ) /* pin 4*/ ,
    .o5        ( Net__UCG405_Pad5_        ) /* pin 5*/ 
);

INV01 UCG408(
    .i         ( _bus_io_BUS_DATA6        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS6 ) /* pin 2*/ 
);

AND02 UCG409(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UCG409_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCG405_Pad2_        ) /* pin 3*/ 
);

INV01 UCG412(
    .i         ( Net__UBU451_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCG409_Pad2_        ) /* pin 2*/ 
);

INV01 UCG414(
    .i         ( _bus_io_BUS_DATA8        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter1_~BUS8 ) /* pin 2*/ 
);

R42 UCG415(
    .le        ( Net__UBW272_Pad1_        ) /* pin 1*/ ,
    .s_n       ( _raster_interrupts_counter1_~RESET ) /* pin 2*/ ,
    .d1_n      ( _raster_interrupts_counter1_~BUS5 ) /* pin 3*/ ,
    .d2_n      ( _raster_interrupts_counter1_~BUS6 ) /* pin 4*/ ,
    .d3_n      ( _raster_interrupts_counter1_~BUS7 ) /* pin 5*/ ,
    .d4_n      ( _raster_interrupts_counter1_~BUS8 ) /* pin 6*/ ,
    .q1        ( Net__UCG415_Pad7_        ) /* pin 7*/ ,
    .q2        ( Net__UCG415_Pad8_        ) /* pin 8*/ ,
    .q3        ( Net__UCG415_Pad9_        ) /* pin 9*/ ,
    .q4        ( Net__UCG415_Pad10_       ) /* pin 10*/ 
);

NOR04 UCG42(
    .o1        ( Net__UCG42_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UCG31_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UCG31_Pad1_         ) /* pin 3*/ ,
    .i4        ( Net__UCG34_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCG27_Pad1_         ) /* pin 5*/ 
);

INV01 UCG430(
    .i         ( Net__UCG430_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG430_Pad2_        ) /* pin 2*/ 
);

AOI33 UCG432(
    .i1        ( Net__UCC430_Pad3_        ) /* pin 3*/ ,
    .o4        ( _palette_control_BASE9   ) /* pin 4*/ ,
    .i2        ( Net__UCC430_Pad3_        ) /* pin 6*/ ,
    .i2        ( Net__UCG430_Pad1_        ) /* pin 7*/ ,
    .i3        ( Net__UCG432_Pad8_        ) /* pin 8*/ ,
    .i3        ( Net__UCG432_Pad9_        ) /* pin 9*/ ,
    .i3        ( Net__UCG432_Pad10_       ) /* pin 10*/ 
);

NOR04 UCG437(
    .o1        ( _palette_control_BASE11  ) /* pin 1*/ ,
    .i2        ( Net__UCG430_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCG432_Pad9_        ) /* pin 3*/ ,
    .i4        ( Net__UCG437_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCG432_Pad8_        ) /* pin 5*/ 
);

INV01 UCG441(
    .i         ( Net__UCC430_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCG437_Pad4_        ) /* pin 2*/ 
);

NOR04 UCG442(
    .o1        ( _palette_control_BASE10  ) /* pin 1*/ ,
    .i2        ( Net__UCG442_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCG442_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCG437_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCG432_Pad8_        ) /* pin 5*/ 
);

INV01 UCG445(
    .i         ( Net__UCC430_Pad5_        ) /* pin 1*/ ,
    .o         ( Net__UCG432_Pad8_        ) /* pin 2*/ 
);

INV01 UCG446(
    .i         ( Net__UCC430_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG432_Pad9_        ) /* pin 2*/ 
);

AND03 UCG447(
    .i1        ( Net__UCC430_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCG430_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UCG432_Pad10_       ) /* pin 3*/ ,
    .o         ( Net__UCG442_Pad3_        ) /* pin 4*/ 
);

INV01 UCG451(
    .i         ( Net__UCG451_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG451_Pad2_        ) /* pin 2*/ 
);

BUF12 UCG452(
    .i         ( Net__UCG452_Pad1_        ) /* pin 1*/ ,
    .o         ( _~LUT_OE                 ) /* pin 2*/ 
);

AND03 UCG454(
    .i1        ( Net__UCC430_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCG430_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UCG451_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UCG442_Pad2_        ) /* pin 4*/ 
);

BUF12 UCG457(
    .i         ( Net__UCG457_Pad1_        ) /* pin 1*/ ,
    .o         ( _~CLK4M                  ) /* pin 2*/ 
);

INV01 UCG459(
    .i         ( Net__UCG459_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG432_Pad10_       ) /* pin 2*/ 
);

BUF12 UCG460(
    .i         ( Net__UCG336_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBW502_Pad1_        ) /* pin 2*/ 
);

OAI23 UCG462(
    .i1        ( Net__UCG451_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC434_Pad10_       ) /* pin 2*/ ,
    .i3        ( Net__UCG459_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UCC434_Pad11_       ) /* pin 4*/ ,
    .i5        ( Net__UCG430_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UCC434_Pad12_       ) /* pin 6*/ ,
    .o7        ( Net__UBZ473_Pad3_        ) /* pin 7*/ 
);

TINVBF UCG466(
    .i1        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .i2        ( Net__UCG466_Pad2_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad1_        ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UCG469;UCG484) 
) UCG471(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( _palette_control_~WSTROM ) /* pin 2*/ ,
    .q         ( Net__UCG471_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UCG471_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCG475(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCG471_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCG452_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UCG475_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCG480(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( _palette_control_CPY_LUT ) /* pin 2*/ ,
    .q         ( _palette_control_CPY_LUT ) /* pin 3*/ ,
    .i4        ( _~RESET                  ) /* pin 4*/ ,
    .q_n       ( Net__UCG480_Pad5_        ) /* pin 5*/ 
);

BUF12 UCG486(
    .i         ( Net__UCG292_Pad6_        ) /* pin 1*/ ,
    .o         ( Net__UCG486_Pad2_        ) /* pin 2*/ 
);

BUF12 UCG487(
    .i         ( Net__UCG486_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBW515_Pad1_        ) /* pin 2*/ 
);

BUF12 UCG490(
    .i         ( Net__UCG292_Pad5_        ) /* pin 1*/ ,
    .o         ( Net__UCG490_Pad2_        ) /* pin 2*/ 
);

INV01 UCG492(
    .i         ( Net__UCG492_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG492_Pad2_        ) /* pin 2*/ 
);

DFFCOR UCG493(
    .clk       ( Net__UCG492_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UCG493_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCG493_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCG493_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UCG493_Pad2_        ) /* pin 5*/ 
);

BUF12 UCG499(
    .i         ( Net__UCG493_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCG457_Pad1_        ) /* pin 2*/ 
);

BUF12 UCG502(
    .i         ( Net__UCG490_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBW517_Pad1_        ) /* pin 2*/ 
);

NAND04 UCG504(
    .i1        ( Net__UBR501_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCG504_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCG504_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCG504_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UCG504_Pad5_        ) /* pin 5*/ 
);

INV01 UCG507(
    .i         ( _main_signals_CLK16M     ) /* pin 1*/ ,
    .o         ( Net__UCG507_Pad2_        ) /* pin 2*/ 
);

NAND02 UCG508(
    .i1        ( Net__UCG504_Pad5_        ) /* pin 1*/ ,
    .i2        ( Net__UCG507_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCG508_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCG51(
    .Unknown_pin( Net__UCG51_Pad1_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG42_Pad1_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG51_Pad3_         ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UCG509;UCG520) 
) UCG511(
    .clk       ( _main_signals_CLK16M     ) /* pin 1*/ ,
    .d         ( _LI                      ) /* pin 2*/ ,
    .q         ( Net__UCG504_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UCG511_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCG516(
    .clk       ( _main_signals_CLK16M     ) /* pin 1*/ ,
    .d         ( Net__UCG504_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCG516_Pad3_        ) /* pin 3*/ ,
    .i4        ( _~RESETI1                ) /* pin 4*/ ,
    .q_n       ( Net__UCG504_Pad2_        ) /* pin 5*/ 
);

NAND02 UCG54(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UCG51_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UCG54_Pad3_         ) /* pin 3*/ 
);

NAND02 UCG56(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UCG27_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UCG56_Pad3_         ) /* pin 3*/ 
);

NAND02 UCG57(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UCG39_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UCG57_Pad3_         ) /* pin 3*/ 
);

NAND02 UCG59(
    .i1        ( _~RESET                  ) /* pin 1*/ ,
    .i2        ( Net__UCG59_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UCG59_Pad3_         ) /* pin 3*/ 
);

NOR02 UCG6(
    .i1        ( Net__UCC12_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCG11_Pad1_         ) /* pin 2*/ ,
    .o         ( Net__UCG6_Pad3_          ) /* pin 3*/ 
);

NOR05 UCG60(
    .o1        ( _stars_row5_counter_INC  ) /* pin 1*/ ,
    .i2        ( Net__UCG31_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UCG31_Pad1_         ) /* pin 3*/ ,
    .i4        ( Net__UCG27_Pad1_         ) /* pin 4*/ ,
    .i5        ( Net__UCG34_Pad3_         ) /* pin 5*/ ,
    .i6        ( Net__UCG51_Pad1_         ) /* pin 6*/ 
);

XOR02 UCG64(
    .i1        ( Net__UCG31_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UCG31_Pad2_         ) /* pin 2*/ ,
    .o         ( Net__UCG59_Pad2_         ) /* pin 3*/ 
);

DFFCOO #(.MiscRef(UCG67) 
) UCG69(
    .clk       ( Net__UBR271_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UCG59_Pad3_         ) /* pin 2*/ ,
    .q         ( Net__UCG31_Pad1_         ) /* pin 3*/ ,
    .q_n       ( Net__UCG69_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCG73(
    .clk       ( Net__UBR271_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UCG57_Pad3_         ) /* pin 2*/ ,
    .q         ( Net__UCG34_Pad3_         ) /* pin 3*/ ,
    .q_n       ( Net__UCG73_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCG76(
    .clk       ( Net__UBR271_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UCG56_Pad3_         ) /* pin 2*/ ,
    .q         ( Net__UCG27_Pad1_         ) /* pin 3*/ ,
    .q_n       ( Net__UCG76_Pad5_         ) /* pin 5*/ 
);

DFFCOO UCG80(
    .clk       ( Net__UBR271_Pad2_        ) /* pin 1*/ ,
    .d         ( Net__UCG54_Pad3_         ) /* pin 2*/ ,
    .q         ( Net__UCG51_Pad1_         ) /* pin 3*/ ,
    .q_n       ( Net__UCG80_Pad5_         ) /* pin 5*/ 
);

XNOR02 UCG86(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY90 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC93_Pad5_         ) /* pin 3*/ 
);

XNOR02 UCG9(
    .Unknown_pin( Net__UCC16_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCG6_Pad3_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC31_Pad12_        ) /* pin 3*/ 
);

XNOR02 UCG90(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY88 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC93_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCG92(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY130 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG92_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCG95(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY87 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC93_Pad2_         ) /* pin 3*/ 
);

XNOR02 UCG98(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY81 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG114_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCJ10(
    .Unknown_pin( _security_check_CHECK2_D1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad15_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ10_Pad3_         ) /* pin 3*/ 
);

NAND05 UCJ107(
    .i0        ( Net__UCG92_Pad3_         ) /* pin 1*/ ,
    .i1        ( Net__UCJ107_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCJ107_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCJ107_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCJ107_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_RASTER3 ) /* pin 6*/ 
);

XNOR02 UCJ110(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY84 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG114_Pad4_        ) /* pin 3*/ 
);

XNOR02 UCJ113(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY83 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG114_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCJ116(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY110 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG127_Pad4_        ) /* pin 3*/ 
);

L8 #(.MiscRef(UCJ118;UCJ123;UCJ125;UCJ128;UCJ131;UCJ134;UCJ136;UCJ139) 
) UCJ120(
    .le        ( Net__UCC202_Pad2_        ) /* pin 1*/ ,
    .d8        ( _bus_io_BUS_DATA7        ) /* pin 2*/ ,
    .q8        ( Net__UCJ120_Pad3_        ) /* pin 3*/ ,
    .d7        ( _bus_io_BUS_DATA6        ) /* pin 4*/ ,
    .q7        ( Net__UCJ120_Pad5_        ) /* pin 5*/ ,
    .d6        ( _bus_io_BUS_DATA5        ) /* pin 6*/ ,
    .q6        ( Net__UCG137_Pad2_        ) /* pin 7*/ ,
    .d5        ( _bus_io_BUS_DATA4        ) /* pin 8*/ ,
    .q5        ( Net__UCJ120_Pad9_        ) /* pin 9*/ ,
    .d4        ( _bus_io_BUS_DATA3        ) /* pin 10*/ ,
    .q4        ( Net__UCJ120_Pad11_       ) /* pin 11*/ ,
    .d3        ( _bus_io_BUS_DATA2        ) /* pin 12*/ ,
    .q3        ( Net__UCJ120_Pad13_       ) /* pin 13*/ ,
    .d2        ( _bus_io_BUS_DATA1        ) /* pin 14*/ ,
    .q2        ( Net__UCJ120_Pad15_       ) /* pin 15*/ ,
    .d1        ( _bus_io_BUS_DATA0        ) /* pin 16*/ ,
    .q1        ( Net__UCJ120_Pad17_       ) /* pin 17*/ 
);

XNOR02 UCJ13(
    .Unknown_pin( _security_check_CHECK2_D2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad13_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ13_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCJ142(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY126 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG143_Pad2_        ) /* pin 3*/ 
);

XNOR02 UCJ145(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY122 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ147_Pad3_        ) /* pin 3*/ 
);

NOR04 UCJ147(
    .o1        ( Net__UCJ147_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ147_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ147_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ147_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCG169_Pad3_        ) /* pin 5*/ 
);

NOR03 UCJ151(
    .i1        ( Net__UCJ147_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ147_Pad4_        ) /* pin 2*/ ,
    .i3        ( Net__UCG169_Pad3_        ) /* pin 3*/ ,
    .o4        ( Net__UCJ151_Pad4_        ) /* pin 4*/ 
);

XNOR02 UCJ154(
    .Unknown_pin( Net__UCJ147_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ151_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ154_Pad3_        ) /* pin 3*/ 
);

XOR02 UCJ159(
    .i1        ( Net__UCG169_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ147_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UCJ159_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCJ16(
    .Unknown_pin( _security_check_CHECK2_D0 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad17_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ16_Pad3_         ) /* pin 3*/ 
);

DFFCOO UCJ162(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCJ162_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCJ147_Pad2_        ) /* pin 3*/ ,
    .q_n       ( Net__UCJ162_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCJ167(
    .Unknown_pin( Net__UCJ167_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ147_Pad1_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ167_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCJ170(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY23 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG191_Pad5_        ) /* pin 3*/ 
);

NOR05 UCJ172(
    .o1        ( Net__UCJ172_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ167_Pad1_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ147_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ147_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCJ147_Pad3_        ) /* pin 5*/ ,
    .i6        ( Net__UCJ172_Pad6_        ) /* pin 6*/ 
);

XNOR02 UCJ177(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY21 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG191_Pad3_        ) /* pin 3*/ 
);

AOI22 UCJ180(
    .i1        ( Net__UCG200_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ180_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ154_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBN451_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UCJ162_Pad2_        ) /* pin 5*/ 
);

AOI22 UCJ182(
    .i1        ( Net__UCG200_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ180_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ167_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU453_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UCJ182_Pad5_        ) /* pin 5*/ 
);

AOI22 UCJ185(
    .i1        ( Net__UCG200_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ180_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ159_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBW496_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UCJ185_Pad5_        ) /* pin 5*/ 
);

INV01 UCJ188(
    .i         ( Net__UBZ200_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCJ172_Pad6_        ) /* pin 2*/ 
);

XNOR02 UCJ19(
    .Unknown_pin( _security_check_CHECK2_D3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad11_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ19_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCJ190(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY36 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC194_Pad3_        ) /* pin 3*/ 
);

M151C UCJ192(
    .i3        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i0        ( _bus_io_BUS_DATA0        ) /* pin 4*/ ,
    .q         ( _security_management_security_decoder_K_LAYER_D2 ) /* pin 5*/ ,
    .s2        ( _security_management_bus_mux_key_KEY11 ) /* pin 9*/ ,
    .s1        ( _security_management_bus_mux_key_KEY12 ) /* pin 10*/ ,
    .s0        ( _security_management_bus_mux_key_KEY13 ) /* pin 11*/ ,
    .i7        ( _bus_io_BUS_DATA7        ) /* pin 12*/ ,
    .i6        ( _bus_io_BUS_DATA6        ) /* pin 13*/ ,
    .i5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .i4        ( _bus_io_BUS_DATA4        ) /* pin 15*/ 
);

NAND08 UCJ21(
    .i1        ( Net__UCJ16_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCJ10_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UCJ13_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCJ19_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCJ21_Pad5_         ) /* pin 5*/ ,
    .i6        ( Net__UCG25_Pad3_         ) /* pin 6*/ ,
    .i7        ( Net__UCJ21_Pad7_         ) /* pin 7*/ ,
    .i8        ( Net__UCJ21_Pad8_         ) /* pin 8*/ ,
    .o9        ( Net__UCJ21_Pad9_         ) /* pin 9*/ 
);

BUF12 UCJ217(
    .i         ( Net__UCG161_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_COL8 ) /* pin 2*/ 
);

XNOR02 UCJ219(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY33 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ219_Pad3_        ) /* pin 3*/ 
);

DFFCOO UCJ222(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCJ182_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCJ167_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UCJ222_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCJ226(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY54 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC218_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCJ229(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY52 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC218_Pad1_        ) /* pin 3*/ 
);

L8L #(.MiscRef(UCJ232;UCJ236;UCJ239;UCJ242;UCJ245;UCJ247;UCJ250;UCJ253) 
) UCJ233(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d8        ( _CD7                     ) /* pin 2*/ ,
    .q8        ( Net__UCG239_Pad5_        ) /* pin 3*/ ,
    .d7        ( _CD6                     ) /* pin 4*/ ,
    .q7        ( Net__UCG239_Pad4_        ) /* pin 5*/ ,
    .d6        ( _CD5                     ) /* pin 6*/ ,
    .q6        ( Net__UCG239_Pad3_        ) /* pin 7*/ ,
    .d5        ( _CD4                     ) /* pin 8*/ ,
    .q5        ( Net__UCG239_Pad2_        ) /* pin 9*/ ,
    .d4        ( _CD3                     ) /* pin 10*/ ,
    .q4        ( Net__UCG174_Pad5_        ) /* pin 11*/ ,
    .d3        ( _CD2                     ) /* pin 12*/ ,
    .q3        ( Net__UCG174_Pad4_        ) /* pin 13*/ ,
    .d2        ( _CD1                     ) /* pin 14*/ ,
    .q2        ( Net__UCG174_Pad3_        ) /* pin 15*/ ,
    .d1        ( _CD0                     ) /* pin 16*/ ,
    .q1        ( Net__UCG174_Pad2_        ) /* pin 17*/ 
);

NAND13 UCJ257(
    .i1        ( _~CLK4M                  ) /* pin 1*/ ,
    .i2        ( Net__UCJ257_Pad2_        ) /* pin 2*/ ,
    .i3        ( _FLIP                    ) /* pin 3*/ ,
    .i4        ( Net__UCJ257_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCJ257_Pad5_        ) /* pin 5*/ ,
    .i6        ( _video_signals_video_counter_COL7 ) /* pin 6*/ ,
    .i7        ( Net__UCJ257_Pad7_        ) /* pin 7*/ ,
    .i8        ( _video_signals_video_counter_COL8 ) /* pin 8*/ ,
    .i9        ( Net__UCJ257_Pad9_        ) /* pin 9*/ ,
    .i11       ( _video_signals_video_counter_COL6 ) /* pin 11*/ ,
    .o14       ( Net__UCJ257_Pad14_       ) /* pin 14*/ 
);

INV01 UCJ266(
    .i         ( _video_signals_video_counter_COL2 ) /* pin 1*/ ,
    .o         ( Net__UCJ257_Pad4_        ) /* pin 2*/ 
);

XNOR02 UCJ27(
    .Unknown_pin( _security_check_CHECK2_D7 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad3_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ21_Pad8_         ) /* pin 3*/ 
);

XNOR02 UCJ270(
    .Unknown_pin( Net__UCC251_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ270_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG221_Pad11_       ) /* pin 3*/ 
);

BUF12 UCJ273(
    .i         ( Net__UCC269_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE2 ) /* pin 2*/ 
);

BUF12 UCJ275(
    .i         ( Net__UBZ234_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE1 ) /* pin 2*/ 
);

INV01 UCJ277(
    .i         ( _video_signals_video_counter_COL1 ) /* pin 1*/ ,
    .o         ( Net__UCJ257_Pad2_        ) /* pin 2*/ 
);

XNOR02 UCJ278(
    .Unknown_pin( Net__UCJ278_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ172_Pad1_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCG405_Pad3_        ) /* pin 3*/ 
);

DFFCOO UCJ280(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCG405_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCJ278_Pad1_        ) /* pin 3*/ ,
    .q_n       ( Net__UCJ280_Pad5_        ) /* pin 5*/ 
);

INV01 UCJ291(
    .i         ( Net__UCJ291_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCC332_Pad2_        ) /* pin 2*/ 
);

BUF12 UCJ293(
    .i         ( Net__UCG275_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE0 ) /* pin 2*/ 
);

BUF12 UCJ295(
    .i         ( Net__UBZ243_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE3 ) /* pin 2*/ 
);

NAND02 UCJ297(
    .i1        ( Net__UBZ294_Pad1_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B8               ) /* pin 2*/ ,
    .o         ( Net__UCJ297_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCJ299(
    .Unknown_pin( _lut_io_B9               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ297_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UBZ299_Pad12_       ) /* pin 3*/ 
);

INV01 UCJ302(
    .i         ( _video_signals_video_counter_LINE4 ) /* pin 1*/ ,
    .o         ( Net__UCJ302_Pad2_        ) /* pin 2*/ 
);

NAND05 UCJ303(
    .i0        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCJ303_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCJ303_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCJ303_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCJ302_Pad2_        ) /* pin 5*/ ,
    .o         ( Net__UCJ303_Pad6_        ) /* pin 6*/ 
);

INV01 UCJ306(
    .i         ( _video_signals_video_counter_LINE0 ) /* pin 1*/ ,
    .o         ( Net__UCJ306_Pad2_        ) /* pin 2*/ 
);

NAND13 UCJ307(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ303_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ303_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ303_Pad4_        ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( Net__UCJ306_Pad2_        ) /* pin 6*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i8        ( _FLIP                    ) /* pin 8*/ ,
    .i9        ( Net__UCJ307_Pad9_        ) /* pin 9*/ ,
    .i11       ( Net__UCJ307_Pad11_       ) /* pin 11*/ ,
    .o14       ( Net__UCJ307_Pad14_       ) /* pin 14*/ 
);

NAND13 UCJ315(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ303_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ303_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ303_Pad4_        ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( _video_signals_video_counter_LINE0 ) /* pin 6*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i8        ( _FLIP                    ) /* pin 8*/ ,
    .i9        ( Net__UCJ307_Pad9_        ) /* pin 9*/ ,
    .i11       ( Net__UCJ307_Pad11_       ) /* pin 11*/ ,
    .o14       ( Net__UCJ315_Pad14_       ) /* pin 14*/ 
);

XNOR02 UCJ32(
    .Unknown_pin( _security_check_CHECK2_D4 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad9_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ21_Pad5_         ) /* pin 3*/ 
);

INV01 UCJ328(
    .i         ( Net__UCJ315_Pad14_       ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_UCJ328_Q ) /* pin 2*/ 
);

INV01 UCJ329(
    .i         ( _video_signals_video_counter_COL8 ) /* pin 1*/ ,
    .o         ( Net__UCJ329_Pad2_        ) /* pin 2*/ 
);

BUF12 UCJ330(
    .i         ( Net__UCJ280_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE8 ) /* pin 2*/ 
);

INV01 UCJ332(
    .i         ( _video_signals_video_counter_COL7 ) /* pin 1*/ ,
    .o         ( Net__UCJ332_Pad2_        ) /* pin 2*/ 
);

AND04 UCJ333(
    .i1        ( Net__UCJ257_Pad14_       ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_COL6 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_COL7 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_COL8 ) /* pin 4*/ ,
    .o         ( Net__UCJ333_Pad5_        ) /* pin 5*/ 
);

INV01 UCJ337(
    .i         ( Net__UCJ307_Pad14_       ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_UCJ337_Q ) /* pin 2*/ 
);

M273C #(.MiscRef(UCJ338;UCJ345;UCJ349;UCJ354;UCJ358;UCJ363;UCJ367;UCJ371;UCJ376) 
) UCJ340(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCJ340_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA8        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA9        ) /* pin 4*/ ,
    .q1        ( Net__UCJ340_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCJ340_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA10       ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA11       ) /* pin 8*/ ,
    .q3        ( Net__UCJ340_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBZ335_Pad3_        ) /* pin 11*/ ,
    .q4        ( Net__UCJ340_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA12       ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA13       ) /* pin 14*/ ,
    .q5        ( Net__UCJ340_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCJ340_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA14       ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA15       ) /* pin 18*/ ,
    .q7        ( Net__UCJ340_Pad19_       ) /* pin 19*/ 
);

XNOR02 UCJ35(
    .Unknown_pin( _security_check_CHECK2_D6 ) /* pin 1*/ ,
    .Unknown_pin( Net__UBW8_Pad5_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ21_Pad7_         ) /* pin 3*/ 
);

M152C UCJ383(
    .i3        ( Net__UCJ340_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ340_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UCJ340_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UCJ340_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UCJ383_Pad5_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCJ340_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UCJ340_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UCJ340_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UCJ340_Pad12_       ) /* pin 15*/ 
);

M273C #(.MiscRef(UCJ401;UCJ408;UCJ412;UCJ417;UCJ421;UCJ426;UCJ430;UCJ435;UCJ439) 
) UCJ404(
    .r_n       ( Net__UCJ404_Pad1_        ) /* pin 1*/ ,
    .q0        ( Net__UCJ404_Pad2_        ) /* pin 2*/ ,
    .d0        ( _ASD0                    ) /* pin 3*/ ,
    .d1        ( _ASD1                    ) /* pin 4*/ ,
    .q1        ( Net__UCJ404_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCJ404_Pad6_        ) /* pin 6*/ ,
    .d2        ( _ASD2                    ) /* pin 7*/ ,
    .d3        ( _ASD3                    ) /* pin 8*/ ,
    .q3        ( Net__UCJ404_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UCJ404_Pad12_       ) /* pin 12*/ ,
    .d4        ( _ASD4                    ) /* pin 13*/ ,
    .d5        ( _ASD5                    ) /* pin 14*/ ,
    .q5        ( Net__UCJ404_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCJ404_Pad16_       ) /* pin 16*/ ,
    .d6        ( _ASD6                    ) /* pin 17*/ ,
    .d7        ( _ASD7                    ) /* pin 18*/ ,
    .q7        ( Net__UCJ404_Pad19_       ) /* pin 19*/ 
);

INV01 UCJ44(
    .i         ( Net__UCC121_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCG31_Pad2_         ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UCJ443;UCJ472) 
) UCJ446(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA5        ) /* pin 2*/ ,
    .q         ( Net__UCJ446_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCG451_Pad1_        ) /* pin 5*/ 
);

DFFCOR UCJ450(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA4        ) /* pin 2*/ ,
    .q         ( Net__UCJ450_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCG459_Pad1_        ) /* pin 5*/ 
);

DFFCOR UCJ455(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA3        ) /* pin 2*/ ,
    .q         ( Net__UCJ455_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCG430_Pad1_        ) /* pin 5*/ 
);

DFFCOR UCJ459(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .q         ( Net__UCJ459_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCC430_Pad1_        ) /* pin 5*/ 
);

DFFCOR UCJ463(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA1        ) /* pin 2*/ ,
    .q         ( Net__UCJ463_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCC430_Pad3_        ) /* pin 5*/ 
);

DFFCOR UCJ468(
    .clk       ( Net__UBZ465_Pad4_        ) /* pin 1*/ ,
    .d         ( _bus_io_BUS_DATA0        ) /* pin 2*/ ,
    .q         ( Net__UCJ468_Pad3_        ) /* pin 3*/ ,
    .i4        ( _counter_fi_~LUT_RESET   ) /* pin 4*/ ,
    .q_n       ( Net__UCC430_Pad5_        ) /* pin 5*/ 
);

BUF12 UCJ475(
    .i         ( Net__UCG492_Pad1_        ) /* pin 1*/ ,
    .o         ( _CLK8M                   ) /* pin 2*/ 
);

XNOR02 UCJ478(
    .Unknown_pin( Net__UBZ504_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ478_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ478_Pad3_        ) /* pin 3*/ 
);

OR02 UCJ481(
    .i1        ( Net__UBZ498_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCC479_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCJ478_Pad2_        ) /* pin 3*/ 
);

M273C #(.MiscRef(UCJ483;UCJ490;UCJ495;UCJ499;UCJ503;UCJ508;UCJ512;UCJ517;UCJ521) 
) UCJ486(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCJ486_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA1        ) /* pin 4*/ ,
    .q1        ( Net__UCJ486_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCJ486_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA2        ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA3        ) /* pin 8*/ ,
    .q3        ( Net__UCJ486_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBK460_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UCJ486_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA4        ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .q5        ( Net__UCJ486_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCJ486_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA6        ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA7        ) /* pin 18*/ ,
    .q7        ( Net__UCJ486_Pad19_       ) /* pin 19*/ 
);

XOR02 UCJ6(
    .i1        ( Net__UCC12_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCG11_Pad1_         ) /* pin 2*/ ,
    .o         ( Net__UCC31_Pad13_        ) /* pin 3*/ 
);

XNOR02 UCJ65(
    .Unknown_pin( _security_check_CHECK2_D10 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ65_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ65_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCJ68(
    .Unknown_pin( _security_check_CHECK2_D13 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ68_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ68_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCJ71(
    .Unknown_pin( _security_check_CHECK2_D11 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ71_Pad2_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ71_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCJ75(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY117 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCC115_Pad3_        ) /* pin 3*/ 
);

L8 #(.MiscRef(UCJ77;UCJ82;UCJ84;UCJ87;UCJ90;UCJ93;UCJ95;UCJ98) 
) UCJ79(
    .le        ( Net__UBW8_Pad1_          ) /* pin 1*/ ,
    .d8        ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q8        ( Net__UCJ79_Pad3_         ) /* pin 3*/ ,
    .d7        ( _bus_io_BUS_DATA14       ) /* pin 4*/ ,
    .q7        ( Net__UCJ79_Pad5_         ) /* pin 5*/ ,
    .d6        ( _bus_io_BUS_DATA13       ) /* pin 6*/ ,
    .q6        ( Net__UCJ68_Pad2_         ) /* pin 7*/ ,
    .d5        ( _bus_io_BUS_DATA12       ) /* pin 8*/ ,
    .q5        ( Net__UCJ79_Pad9_         ) /* pin 9*/ ,
    .d4        ( _bus_io_BUS_DATA11       ) /* pin 10*/ ,
    .q4        ( Net__UCJ71_Pad2_         ) /* pin 11*/ ,
    .d3        ( _bus_io_BUS_DATA10       ) /* pin 12*/ ,
    .q3        ( Net__UCJ65_Pad2_         ) /* pin 13*/ ,
    .d2        ( _bus_io_BUS_DATA9        ) /* pin 14*/ ,
    .q2        ( Net__UCJ79_Pad15_        ) /* pin 15*/ ,
    .d1        ( _bus_io_BUS_DATA8        ) /* pin 16*/ ,
    .q1        ( Net__UCJ79_Pad17_        ) /* pin 17*/ 
);

L8 #(.MiscRef(UCL106;UCL110;UCL113;UCL116;UCL119;UCL122;UCL124;UCL127) 
) UCL108(
    .le        ( Net__UCC202_Pad2_        ) /* pin 1*/ ,
    .d8        ( _bus_io_BUS_DATA15       ) /* pin 2*/ ,
    .q8        ( Net__UCL108_Pad3_        ) /* pin 3*/ ,
    .d7        ( _bus_io_BUS_DATA14       ) /* pin 4*/ ,
    .q7        ( Net__UCL108_Pad5_        ) /* pin 5*/ ,
    .d6        ( _bus_io_BUS_DATA13       ) /* pin 6*/ ,
    .q6        ( Net__UCL108_Pad7_        ) /* pin 7*/ ,
    .d5        ( _bus_io_BUS_DATA12       ) /* pin 8*/ ,
    .q5        ( Net__UCL108_Pad9_        ) /* pin 9*/ ,
    .d4        ( _bus_io_BUS_DATA11       ) /* pin 10*/ ,
    .q4        ( Net__UCL108_Pad11_       ) /* pin 11*/ ,
    .d3        ( _bus_io_BUS_DATA10       ) /* pin 12*/ ,
    .q3        ( Net__UCL108_Pad13_       ) /* pin 13*/ ,
    .d2        ( _bus_io_BUS_DATA9        ) /* pin 14*/ ,
    .q2        ( Net__UCL108_Pad15_       ) /* pin 15*/ ,
    .d1        ( _bus_io_BUS_DATA8        ) /* pin 16*/ ,
    .q1        ( Net__UCL108_Pad17_       ) /* pin 17*/ 
);

XNOR02 UCL131(
    .Unknown_pin( _security_check_CHECK1_D13 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad7_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL131_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCL134(
    .Unknown_pin( _security_check_CHECK1_D1 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad15_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL134_Pad3_        ) /* pin 3*/ 
);

NAND08 UCL137(
    .i1        ( Net__UCL137_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCL134_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UCL137_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCL137_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCL137_Pad5_        ) /* pin 5*/ ,
    .i6        ( Net__UCG137_Pad3_        ) /* pin 6*/ ,
    .i7        ( Net__UCL137_Pad7_        ) /* pin 7*/ ,
    .i8        ( Net__UCL137_Pad8_        ) /* pin 8*/ ,
    .o9        ( Net__UCL137_Pad9_        ) /* pin 9*/ 
);

XNOR02 UCL143(
    .Unknown_pin( _security_check_CHECK1_D3 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad11_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad4_        ) /* pin 3*/ 
);

XNOR02 UCL146(
    .Unknown_pin( _security_check_CHECK1_D4 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad9_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad5_        ) /* pin 3*/ 
);

XNOR02 UCL149(
    .Unknown_pin( _security_check_CHECK1_D6 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad7_        ) /* pin 3*/ 
);

XNOR02 UCL152(
    .Unknown_pin( _security_check_CHECK1_D7 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad8_        ) /* pin 3*/ 
);

XNOR02 UCL160(
    .Unknown_pin( Net__UCJ147_Pad3_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL160_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL160_Pad3_        ) /* pin 3*/ 
);

NOR02 UCL164(
    .i1        ( Net__UCG169_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ147_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UCL160_Pad2_        ) /* pin 3*/ 
);

M151C UCL166(
    .i3        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i0        ( _bus_io_BUS_DATA0        ) /* pin 4*/ ,
    .q         ( _security_management_security_decoder_K_LAYER_D5 ) /* pin 5*/ ,
    .s2        ( _security_management_bus_mux_key_KEY78 ) /* pin 9*/ ,
    .s1        ( _security_management_bus_mux_key_KEY79 ) /* pin 10*/ ,
    .s0        ( _security_management_bus_mux_key_KEY80 ) /* pin 11*/ ,
    .i7        ( _bus_io_BUS_DATA7        ) /* pin 12*/ ,
    .i6        ( _bus_io_BUS_DATA6        ) /* pin 13*/ ,
    .i5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .i4        ( _bus_io_BUS_DATA4        ) /* pin 15*/ 
);

DFFCOO UCL186(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCJ185_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCJ147_Pad4_        ) /* pin 3*/ ,
    .q_n       ( Net__UCL186_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCL192(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY43 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL192_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCL195(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY44 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL195_Pad3_        ) /* pin 3*/ 
);

NAND05 UCL197(
    .i0        ( Net__UCL197_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCL192_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCL195_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCL197_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCL197_Pad5_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_MULT_MSB ) /* pin 6*/ 
);

INV01 UCL201(
    .i         ( Net__UBU451_Pad3_        ) /* pin 1*/ ,
    .o         ( Net__UCL201_Pad2_        ) /* pin 2*/ 
);

AOI22 UCL202(
    .i1        ( Net__UCG200_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ180_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCL160_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBU496_Pad3_        ) /* pin 4*/ ,
    .o5        ( Net__UCL202_Pad5_        ) /* pin 5*/ 
);

AND02 UCL205(
    .i1        ( _~RESETI1                ) /* pin 1*/ ,
    .i2        ( Net__UCL201_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCJ180_Pad2_        ) /* pin 3*/ 
);

DFFCOO UCL207(
    .clk       ( _~CLK4M                  ) /* pin 1*/ ,
    .d         ( Net__UCL202_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCJ147_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UCL207_Pad5_        ) /* pin 5*/ 
);

XNOR02 UCL219(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY31 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL219_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCL221(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY32 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL221_Pad3_        ) /* pin 3*/ 
);

NAND05 UCL224(
    .i0        ( Net__UCL224_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCL224_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCL219_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCL221_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCJ219_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_MULT_LSB ) /* pin 6*/ 
);

XNOR02 UCL228(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY29 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL224_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCL231(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY8 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL231_Pad3_        ) /* pin 3*/ 
);

M368C UCL235(
    .oe_n      ( Net__UCG174_Pad1_        ) /* pin 1*/ ,
    .a0        ( Net__UCL235_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UCL235_Pad3_        ) /* pin 3*/ ,
    .a2        ( Net__UCL235_Pad4_        ) /* pin 4*/ ,
    .a3        ( Net__UCL235_Pad5_        ) /* pin 5*/ ,
    .y3        ( _bus_io_BUS_DATA11       ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA10       ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA9        ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA8        ) /* pin 18*/ 
);

INV01 UCL252(
    .i         ( _video_signals_video_counter_COL6 ) /* pin 1*/ ,
    .o         ( Net__UCL252_Pad2_        ) /* pin 2*/ 
);

INV01 UCL253(
    .i         ( _video_signals_video_counter_COL8 ) /* pin 1*/ ,
    .o         ( Net__UCL253_Pad2_        ) /* pin 2*/ 
);

INV01 UCL255(
    .i         ( _video_signals_video_counter_COL5 ) /* pin 1*/ ,
    .o         ( Net__UCJ257_Pad5_        ) /* pin 2*/ 
);

AND03 UCL256(
    .i1        ( Net__UCL253_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCG262_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCL252_Pad2_        ) /* pin 3*/ ,
    .o         ( Net__UCL256_Pad4_        ) /* pin 4*/ 
);

BUF12 UCL259(
    .i         ( Net__UCJ222_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE7 ) /* pin 2*/ 
);

INV01 UCL261(
    .i         ( _video_signals_video_counter_COL3 ) /* pin 1*/ ,
    .o         ( Net__UCJ257_Pad7_        ) /* pin 2*/ 
);

NAND08 UCL262(
    .i1        ( Net__UCL253_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCG262_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCL252_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ257_Pad5_        ) /* pin 4*/ ,
    .i5        ( Net__UCJ257_Pad9_        ) /* pin 5*/ ,
    .i6        ( Net__UCJ257_Pad7_        ) /* pin 6*/ ,
    .i7        ( Net__UCJ257_Pad4_        ) /* pin 7*/ ,
    .o9        ( Net__UCJ291_Pad1_        ) /* pin 9*/ 
);

NOR02 UCL269(
    .i1        ( Net__UCC247_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCC243_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCJ270_Pad2_        ) /* pin 3*/ 
);

NAND13 UCL272(
    .i1        ( _FLIP                    ) /* pin 1*/ ,
    .i2        ( Net__UCJ257_Pad2_        ) /* pin 2*/ ,
    .i3        ( _~CLK4M                  ) /* pin 3*/ ,
    .i6        ( Net__UCJ257_Pad7_        ) /* pin 6*/ ,
    .i8        ( Net__UCJ257_Pad9_        ) /* pin 8*/ ,
    .i9        ( Net__UCL253_Pad2_        ) /* pin 9*/ ,
    .i10       ( Net__UCJ257_Pad5_        ) /* pin 10*/ ,
    .i11       ( Net__UCJ257_Pad4_        ) /* pin 11*/ ,
    .i12       ( _video_signals_video_counter_COL6 ) /* pin 12*/ ,
    .i13       ( Net__UCG262_Pad2_        ) /* pin 13*/ ,
    .o14       ( Net__UCL272_Pad14_       ) /* pin 14*/ 
);

AND02 UCL28(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY106 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D0 ) /* pin 3*/ 
);

INV01 UCL280(
    .i         ( _video_signals_video_counter_COL4 ) /* pin 1*/ ,
    .o         ( Net__UCJ257_Pad9_        ) /* pin 2*/ 
);

BUF12 UCL283(
    .i         ( Net__UCL186_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE4 ) /* pin 2*/ 
);

INV01 UCL291(
    .i         ( _video_signals_video_counter_LINE5 ) /* pin 1*/ ,
    .o         ( Net__UCJ303_Pad4_        ) /* pin 2*/ 
);

M368C UCL292(
    .oe_n      ( Net__UCG174_Pad1_        ) /* pin 1*/ ,
    .a0        ( Net__UCL292_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UCL292_Pad3_        ) /* pin 3*/ ,
    .a2        ( Net__UCL292_Pad4_        ) /* pin 4*/ ,
    .a3        ( Net__UCL292_Pad5_        ) /* pin 5*/ ,
    .y3        ( _bus_io_BUS_DATA15       ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA14       ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA13       ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA12       ) /* pin 18*/ 
);

AND02 UCL30(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY105 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D1 ) /* pin 3*/ 
);

XOR02 UCL310(
    .i1        ( Net__UCG328_Pad2_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B4               ) /* pin 2*/ ,
    .o         ( Net__UCL310_Pad3_        ) /* pin 3*/ 
);

NAND13 UCL313(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ303_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ303_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ303_Pad4_        ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( Net__UCJ306_Pad2_        ) /* pin 6*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i8        ( Net__UCL313_Pad8_        ) /* pin 8*/ ,
    .i9        ( Net__UCJ307_Pad9_        ) /* pin 9*/ ,
    .i11       ( Net__UCJ307_Pad11_       ) /* pin 11*/ ,
    .o14       ( Net__UCL313_Pad14_       ) /* pin 14*/ 
);

AND02 UCL32(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY104 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D2 ) /* pin 3*/ 
);

INV01 UCL321(
    .i         ( _FLIP                    ) /* pin 1*/ ,
    .o         ( Net__UCL313_Pad8_        ) /* pin 2*/ 
);

INV01 UCL322(
    .i         ( _video_signals_video_counter_COL6 ) /* pin 1*/ ,
    .o         ( Net__UCL322_Pad2_        ) /* pin 2*/ 
);

NAND13 UCL328(
    .i1        ( Net__UCL328_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ329_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ332_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UCL322_Pad2_        ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_COL5 ) /* pin 5*/ ,
    .i6        ( _video_signals_video_counter_COL1 ) /* pin 6*/ ,
    .i7        ( _video_signals_video_counter_COL4 ) /* pin 7*/ ,
    .i9        ( _video_signals_video_counter_COL3 ) /* pin 9*/ ,
    .i11       ( _video_signals_video_counter_COL2 ) /* pin 11*/ ,
    .o14       ( Net__UCL328_Pad14_       ) /* pin 14*/ 
);

NAND13 UCL336(
    .i1        ( _video_signals_video_signal_generator_UCL375_Q ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_COL8 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_COL7 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_COL6 ) /* pin 4*/ ,
    .i5        ( Net__UCL336_Pad5_        ) /* pin 5*/ ,
    .i6        ( _video_signals_video_counter_COL1 ) /* pin 6*/ ,
    .i7        ( _video_signals_video_counter_COL4 ) /* pin 7*/ ,
    .i9        ( _video_signals_video_counter_COL3 ) /* pin 9*/ ,
    .i11       ( _video_signals_video_counter_COL2 ) /* pin 11*/ ,
    .o14       ( Net__UCL336_Pad14_       ) /* pin 14*/ 
);

AND02 UCL34(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY103 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D3 ) /* pin 3*/ 
);

INV01 UCL344(
    .i         ( _video_signals_video_counter_COL5 ) /* pin 1*/ ,
    .o         ( Net__UCL336_Pad5_        ) /* pin 2*/ 
);

INV01 UCL345(
    .i         ( Net__UCL272_Pad14_       ) /* pin 1*/ ,
    .o         ( Net__UCL345_Pad2_        ) /* pin 2*/ 
);

INV01 UCL347(
    .i         ( _video_signals_video_signal_generator_~VBLANK ) /* pin 1*/ ,
    .o         ( Net__UCL347_Pad2_        ) /* pin 2*/ 
);

NOR03 UCL348(
    .i1        ( Net__UCL345_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCL256_Pad4_        ) /* pin 2*/ ,
    .i3        ( Net__UCJ333_Pad5_        ) /* pin 3*/ ,
    .o4        ( _video_signals_video_signal_generator_~HBLANK ) /* pin 4*/ 
);

NOR04 UCL350(
    .o1        ( Net__UCG402_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ333_Pad5_        ) /* pin 2*/ ,
    .i3        ( Net__UCL345_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UCL256_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCL347_Pad2_        ) /* pin 5*/ 
);

M541C UCL354(
    .oe_n      ( _video_signals_video_signal_generator_~DEBUG_HI ) /* pin 1*/ ,
    .a0        ( _video_signals_video_counter_LINE8 ) /* pin 2*/ ,
    .a1        ( _video_signals_video_counter_LINE7 ) /* pin 3*/ ,
    .a2        ( _video_signals_video_counter_LINE6 ) /* pin 4*/ ,
    .a3        ( _video_signals_video_counter_LINE5 ) /* pin 5*/ ,
    .a4        ( _video_signals_video_counter_LINE4 ) /* pin 6*/ ,
    .a5        ( _video_signals_video_counter_LINE3 ) /* pin 7*/ ,
    .a6        ( _video_signals_video_counter_LINE2 ) /* pin 8*/ ,
    .a7        ( _video_signals_video_counter_LINE1 ) /* pin 9*/ ,
    .y7        ( _debug_td_io_IN7         ) /* pin 11*/ ,
    .y6        ( _debug_td_io_IN6         ) /* pin 12*/ ,
    .y5        ( _debug_td_io_IN5         ) /* pin 13*/ ,
    .y4        ( _debug_td_io_IN4         ) /* pin 14*/ ,
    .y3        ( _debug_td_io_IN3         ) /* pin 15*/ ,
    .y2        ( _obj_manager_~DEBUG2     ) /* pin 16*/ ,
    .y1        ( _debug_td_io_IN1         ) /* pin 17*/ ,
    .y0        ( _debug_td_io_IN0         ) /* pin 18*/ 
);

AND02 UCL37(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY102 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D4 ) /* pin 3*/ 
);

NBUF03 UCL373(
    .i         ( _~CLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UBN388_Pad3_        ) /* pin 2*/ 
);

INV01 UCL375(
    .i         ( Net__UCL375_Pad1_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_UCL375_Q ) /* pin 2*/ 
);

INV01 UCL377(
    .i         ( Net__UCL377_Pad1_        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_counter2_~ZERO ) /* pin 2*/ 
);

NAND13 UCL383(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( Net__UCJ306_Pad2_        ) /* pin 6*/ ,
    .i7        ( _video_signals_video_counter_LINE3 ) /* pin 7*/ ,
    .i9        ( _video_signals_video_counter_LINE2 ) /* pin 9*/ ,
    .i11       ( _video_signals_video_counter_LINE1 ) /* pin 11*/ ,
    .o14       ( Net__UCL383_Pad14_       ) /* pin 14*/ 
);

AND02 UCL39(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY101 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D5 ) /* pin 3*/ 
);

NAND13 UCL391(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( Net__UCJ306_Pad2_        ) /* pin 6*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i9        ( _video_signals_video_counter_LINE2 ) /* pin 9*/ ,
    .i11       ( _video_signals_video_counter_LINE1 ) /* pin 11*/ ,
    .o14       ( Net__UCL375_Pad1_        ) /* pin 14*/ 
);

TINVBF UCL400(
    .i1        ( Net__UBW505_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ383_Pad5_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad2_        ) /* pin 3*/ 
);

BUF12 UCL404(
    .i         ( _~RESET                  ) /* pin 1*/ ,
    .o         ( Net__UCJ404_Pad1_        ) /* pin 2*/ 
);

M273C #(.MiscRef(UCL405;UCL412;UCL417;UCL421;UCL426;UCL430;UCL435;UCL439;UCL444) 
) UCL408(
    .r_n       ( Net__UCJ404_Pad1_        ) /* pin 1*/ ,
    .q0        ( Net__UCL408_Pad2_        ) /* pin 2*/ ,
    .d0        ( _BSD0                    ) /* pin 3*/ ,
    .d1        ( _BSD1                    ) /* pin 4*/ ,
    .q1        ( Net__UCL408_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCL408_Pad6_        ) /* pin 6*/ ,
    .d2        ( _BSD2                    ) /* pin 7*/ ,
    .d3        ( _BSD3                    ) /* pin 8*/ ,
    .q3        ( Net__UCL408_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 11*/ ,
    .q4        ( Net__UCL408_Pad12_       ) /* pin 12*/ ,
    .d4        ( _BSD4                    ) /* pin 13*/ ,
    .d5        ( _BSD5                    ) /* pin 14*/ ,
    .q5        ( Net__UCL408_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCL408_Pad16_       ) /* pin 16*/ ,
    .d6        ( _BSD6                    ) /* pin 17*/ ,
    .d7        ( _BSD7                    ) /* pin 18*/ ,
    .q7        ( Net__UCL408_Pad19_       ) /* pin 19*/ 
);

AND02 UCL41(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY100 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D6 ) /* pin 3*/ 
);

AND02 UCL43(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY99 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D7 ) /* pin 3*/ 
);

INV01 UCL446(
    .i         ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .o         ( Net__UBW505_Pad1_        ) /* pin 2*/ 
);

INV01 UCL448(
    .i         ( Net__JP2_Pad2_           ) /* pin 1*/ ,
    .o         ( Net__UCL448_Pad2_        ) /* pin 2*/ 
);

OR02 UCL450(
    .i1        ( Net__UCL450_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCL450_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCL450_Pad3_        ) /* pin 3*/ 
);

BUF12 UCL456(
    .i         ( Net__UCG457_Pad1_        ) /* pin 1*/ ,
    .o         ( _~CLK4M                  ) /* pin 2*/ 
);

BUF12 UCL458(
    .i         ( Net__UCG457_Pad1_        ) /* pin 1*/ ,
    .o         ( _~CLK4M                  ) /* pin 2*/ 
);

XNOR02 UCL460(
    .Unknown_pin( Net__UCL460_Pad1_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL460_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL460_Pad3_        ) /* pin 3*/ 
);

OAI41 UCL463(
    .i1        ( Net__UCL460_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCL460_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UCL460_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UCL463_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UCL463_Pad5_        ) /* pin 5*/ ,
    .o         ( Net__UCL463_Pad6_        ) /* pin 6*/ 
);

DFFCOR #(.MiscRef(UCL466;UCL486) 
) UCL468(
    .clk       ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCL460_Pad1_        ) /* pin 2*/ ,
    .q         ( Net__UCL460_Pad2_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UCL468_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCL472(
    .clk       ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCL463_Pad4_        ) /* pin 2*/ ,
    .q         ( Net__UCL460_Pad1_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UCL472_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCL477(
    .clk       ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCL463_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UCL463_Pad4_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UCL477_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCL481(
    .clk       ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCL463_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UCL463_Pad5_        ) /* pin 3*/ ,
    .i4        ( _~RESETI2                ) /* pin 4*/ ,
    .q_n       ( Net__UCL481_Pad5_        ) /* pin 5*/ 
);

TINVBF UCL487(
    .i1        ( _lut_assembly_priority_transparency_OUT3 ) /* pin 1*/ ,
    .i2        ( Net__UCL487_Pad2_        ) /* pin 2*/ ,
    .t3        ( Net__UBW401_Pad2_        ) /* pin 3*/ 
);

XNOR02 UCL491(
    .Unknown_pin( Net__UCC479_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UBZ498_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL491_Pad3_        ) /* pin 3*/ 
);

BUF12 UCL496(
    .i         ( Net__UCG492_Pad1_        ) /* pin 1*/ ,
    .o         ( _CLK8M                   ) /* pin 2*/ 
);

M152C UCL498(
    .i3        ( Net__UCJ486_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ486_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UCJ486_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UCJ486_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UBW498_Pad2_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCJ486_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UCJ486_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UCJ486_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UCJ486_Pad12_       ) /* pin 15*/ 
);

NOR05 UCL516(
    .o1        ( Net__UCL377_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCC479_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ498_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ509_Pad2_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ504_Pad2_        ) /* pin 5*/ ,
    .i6        ( Net__UBZ515_Pad2_        ) /* pin 6*/ 
);

OR04 UCL519(
    .i1        ( Net__UCC479_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ498_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ504_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ509_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UCL519_Pad5_        ) /* pin 5*/ 
);

OR04 UCL53(
    .i1        ( Net__UCL137_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCJ21_Pad9_         ) /* pin 2*/ ,
    .i3        ( Net__UCL53_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCL53_Pad4_         ) /* pin 4*/ ,
    .o         ( Net__UBZ94_Pad2_         ) /* pin 5*/ 
);

XNOR02 UCL56(
    .Unknown_pin( _security_check_CHECK2_D14 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ79_Pad5_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL56_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCL59(
    .Unknown_pin( _security_check_CHECK2_D8 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ79_Pad17_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL59_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCL62(
    .Unknown_pin( _security_check_CHECK2_D9 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ79_Pad15_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL62_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCL65(
    .Unknown_pin( _security_check_CHECK2_D15 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ79_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL65_Pad3_         ) /* pin 3*/ 
);

NAND08 UCL67(
    .i1        ( Net__UCL59_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCL62_Pad3_         ) /* pin 2*/ ,
    .i3        ( Net__UCJ65_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCJ71_Pad3_         ) /* pin 4*/ ,
    .i5        ( Net__UCL67_Pad5_         ) /* pin 5*/ ,
    .i6        ( Net__UCJ68_Pad3_         ) /* pin 6*/ ,
    .i7        ( Net__UCL56_Pad3_         ) /* pin 7*/ ,
    .i8        ( Net__UCL65_Pad3_         ) /* pin 8*/ ,
    .o9        ( Net__UCL53_Pad3_         ) /* pin 9*/ 
);

XNOR02 UCL73(
    .Unknown_pin( _security_check_CHECK2_D12 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ79_Pad9_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL67_Pad5_         ) /* pin 3*/ 
);

NAND08 UCL76(
    .i1        ( Net__UCL76_Pad1_         ) /* pin 1*/ ,
    .i2        ( Net__UCL76_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UCL76_Pad3_         ) /* pin 3*/ ,
    .i4        ( Net__UCL76_Pad4_         ) /* pin 4*/ ,
    .i5        ( Net__UCL76_Pad5_         ) /* pin 5*/ ,
    .i6        ( Net__UCL131_Pad3_        ) /* pin 6*/ ,
    .i7        ( Net__UCL76_Pad7_         ) /* pin 7*/ ,
    .i8        ( Net__UCL76_Pad8_         ) /* pin 8*/ ,
    .o9        ( Net__UCL53_Pad4_         ) /* pin 9*/ 
);

XNOR02 UCL82(
    .Unknown_pin( _security_check_CHECK1_D9 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad15_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad2_         ) /* pin 3*/ 
);

XNOR02 UCL85(
    .Unknown_pin( _security_check_CHECK1_D12 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad9_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad5_         ) /* pin 3*/ 
);

XNOR02 UCL87(
    .Unknown_pin( _security_check_CHECK1_D8 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad17_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad1_         ) /* pin 3*/ 
);

XNOR02 UCL90(
    .Unknown_pin( _security_check_CHECK1_D14 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad7_         ) /* pin 3*/ 
);

XNOR02 UCL93(
    .Unknown_pin( _security_check_CHECK1_D11 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad11_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad4_         ) /* pin 3*/ 
);

XNOR02 UCL96(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY134 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ107_Pad5_        ) /* pin 3*/ 
);

XNOR02 UCL98(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY132 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ107_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO106(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY131 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ107_Pad2_        ) /* pin 3*/ 
);

M151C UCO109(
    .i3        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i0        ( _bus_io_BUS_DATA0        ) /* pin 4*/ ,
    .q         ( _security_management_security_decoder_K_LAYER_D1 ) /* pin 5*/ ,
    .s2        ( _security_management_bus_mux_key_KEY135 ) /* pin 9*/ ,
    .s1        ( _security_management_bus_mux_key_KEY136 ) /* pin 10*/ ,
    .s0        ( _security_management_bus_mux_key_KEY137 ) /* pin 11*/ ,
    .i7        ( _bus_io_BUS_DATA7        ) /* pin 12*/ ,
    .i6        ( _bus_io_BUS_DATA6        ) /* pin 13*/ ,
    .i5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .i4        ( _bus_io_BUS_DATA4        ) /* pin 15*/ 
);

XNOR02 UCO128(
    .Unknown_pin( _security_check_CHECK1_D0 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad17_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCO131(
    .Unknown_pin( _security_check_CHECK1_D15 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad8_         ) /* pin 3*/ 
);

XNOR02 UCO134(
    .Unknown_pin( _security_check_CHECK1_D2 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCJ120_Pad13_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL137_Pad3_        ) /* pin 3*/ 
);

NBUF03 UCO137(
    .i         ( _main_signals_DEFAULT    ) /* pin 1*/ ,
    .o         ( Net__UCL28_Pad1_         ) /* pin 2*/ 
);

AND02 UCO139(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY77 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D0 ) /* pin 3*/ 
);

AND02 UCO141(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY76 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D1 ) /* pin 3*/ 
);

AND02 UCO143(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY75 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D2 ) /* pin 3*/ 
);

AND02 UCO145(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY74 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D3 ) /* pin 3*/ 
);

AND02 UCO148(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY73 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D4 ) /* pin 3*/ 
);

AND02 UCO150(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY72 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D5 ) /* pin 3*/ 
);

AND02 UCO152(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY71 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D6 ) /* pin 3*/ 
);

AND02 UCO154(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY70 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D7 ) /* pin 3*/ 
);

AND02 UCO160(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY69 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D8 ) /* pin 3*/ 
);

AND02 UCO162(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY68 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D9 ) /* pin 3*/ 
);

AND02 UCO165(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY67 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D10 ) /* pin 3*/ 
);

AND02 UCO167(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY66 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D11 ) /* pin 3*/ 
);

AND02 UCO169(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY65 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D12 ) /* pin 3*/ 
);

INV01 UCO17(
    .i         ( Net__UCG259_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG11_Pad1_         ) /* pin 2*/ 
);

AND02 UCO171(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY64 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D13 ) /* pin 3*/ 
);

AND02 UCO174(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY63 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D14 ) /* pin 3*/ 
);

AND02 UCO176(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY62 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK1_D15 ) /* pin 3*/ 
);

XNOR02 UCO179(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY46 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL197_Pad5_        ) /* pin 3*/ 
);

XNOR02 UCO182(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY45 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL197_Pad4_        ) /* pin 3*/ 
);

M151C UCO184(
    .i3        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i0        ( _bus_io_BUS_DATA0        ) /* pin 4*/ ,
    .q         ( _security_management_security_decoder_K_LAYER_D3 ) /* pin 5*/ ,
    .s2        ( _security_management_bus_mux_key_KEY39 ) /* pin 9*/ ,
    .s1        ( _security_management_bus_mux_key_KEY40 ) /* pin 10*/ ,
    .s0        ( _security_management_bus_mux_key_KEY41 ) /* pin 11*/ ,
    .i7        ( _bus_io_BUS_DATA7        ) /* pin 12*/ ,
    .i6        ( _bus_io_BUS_DATA6        ) /* pin 13*/ ,
    .i5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .i4        ( _bus_io_BUS_DATA4        ) /* pin 15*/ 
);

XNOR02 UCO204(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY42 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL197_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCO206(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY18 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO206_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO209(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY47 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO209_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO217(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY15 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO217_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO220(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY17 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO220_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO223(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY16 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO223_Pad3_        ) /* pin 3*/ 
);

NAND05 UCO225(
    .i0        ( Net__UCO225_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCO217_Pad3_        ) /* pin 2*/ ,
    .i2        ( Net__UCO223_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCO220_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCO206_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_addr_decoder_registry_~CPS_ID ) /* pin 6*/ 
);

XNOR02 UCO229(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY14 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO225_Pad1_        ) /* pin 3*/ 
);

BUF12 UCO232(
    .i         ( Net__UBW371_Pad3_        ) /* pin 1*/ ,
    .o         ( _raster_interrupts_~OBJUP ) /* pin 2*/ 
);

XNOR02 UCO234(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY10 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO234_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO237(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY9 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO237_Pad3_        ) /* pin 3*/ 
);

NAND05 UCO240(
    .i0        ( Net__UCO240_Pad1_        ) /* pin 1*/ ,
    .i1        ( Net__UCO240_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCL231_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCO237_Pad3_        ) /* pin 4*/ ,
    .i4        ( Net__UCO234_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_MULT_X ) /* pin 6*/ 
);

XNOR02 UCO243(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY51 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO243_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO246(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY6 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO240_Pad1_        ) /* pin 3*/ 
);

XNOR02 UCO250(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY7 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO240_Pad2_        ) /* pin 3*/ 
);

NAND05 UCO252(
    .i0        ( Net__UCO209_Pad3_        ) /* pin 1*/ ,
    .i1        ( Net__UCO252_Pad2_        ) /* pin 2*/ ,
    .i2        ( Net__UCO252_Pad3_        ) /* pin 3*/ ,
    .i3        ( Net__UCO252_Pad4_        ) /* pin 4*/ ,
    .i4        ( Net__UCO243_Pad3_        ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_PRIORITY2 ) /* pin 6*/ 
);

XNOR02 UCO256(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY50 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO252_Pad4_        ) /* pin 3*/ 
);

XNOR02 UCO259(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY49 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO252_Pad3_        ) /* pin 3*/ 
);

M151C UCO26(
    .i3        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( _bus_io_BUS_DATA2        ) /* pin 2*/ ,
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 3*/ ,
    .i0        ( _bus_io_BUS_DATA0        ) /* pin 4*/ ,
    .q         ( _security_management_security_decoder_K_LAYER_D4 ) /* pin 5*/ ,
    .s2        ( _security_management_bus_mux_key_KEY112 ) /* pin 9*/ ,
    .s1        ( _security_management_bus_mux_key_KEY113 ) /* pin 10*/ ,
    .s0        ( _security_management_bus_mux_key_KEY114 ) /* pin 11*/ ,
    .i7        ( _bus_io_BUS_DATA7        ) /* pin 12*/ ,
    .i6        ( _bus_io_BUS_DATA6        ) /* pin 13*/ ,
    .i5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .i4        ( _bus_io_BUS_DATA4        ) /* pin 15*/ 
);

XNOR02 UCO262(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY48 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO252_Pad2_        ) /* pin 3*/ 
);

BUF12 UCO265(
    .i         ( Net__UCL207_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE5 ) /* pin 2*/ 
);

NAND03 UCO269(
    .i1        ( _lut_io_B5               ) /* pin 1*/ ,
    .i2        ( Net__UCG328_Pad2_        ) /* pin 2*/ ,
    .i3        ( _lut_io_B4               ) /* pin 3*/ ,
    .o         ( Net__UCO269_Pad4_        ) /* pin 4*/ 
);

XNOR02 UCO272(
    .Unknown_pin( _lut_io_B6               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCO269_Pad4_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO272_Pad3_        ) /* pin 3*/ 
);

NAND04 UCO274(
    .i1        ( _lut_io_B6               ) /* pin 1*/ ,
    .i2        ( _lut_io_B5               ) /* pin 2*/ ,
    .i3        ( _lut_io_B4               ) /* pin 3*/ ,
    .i4        ( Net__UCG328_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UCO274_Pad5_        ) /* pin 5*/ 
);

NAND02 UCO277(
    .i1        ( Net__UCG328_Pad2_        ) /* pin 1*/ ,
    .i2        ( _lut_io_B4               ) /* pin 2*/ ,
    .o         ( Net__UCO277_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO279(
    .Unknown_pin( _lut_io_B5               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCO277_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO279_Pad3_        ) /* pin 3*/ 
);

XNOR02 UCO282(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY30 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL224_Pad2_        ) /* pin 3*/ 
);

L8L #(.MiscRef(UCO291;UCO296;UCO298;UCO301;UCO304;UCO307;UCO309;UCO312) 
) UCO293(
    .le        ( _~IO_READ                ) /* pin 1*/ ,
    .d8        ( _CD15                    ) /* pin 2*/ ,
    .q8        ( Net__UCL292_Pad5_        ) /* pin 3*/ ,
    .d7        ( _CD14                    ) /* pin 4*/ ,
    .q7        ( Net__UCL292_Pad4_        ) /* pin 5*/ ,
    .d6        ( _CD13                    ) /* pin 6*/ ,
    .q6        ( Net__UCL292_Pad3_        ) /* pin 7*/ ,
    .d5        ( _CD12                    ) /* pin 8*/ ,
    .q5        ( Net__UCL292_Pad2_        ) /* pin 9*/ ,
    .d4        ( _CD11                    ) /* pin 10*/ ,
    .q4        ( Net__UCL235_Pad5_        ) /* pin 11*/ ,
    .d3        ( _CD10                    ) /* pin 12*/ ,
    .q3        ( Net__UCL235_Pad4_        ) /* pin 13*/ ,
    .d2        ( _CD9                     ) /* pin 14*/ ,
    .q2        ( Net__UCL235_Pad3_        ) /* pin 15*/ ,
    .d1        ( _CD8                     ) /* pin 16*/ ,
    .q1        ( Net__UCL235_Pad2_        ) /* pin 17*/ 
);

NAND02 UCO315(
    .i1        ( Net__UCG334_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCL310_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCO315_Pad3_        ) /* pin 3*/ 
);

NAND02 UCO317(
    .i1        ( Net__UCG334_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCO279_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCO317_Pad3_        ) /* pin 3*/ 
);

NAND02 UCO318(
    .i1        ( Net__UCG334_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCO272_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UCO318_Pad3_        ) /* pin 3*/ 
);

NAND02 UCO320(
    .i1        ( Net__UCG334_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCO320_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCO320_Pad3_        ) /* pin 3*/ 
);

INV01 UCO322(
    .i         ( _video_signals_video_counter_LINE6 ) /* pin 1*/ ,
    .o         ( Net__UCJ303_Pad3_        ) /* pin 2*/ 
);

INV01 UCO328(
    .i         ( Net__UCL313_Pad14_       ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_UCO328_Q ) /* pin 2*/ 
);

XNOR02 UCO329(
    .Unknown_pin( _lut_io_B7               ) /* pin 1*/ ,
    .Unknown_pin( Net__UCO274_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO320_Pad2_        ) /* pin 3*/ 
);

NBUF03 UCO332(
    .i         ( _main_signals_DEFAULT    ) /* pin 1*/ ,
    .o         ( Net__UCO332_Pad2_        ) /* pin 2*/ 
);

DFFCOO #(.MiscRef(UCO334) 
) UCO335(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCO315_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCO335_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B4               ) /* pin 5*/ 
);

DFFCOO UCO339(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCO317_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCO339_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B5               ) /* pin 5*/ 
);

DFFCOO UCO343(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCO318_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCO343_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B6               ) /* pin 5*/ 
);

DFFCOO UCO347(
    .clk       ( _lut_counter_CLK4M       ) /* pin 1*/ ,
    .d         ( Net__UCO320_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCO347_Pad3_        ) /* pin 3*/ ,
    .q_n       ( _lut_io_B7               ) /* pin 5*/ 
);

AND02 UCO354(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY142 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D11       ) /* pin 3*/ 
);

AND02 UCO356(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY143 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D10       ) /* pin 3*/ 
);

AND02 UCO358(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY5 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D0        ) /* pin 3*/ 
);

AND02 UCO360(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY4 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D1        ) /* pin 3*/ 
);

AND02 UCO363(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY3 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D2        ) /* pin 3*/ 
);

AND02 UCO365(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY2 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D3        ) /* pin 3*/ 
);

AND02 UCO367(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY1 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D4        ) /* pin 3*/ 
);

AND02 UCO369(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY0 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D5        ) /* pin 3*/ 
);

INV01 UCO372(
    .i         ( _video_signals_video_counter_LINE3 ) /* pin 1*/ ,
    .o         ( Net__UCJ307_Pad7_        ) /* pin 2*/ 
);

OR02 UCO374(
    .i1        ( _~BUS_RD                 ) /* pin 1*/ ,
    .i2        ( _security_management_~CPS_ID ) /* pin 2*/ ,
    .o         ( Net__UCL468_Pad1_        ) /* pin 3*/ 
);

M541C UCO383(
    .oe_n      ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .a0        ( _cps_id_CPS_ID_D11       ) /* pin 2*/ ,
    .a1        ( _cps_id_CPS_ID_D10       ) /* pin 3*/ ,
    .a2        ( _cps_id_CPS_ID_D5        ) /* pin 4*/ ,
    .a3        ( _cps_id_CPS_ID_D4        ) /* pin 5*/ ,
    .a4        ( _cps_id_CPS_ID_D3        ) /* pin 6*/ ,
    .a5        ( _cps_id_CPS_ID_D2        ) /* pin 7*/ ,
    .a6        ( _cps_id_CPS_ID_D1        ) /* pin 8*/ ,
    .a7        ( _cps_id_CPS_ID_D0        ) /* pin 9*/ ,
    .y7        ( _bus_io_BUS_DATA0        ) /* pin 11*/ ,
    .y6        ( _bus_io_BUS_DATA1        ) /* pin 12*/ ,
    .y5        ( _bus_io_BUS_DATA2        ) /* pin 13*/ ,
    .y4        ( _bus_io_BUS_DATA3        ) /* pin 14*/ ,
    .y3        ( _bus_io_BUS_DATA4        ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA5        ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA10       ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA11       ) /* pin 18*/ 
);

MUX28H #(.MiscRef(UCO406;UCO410;UCO413;UCO416;UCO420;UCO423;UCO426;UCO430) 
) UCO404(
    .a0        ( Net__UCJ404_Pad2_        ) /* pin 1*/ ,
    .a1        ( Net__UCJ404_Pad5_        ) /* pin 2*/ ,
    .a2        ( Net__UCJ404_Pad6_        ) /* pin 3*/ ,
    .a3        ( Net__UCJ404_Pad9_        ) /* pin 4*/ ,
    .a4        ( Net__UCJ404_Pad12_       ) /* pin 5*/ ,
    .a7        ( Net__UCJ404_Pad19_       ) /* pin 6*/ ,
    .a6        ( Net__UCJ404_Pad16_       ) /* pin 7*/ ,
    .a5        ( Net__UCJ404_Pad15_       ) /* pin 8*/ ,
    .b0        ( Net__UCL408_Pad2_        ) /* pin 9*/ ,
    .b1        ( Net__UCL408_Pad5_        ) /* pin 10*/ ,
    .b2        ( Net__UCL408_Pad6_        ) /* pin 11*/ ,
    .b3        ( Net__UCL408_Pad9_        ) /* pin 12*/ ,
    .b6        ( Net__UCL408_Pad16_       ) /* pin 13*/ ,
    .b4        ( Net__UCL408_Pad12_       ) /* pin 14*/ ,
    .b7        ( Net__UCL408_Pad19_       ) /* pin 15*/ ,
    .b5        ( Net__UCL408_Pad15_       ) /* pin 16*/ ,
    .q0        ( _lut_assembly_layer_draw_order_COL1_D0 ) /* pin 17*/ ,
    .q1        ( _lut_assembly_layer_draw_order_COL1_D1 ) /* pin 18*/ ,
    .q2        ( _lut_assembly_layer_draw_order_COL1_D2 ) /* pin 19*/ ,
    .q3        ( _lut_assembly_layer_draw_order_COL1_D3 ) /* pin 20*/ ,
    .q6        ( _lut_assembly_layer_draw_order_COL1_D6 ) /* pin 21*/ ,
    .q4        ( _lut_assembly_layer_draw_order_COL1_D4 ) /* pin 22*/ ,
    .q5        ( _lut_assembly_layer_draw_order_COL1_D5 ) /* pin 23*/ ,
    .q7        ( _lut_assembly_layer_draw_order_COL1_D7 ) /* pin 24*/ ,
    .anb       ( _obj_manager_sd_bus_~A~B ) /* pin 25*/ 
);

DFFCOR #(.MiscRef(UCO433;UCO453) 
) UCO435(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UCO435_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCO435_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCO435_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCO439(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UCO439_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCO439_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCO439_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCO444(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UCL450_Pad3_        ) /* pin 2*/ ,
    .q         ( Net__UCO444_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCO444_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCO448(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( Net__UCO448_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCO448_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCO448_Pad5_        ) /* pin 5*/ 
);

OR02 UCO455(
    .i1        ( Net__UCL450_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCO455_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCO439_Pad2_        ) /* pin 3*/ 
);

MUX24H #(.MiscRef(UCO459;UCO462;UCO466;UCO469) 
) UCO457(
    .a3        ( Net__UCL408_Pad9_        ) /* pin 1*/ ,
    .a2        ( Net__UCL408_Pad6_        ) /* pin 2*/ ,
    .a1        ( Net__UCL408_Pad5_        ) /* pin 3*/ ,
    .a0        ( Net__UCL408_Pad2_        ) /* pin 4*/ ,
    .q0        ( Net__UCO457_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UCO455_Pad2_        ) /* pin 6*/ ,
    .q2        ( Net__UCL450_Pad2_        ) /* pin 7*/ ,
    .q3        ( Net__UCO457_Pad8_        ) /* pin 8*/ ,
    .anb       ( _obj_manager_sd_bus_~A~B ) /* pin 9*/ ,
    .b3        ( Net__UCJ404_Pad9_        ) /* pin 10*/ ,
    .b2        ( Net__UCJ404_Pad6_        ) /* pin 11*/ ,
    .b1        ( Net__UCJ404_Pad5_        ) /* pin 12*/ ,
    .b0        ( Net__UCJ404_Pad2_        ) /* pin 13*/ 
);

MUX24H #(.MiscRef(UCO476;UCO479;UCO482;UCO486) 
) UCO474(
    .a3        ( Net__UCG415_Pad10_       ) /* pin 1*/ ,
    .a2        ( Net__UCG415_Pad9_        ) /* pin 2*/ ,
    .a1        ( Net__UCG415_Pad8_        ) /* pin 3*/ ,
    .a0        ( Net__UCG415_Pad7_        ) /* pin 4*/ ,
    .q0        ( Net__UCO474_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UCO474_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UCO474_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UCO474_Pad8_        ) /* pin 8*/ ,
    .anb       ( Net__UBH336_Pad11_       ) /* pin 9*/ ,
    .b3        ( Net__UCO474_Pad10_       ) /* pin 10*/ ,
    .b2        ( Net__UCO474_Pad11_       ) /* pin 11*/ ,
    .b1        ( Net__UCJ478_Pad3_        ) /* pin 12*/ ,
    .b0        ( Net__UCL491_Pad3_        ) /* pin 13*/ 
);

OR02 UCO491(
    .i1        ( Net__UCL448_Pad2_        ) /* pin 1*/ ,
    .i2        ( _~RESETI1                ) /* pin 2*/ ,
    .o         ( Net__UCG493_Pad4_        ) /* pin 3*/ 
);

BUF12 UCO494(
    .i         ( Net__UCG492_Pad1_        ) /* pin 1*/ ,
    .o         ( _CLK8M                   ) /* pin 2*/ 
);

DFFCOS #(.MiscRef(UCO496;UCO515) 
) UCO497(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCO474_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ498_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCO502(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCO474_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UBZ504_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCO506(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCO474_Pad7_        ) /* pin 2*/ ,
    .q         ( Net__UBZ509_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

DFFCOS UCO511(
    .clk       ( Net__UBD342_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UCO474_Pad8_        ) /* pin 2*/ ,
    .q         ( Net__UBZ515_Pad2_        ) /* pin 3*/ ,
    .i4        ( _raster_interrupts_counter1_~RESET ) /* pin 4*/ 
);

XNOR02 UCO519(
    .Unknown_pin( Net__UBZ515_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL519_Pad5_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO474_Pad10_       ) /* pin 3*/ 
);

XNOR02 UCO59(
    .Unknown_pin( _security_check_CHECK1_D10 ) /* pin 1*/ ,
    .Unknown_pin( Net__UCL108_Pad13_       ) /* pin 2*/ ,
    .Unknown_pin( Net__UCL76_Pad3_         ) /* pin 3*/ 
);

AND02 UCO61(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY98 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D8 ) /* pin 3*/ 
);

AND02 UCO63(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY97 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D9 ) /* pin 3*/ 
);

AND02 UCO66(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY96 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D10 ) /* pin 3*/ 
);

AND02 UCO68(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY95 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D11 ) /* pin 3*/ 
);

AND02 UCO70(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY94 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D12 ) /* pin 3*/ 
);

AND02 UCO72(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY93 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D13 ) /* pin 3*/ 
);

AND02 UCO74(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY92 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D14 ) /* pin 3*/ 
);

AND02 UCO77(
    .i1        ( Net__UCL28_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY91 ) /* pin 2*/ ,
    .o         ( _security_check_CHECK2_D15 ) /* pin 3*/ 
);

XNOR02 UCO79(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY59 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA3 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO79_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCO82(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY57 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO82_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCO85(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY60 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO85_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCO90(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY61 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA1 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO90_Pad3_         ) /* pin 3*/ 
);

NAND05 UCO92(
    .i0        ( Net__UCO82_Pad3_         ) /* pin 1*/ ,
    .i1        ( Net__UCO92_Pad2_         ) /* pin 2*/ ,
    .i2        ( Net__UCO79_Pad3_         ) /* pin 3*/ ,
    .i3        ( Net__UCO85_Pad3_         ) /* pin 4*/ ,
    .i4        ( Net__UCO90_Pad3_         ) /* pin 5*/ ,
    .o         ( _security_management_security_decoder_~K_PRIORITY1 ) /* pin 6*/ 
);

XNOR02 UCO96(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY58 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO92_Pad2_         ) /* pin 3*/ 
);

XNOR02 UCO98(
    .Unknown_pin( _security_management_addr_decoder_registry_KEY133 ) /* pin 1*/ ,
    .Unknown_pin( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .Unknown_pin( Net__UCJ107_Pad4_        ) /* pin 3*/ 
);

XNOR02 UCQ1(
    .Unknown_pin( Net__UCQ1_Pad1_          ) /* pin 1*/ ,
    .Unknown_pin( Net__UCQ1_Pad2_          ) /* pin 2*/ ,
    .Unknown_pin( Net__UCQ1_Pad3_          ) /* pin 3*/ 
);

DFFCOR UCQ10(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCQ1_Pad3_          ) /* pin 2*/ ,
    .q         ( Net__UCQ1_Pad2_          ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ10_Pad5_         ) /* pin 5*/ 
);

DFFCOR UCQ15(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCQ15_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCQ15_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ15_Pad5_         ) /* pin 5*/ 
);

DFFCOR UCQ19(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCQ19_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCQ19_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ19_Pad5_         ) /* pin 5*/ 
);

AND02 UCQ25(
    .i1        ( _security_management_keyinject_~RESET ) /* pin 1*/ ,
    .i2        ( _CD0                     ) /* pin 2*/ ,
    .o         ( _security_management_keyinject_DATA ) /* pin 3*/ 
);

AND02 UCQ28(
    .i1        ( _security_management_keyinject_DATA ) /* pin 1*/ ,
    .i2        ( _security_management_keyinject_STAGE3_Q ) /* pin 2*/ ,
    .o         ( _security_management_security_key_DATA ) /* pin 3*/ 
);

INV01 UCQ31(
    .i         ( _security_management_~SEC_E1 ) /* pin 1*/ ,
    .o         ( Net__UCQ31_Pad2_         ) /* pin 2*/ 
);

AND02 UCQ32(
    .i1        ( _security_management_SEC_E2 ) /* pin 1*/ ,
    .i2        ( Net__UCQ31_Pad2_         ) /* pin 2*/ ,
    .o         ( _security_management_keyinject_~RESET ) /* pin 3*/ 
);

INV01 UCQ36(
    .i         ( _security_management_SEC_STROBE ) /* pin 1*/ ,
    .o         ( Net__UCQ36_Pad2_         ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UCQ38;UCQ53) 
) UCQ40(
    .clk       ( _security_management_SEC_STROBE ) /* pin 1*/ ,
    .d         ( Net__UCQ40_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCQ40_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ40_Pad5_         ) /* pin 5*/ 
);

DFFCOR UCQ44(
    .clk       ( _security_management_SEC_STROBE ) /* pin 1*/ ,
    .d         ( Net__UCQ44_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCQ44_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ44_Pad5_         ) /* pin 5*/ 
);

DFFCOR UCQ48(
    .clk       ( _security_management_SEC_STROBE ) /* pin 1*/ ,
    .d         ( Net__UCQ48_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCQ48_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ48_Pad5_         ) /* pin 5*/ 
);

DFFCOR #(.MiscRef(UCQ4;UCQ24) 
) UCQ6(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCQ6_Pad2_          ) /* pin 2*/ ,
    .q         ( Net__UCQ6_Pad3_          ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCQ6_Pad5_          ) /* pin 5*/ 
);

M164C #(.MiscRef(UCR130;UCR134;UCR138;UCR142;UCR145;UCR149;UCR153;UCR157;UCR161) 
) UCR130(
    .d         ( _security_management_bus_mux_key_KEY80 ) /* pin 1*/ ,
    .q0        ( _security_management_bus_mux_key_KEY79 ) /* pin 3*/ ,
    .q1        ( _security_management_bus_mux_key_KEY78 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY77 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY76 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY75 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY74 ) /* pin 11*/ ,
    .q6        ( _security_management_security_decoder_KEY73 ) /* pin 12*/ ,
    .q7        ( _security_management_security_decoder_KEY72 ) /* pin 13*/ 
);

M164C #(.MiscRef(UCR170;UCR174;UCR178;UCR181;UCR185;UCR189;UCR193;UCR197) 
) UCR166(
    .d         ( _security_management_addr_decoder_registry_KEY24 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY23 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY22 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY21 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY20 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY19 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY18 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY17 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY16 ) /* pin 13*/ 
);

M164C #(.MiscRef(UCR204;UCR207;UCR211;UCR215;UCR219;UCR223;UCR227;UCR231) 
) UCR200(
    .d         ( _security_management_addr_decoder_registry_KEY16 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY15 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY14 ) /* pin 4*/ ,
    .q2        ( _security_management_bus_mux_key_KEY13 ) /* pin 5*/ ,
    .q3        ( _security_management_bus_mux_key_KEY12 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_bus_mux_key_KEY11 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY10 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY9 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY8 ) /* pin 13*/ 
);

M164C #(.MiscRef(UCR237;UCR241;UCR245;UCR249;UCR253;UCR257;UCR261;UCR265) 
) UCR233(
    .d         ( _security_management_addr_decoder_registry_KEY8 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY7 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY6 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY5 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY4 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY3 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY2 ) /* pin 11*/ ,
    .q6        ( _security_management_security_decoder_KEY1 ) /* pin 12*/ ,
    .q7        ( _security_management_security_decoder_KEY0 ) /* pin 13*/ 
);

AND02 UCR291(
    .i1        ( _main_signals_SEC_MODE0  ) /* pin 1*/ ,
    .i2        ( _main_signals_SEC_MODE1  ) /* pin 2*/ ,
    .o         ( _main_signals_DEFAULT    ) /* pin 3*/ 
);

NAND02 UCR294(
    .i1        ( Net__UCR294_Pad1_        ) /* pin 1*/ ,
    .i2        ( _main_signals_SEC_MODE1  ) /* pin 2*/ ,
    .o         ( Net__UCG504_Pad4_        ) /* pin 3*/ 
);

INV01 UCR296(
    .i         ( _main_signals_SEC_MODE0  ) /* pin 1*/ ,
    .o         ( Net__UCR294_Pad1_        ) /* pin 2*/ 
);

INV01 UCR297(
    .i         ( _video_signals_video_counter_LINE7 ) /* pin 1*/ ,
    .o         ( Net__UCJ303_Pad2_        ) /* pin 2*/ 
);

OR02 UCR298(
    .i1        ( _bus_io_MUTE_OUT         ) /* pin 1*/ ,
    .i2        ( _~BUS_RD                 ) /* pin 2*/ ,
    .o         ( Net__UCR298_Pad3_        ) /* pin 3*/ 
);

OR02 UCR301(
    .i1        ( Net__UCR301_Pad1_        ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_A  ) /* pin 2*/ ,
    .o         ( _FAD0                    ) /* pin 3*/ 
);

OR02 UCR303(
    .i1        ( Net__UCR301_Pad1_        ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_B  ) /* pin 2*/ ,
    .o         ( _FAD1                    ) /* pin 3*/ 
);

OR02 UCR305(
    .i1        ( Net__UCR301_Pad1_        ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_C  ) /* pin 2*/ ,
    .o         ( _FAD2                    ) /* pin 3*/ 
);

OR02 UCR308(
    .i1        ( Net__UCR301_Pad1_        ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_D  ) /* pin 2*/ ,
    .o         ( _FAD3                    ) /* pin 3*/ 
);

OR02 UCR311(
    .i1        ( _obj_manager_sd_bus_~A~B ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_A  ) /* pin 2*/ ,
    .o         ( _FBD0                    ) /* pin 3*/ 
);

OR02 UCR313(
    .i1        ( _obj_manager_sd_bus_~A~B ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_B  ) /* pin 2*/ ,
    .o         ( _FBD1                    ) /* pin 3*/ 
);

OR02 UCR315(
    .i1        ( _obj_manager_sd_bus_~A~B ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_C  ) /* pin 2*/ ,
    .o         ( _FBD2                    ) /* pin 3*/ 
);

OR02 UCR318(
    .i1        ( _obj_manager_sd_bus_~A~B ) /* pin 1*/ ,
    .i2        ( _obj_manager_xpd_bus4_D  ) /* pin 2*/ ,
    .o         ( _FBD3                    ) /* pin 3*/ 
);

INV01 UCR322(
    .i         ( Net__UCR322_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCL328_Pad1_        ) /* pin 2*/ 
);

NOR02 UCR329(
    .i1        ( _video_signals_video_signal_generator_UCO328_Q ) /* pin 1*/ ,
    .i2        ( _video_signals_video_signal_generator_UCJ328_Q ) /* pin 2*/ ,
    .o         ( Net__UCR322_Pad1_        ) /* pin 3*/ 
);

NAND13 UCR332(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( Net__UCJ306_Pad2_        ) /* pin 6*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i8        ( _FLIP                    ) /* pin 8*/ ,
    .i9        ( Net__UCJ307_Pad9_        ) /* pin 9*/ ,
    .i11       ( Net__UCJ307_Pad11_       ) /* pin 11*/ ,
    .o14       ( _video_signals_video_signal_generator_UCR332_Q ) /* pin 14*/ 
);

AND02 UCR339(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY141 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D12       ) /* pin 3*/ 
);

AND02 UCR342(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY140 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D13       ) /* pin 3*/ 
);

AND02 UCR344(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY139 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D14       ) /* pin 3*/ 
);

AND02 UCR346(
    .i1        ( Net__UCO332_Pad2_        ) /* pin 1*/ ,
    .i2        ( _security_management_security_decoder_KEY138 ) /* pin 2*/ ,
    .o         ( _cps_id_CPS_ID_D15       ) /* pin 3*/ 
);

D38GL UCR348(
    .sa        ( _security_management_addr_decoder_registry_CA1 ) /* pin 1*/ ,
    .sb        ( _security_management_addr_decoder_registry_CA2 ) /* pin 2*/ ,
    .sc        ( _security_management_addr_decoder_registry_CA3 ) /* pin 3*/ ,
    .g1        ( _security_management_addr_decoder_registry_CA1 ) /* pin 4*/ ,
    .g3_n      ( _security_management_addr_decoder_registry_CA2 ) /* pin 6*/ ,
    .o7_n      ( _security_management_addr_decoder_0x2E ) /* pin 7*/ ,
    .o6_n      ( _security_management_addr_decoder_0x2C ) /* pin 9*/ ,
    .o5_n      ( _security_management_addr_decoder_0x2A ) /* pin 10*/ ,
    .o4_n      ( _security_management_addr_decoder_0x28 ) /* pin 11*/ ,
    .o3_n      ( _security_management_addr_decoder_0x26 ) /* pin 12*/ ,
    .o2_n      ( Net__UCR348_Pad13_       ) /* pin 13*/ ,
    .o1_n      ( Net__UCR348_Pad14_       ) /* pin 14*/ ,
    .o0_n      ( Net__UCR348_Pad15_       ) /* pin 15*/ 
);

INV01 UCR375(
    .i         ( _video_signals_video_counter_LINE1 ) /* pin 1*/ ,
    .o         ( Net__UCJ307_Pad11_       ) /* pin 2*/ 
);

INV01 UCR384(
    .i         ( _video_signals_video_counter_LINE2 ) /* pin 1*/ ,
    .o         ( Net__UCJ307_Pad9_        ) /* pin 2*/ 
);

NOR02 UCR385(
    .i1        ( _video_signals_video_signal_generator_UCR388_Q ) /* pin 1*/ ,
    .i2        ( Net__UCR385_Pad2_        ) /* pin 2*/ ,
    .o         ( _video_signals_video_signal_generator_VSYNC ) /* pin 3*/ 
);

INV01 UCR388(
    .i         ( Net__UCL383_Pad14_       ) /* pin 1*/ ,
    .o         ( _video_signals_video_signal_generator_UCR388_Q ) /* pin 2*/ 
);

NAND13 UCR389(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i9        ( Net__UCJ307_Pad9_        ) /* pin 9*/ ,
    .o14       ( Net__UCR389_Pad14_       ) /* pin 14*/ 
);

INV01 UCR404(
    .i         ( Net__UCR389_Pad14_       ) /* pin 1*/ ,
    .o         ( Net__UCR404_Pad2_        ) /* pin 2*/ 
);

M541C UCR405(
    .oe_n      ( Net__UCL468_Pad1_        ) /* pin 1*/ ,
    .a0        ( _cps_id_CPS_ID_D12       ) /* pin 2*/ ,
    .a1        ( _cps_id_CPS_ID_D13       ) /* pin 3*/ ,
    .a2        ( _cps_id_CPS_ID_D14       ) /* pin 4*/ ,
    .a3        ( _cps_id_CPS_ID_D15       ) /* pin 5*/ ,
    .a4        ( Net__UCL463_Pad5_        ) /* pin 6*/ ,
    .a5        ( Net__UCL463_Pad4_        ) /* pin 7*/ ,
    .a6        ( Net__UCL460_Pad1_        ) /* pin 8*/ ,
    .a7        ( Net__UCL460_Pad2_        ) /* pin 9*/ ,
    .y7        ( _bus_io_BUS_DATA9        ) /* pin 11*/ ,
    .y6        ( _bus_io_BUS_DATA8        ) /* pin 12*/ ,
    .y5        ( _bus_io_BUS_DATA7        ) /* pin 13*/ ,
    .y4        ( _bus_io_BUS_DATA6        ) /* pin 14*/ ,
    .y3        ( _bus_io_BUS_DATA15       ) /* pin 15*/ ,
    .y2        ( _bus_io_BUS_DATA14       ) /* pin 16*/ ,
    .y1        ( _bus_io_BUS_DATA13       ) /* pin 17*/ ,
    .y0        ( _bus_io_BUS_DATA12       ) /* pin 18*/ 
);

INV01 UCR425(
    .i         ( _CLK8M                   ) /* pin 1*/ ,
    .o         ( Net__UCJ404_Pad11_       ) /* pin 2*/ 
);

OUTTRI UCR427(
    .i1        ( _bus_io_BUS_DATA12       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD12                    ) /* pin 3*/ 
);

NAND04 UCR443(
    .i1        ( Net__UCO448_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UCO444_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UCO439_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCO435_Pad3_        ) /* pin 4*/ ,
    .o         ( _obj_manager_~REWEN      ) /* pin 5*/ 
);

OUTTRI UCR446(
    .i1        ( _bus_io_BUS_DATA13       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD13                    ) /* pin 3*/ 
);

OR02 UCR451(
    .i1        ( Net__UCL450_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCO457_Pad5_        ) /* pin 2*/ ,
    .o         ( Net__UCO435_Pad2_        ) /* pin 3*/ 
);

OR02 UCR453(
    .i1        ( Net__UCL450_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UCO457_Pad8_        ) /* pin 2*/ ,
    .o         ( Net__UCO448_Pad2_        ) /* pin 3*/ 
);

M273C #(.MiscRef(UCR456;UCR463;UCR467;UCR472;UCR476;UCR481;UCR485;UCR490;UCR494) 
) UCR458(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCR458_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA1        ) /* pin 4*/ ,
    .q1        ( Net__UCR458_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCR458_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA2        ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA3        ) /* pin 8*/ ,
    .q3        ( Net__UCR458_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UCG318_Pad3_        ) /* pin 11*/ ,
    .q4        ( Net__UCR458_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA4        ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .q5        ( Net__UCR458_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCR458_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA6        ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA7        ) /* pin 18*/ ,
    .q7        ( Net__UCR458_Pad19_       ) /* pin 19*/ 
);

M152C UCR496(
    .i3        ( Net__UCR458_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCR458_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UCR458_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UCR458_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UCG466_Pad2_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCR458_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UCR458_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UCR458_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UCR458_Pad12_       ) /* pin 15*/ 
);

XNOR02 UCR515(
    .Unknown_pin( Net__UBZ509_Pad2_        ) /* pin 1*/ ,
    .Unknown_pin( Net__UCR515_Pad2_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCO474_Pad11_       ) /* pin 3*/ 
);

OR03 UCR519(
    .i1        ( Net__UCC479_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ498_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ504_Pad2_        ) /* pin 3*/ ,
    .o4        ( Net__UCR515_Pad2_        ) /* pin 4*/ 
);

M164C #(.MiscRef(UCR66;UCR70;UCR74;UCR78;UCR82;UCR86;UCR89;UCR93) 
) UCR62(
    .d         ( _security_management_security_decoder_KEY96 ) /* pin 1*/ ,
    .q0        ( _security_management_security_decoder_KEY95 ) /* pin 3*/ ,
    .q1        ( _security_management_security_decoder_KEY94 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY93 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY92 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY91 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY90 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY89 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY88 ) /* pin 13*/ 
);

M164C #(.MiscRef(UCR96;UCR100;UCR104;UCR108;UCR112;UCR115;UCR119;UCR123;UCR127) 
) UCR96(
    .d         ( _security_management_addr_decoder_registry_KEY88 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY87 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY86 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY85 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY84 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY83 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY82 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY81 ) /* pin 12*/ ,
    .q7        ( _security_management_bus_mux_key_KEY80 ) /* pin 13*/ 
);

NAND03 UCS1(
    .i1        ( Net__UCQ1_Pad2_          ) /* pin 1*/ ,
    .i2        ( Net__UCQ6_Pad3_          ) /* pin 2*/ ,
    .i3        ( _security_management_keyinject_STAGE2_Q ) /* pin 3*/ ,
    .o         ( Net__UCS1_Pad4_          ) /* pin 4*/ 
);

NAND05 UCS12(
    .i0        ( Net__UCQ19_Pad3_         ) /* pin 1*/ ,
    .i1        ( Net__UCQ15_Pad3_         ) /* pin 2*/ ,
    .i2        ( Net__UCQ1_Pad2_          ) /* pin 3*/ ,
    .i3        ( Net__UCQ6_Pad3_          ) /* pin 4*/ ,
    .i4        ( _security_management_keyinject_STAGE2_Q ) /* pin 5*/ ,
    .o         ( Net__UCS12_Pad6_         ) /* pin 6*/ 
);

M164C #(.MiscRef(UCS130;UCS134;UCS138;UCS142;UCS145;UCS149;UCS153;UCS157;UCS161) 
) UCS130(
    .d         ( _security_management_security_decoder_KEY104 ) /* pin 1*/ ,
    .q0        ( _security_management_security_decoder_KEY103 ) /* pin 3*/ ,
    .q1        ( _security_management_security_decoder_KEY102 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY101 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY100 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY99 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY98 ) /* pin 11*/ ,
    .q6        ( _security_management_security_decoder_KEY97 ) /* pin 12*/ ,
    .q7        ( _security_management_security_decoder_KEY96 ) /* pin 13*/ 
);

INV01 UCS16(
    .i         ( _security_management_keyinject_CLK ) /* pin 1*/ ,
    .o         ( _security_management_keyinject_~CLK ) /* pin 2*/ 
);

M164C #(.MiscRef(UCS170;UCS174;UCS178;UCS181;UCS185;UCS189;UCS193;UCS197) 
) UCS166(
    .d         ( _security_management_addr_decoder_registry_KEY48 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY47 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY46 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY45 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY44 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY43 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY42 ) /* pin 11*/ ,
    .q6        ( _security_management_bus_mux_key_KEY41 ) /* pin 12*/ ,
    .q7        ( _security_management_bus_mux_key_KEY40 ) /* pin 13*/ 
);

XNOR02 UCS18(
    .Unknown_pin( Net__UCS18_Pad1_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCQ19_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCQ19_Pad2_         ) /* pin 3*/ 
);

M164C #(.MiscRef(UCS204;UCS207;UCS211;UCS215;UCS219;UCS223;UCS227;UCS231) 
) UCS200(
    .d         ( _security_management_bus_mux_key_KEY40 ) /* pin 1*/ ,
    .q0        ( _security_management_bus_mux_key_KEY39 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY38 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY37 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY36 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY35 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY34 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY33 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY32 ) /* pin 13*/ 
);

AND02 UCS21(
    .i1        ( _security_management_keyinject_~RESET ) /* pin 1*/ ,
    .i2        ( _CD1                     ) /* pin 2*/ ,
    .o         ( _security_management_keyinject_CLK ) /* pin 3*/ 
);

M164C #(.MiscRef(UCS237;UCS241;UCS245;UCS249;UCS253;UCS257;UCS261;UCS265) 
) UCS234(
    .d         ( _security_management_addr_decoder_registry_KEY32 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY31 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY30 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY29 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY28 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY27 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY26 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY25 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY24 ) /* pin 13*/ 
);

INV01 UCS24(
    .i         ( Net__UCS24_Pad1_         ) /* pin 1*/ ,
    .o         ( Net__UCS24_Pad2_         ) /* pin 2*/ 
);

AND02 UCS26(
    .i1        ( _security_management_keyinject_CLK ) /* pin 1*/ ,
    .i2        ( _security_management_keyinject_STAGE3_Q ) /* pin 2*/ ,
    .o         ( _security_management_security_key_CLK ) /* pin 3*/ 
);

INV01 UCS29(
    .i         ( Net__UCS12_Pad6_         ) /* pin 1*/ ,
    .o         ( Net__UCS29_Pad2_         ) /* pin 2*/ 
);

DFFCOR #(.MiscRef(UCS31;UCS37) 
) UCS32(
    .clk       ( _security_management_keyinject_CLK ) /* pin 1*/ ,
    .d         ( Net__UCS29_Pad2_         ) /* pin 2*/ ,
    .q         ( Net__UCS32_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCS32_Pad5_         ) /* pin 5*/ 
);

INV01 UCS39(
    .i         ( Net__UCQ40_Pad3_         ) /* pin 1*/ ,
    .o         ( Net__UCQ40_Pad2_         ) /* pin 2*/ 
);

NAND02 UCS4(
    .i1        ( Net__UCQ6_Pad3_          ) /* pin 1*/ ,
    .i2        ( _security_management_keyinject_STAGE2_Q ) /* pin 2*/ ,
    .o         ( Net__UCQ1_Pad1_          ) /* pin 3*/ 
);

XOR02 UCS41(
    .i1        ( Net__UCQ44_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCQ40_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UCQ44_Pad2_         ) /* pin 3*/ 
);

AND03 UCS44(
    .i1        ( Net__UCQ48_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCS44_Pad2_         ) /* pin 2*/ ,
    .i3        ( Net__UCQ40_Pad2_         ) /* pin 3*/ ,
    .o         ( Net__UCS44_Pad4_         ) /* pin 4*/ 
);

INV01 UCS48(
    .i         ( Net__UCQ44_Pad3_         ) /* pin 1*/ ,
    .o         ( Net__UCS44_Pad2_         ) /* pin 2*/ 
);

NAND02 UCS50(
    .i1        ( Net__UCQ44_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCQ40_Pad3_         ) /* pin 2*/ ,
    .o         ( Net__UCS50_Pad3_         ) /* pin 3*/ 
);

XNOR02 UCS52(
    .Unknown_pin( Net__UCQ48_Pad3_         ) /* pin 1*/ ,
    .Unknown_pin( Net__UCS50_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCQ48_Pad2_         ) /* pin 3*/ 
);

XNOR02 UCS6(
    .Unknown_pin( Net__UCS1_Pad4_          ) /* pin 1*/ ,
    .Unknown_pin( Net__UCQ15_Pad3_         ) /* pin 2*/ ,
    .Unknown_pin( Net__UCQ15_Pad2_         ) /* pin 3*/ 
);

M164C #(.MiscRef(UCS66;UCS70;UCS74;UCS78;UCS82;UCS86;UCS89;UCS93) 
) UCS62(
    .d         ( _security_management_addr_decoder_registry_KEY120 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY119 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY118 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY117 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY116 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY115 ) /* pin 10*/ ,
    .q5        ( _security_management_bus_mux_key_KEY114 ) /* pin 11*/ ,
    .q6        ( _security_management_bus_mux_key_KEY113 ) /* pin 12*/ ,
    .q7        ( _security_management_bus_mux_key_KEY112 ) /* pin 13*/ 
);

NAND04 UCS9(
    .i1        ( Net__UCQ15_Pad3_         ) /* pin 1*/ ,
    .i2        ( Net__UCQ1_Pad2_          ) /* pin 2*/ ,
    .i3        ( Net__UCQ6_Pad3_          ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_STAGE2_Q ) /* pin 4*/ ,
    .o         ( Net__UCS18_Pad1_         ) /* pin 5*/ 
);

M164C #(.MiscRef(UCS96;UCS100;UCS104;UCS108;UCS112;UCS115;UCS119;UCS123;UCS127) 
) UCS96(
    .d         ( _security_management_bus_mux_key_KEY112 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY111 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY110 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY109 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY108 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY107 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY106 ) /* pin 11*/ ,
    .q6        ( _security_management_security_decoder_KEY105 ) /* pin 12*/ ,
    .q7        ( _security_management_security_decoder_KEY104 ) /* pin 13*/ 
);

INV01 UCT0(
    .i         ( Net__UCT0_Pad1_          ) /* pin 1*/ ,
    .o         ( Net__UCT0_Pad2_          ) /* pin 2*/ 
);

DFFCOR UCT12(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCT0_Pad1_          ) /* pin 2*/ ,
    .q         ( Net__UCT12_Pad3_         ) /* pin 3*/ ,
    .i4        ( _bus_io_MUTE_OUT         ) /* pin 4*/ ,
    .q_n       ( Net__UCT12_Pad5_         ) /* pin 5*/ 
);

M164C #(.MiscRef(UCT130;UCT134;UCT138;UCT142;UCT145;UCT149;UCT153;UCT157;UCT161) 
) UCT130(
    .d         ( _security_management_addr_decoder_registry_KEY128 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY127 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY126 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY125 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY124 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY123 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY122 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY121 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY120 ) /* pin 13*/ 
);

DFFCOR UCT16(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCT12_Pad3_         ) /* pin 2*/ ,
    .q         ( Net__UCS24_Pad1_         ) /* pin 3*/ ,
    .i4        ( _bus_io_MUTE_OUT         ) /* pin 4*/ ,
    .q_n       ( Net__UCT16_Pad5_         ) /* pin 5*/ 
);

BUF12 UCT164(
    .i         ( _security_management_security_key_CLK ) /* pin 1*/ ,
    .o         ( Net__UCR166_Pad8_        ) /* pin 2*/ 
);

M164C #(.MiscRef(UCT170;UCT174;UCT178;UCT181;UCT185;UCT189;UCT193;UCT197) 
) UCT166(
    .d         ( _security_management_security_decoder_KEY72 ) /* pin 1*/ ,
    .q0        ( _security_management_security_decoder_KEY71 ) /* pin 3*/ ,
    .q1        ( _security_management_security_decoder_KEY70 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY69 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY68 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY67 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY66 ) /* pin 11*/ ,
    .q6        ( _security_management_security_decoder_KEY65 ) /* pin 12*/ ,
    .q7        ( _security_management_security_decoder_KEY64 ) /* pin 13*/ 
);

XOR02 UCT2(
    .i1        ( _security_management_keyinject_STAGE2_Q ) /* pin 1*/ ,
    .i2        ( Net__UCQ6_Pad3_          ) /* pin 2*/ ,
    .o         ( Net__UCQ6_Pad2_          ) /* pin 3*/ 
);

M164C #(.MiscRef(UCT204;UCT207;UCT211;UCT215;UCT219;UCT223;UCT227;UCT231) 
) UCT200(
    .d         ( _security_management_security_decoder_KEY64 ) /* pin 1*/ ,
    .q0        ( _security_management_security_decoder_KEY63 ) /* pin 3*/ ,
    .q1        ( _security_management_security_decoder_KEY62 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY61 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY60 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY59 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY58 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY57 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY56 ) /* pin 13*/ 
);

DFFCOR UCT21(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( Net__UCS24_Pad1_         ) /* pin 2*/ ,
    .q         ( Net__UCT21_Pad3_         ) /* pin 3*/ ,
    .i4        ( _bus_io_MUTE_OUT         ) /* pin 4*/ ,
    .q_n       ( Net__UCT21_Pad5_         ) /* pin 5*/ 
);

M164C #(.MiscRef(UCT237;UCT241;UCT245;UCT249;UCT253;UCT257;UCT261;UCT265) 
) UCT234(
    .d         ( _security_management_addr_decoder_registry_KEY56 ) /* pin 1*/ ,
    .q0        ( _security_management_addr_decoder_registry_KEY55 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY54 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY53 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY52 ) /* pin 6*/ ,
    .cp        ( Net__UCR166_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY51 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY50 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY49 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY48 ) /* pin 13*/ 
);

NOR05 UCT27(
    .o1        ( Net__UCT27_Pad1_         ) /* pin 1*/ ,
    .i2        ( _security_management_keyinject_STAGE_RESET ) /* pin 2*/ ,
    .i3        ( Net__UCT0_Pad2_          ) /* pin 3*/ ,
    .i4        ( Net__UCS24_Pad2_         ) /* pin 4*/ ,
    .i5        ( Net__UCT12_Pad3_         ) /* pin 5*/ ,
    .i6        ( Net__UCT21_Pad3_         ) /* pin 6*/ 
);

OUTTRI UCT292(
    .i1        ( _bus_io_BUS_DATA0        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD0                     ) /* pin 3*/ 
);

OUTTRI UCT296(
    .i1        ( _bus_io_BUS_DATA1        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD1                     ) /* pin 3*/ 
);

OUTTRI UCT301(
    .i1        ( _bus_io_BUS_DATA2        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD2                     ) /* pin 3*/ 
);

OUTTRI UCT305(
    .i1        ( _bus_io_BUS_DATA3        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD3                     ) /* pin 3*/ 
);

DBUF UCT310(
    .i2        ( _obj_manager_FRAME       ) /* pin 2*/ ,
    .o3        ( Net__UCR301_Pad1_        ) /* pin 3*/ ,
    .o4        ( _obj_manager_sd_bus_~A~B ) /* pin 4*/ 
);

BUF12 UCT312(
    .i         ( Net__UCJ162_Pad5_        ) /* pin 1*/ ,
    .o         ( _video_signals_video_counter_LINE6 ) /* pin 2*/ 
);

OUTTRI UCT317(
    .i1        ( _bus_io_BUS_DATA4        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD4                     ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UCT31;UCT37) 
) UCT32(
    .clk       ( _security_management_keyinject_CLK ) /* pin 1*/ ,
    .d         ( Net__UCT27_Pad1_         ) /* pin 2*/ ,
    .q         ( Net__UCT32_Pad3_         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCT32_Pad5_         ) /* pin 5*/ 
);

INV01 UCT330(
    .i         ( Net__UCJ303_Pad6_        ) /* pin 1*/ ,
    .o         ( Net__UCT330_Pad2_        ) /* pin 2*/ 
);

OUTTRI UCT331(
    .i1        ( _bus_io_BUS_DATA5        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD5                     ) /* pin 3*/ 
);

NAND06 UCT335(
    .i1        ( _video_signals_video_counter_LINE4 ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE5 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE7 ) /* pin 4*/ ,
    .i5        ( Net__UCJ303_Pad1_        ) /* pin 5*/ ,
    .i6        ( _video_signals_video_signal_generator_UCR332_Q ) /* pin 6*/ ,
    .o9        ( Net__UCT335_Pad9_        ) /* pin 9*/ 
);

INV01 UCT340(
    .i         ( Net__UCT335_Pad9_        ) /* pin 1*/ ,
    .o         ( Net__UCT340_Pad2_        ) /* pin 2*/ 
);

NOR04 UCT341(
    .o1        ( _video_signals_video_signal_generator_~VBLANK ) /* pin 1*/ ,
    .i2        ( Net__UCT330_Pad2_        ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE8 ) /* pin 3*/ ,
    .i4        ( Net__UCT340_Pad2_        ) /* pin 4*/ ,
    .i5        ( _video_signals_video_signal_generator_UCJ337_Q ) /* pin 5*/ 
);

INV01 UCT345(
    .i         ( _video_signals_video_counter_LINE8 ) /* pin 1*/ ,
    .o         ( Net__UCJ303_Pad1_        ) /* pin 2*/ 
);

OUTTRI UCT346(
    .i1        ( _bus_io_BUS_DATA6        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD6                     ) /* pin 3*/ 
);

OUTTRI UCT359(
    .i1        ( _bus_io_BUS_DATA7        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD7                     ) /* pin 3*/ 
);

NAND13 UCT365(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i7        ( Net__UCJ307_Pad7_        ) /* pin 7*/ ,
    .i9        ( _video_signals_video_counter_LINE2 ) /* pin 9*/ ,
    .i11       ( Net__UCJ307_Pad11_       ) /* pin 11*/ ,
    .o14       ( Net__UCT365_Pad14_       ) /* pin 14*/ 
);

NAND08 UCT373(
    .i1        ( Net__UCJ303_Pad1_        ) /* pin 1*/ ,
    .i2        ( _video_signals_video_counter_LINE7 ) /* pin 2*/ ,
    .i3        ( _video_signals_video_counter_LINE6 ) /* pin 3*/ ,
    .i4        ( _video_signals_video_counter_LINE5 ) /* pin 4*/ ,
    .i5        ( _video_signals_video_counter_LINE4 ) /* pin 5*/ ,
    .i6        ( _video_signals_video_counter_LINE3 ) /* pin 6*/ ,
    .i7        ( _video_signals_video_counter_LINE2 ) /* pin 7*/ ,
    .i8        ( Net__UCJ307_Pad11_       ) /* pin 8*/ ,
    .o9        ( Net__UCT373_Pad9_        ) /* pin 9*/ 
);

RSLT UCT38(
    .s         ( Net__UCT32_Pad3_         ) /* pin 1*/ ,
    .r         ( _security_management_keyinject_STAGE_RESET ) /* pin 2*/ ,
    .q         ( _security_management_keyinject_STAGE2_Q ) /* pin 3*/ 
);

OUTTRI UCT383(
    .i1        ( _bus_io_BUS_DATA8        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD8                     ) /* pin 3*/ 
);

INV01 UCT388(
    .i         ( Net__UCT373_Pad9_        ) /* pin 1*/ ,
    .o         ( Net__UCR385_Pad2_        ) /* pin 2*/ 
);

OUTTRI UCT389(
    .i1        ( _bus_io_BUS_DATA9        ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD9                     ) /* pin 3*/ 
);

INV01 UCT393(
    .i         ( Net__UCT365_Pad14_       ) /* pin 1*/ ,
    .o         ( Net__UCT393_Pad2_        ) /* pin 2*/ 
);

OUTTRI UCT399(
    .i1        ( _bus_io_BUS_DATA10       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD10                    ) /* pin 3*/ 
);

INV01 UCT404(
    .i         ( Net__UCT404_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCT404_Pad2_        ) /* pin 2*/ 
);

NOR02 UCT405(
    .i1        ( Net__UCT393_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UCR404_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UCT404_Pad1_        ) /* pin 3*/ 
);

DFFCOR #(.MiscRef(UCT407;UCT422) 
) UCT409(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( _ASD8                    ) /* pin 2*/ ,
    .q         ( Net__UCT409_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCT409_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCT413(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( _BSD8                    ) /* pin 2*/ ,
    .q         ( Net__UCT413_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCT413_Pad5_        ) /* pin 5*/ 
);

DFFCOR UCT418(
    .clk       ( Net__UCJ404_Pad11_       ) /* pin 1*/ ,
    .d         ( _obj_manager_OBJEO       ) /* pin 2*/ ,
    .q         ( Net__UCL450_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UCJ404_Pad1_        ) /* pin 4*/ ,
    .q_n       ( Net__UCT418_Pad5_        ) /* pin 5*/ 
);

RSLT UCT42(
    .s         ( Net__UCS32_Pad3_         ) /* pin 1*/ ,
    .r         ( _security_management_keyinject_STAGE_RESET ) /* pin 2*/ ,
    .q         ( _security_management_keyinject_STAGE3_Q ) /* pin 3*/ 
);

MUX2H #(.MiscRef(UCT425) 
) UCT426(
    .Unknown_pin( _obj_manager_sd_bus_~A~B ) /* pin 1*/ ,
    .Unknown_pin( Net__UCT413_Pad3_        ) /* pin 2*/ ,
    .Unknown_pin( Net__UCT409_Pad3_        ) /* pin 3*/ ,
    .Unknown_pin( _lut_assembly_layer_draw_order_COL1_D8 ) /* pin 4*/ 
);

OUTTRI UCT432(
    .i1        ( _bus_io_BUS_DATA11       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD11                    ) /* pin 3*/ 
);

OUTTRI UCT447(
    .i1        ( _bus_io_BUS_DATA14       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD14                    ) /* pin 3*/ 
);

INV01 UCT45(
    .i         ( _bus_io_MUTE_OUT         ) /* pin 1*/ ,
    .o         ( _security_management_keyinject_STAGE_RESET ) /* pin 2*/ 
);

OUTTRI UCT452(
    .i1        ( _bus_io_BUS_DATA15       ) /* pin 1*/ ,
    .i2        ( Net__UCR298_Pad3_        ) /* pin 2*/ ,
    .t3        ( _CD15                    ) /* pin 3*/ 
);

M273C #(.MiscRef(UCT456;UCT464;UCT468;UCT472;UCT477;UCT481;UCT486;UCT490;UCT495) 
) UCT459(
    .r_n       ( _counter_fi_~LUT_RESET   ) /* pin 1*/ ,
    .q0        ( Net__UCT459_Pad2_        ) /* pin 2*/ ,
    .d0        ( _bus_io_BUS_DATA0        ) /* pin 3*/ ,
    .d1        ( _bus_io_BUS_DATA1        ) /* pin 4*/ ,
    .q1        ( Net__UCT459_Pad5_        ) /* pin 5*/ ,
    .q2        ( Net__UCT459_Pad6_        ) /* pin 6*/ ,
    .d2        ( _bus_io_BUS_DATA2        ) /* pin 7*/ ,
    .d3        ( _bus_io_BUS_DATA3        ) /* pin 8*/ ,
    .q3        ( Net__UCT459_Pad9_        ) /* pin 9*/ ,
    .clk       ( Net__UBZ335_Pad3_        ) /* pin 11*/ ,
    .q4        ( Net__UCT459_Pad12_       ) /* pin 12*/ ,
    .d4        ( _bus_io_BUS_DATA4        ) /* pin 13*/ ,
    .d5        ( _bus_io_BUS_DATA5        ) /* pin 14*/ ,
    .q5        ( Net__UCT459_Pad15_       ) /* pin 15*/ ,
    .q6        ( Net__UCT459_Pad16_       ) /* pin 16*/ ,
    .d6        ( _bus_io_BUS_DATA6        ) /* pin 17*/ ,
    .d7        ( _bus_io_BUS_DATA7        ) /* pin 18*/ ,
    .q7        ( Net__UCT459_Pad19_       ) /* pin 19*/ 
);

DFFCOR #(.MiscRef(UCT47;UCT53) 
) UCT48(
    .clk       ( Net__UCQ36_Pad2_         ) /* pin 1*/ ,
    .d         ( Net__UCS44_Pad4_         ) /* pin 2*/ ,
    .q         ( _bus_io_MUTE_OUT         ) /* pin 3*/ ,
    .i4        ( _security_management_keyinject_~RESET ) /* pin 4*/ ,
    .q_n       ( Net__UCT48_Pad5_         ) /* pin 5*/ 
);

M152C UCT497(
    .i3        ( Net__UCT459_Pad9_        ) /* pin 1*/ ,
    .i2        ( Net__UCT459_Pad6_        ) /* pin 2*/ ,
    .i1        ( Net__UCT459_Pad5_        ) /* pin 3*/ ,
    .i0        ( Net__UCT459_Pad2_        ) /* pin 4*/ ,
    .q_n       ( Net__UCL487_Pad2_        ) /* pin 5*/ ,
    .s2        ( _lut_assembly_priority_transparency_OUT2 ) /* pin 9*/ ,
    .s1        ( _lut_assembly_priority_transparency_OUT1 ) /* pin 10*/ ,
    .s0        ( _lut_assembly_priority_transparency_OUT0 ) /* pin 11*/ ,
    .i7        ( Net__UCT459_Pad19_       ) /* pin 12*/ ,
    .i6        ( Net__UCT459_Pad16_       ) /* pin 13*/ ,
    .i5        ( Net__UCT459_Pad15_       ) /* pin 14*/ ,
    .i4        ( Net__UCT459_Pad12_       ) /* pin 15*/ 
);

BUF12 UCT516(
    .i         ( Net__UCT516_Pad1_        ) /* pin 1*/ ,
    .o         ( Net__UCG492_Pad1_        ) /* pin 2*/ 
);

DFFCOR UCT518(
    .clk       ( Net__UCG508_Pad3_        ) /* pin 1*/ ,
    .d         ( Net__UCT518_Pad2_        ) /* pin 2*/ ,
    .q         ( Net__UCT516_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UCG493_Pad4_        ) /* pin 4*/ ,
    .q_n       ( Net__UCT518_Pad2_        ) /* pin 5*/ 
);

BUF12 UCT60(
    .i         ( _security_management_security_key_CLK ) /* pin 1*/ ,
    .o         ( Net__UCR130_Pad8_        ) /* pin 2*/ 
);

M164C #(.MiscRef(UCT66;UCT70;UCT74;UCT78;UCT82;UCT86;UCT89;UCT93) 
) UCT62(
    .d         ( _security_management_security_key_DATA ) /* pin 1*/ ,
    .q0        ( _security_management_security_decoder_KEY143 ) /* pin 3*/ ,
    .q1        ( _security_management_security_decoder_KEY142 ) /* pin 4*/ ,
    .q2        ( _security_management_security_decoder_KEY141 ) /* pin 5*/ ,
    .q3        ( _security_management_security_decoder_KEY140 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_security_decoder_KEY139 ) /* pin 10*/ ,
    .q5        ( _security_management_security_decoder_KEY138 ) /* pin 11*/ ,
    .q6        ( _security_management_bus_mux_key_KEY137 ) /* pin 12*/ ,
    .q7        ( _security_management_bus_mux_key_KEY136 ) /* pin 13*/ 
);

DFFCOR #(.MiscRef(UCT6;UCT25) 
) UCT7(
    .clk       ( _security_management_keyinject_~CLK ) /* pin 1*/ ,
    .d         ( _security_management_keyinject_DATA ) /* pin 2*/ ,
    .q         ( Net__UCT0_Pad1_          ) /* pin 3*/ ,
    .i4        ( _bus_io_MUTE_OUT         ) /* pin 4*/ ,
    .q_n       ( Net__UCT7_Pad5_          ) /* pin 5*/ 
);

M164C #(.MiscRef(UCT96;UCT100;UCT104;UCT108;UCT112;UCT115;UCT119;UCT123;UCT127) 
) UCT96(
    .d         ( _security_management_bus_mux_key_KEY136 ) /* pin 1*/ ,
    .q0        ( _security_management_bus_mux_key_KEY135 ) /* pin 3*/ ,
    .q1        ( _security_management_addr_decoder_registry_KEY134 ) /* pin 4*/ ,
    .q2        ( _security_management_addr_decoder_registry_KEY133 ) /* pin 5*/ ,
    .q3        ( _security_management_addr_decoder_registry_KEY132 ) /* pin 6*/ ,
    .cp        ( Net__UCR130_Pad8_        ) /* pin 8*/ ,
    .q4        ( _security_management_addr_decoder_registry_KEY131 ) /* pin 10*/ ,
    .q5        ( _security_management_addr_decoder_registry_KEY130 ) /* pin 11*/ ,
    .q6        ( _security_management_addr_decoder_registry_KEY129 ) /* pin 12*/ ,
    .q7        ( _security_management_addr_decoder_registry_KEY128 ) /* pin 13*/ 
);

