`timescale 1ns/1ps

module _8BUF11( // ref: 8BUF11
    inout     y7, // pin: 11
    inout     y6, // pin: 12
    inout     y5, // pin: 13
    inout     y4, // pin: 14
    inout     y3, // pin: 15
    inout     y2, // pin: 16
    inout     y1, // pin: 17
    inout     y0, // pin: 18
    input     a0, // pin: 2
    input     a1, // pin: 3
    input     a2, // pin: 4
    input     a3, // pin: 5
    input     a4, // pin: 6
    input     a5, // pin: 7
    input     a6, // pin: 8
    input     a7  // pin: 9
);

assign #1 {y7, y6, y5, y4, y3, y2, y1, y0} =
       {a7, a6, a5, a4, a3, a2, a1, a0};

endmodule


module AND02( // ref: AND02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o = i1&i2;

endmodule


module AND03( // ref: AND03
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o  // pin: 4
);

assign #1 o = i1&i2&i3;

endmodule


module AND04( // ref: AND04
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o  // pin: 5
);

assign #1 o = &{i1,i2,i3,i4};

endmodule


module AND08( // ref: AND08
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    input     i7, // pin: 7
    input     i8, // pin: 8
    output    o9  // pin: 9
);

assign #1 o9 = &{i1,i2,i3,i4,i5,i6,i7,i8};

endmodule


module AO22( // ref: AO22
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o5  // pin: 5
);

assign #1 o5 = (i2&i4) | (i1&i3);

endmodule


module AOI21( // ref: AOI21
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4  // pin: 4
);

assign #1 o4 = ~((i2&i1) | i3);

endmodule


module AOI22( // ref: AOI22
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o5  // pin: 5
);

assign #1 o5 = ~((i2&i4) | (i1&i3));

endmodule


module AOI23( // ref: AOI23
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    input     i7  // pin: 7
);

assign #1 o4 = ~((i7&i3) | (i6&i2) | (i1&i5));

endmodule


module AOI33( // ref: AOI33
    input     i1, // pin: 1
    input     i10, // pin: 10
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    input     i7, // pin: 7
    input     i8, // pin: 8
    input     i9  // pin: 9
);

assign #1 o4 = ~((i10&i7&i3) | (i9&i6&i2) | (i8&i1&i5));

endmodule


module AOI41( // ref: AOI41
    input     i1, // pin: 1
    input     i2, // pin: 3
    input     i3, // pin: 4
    input     i4, // pin: 5
    input     i6, // pin: 6
    output    o2  // pin: 2
);

assign #1 o2 = ~(i6 | (i1&i2&i3&i4));

endmodule


module BUF01( // ref: BUF01
    input     i, // pin: 1
    inout     o  // pin: 2
);

assign #1 o=i;

endmodule


module BUF02( // ref: BUF02
    input     i, // pin: 1
    inout     o  // pin: 2
);

assign #1 o=i;

endmodule


module BUF12( // ref: BUF??
    input     i, // pin: 1
    inout     o  // pin: 2
);

assign #1 o=i;

endmodule

module DELAY( // ref: DELAY?
    input     i, // pin: 1
    inout     o  // pin: 2
);
assign #1 o=i;
endmodule

// is it three-state or a regular enable signal?
module D24GL( // ref: D24GL
    input     sa, // pin: 1
    input     sb, // pin: 2
    input     g_n, // pin: 3
    inout     o3_n, // pin: 4
    inout     o2_n, // pin: 5
    inout     o1_n, // pin: 6
    inout     o0_n  // pin: 7
);

wire [1:0] s = {sb,sa};

assign #1 o0_n = !g_n ? s!=2'd0 : 1'b1;
assign #1 o1_n = !g_n ? s!=2'd1 : 1'b1;
assign #1 o2_n = !g_n ? s!=2'd2 : 1'b1;
assign #1 o3_n = !g_n ? s!=2'd3 : 1'b1;

endmodule


module D24L( // ref: D24L
    input     sa, // pin: 1
    input     sb, // pin: 2
    output    o3_n, // pin: 3
    output    o2_n, // pin: 4
    output    o1_n, // pin: 5
    output    o0_n  // pin: 6
);

wire [1:0] s = {sb,sa};

assign #1 o0_n = s!=2'd0;
assign #1 o1_n = s!=2'd1;
assign #1 o2_n = s!=2'd2;
assign #1 o3_n = s!=2'd3;

endmodule


module D38GL( // ref: D38GL
    input     sa, // pin: 1
    inout     o5_n, // pin: 10
    inout     o4_n, // pin: 11
    inout     o3_n, // pin: 12
    inout     o2_n, // pin: 13
    inout     o1_n, // pin: 14
    inout     o0_n, // pin: 15
    input     sb, // pin: 2
    input     sc, // pin: 3
    input     g1, // pin: 4
    input     g2_n, // pin: 5
    input     g3_n, // pin: 6
    inout     o7_n, // pin: 7
    inout     o6_n  // pin: 9
);

wire [2:0] s = {sc, sb, sa};
wire en = g1 & ~g2_n & ~g3_n;
assign #1 o0_n = en ? s!=3'd0 : 1'b1;
assign #1 o1_n = en ? s!=3'd1 : 1'b1;
assign #1 o2_n = en ? s!=3'd2 : 1'b1;
assign #1 o3_n = en ? s!=3'd3 : 1'b1;
assign #1 o4_n = en ? s!=3'd4 : 1'b1;
assign #1 o5_n = en ? s!=3'd5 : 1'b1;
assign #1 o6_n = en ? s!=3'd6 : 1'b1;
assign #1 o7_n = en ? s!=3'd7 : 1'b1;

endmodule


module DBUF( // ref: DBUF
    input     i2, // pin: 2
    output    o3, // pin: 3
    output    o4  // pin: 4
);

assign #1 o3 = ~i2;
assign #1 o4 = i2;

endmodule


module DFFCOO( // ref: DFFCOO
    input      clk, // pin: 1
    input      d, // pin: 2
    output reg  q, // pin: 3
    output reg q_n  // pin: 5
);

always @(posedge clk) begin
    q   <= d;
    q_n <= ~d;
end

endmodule

module FFCOO( // ref: FFCOO
    input      clk, // pin: 1
    input      d, // pin: 2
    output reg q, // pin: 3
    output reg q_n  // pin: 5
);

always @(posedge clk) begin
    q   <= d;
    q_n <= ~d;
end

endmodule


module DFFCOR( // ref: DFFCOR
    input      clk, // pin: 1
    input      d, // pin: 2
    input      i4, // pin: 4
    output reg q, // pin: 3
    output reg q_n  // pin: 5
);

always @(posedge clk, negedge i4) begin
    if( !i4 ) begin
        q  <= 1'b0;
        q_n<= 1'b1;
    end else begin
        q   <= d;
        q_n <= ~d;
    end
end

endmodule


module DFFCOS( // ref: DFFCOS
    input     clk, // pin: 1
    input     d, // pin: 2
    output reg q, // pin: 3
    input     i4  // pin: 4
);

always @(posedge clk, negedge i4) begin
    if( !i4 ) begin
        q  <= 1'b1;
    end else begin
        q   <= d;
    end
end

endmodule


module DFGOO( // ref: DFGOO
    input     clk, // pin: 1
    input     d, // pin: 2
    output reg q, // pin: 3
    input     i5  // pin: 5
);

always @(posedge clk, negedge i5) begin
    if( !i5 ) begin
        q  <= 1'b1;
    end else begin
        q   <= d;
    end
end

endmodule


module DLTOO( // ref: DLTOO
    input     le, // pin: 1
    input     d, // pin: 2
    output reg q  // pin: 3
);

// latch
always @(le)
    q = d;

endmodule


module INV01( // ref: INV01
    input     i, // pin: 1
    output    o  // pin: 2
);

assign #1 o = i;

endmodule


module L( // ref: L
    input     le, // pin: 1
    input     d, // pin: 2
    output reg q  // pin: 3
);

// latch
always @(le)
    q = d;

endmodule


module L4( // ref: L4
    input     le, // pin: 1
    input     d1, // pin: 2
    input     d2, // pin: 3
    input     d3, // pin: 4
    input     d4, // pin: 5
    output reg q1, // pin: 6
    output reg q2, // pin: 7
    output reg q3, // pin: 8
    output reg q4  // pin: 9
);

always @(le) begin
    q1 = ~d1;
    q2 = ~d2;
    q3 = ~d3;
    q4 = ~d4;
end

endmodule


module L8( // ref: L8
    input     le, // pin: 1
    input     d1, // pin: 16
    input     d2, // pin: 14
    input     d3, // pin: 12
    input     d4, // pin: 10
    input     d5, // pin: 8
    input     d8, // pin: 2
    input     d7, // pin: 4
    input     d6, // pin: 6

    output reg q1, // pin: 17
    output reg q2, // pin: 15
    output reg q3, // pin: 13
    output reg q4, // pin: 11
    output reg q5, // pin: 9
    output reg q6, // pin: 7
    output reg q7, // pin: 5
    output reg q8  // pin: 3
);

always @(le) begin
    q1 = d1;
    q2 = d2;
    q3 = d3;
    q4 = d4;
    q5 = d5;
    q6 = d6;
    q7 = d7;
    q8 = d8;
end

endmodule


module L8G( // ref: L8G
    input     le, // pin: 1
    input     d4, // pin: 10
    input     d3, // pin: 12
    input     d2, // pin: 14
    input     d1, // pin: 16
    input     g_n, // pin: 18
    input     d8, // pin: 2
    input     d7, // pin: 4
    input     d6, // pin: 6
    input     d5, // pin: 8

    inout     q1, // pin: 17
    inout     q2, // pin: 15
    inout     q3, // pin: 13
    inout     q4, // pin: 11
    inout     q5, // pin: 9
    inout     q6, // pin: 7
    inout     q7, // pin: 5
    inout     q8  // pin: 3
);

reg [8:1] latch;

assign {
    q8, q7, q6, q5,
    q4, q3, q2, q1
} = !g_n ? latch : 8'hzz;

always @(le) begin
    latch[1] = d1;
    latch[2] = d2;
    latch[3] = d3;
    latch[4] = d4;
    latch[5] = d5;
    latch[6] = d6;
    latch[7] = d7;
    latch[8] = d8;
end

endmodule


module L8L( // ref: L8L
    input     le, // pin: 1
    input     d4, // pin: 10
    output    q4, // pin: 11
    input     d3, // pin: 12
    output    q3, // pin: 13
    input     d2, // pin: 14
    output    q2, // pin: 15
    input     d1, // pin: 16
    output    q1, // pin: 17
    input     d8, // pin: 2
    output    q8, // pin: 3
    input     d7, // pin: 4
    output    q7, // pin: 5
    input     d6, // pin: 6
    output    q6, // pin: 7
    input     d5, // pin: 8
    output    q5  // pin: 9
);

reg [8:1] latch;

assign {
    q8, q7, q6, q5,
    q4, q3, q2, q1
} = latch;

always @(le) begin
    latch[1] = ~d1;
    latch[2] = ~d2;
    latch[3] = ~d3;
    latch[4] = ~d4;
    latch[5] = ~d5;
    latch[6] = ~d6;
    latch[7] = ~d7;
    latch[8] = ~d8;
end

endmodule


module M125C( // ref: M125C
    input     i1, // pin: 1
    input     i2, // pin: 2
    inout     t3  // pin: 3
);

assign #1 t3 = i1 ? i2 : 1'bz;

endmodule

module M125CX6( // ref: M125C-X6
    input     oe_n, // pin: 1
    inout     y5, // pin: 13
    inout     y4, // pin: 14
    inout     y3, // pin: 15
    inout     y2, // pin: 16
    inout     y1, // pin: 17
    inout     y0, // pin: 18
    input     a0, // pin: 2
    input     a1, // pin: 3
    input     a2, // pin: 4
    input     a3, // pin: 5
    input     a4, // pin: 6
    input     a5  // pin: 7
);

assign #1 y0 = oe_n ? a0 : 1'bz;
assign #1 y1 = oe_n ? a1 : 1'bz;
assign #1 y2 = oe_n ? a2 : 1'bz;
assign #1 y3 = oe_n ? a3 : 1'bz;
assign #1 y4 = oe_n ? a4 : 1'bz;
assign #1 y5 = oe_n ? a5 : 1'bz;

endmodule


module M151C( // ref: M151C
    input     i3, // pin: 1
    input     s1, // pin: 10
    input     s0, // pin: 11
    input     i7, // pin: 12
    input     i6, // pin: 13
    input     i5, // pin: 14
    input     i4, // pin: 15
    input     i2, // pin: 2
    input     i1, // pin: 3
    input     i0, // pin: 4
    output reg q, // pin: 5
    input     e_n, // pin: 7
    input     s2  // pin: 9
);

wire [2:0] s = {s2,s1,s0};

always @(*) begin
    if( e_n )
        q = 0;
    else
        case( s )
            3'd0: q = i0;
            3'd1: q = i1;
            3'd2: q = i2;
            3'd3: q = i3;
            3'd4: q = i4;
            3'd5: q = i5;
            3'd6: q = i6;
            3'd7: q = i7;
        endcase
end

endmodule


module M152C( // ref: M152C
    input     i3, // pin: 1
    input     s1, // pin: 10
    input     s0, // pin: 11
    input     i7, // pin: 12
    input     i6, // pin: 13
    input     i5, // pin: 14
    input     i4, // pin: 15
    input     i2, // pin: 2
    input     i1, // pin: 3
    input     i0, // pin: 4
    output reg q_n, // pin: 5
    input     s2  // pin: 9
);

wire [2:0] s = {s2,s1,s0};

always @(*) begin
    case( s )
        3'd0: q_n = ~i0;
        3'd1: q_n = ~i1;
        3'd2: q_n = ~i2;
        3'd3: q_n = ~i3;
        3'd4: q_n = ~i4;
        3'd5: q_n = ~i5;
        3'd6: q_n = ~i6;
        3'd7: q_n = ~i7;
    endcase
end

endmodule


module M157C( // ref: M157C
    input     a3, // pin: 1
    input     b3, // pin: 10
    input     b2, // pin: 11
    input     b1, // pin: 12
    input     b0, // pin: 13
    input     e_n, // pin: 14
    input     a2, // pin: 2
    input     a1, // pin: 3
    input     a0, // pin: 4
    output    q0, // pin: 5
    input     anb, // pin: 9
    output    q1, // pin: 6
    output    q2, // pin: 7
    output    q3  // pin: 8
);

assign q0 = !e_n ? 1'b0 : (anb ? b0 : a0);
assign q1 = !e_n ? 1'b0 : (anb ? b1 : a1);
assign q2 = !e_n ? 1'b0 : (anb ? b2 : a2);
assign q3 = !e_n ? 1'b0 : (anb ? b3 : a3);

endmodule


// shift register, but is it left or right?
module M164C( // ref: M164C
    input      d, // pin: 1
    output reg q0, // pin: 3
    output reg q1, // pin: 4
    output reg q2, // pin: 5
    output reg q3, // pin: 6
    output reg q4, // pin: 10
    output reg q5, // pin: 11
    output reg q6, // pin: 12
    output reg q7, // pin: 13
    input      cp  // pin: 8
);

always @(posedge cp)
    {q7,q6,q5,q4,q3,q2,q1,q0} <= {q6,q5,q4,q3,q2,q1,q0,d};

endmodule


module M198C( // ref: M198C
    input     l0, // pin: 1
    output    q0, // pin: 10
    output    q1, // pin: 11
    output    q2, // pin: 12
    output    q3, // pin: 13
    output    q4, // pin: 14
    output    q5, // pin: 15
    output    q6, // pin: 16
    output    q7, // pin: 17
    input     sr, // pin: 18
    input     clk, // pin: 19
    input     l1, // pin: 2
    input     load_n, // pin: 20
    input     lnr, // pin: 21
    input     r_n, // pin: 22
    input     l2, // pin: 3
    input     l3, // pin: 4
    input     l4, // pin: 5
    input     l5, // pin: 6
    input     l6, // pin: 7
    input     sl, // pin: 8
    input     l7  // pin: 9
);

reg [7:0] d;

assign { q7,q6,q5,q4,q3,q2,q1,q0 } = d;

always @(posedge clk, negedge r_n) begin
    if( !r_n )
        d <= 8'd0;
    else begin
        if(!load_n)
            d <= {l7,l6,l5,l4,l3,l2,l1,l0};
        else if( lnr ) // right
            d <= { sr, d[7:1] };
        else
            d <= { d[6:0], sl };
    end
end

endmodule


module M258C( // ref: M258C
    input     a3, // pin: 1
    input     b3, // pin: 10
    input     b2, // pin: 11
    input     b1, // pin: 12
    input     b0, // pin: 13
    input     e_n, // pin: 14
    input     a2, // pin: 2
    input     a1, // pin: 3
    input     a0, // pin: 4
    inout     q0_n, // pin: 5
    input     anb, // pin: 9
    inout     q1_n, // pin: 6
    inout     q2_n, // pin: 7
    inout     q3_n  // pin: 8
);

assign q0 = !e_n ? 1'b0 : ~(anb ? b0 : a0);
assign q1 = !e_n ? 1'b0 : ~(anb ? b1 : a1);
assign q2 = !e_n ? 1'b0 : ~(anb ? b2 : a2);
assign q3 = !e_n ? 1'b0 : ~(anb ? b3 : a3);

endmodule


module M273C( // ref: M273C
    input     r_n, // pin: 1
    input     clk, // pin: 11
    output    q4, // pin: 12
    input     d4, // pin: 13
    input     d5, // pin: 14
    output    q5, // pin: 15
    output    q6, // pin: 16
    input     d6, // pin: 17
    input     d7, // pin: 18
    output    q7, // pin: 19
    output    q0, // pin: 2
    input     d0, // pin: 3
    input     d1, // pin: 4
    output    q1, // pin: 5
    output    q2, // pin: 6
    input     d2, // pin: 7
    input     d3, // pin: 8
    output    q3  // pin: 9
);

reg [7:0] q;

assign {q7,q6,q5,q4,q3,q2,q1,q0} = q;

always @(posedge clk, negedge r_n) begin
    if( !r_n )
        q <= 8'h0;
    else
        q <= {d7,d6,d5,d4,d3,d2,d1,d0};
end

endmodule


module M273CG( // ref: M273CG
    input     g_n, // pin: 1
    input     clk, // pin: 11
    inout     q4, // pin: 12
    input     d4, // pin: 13
    input     d5, // pin: 14
    inout     q5, // pin: 15
    inout     q6, // pin: 16
    input     d6, // pin: 17
    input     d7, // pin: 18
    inout     q7, // pin: 19
    inout     q0, // pin: 2
    input     d0, // pin: 3
    input     d1, // pin: 4
    inout     q1, // pin: 5
    inout     q2, // pin: 6
    input     d2, // pin: 7
    input     d3, // pin: 8
    inout     q3  // pin: 9
);

reg [7:0] q;

assign {q7,q6,q5,q4,q3,q2,q1,q0} = g_n ? q : 8'hzz;

always @(posedge clk) begin
    q <= {d7,d6,d5,d4,d3,d2,d1,d0};
end

endmodule


module M367C( // ref: M367C
    input     oe_n, // pin: 1
    inout     y3, // pin: 15
    inout     y2, // pin: 16
    inout     y1, // pin: 17
    inout     y0, // pin: 18
    input     a0, // pin: 2
    input     a1, // pin: 3
    input     a2, // pin: 4
    input     a3  // pin: 5
);

assign #1 y0 = oe_n ? a0 : 1'bz;
assign #1 y1 = oe_n ? a1 : 1'bz;
assign #1 y2 = oe_n ? a2 : 1'bz;
assign #1 y3 = oe_n ? a3 : 1'bz;

endmodule


module M368C( // ref: M368C
    input     oe_n, // pin: 1
    inout     y3, // pin: 15
    inout     y2, // pin: 16
    inout     y1, // pin: 17
    inout     y0, // pin: 18
    input     a0, // pin: 2
    input     a1, // pin: 3
    input     a2, // pin: 4
    input     a3  // pin: 5
);

assign #1 y0 = oe_n ? ~a0 : 1'bz;
assign #1 y1 = oe_n ? ~a1 : 1'bz;
assign #1 y2 = oe_n ? ~a2 : 1'bz;
assign #1 y3 = oe_n ? ~a3 : 1'bz;

endmodule


module M541C( // ref: M541C
    input     oe_n, // pin: 1
    inout     y7, // pin: 11
    inout     y6, // pin: 12
    inout     y5, // pin: 13
    inout     y4, // pin: 14
    inout     y3, // pin: 15
    inout     y2, // pin: 16
    inout     y1, // pin: 17
    inout     y0, // pin: 18
    input     a0, // pin: 2
    input     a1, // pin: 3
    input     a2, // pin: 4
    input     a3, // pin: 5
    input     a4, // pin: 6
    input     a5, // pin: 7
    input     a6, // pin: 8
    input     a7  // pin: 9
);

assign #1 y0 = oe_n ? a0 : 1'bz;
assign #1 y1 = oe_n ? a1 : 1'bz;
assign #1 y2 = oe_n ? a2 : 1'bz;
assign #1 y3 = oe_n ? a3 : 1'bz;
assign #1 y4 = oe_n ? a4 : 1'bz;
assign #1 y5 = oe_n ? a5 : 1'bz;
assign #1 y6 = oe_n ? a6 : 1'bz;
assign #1 y7 = oe_n ? a7 : 1'bz;

endmodule


module MULT( // ref: MULT
    input     x0, // pin: 1
    input     x9, // pin: 10
    input     x10, // pin: 11
    input     x11, // pin: 12
    input     x12, // pin: 13
    input     x13, // pin: 14
    input     x14, // pin: 15
    input     x15, // pin: 16
    input     y0, // pin: 17
    input     y1, // pin: 18
    input     y2, // pin: 19
    input     x1, // pin: 2
    input     y3, // pin: 20
    input     y4, // pin: 21
    input     y5, // pin: 22
    input     y6, // pin: 23
    input     y7, // pin: 24
    input     y8, // pin: 25
    input     y9, // pin: 26
    input     y10, // pin: 27
    input     y11, // pin: 28
    input     y12, // pin: 29
    input     x2, // pin: 3
    input     y13, // pin: 30
    input     y14, // pin: 31
    input     y15, // pin: 32
    input     tcx, // pin: 33
    input     tcy, // pin: 34
    input     ckx, // pin: 35
    input     cky, // pin: 36
    input     ckm, // pin: 37
    inout     lsb0, // pin: 38
    inout     lsb1, // pin: 39
    input     x3, // pin: 4
    inout     lsb2, // pin: 40
    inout     lsb3, // pin: 41
    inout     lsb4, // pin: 42
    inout     lsb5, // pin: 43
    inout     lsb6, // pin: 44
    inout     lsb7, // pin: 45
    inout     lsb8, // pin: 46
    inout     lsb9, // pin: 47
    inout     lsb10, // pin: 48
    inout     lsb11, // pin: 49
    input     x4, // pin: 5
    inout     lsb12, // pin: 50
    inout     lsb13, // pin: 51
    inout     lsb14, // pin: 52
    inout     lsb15, // pin: 53
    inout     msb0, // pin: 54
    inout     msb1, // pin: 55
    inout     msb2, // pin: 56
    inout     msb3, // pin: 57
    inout     msb4, // pin: 58
    inout     msb5, // pin: 59
    input     x5, // pin: 6
    inout     msb6, // pin: 60
    inout     msb7, // pin: 61
    inout     msb8, // pin: 62
    inout     msb9, // pin: 63
    inout     msb10, // pin: 64
    inout     msb11, // pin: 65
    inout     msb12, // pin: 66
    inout     msb13, // pin: 67
    inout     msb14, // pin: 68
    inout     msb15, // pin: 69
    input     x6, // pin: 7
    input     tsl_n, // pin: 70
    input     tsm_n, // pin: 71
    input     ckl, // pin: 72
    input     x7, // pin: 8
    input     x8  // pin: 9
);

wire [15:0] xin = {x15,x14,x13,x12,
                   x11,x10, x9, x8,
                    x7, x6, x5, x4,
                    x3, x2, x1, x0 };

wire [15:0] yin = {y15,y14,y13,y12,
                   y11,y10, y9, y8,
                    y7, y6, y5, y4,
                    y3, y2, y1, y0 };
reg [31:0] prod;

assign { msb15, msb14, msb13, msb12,
         msb11, msb10,  msb9,  msb8,
          msb7,  msb6,  msb5,  msb4,
          msb3,  msb2,  msb1,  msb0,
          lsb15, lsb14, lsb13, lsb12,
         lsb11, lsb10,  lsb9,  lsb8,
          lsb7,  lsb6,  lsb5,  lsb4,
          lsb3,  lsb2,  lsb1,  lsb0
        } = prod;

// other clock inputs are ignored
always @(posedge ckl)
    prod <= xin*yin;

endmodule


module MUX24H( // ref: MUX24H
    input     a3, // pin: 1
    input     b3, // pin: 10
    input     b2, // pin: 11
    input     b1, // pin: 12
    input     b0, // pin: 13
    input     a2, // pin: 2
    input     a1, // pin: 3
    input     a0, // pin: 4
    output    q0, // pin: 5
    input     anb, // pin: 9
    output    q1, // pin: 6
    output    q2, // pin: 7
    output    q3  // pin: 8
);

assign #1 q0 = anb ? b0 : a0;
assign #1 q1 = anb ? b1 : a1;
assign #1 q2 = anb ? b2 : a2;
assign #1 q3 = anb ? b3 : a3;

endmodule


module MUX25H( // ref: MUX25H
    input     a3, // pin: 1
    output    q4, // pin: 10
    input     anb, // pin: 11
    input     b3, // pin: 12
    input     b4, // pin: 13
    input     b2, // pin: 14
    input     b1, // pin: 15
    input     b0, // pin: 16
    input     a4, // pin: 2
    input     a2, // pin: 3
    input     a1, // pin: 4
    input     a0, // pin: 5
    output    q0, // pin: 6
    output    q1, // pin: 7
    output    q2, // pin: 8
    output    q3  // pin: 9
);

assign #1 q0 = anb ? b0 : a0;
assign #1 q1 = anb ? b1 : a1;
assign #1 q2 = anb ? b2 : a2;
assign #1 q3 = anb ? b3 : a3;
assign #1 q4 = anb ? b4 : a4;

endmodule


module MUX26H( // ref: MUX26H
    input     a0, // pin: 1
    input     b3, // pin: 10
    input     b4, // pin: 11
    input     b5, // pin: 12
    output    q0, // pin: 13
    output    q1, // pin: 14
    output    q2, // pin: 15
    output    q3, // pin: 16
    output    q4, // pin: 17
    output    q5, // pin: 18
    input     anb, // pin: 19
    input     a1, // pin: 2
    input     a2, // pin: 3
    input     a3, // pin: 4
    input     a4, // pin: 5
    input     a5, // pin: 6
    input     b0, // pin: 7
    input     b1, // pin: 8
    input     b2  // pin: 9
);

assign #1 q0 = anb ? b0 : a0;
assign #1 q1 = anb ? b1 : a1;
assign #1 q2 = anb ? b2 : a2;
assign #1 q3 = anb ? b3 : a3;
assign #1 q4 = anb ? b4 : a4;
assign #1 q5 = anb ? b5 : a5;

endmodule


module MUX28H( // ref: MUX28H
    input     a0, // pin: 1
    input     b1, // pin: 10
    input     b2, // pin: 11
    input     b3, // pin: 12
    input     b6, // pin: 13
    input     b4, // pin: 14
    input     b7, // pin: 15
    input     b5, // pin: 16
    output    q0, // pin: 17
    output    q1, // pin: 18
    output    q2, // pin: 19
    input     a1, // pin: 2
    output    q3, // pin: 20
    output    q6, // pin: 21
    output    q4, // pin: 22
    output    q5, // pin: 23
    output    q7, // pin: 24
    input     anb, // pin: 25
    input     a2, // pin: 3
    input     a3, // pin: 4
    input     a4, // pin: 5
    input     a7, // pin: 6
    input     a6, // pin: 7
    input     a5, // pin: 8
    input     b0  // pin: 9
);

assign #1 q0 = anb ? b0 : a0;
assign #1 q1 = anb ? b1 : a1;
assign #1 q2 = anb ? b2 : a2;
assign #1 q3 = anb ? b3 : a3;
assign #1 q4 = anb ? b4 : a4;
assign #1 q5 = anb ? b5 : a5;
assign #1 q6 = anb ? b6 : a6;
assign #1 q7 = anb ? b7 : a7;

endmodule


module MUX2H( // ref: MUX2H
    input     i1,
    input     i2,
    input     i3,
    output    o4
);

assign #1 o4 = i1 ? i2 : i3;

endmodule


module MUX2L( // ref: MUX2L
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4  // pin: 4
);

assign #1 o4 = ~(i1 ? i2 : i3);

endmodule


module MUX4H( // ref: MUX4H
    input     x0, // pin: 1
    input     x1, // pin: 2
    input     x2, // pin: 3
    input     x3, // pin: 4
    input     s0, // pin: 5
    input     s1, // pin: 6
    output    q  // pin: 7
);

wire [1:0] s = {s1,s0};

assign #1 q = s==2'd0 ? x0 : (
              s==2'd1 ? x1 : (
              s==2'd2 ? x2 : x3 ));

endmodule


module MUX8GLH( // ref: MUX8GLH
    input     i3, // pin: 1
    input     s1, // pin: 10
    input     s0, // pin: 11
    input     i7, // pin: 12
    input     i6, // pin: 13
    input     i5, // pin: 14
    input     i4, // pin: 15
    input     i2, // pin: 2
    input     i1, // pin: 3
    input     i0, // pin: 4
    inout     q, // pin: 5
    input     e_n, // pin: 7
    input     s2, // pin: 9
    inout     q_n  // pin: 6
);

wire [2:0] s = {s2,s1,s0};

assign #1 q = !e_n ? 1'bz : (
              s==3'd0 ? i0 : (
              s==3'd1 ? i1 : (
              s==3'd2 ? i2 : (
              s==3'd3 ? i3 : (
              s==3'd4 ? i4 : (
              s==3'd5 ? i5 : (
              s==3'd6 ? i6 : i7 )))))));

assign q_n = !e_n ? 1'bz : ~q;

endmodule


module NAND02( // ref: NAND02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o = ~(i1&i2);

endmodule


module NAND03( // ref: NAND03
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o  // pin: 4
);

assign #1 o = ~(i1&i2&i3);

endmodule


module NAND04( // ref: NAND04
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o  // pin: 5
);

assign #1 o = ~(i1&i2&i3&i4);

endmodule


module NAND05( // ref: NAND05
    input     i0, // pin: 1
    input     i1, // pin: 2
    input     i2, // pin: 3
    input     i3, // pin: 4
    input     i4, // pin: 5
    output    o  // pin: 6
);

assign #1 o = ~(i0&i1&i2&i3&i4);

endmodule


module NAND06( // ref: NAND06
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    output    o9  // pin: 9
);

assign #1 o = ~(i1&i2&i3&i4&i5&i6);

endmodule


module NAND08( // ref: NAND08
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    input     i7, // pin: 7
    input     i8, // pin: 8
    output    o9  // pin: 9
);

assign #1 o = ~(i1&i2&i3&i4&i5&i6&i7&i8);

endmodule


module NAND13( // ref: NAND13
    input     i1, // pin: 1
    input     i10, // pin: 10
    input     i11, // pin: 11
    input     i12, // pin: 12
    input     i13, // pin: 13
    output    o14, // pin: 14
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    input     i7, // pin: 7
    input     i8, // pin: 8
    input     i9  // pin: 9
);

assign #1 o = ~(i1&i2&i3&i4&i5&i6&i7&i8&i9&i10&i11&i12&i13);

endmodule


module NBUF02( // ref: NBUF02
    input     i, // pin: 1
    inout     o  // pin: 2
);

assign #1 o=~i;

endmodule


module NBUF03( // ref: NBUF03
    input     i, // pin: 1
    inout     o  // pin: 2
);

assign #1 o=~i;

endmodule


module NDFCOR( // ref: NDFCOR
    input      clk, // pin: 1
    input      d, // pin: 2
    output reg q, // pin: 3
    output reg q_n, // pin: 4
    input      i5  // pin: 5
);

always @(posedge clk, negedge i5)
    if(!i5) begin
        q<=0;
        q_n<=1;
    end else begin
        q<=d;
        q_n<=~d;
    end

endmodule


module NMOS( // ref: NMOS
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3  // pin: 3
);

// unmodelled

endmodule


module NOR02( // ref: NOR02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o=~(i1|i2);

endmodule


module NOR03( // ref: NOR03
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4  // pin: 4
);

assign #1 o=~(i1|i2|i3);

endmodule


module NOR04( // ref: NOR04
    output    o1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5  // pin: 5
);

assign #1 o=~(i5|i2|i3|i4);

endmodule


module NOR05( // ref: NOR05
    output    o1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6  // pin: 6
);

assign #1 o=~(i5|i2|i3|i4|i6);

endmodule


module OAI21( // ref: OAI21
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o  // pin: 4
);

assign #1 o = ~(i1 & (i2|i3));

endmodule


module OAI211( // ref: OAI211
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o  // pin: 5
);

assign #1 o = ~(i1 & i2 & (i3|i4));

endmodule


module OAI23( // ref: OAI23
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    input     i6, // pin: 6
    output    o7  // pin: 7
);

assign #1 o = ~&{ i1|i2, i3|i4, i5|i6 };

endmodule


module OAI41( // ref: OAI41
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    input     i5, // pin: 5
    output    o  // pin: 6
);

assign #1 o = ~{ i1, |{i2,i3,i4,i5}};

endmodule


module OR02( // ref: OR02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o = i1|i2;

endmodule


module OR03( // ref: OR03
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    output    o4  // pin: 4
);

assign #1 o = i1|i2|i3;

endmodule


module OR04( // ref: OR04
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3, // pin: 3
    input     i4, // pin: 4
    output    o  // pin: 5
);

assign #1 o = i1|i2|i3|i4;

endmodule


module OUTTRI( // ref: OUTTRI
    input     i1, // pin: 1
    input     i2, // pin: 2
    inout     t3  // pin: 3
);

assign t3 = !i2 ? 1'bz : i1;

endmodule


module PIN( // ref: PIN
    input    p // pin: ~
);

// ignored

endmodule


module PMOS( // ref: PMOS
    input     i1, // pin: 1
    input     i2, // pin: 2
    input     i3  // pin: 3
);

// ignored
endmodule


module PRIO( // ref: PRIO
    input     x0_n, // pin: 1
    output    q2_n, // pin: 10
    output    q3_n, // pin: 11
    output    q4_n, // pin: 12
    output    q5_n, // pin: 13
    output    qx_n, // pin: 14
    input     x1_n, // pin: 2
    input     x2_n, // pin: 3
    input     x3_n, // pin: 4
    input     x4_n, // pin: 5
    input     x5_n, // pin: 6
    input     xe_n, // pin: 7
    output    q0_n, // pin: 8
    output    q1_n  // pin: 9
);

// formula?

endmodule


module R42( // ref: R42
    input     le, // pin: 1
    output    q4, // pin: 10
    input     s_n, // pin: 2
    input     d1_n, // pin: 3
    input     d2_n, // pin: 4
    input     d3_n, // pin: 5
    input     d4_n, // pin: 6
    output    q1, // pin: 7
    output    q2, // pin: 8
    output    q3  // pin: 9
);

reg [3:0] q;

assign {q4,q3,q2,q1} = q;

always @(*) begin
    if( !s_n )
        q=4'b1111;
    else if( le )
        q=~{d4_n,d3_n,d2_n,d1_n};
end

endmodule


module R52( // ref: R52
    input     le, // pin: 1
    output    q3, // pin: 10
    output    q4, // pin: 11
    output    q5, // pin: 12
    input     s_n, // pin: 2
    input     d1_n, // pin: 3
    input     d2_n, // pin: 4
    input     d3_n, // pin: 5
    input     d4_n, // pin: 6
    input     d5_n, // pin: 7
    output    q1, // pin: 8
    output    q2  // pin: 9
);

reg [4:0] q;

assign {q5,q4,q3,q2,q1} = q;

always @(*) begin
    if( !s_n )
        q=5'b1_1111;
    else if( le )
        q=~{d5_n,d4_n,d3_n,d2_n,d1_n};
end

endmodule


module RSLT( // ref: RSLT
    input      s, // pin: 1
    input      r, // pin: 2
    output reg q  // pin: 3
);

always @(*) begin
    if( r )
        q = 0;
    else if( s )
        q = 1;
end

endmodule


module TINVBF( // ref: TINVBF
    input     i1, // pin: 1
    input     i2, // pin: 2
    inout     t3  // pin: 3
);

assign #1 t3 = !i1 ? 1'bz : ~i2;

endmodule


module XNOR02( // ref: XNOR02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o = ~(i1^i2);

endmodule


module XOR02( // ref: XOR02
    input     i1, // pin: 1
    input     i2, // pin: 2
    output    o  // pin: 3
);

assign #1 o = i1^i2;

endmodule


module SolderJumper_2_Bridged( // ref: SolderJumper_2_Bridged
    inout a, // pin: 1
    inout b  // pin: 2
);

assign #1 b=a;

endmodule