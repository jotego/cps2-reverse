module counter3(
    input  [8:0] din_n,
    output [8:0] dout,
    input pENABLE,
    input pLI,
    input pRD,
    input pWR,
    input p_OUT0,
    input pnCLK4M,
    output pnOUT_ENABLE,
    input pnRESET,
    output pnZERO
);

wire
     pBUS_OUT0,
     pBUS_OUT1,
     pBUS_OUT2,
     pBUS_OUT3,
     pBUS_OUT4,
     pBUS_OUT5,
     pBUS_OUT6,
     pBUS_OUT7,
     pBUS_OUT8;

wire
     pnBUS0,
     pnBUS1,
     pnBUS2,
     pnBUS3,
     pnBUS4,
     pnBUS5,
     pnBUS6,
     pnBUS7,
     pnBUS8;

assign dout = {
    pBUS_OUT8,
    pBUS_OUT7,
    pBUS_OUT6,
    pBUS_OUT5,
    pBUS_OUT4,
    pBUS_OUT3,
    pBUS_OUT2,
    pBUS_OUT1,
    pBUS_OUT0
};

assign {
    pnBUS8,
    pnBUS7,
    pnBUS6,
    pnBUS5,
    pnBUS4,
    pnBUS3,
    pnBUS2,
    pnBUS1,
    pnBUS0
} = din_n;

`include "counter3.v"

endmodule


