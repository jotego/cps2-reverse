wire Net__UBR456_Pad1_;
wire Net__UBR462_Pad2_;
wire Net__UBR468_Pad2_;
wire Net__UBR474_Pad2_;
wire Net__UBR479_Pad2_;
wire Net__UBU439_Pad3_;
wire Net__UBU439_Pad5_;
wire Net__UBW384_Pad2_;
wire Net__UBW414_Pad2_;
wire Net__UBW414_Pad3_;
wire Net__UBZ337_Pad1_;
wire Net__UBZ338_Pad1_;
wire Net__UBZ338_Pad2_;
wire Net__UBZ338_Pad3_;
wire Net__UBZ341_Pad1_;
wire Net__UBZ341_Pad2_;
wire Net__UBZ341_Pad3_;
wire Net__UBZ344_Pad3_;
wire Net__UBZ346_Pad2_;
wire Net__UBZ346_Pad3_;
wire Net__UBZ346_Pad4_;
wire Net__UBZ355_Pad3_;
wire Net__UBZ357_Pad3_;
wire Net__UBZ360_Pad10_;
wire Net__UBZ360_Pad11_;
wire Net__UBZ360_Pad12_;
wire Net__UBZ360_Pad9_;
wire Net__UBZ377_Pad2_;
wire Net__UBZ383_Pad12_;
wire Net__UBZ383_Pad5_;
wire Net__UBZ383_Pad6_;
wire Net__UBZ383_Pad7_;
wire Net__UBZ383_Pad8_;
wire Net__UBZ383_Pad9_;
wire Net__UBZ398_Pad11_;
wire Net__UBZ398_Pad13_;
wire Net__UBZ398_Pad1_;
wire Net__UBZ398_Pad2_;
wire Net__UBZ398_Pad3_;
wire Net__UBZ398_Pad4_;
wire Net__UBZ398_Pad5_;
wire Net__UBZ398_Pad6_;
wire Net__UBZ398_Pad7_;
wire Net__UBZ398_Pad8_;
wire Net__UCC359_Pad1_;
wire Net__UCC393_Pad4_;


DFGOO UBR456(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( p_OUT0                   ) /* pin 2*/ ,
    .q         ( pBUS_OUT0                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBR462(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR462_Pad2_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT1                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBR468(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT2                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBR474(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR474_Pad2_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT3                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBR479(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBR479_Pad2_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT4                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFFCOO UBU439(
    .clk       ( pnCLK4M                  ) /* pin 1*/ ,
    .d         ( pLI                      ) /* pin 2*/ ,
    .q         ( Net__UBU439_Pad3_        ) /* pin 3*/ ,
    .q_n       ( Net__UBU439_Pad5_        ) /* pin 5*/
);

OR02 UBW366(
    .i1        ( pRD                      ) /* pin 1*/ ,
    .i2        ( pENABLE                  ) /* pin 2*/ ,
    .o         ( pnOUT_ENABLE             ) /* pin 3*/
);

XNOR02 UBW384(
    .i1        ( pnCLK4M                  ) /* pin 1*/ ,
    .i2        ( Net__UBW384_Pad2_        ) /* pin 2*/ ,
    .o         ( p_OUT0                   ) /* pin 3*/
);

XNOR02 UBW414(
    .i1        ( Net__UBR479_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBW414_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBW414_Pad3_        ) /* pin 3*/
);

OR03 UBW419(
    .i1        ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .o4        ( Net__UBW414_Pad2_        ) /* pin 4*/
);

NAND02 UBW440(
    .i1        ( pnOUT_ENABLE             ) /* pin 1*/ ,
    .i2        ( pnCLK4M                  ) /* pin 2*/ ,
    .o         ( Net__UBR456_Pad1_        ) /* pin 3*/
);

INV01 UBZ337(
    .i         ( Net__UBZ337_Pad1_        ) /* pin 1*/ ,
    .o         ( pnZERO                   ) /* pin 2*/
);

XNOR02 UBZ338(
    .i1        ( Net__UBZ338_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ338_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ338_Pad3_        ) /* pin 3*/
);

XNOR02 UBZ341(
    .i1        ( Net__UBZ341_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ341_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ341_Pad3_        ) /* pin 3*/
);

NOR02 UBZ344(
    .i1        ( pENABLE                  ) /* pin 1*/ ,
    .i2        ( pWR                      ) /* pin 2*/ ,
    .o         ( Net__UBZ344_Pad3_        ) /* pin 3*/
);

NOR05 UBZ346(
    .o1        ( Net__UBZ337_Pad1_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ346_Pad3_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ346_Pad4_        ) /* pin 4*/ ,
    .i5        ( Net__UBZ341_Pad1_        ) /* pin 5*/ ,
    .i6        ( Net__UBZ338_Pad1_        ) /* pin 6*/
);

OR04 UBZ349(
    .i1        ( Net__UBZ346_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .i4        ( Net__UBZ346_Pad4_        ) /* pin 4*/ ,
    .o         ( Net__UBZ338_Pad2_        ) /* pin 5*/
);

OR02 UBZ355(
    .i1        ( Net__UBR468_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR462_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ355_Pad3_        ) /* pin 3*/
);

XNOR02 UBZ357(
    .i1        ( Net__UBR474_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ355_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBZ357_Pad3_        ) /* pin 3*/
);

R52 UBZ360(
    .le        ( Net__UBZ344_Pad3_        ) /* pin 1*/ ,
    .s_n       ( pnRESET                  ) /* pin 2*/ ,
    .d1_n      ( pnBUS0                   ) /* pin 3*/ ,
    .d2_n      ( pnBUS1                   ) /* pin 4*/ ,
    .d3_n      ( pnBUS2                   ) /* pin 5*/ ,
    .d4_n      ( pnBUS3                   ) /* pin 6*/ ,
    .d5_n      ( pnBUS4                   ) /* pin 7*/ ,
    .q1        ( Net__UBW384_Pad2_        ) /* pin 8*/ ,
    .q2        ( Net__UBZ360_Pad9_        ) /* pin 9*/ ,
    .q3        ( Net__UBZ360_Pad10_       ) /* pin 10*/ ,
    .q4        ( Net__UBZ360_Pad11_       ) /* pin 11*/ ,
    .q5        ( Net__UBZ360_Pad12_       ) /* pin 12*/
);

INV01 UBZ377(
    .i         ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .o         ( Net__UBZ377_Pad2_        ) /* pin 2*/
);

MUX24H UBZ383(
    .a3        ( Net__UBZ360_Pad12_       ) /* pin 1*/ ,
    .a2        ( Net__UBZ360_Pad11_       ) /* pin 2*/ ,
    .a1        ( Net__UBZ360_Pad10_       ) /* pin 3*/ ,
    .a0        ( Net__UBZ360_Pad9_        ) /* pin 4*/ ,
    .q0        ( Net__UBZ383_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UBZ383_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBZ383_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UBZ383_Pad8_        ) /* pin 8*/ ,
    .anb       ( Net__UBZ383_Pad9_        ) /* pin 9*/ ,
    .b3        ( Net__UBW414_Pad3_        ) /* pin 10*/ ,
    .b2        ( Net__UBZ357_Pad3_        ) /* pin 11*/ ,
    .b1        ( Net__UBZ383_Pad12_       ) /* pin 12*/ ,
    .b0        ( Net__UBZ377_Pad2_        ) /* pin 13*/
);

MUX24H UBZ398(
    .a3        ( Net__UBZ398_Pad1_        ) /* pin 1*/ ,
    .a2        ( Net__UBZ398_Pad2_        ) /* pin 2*/ ,
    .a1        ( Net__UBZ398_Pad3_        ) /* pin 3*/ ,
    .a0        ( Net__UBZ398_Pad4_        ) /* pin 4*/ ,
    .q0        ( Net__UBZ398_Pad5_        ) /* pin 5*/ ,
    .q1        ( Net__UBZ398_Pad6_        ) /* pin 6*/ ,
    .q2        ( Net__UBZ398_Pad7_        ) /* pin 7*/ ,
    .q3        ( Net__UBZ398_Pad8_        ) /* pin 8*/ ,
    .anb       ( Net__UBZ383_Pad9_        ) /* pin 9*/ ,
    .b3        ( Net__UBZ338_Pad3_        ) /* pin 10*/ ,
    .b2        ( Net__UBZ398_Pad11_       ) /* pin 11*/ ,
    .b1        ( Net__UBZ341_Pad3_        ) /* pin 12*/ ,
    .b0        ( Net__UBZ398_Pad13_       ) /* pin 13*/
);

R42 UBZ413(
    .le        ( Net__UBZ344_Pad3_        ) /* pin 1*/ ,
    .s_n       ( pnRESET                  ) /* pin 2*/ ,
    .d1_n      ( pnBUS5                   ) /* pin 3*/ ,
    .d2_n      ( pnBUS6                   ) /* pin 4*/ ,
    .d3_n      ( pnBUS7                   ) /* pin 5*/ ,
    .d4_n      ( pnBUS8                   ) /* pin 6*/ ,
    .q1        ( Net__UBZ398_Pad4_        ) /* pin 7*/ ,
    .q2        ( Net__UBZ398_Pad3_        ) /* pin 8*/ ,
    .q3        ( Net__UBZ398_Pad2_        ) /* pin 9*/ ,
    .q4        ( Net__UBZ398_Pad1_        ) /* pin 10*/
);

NOR02 UBZ428(
    .i1        ( pLI                      ) /* pin 1*/ ,
    .i2        ( Net__UBU439_Pad3_        ) /* pin 2*/ ,
    .o         ( Net__UBZ383_Pad9_        ) /* pin 3*/
);

DFGOO UBZ440(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT5                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBZ446(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ341_Pad1_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT6                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBZ452(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ346_Pad4_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT7                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

DFGOO UBZ458(
    .clk       ( Net__UBR456_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ338_Pad1_        ) /* pin 2*/ ,
    .q         ( pBUS_OUT8                ) /* pin 3*/ ,
    .i5        ( pnOUT_ENABLE             ) /* pin 5*/
);

OR02 UCC354(
    .i1        ( Net__UBZ346_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ341_Pad2_        ) /* pin 3*/
);

DFFCOS UCC359(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBR462_Pad2_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC363(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UBR468_Pad2_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC368(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad7_        ) /* pin 2*/ ,
    .q         ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC372(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ383_Pad8_        ) /* pin 2*/ ,
    .q         ( Net__UBR479_Pad2_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

INV01 UCC383(
    .i         ( pnCLK4M                  ) /* pin 1*/ ,
    .o         ( Net__UCC359_Pad1_        ) /* pin 2*/
);

OR04 UCC385(
    .i1        ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .i3        ( Net__UBR474_Pad2_        ) /* pin 3*/ ,
    .i4        ( Net__UBR479_Pad2_        ) /* pin 4*/ ,
    .o         ( Net__UBZ346_Pad2_        ) /* pin 5*/
);

XNOR02 UCC388(
    .i1        ( Net__UBR462_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBR468_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ383_Pad12_       ) /* pin 3*/
);

OR03 UCC393(
    .i1        ( Net__UBZ346_Pad2_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad3_        ) /* pin 2*/ ,
    .i3        ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .o4        ( Net__UCC393_Pad4_        ) /* pin 4*/
);

XNOR02 UCC396(
    .i1        ( Net__UBZ346_Pad3_        ) /* pin 1*/ ,
    .i2        ( Net__UBZ346_Pad2_        ) /* pin 2*/ ,
    .o         ( Net__UBZ398_Pad13_       ) /* pin 3*/
);

DFFCOS UCC400(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad5_        ) /* pin 2*/ ,
    .q         ( Net__UBZ346_Pad3_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC405(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad6_        ) /* pin 2*/ ,
    .q         ( Net__UBZ341_Pad1_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC409(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad7_        ) /* pin 2*/ ,
    .q         ( Net__UBZ346_Pad4_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

DFFCOS UCC414(
    .clk       ( Net__UCC359_Pad1_        ) /* pin 1*/ ,
    .d         ( Net__UBZ398_Pad8_        ) /* pin 2*/ ,
    .q         ( Net__UBZ338_Pad1_        ) /* pin 3*/ ,
    .i4        ( pnRESET                  ) /* pin 4*/
);

XNOR02 UCC422(
    .i1        ( Net__UBZ346_Pad4_        ) /* pin 1*/ ,
    .i2        ( Net__UCC393_Pad4_        ) /* pin 2*/ ,
    .o         ( Net__UBZ398_Pad11_       ) /* pin 3*/
);

