update=15/04/2020 23:27:33
version=1
last_client=kicad
[general]
version=1
RootSch=
BoardNm=
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[cvpcb]
version=1
NetIExt=net
[eeschema]
version=1
LibDir=
[eeschema/libraries]
[schematic_editor]
version=1
PageLayoutDescrFile=
PlotDirectoryName=/tmp
SubpartIdSeparator=0
SubpartFirstId=65
NetFmtName=
SpiceAjustPassiveValues=0
LabSize=50
ERC_TestSimilarLabels=1
[SchematicFrame]
version=1
[LibeditFrame]
version=1
[sheetnames]
1=00000000-0000-0000-0000-00005e97257e:
2=00000000-0000-0000-0000-000062c44e7c:gfx-addr-bus
3=00000000-0000-0000-0000-000062d0139c:gfx-data-bus
4=00000000-0000-0000-0000-00005d9f70e5:processor
5=00000000-0000-0000-0000-00006466cfce:CN3
6=00000000-0000-0000-0000-0000644bb5c1:CN4
7=00000000-0000-0000-0000-000062e08d9b:qsound-rom
8=00000000-0000-0000-0000-000063f411dc:sound-rom
9=00000000-0000-0000-0000-00005e05893c:gfx-rom
10=00000000-0000-0000-0000-00005dc1e7f0:code-rom-control
11=00000000-0000-0000-0000-00005e35b989:CIF
12=00000000-0000-0000-0000-00005db29ff0:CN7
13=00000000-0000-0000-0000-00006492efbf:CN1
14=00000000-0000-0000-0000-0000635beeb0:rom-bus
15=00000000-0000-0000-0000-00005d957a6e:code-rom
16=00000000-0000-0000-0000-00005e37b0fc:SRAM
17=00000000-0000-0000-0000-0000640add2b:CN2
