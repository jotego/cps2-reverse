EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 17
Title ""
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
$EndDescr
Connection ~ 4250 2000
Connection ~ 3200 3650
Connection ~ 4250 5300
Connection ~ 5000 7450
Entry Wire Line
	2450 2350 2550 2450
Entry Wire Line
	2450 2450 2550 2550
Entry Wire Line
	2450 2550 2550 2650
Entry Wire Line
	2450 2650 2550 2750
Entry Wire Line
	2450 2750 2550 2850
Entry Wire Line
	2450 2850 2550 2950
Entry Wire Line
	2450 2950 2550 3050
Entry Wire Line
	2450 3050 2550 3150
Entry Wire Line
	2450 3150 2550 3250
Entry Wire Line
	2450 3250 2550 3350
Entry Wire Line
	2450 3350 2550 3450
Entry Wire Line
	2450 3450 2550 3550
Entry Wire Line
	2450 3750 2550 3850
Entry Wire Line
	2450 3850 2550 3950
Entry Wire Line
	2450 4150 2550 4050
Entry Wire Line
	2450 4250 2550 4150
Entry Wire Line
	2450 4350 2550 4250
Entry Wire Line
	2450 4450 2550 4350
Entry Wire Line
	2450 4550 2550 4450
Entry Wire Line
	2450 4650 2550 4550
Entry Wire Line
	2450 4750 2550 4650
Entry Wire Line
	2450 4850 2550 4750
Entry Wire Line
	2450 4950 2550 4850
Entry Wire Line
	2450 5050 2550 4950
Entry Wire Line
	2450 5150 2550 5050
Entry Wire Line
	2450 5250 2550 5150
Entry Wire Line
	2450 5350 2550 5250
Entry Wire Line
	2450 5450 2550 5350
Entry Wire Line
	2450 5550 2550 5450
Entry Wire Line
	2450 5650 2550 5550
Entry Wire Line
	4900 800  5000 700 
Entry Wire Line
	4900 900  5000 800 
Entry Wire Line
	4900 1000 5000 900 
Entry Wire Line
	4900 1100 5000 1000
Entry Wire Line
	4900 1200 5000 1100
Entry Wire Line
	4900 1300 5000 1200
Entry Wire Line
	4900 1400 5000 1300
Entry Wire Line
	4900 1500 5000 1400
Entry Wire Line
	4900 1600 5000 1500
Entry Wire Line
	4900 1700 5000 1600
Entry Wire Line
	4900 1800 5000 1700
Entry Wire Line
	4900 1900 5000 1800
Entry Wire Line
	4900 2200 5000 2100
Entry Wire Line
	4900 2300 5000 2200
Entry Wire Line
	4900 2400 5000 2300
Entry Wire Line
	4900 2500 5000 2400
Entry Wire Line
	4900 2600 5000 2500
Entry Wire Line
	4900 2700 5000 2600
Entry Wire Line
	4900 2800 5000 2700
Entry Wire Line
	4900 2900 5000 2800
Entry Wire Line
	4900 3000 5000 2900
Entry Wire Line
	4900 3100 5000 3000
Entry Wire Line
	4900 3200 5000 3100
Entry Wire Line
	4900 3300 5000 3200
Entry Wire Line
	4900 3400 5000 3300
Entry Wire Line
	4900 3500 5000 3400
Entry Wire Line
	4900 3600 5000 3500
Entry Wire Line
	4900 3700 5000 3600
Entry Wire Line
	4900 3800 5000 3700
Entry Wire Line
	4900 3900 5000 3800
Entry Wire Line
	4900 4100 5000 4000
Entry Wire Line
	4900 4200 5000 4100
Entry Wire Line
	4900 4300 5000 4200
Entry Wire Line
	4900 4400 5000 4300
Entry Wire Line
	4900 4500 5000 4400
Entry Wire Line
	4900 4600 5000 4500
Entry Wire Line
	4900 4700 5000 4600
Entry Wire Line
	4900 4800 5000 4700
Entry Wire Line
	4900 4900 5000 4800
Entry Wire Line
	4900 5000 5000 4900
Entry Wire Line
	4900 5100 5000 5000
Entry Wire Line
	4900 5200 5000 5100
Entry Wire Line
	4900 5500 5000 5400
Entry Wire Line
	4900 5600 5000 5500
Entry Wire Line
	4900 5700 5000 5800
Entry Wire Line
	4900 5800 5000 5900
Entry Wire Line
	4900 5900 5000 6000
Entry Wire Line
	4900 6000 5000 6100
Entry Wire Line
	4900 6100 5000 6200
Entry Wire Line
	4900 6200 5000 6300
Entry Wire Line
	4900 6300 5000 6400
Entry Wire Line
	4900 6400 5000 6500
Entry Wire Line
	4900 6500 5000 6600
Entry Wire Line
	4900 6600 5000 6700
Entry Wire Line
	4900 6700 5000 6800
Entry Wire Line
	4900 6800 5000 6900
Entry Wire Line
	4900 6900 5000 7000
Entry Wire Line
	4900 7000 5000 7100
Entry Wire Line
	4900 7100 5000 7200
Entry Wire Line
	4900 7200 5000 7300
Wire Wire Line
	2550 3850 3200 3850
Wire Wire Line
	2550 3950 3200 3950
Wire Wire Line
	3050 3650 3200 3650
Wire Wire Line
	3200 2450 2550 2450
Wire Wire Line
	3200 2550 2550 2550
Wire Wire Line
	3200 2650 2550 2650
Wire Wire Line
	3200 2750 2550 2750
Wire Wire Line
	3200 2850 2550 2850
Wire Wire Line
	3200 2950 2550 2950
Wire Wire Line
	3200 3050 2550 3050
Wire Wire Line
	3200 3150 2550 3150
Wire Wire Line
	3200 3250 2550 3250
Wire Wire Line
	3200 3350 2550 3350
Wire Wire Line
	3200 3450 2550 3450
Wire Wire Line
	3200 3550 2550 3550
Wire Wire Line
	3200 3650 3200 3750
Wire Wire Line
	3200 4050 2550 4050
Wire Wire Line
	3200 4150 2550 4150
Wire Wire Line
	3200 4250 2550 4250
Wire Wire Line
	3200 4350 2550 4350
Wire Wire Line
	3200 4450 2550 4450
Wire Wire Line
	3200 4550 2550 4550
Wire Wire Line
	3200 4650 2550 4650
Wire Wire Line
	3200 4750 2550 4750
Wire Wire Line
	3200 4850 2550 4850
Wire Wire Line
	3200 4950 2550 4950
Wire Wire Line
	3200 5050 2550 5050
Wire Wire Line
	3200 5150 2550 5150
Wire Wire Line
	3200 5250 2550 5250
Wire Wire Line
	3200 5350 2550 5350
Wire Wire Line
	3200 5450 2550 5450
Wire Wire Line
	3200 5550 2550 5550
Wire Wire Line
	4250 2000 4250 2100
Wire Wire Line
	4250 2200 4900 2200
Wire Wire Line
	4250 2300 4900 2300
Wire Wire Line
	4250 2400 4900 2400
Wire Wire Line
	4250 4100 4900 4100
Wire Wire Line
	4250 4200 4900 4200
Wire Wire Line
	4250 4300 4900 4300
Wire Wire Line
	4250 4400 4900 4400
Wire Wire Line
	4250 4500 4900 4500
Wire Wire Line
	4250 4600 4900 4600
Wire Wire Line
	4250 4700 4900 4700
Wire Wire Line
	4250 4800 4900 4800
Wire Wire Line
	4250 4900 4900 4900
Wire Wire Line
	4250 5000 4900 5000
Wire Wire Line
	4250 5100 4900 5100
Wire Wire Line
	4250 5200 4900 5200
Wire Wire Line
	4250 5400 4250 5300
Wire Wire Line
	4250 5500 4900 5500
Wire Wire Line
	4250 5700 4900 5700
Wire Wire Line
	4250 5800 4900 5800
Wire Wire Line
	4250 5900 4900 5900
Wire Wire Line
	4250 6000 4900 6000
Wire Wire Line
	4250 6100 4900 6100
Wire Wire Line
	4250 6200 4900 6200
Wire Wire Line
	4250 6300 4900 6300
Wire Wire Line
	4250 6400 4900 6400
Wire Wire Line
	4250 6500 4900 6500
Wire Wire Line
	4250 6600 4900 6600
Wire Wire Line
	4250 6700 4900 6700
Wire Wire Line
	4250 6800 4900 6800
Wire Wire Line
	4250 6900 4900 6900
Wire Wire Line
	4250 7000 4900 7000
Wire Wire Line
	4250 7100 4900 7100
Wire Wire Line
	4250 7200 4900 7200
Wire Wire Line
	4400 2000 4250 2000
Wire Wire Line
	4400 5300 4250 5300
Wire Wire Line
	4900 800  4250 800 
Wire Wire Line
	4900 900  4250 900 
Wire Wire Line
	4900 1000 4250 1000
Wire Wire Line
	4900 1100 4250 1100
Wire Wire Line
	4900 1200 4250 1200
Wire Wire Line
	4900 1300 4250 1300
Wire Wire Line
	4900 1400 4250 1400
Wire Wire Line
	4900 1500 4250 1500
Wire Wire Line
	4900 1600 4250 1600
Wire Wire Line
	4900 1700 4250 1700
Wire Wire Line
	4900 1800 4250 1800
Wire Wire Line
	4900 1900 4250 1900
Wire Wire Line
	4900 2500 4250 2500
Wire Wire Line
	4900 2600 4250 2600
Wire Wire Line
	4900 2700 4250 2700
Wire Wire Line
	4900 2800 4250 2800
Wire Wire Line
	4900 2900 4250 2900
Wire Wire Line
	4900 3000 4250 3000
Wire Wire Line
	4900 3100 4250 3100
Wire Wire Line
	4900 3200 4250 3200
Wire Wire Line
	4900 3300 4250 3300
Wire Wire Line
	4900 3400 4250 3400
Wire Wire Line
	4900 3500 4250 3500
Wire Wire Line
	4900 3600 4250 3600
Wire Wire Line
	4900 3700 4250 3700
Wire Wire Line
	4900 3800 4250 3800
Wire Wire Line
	4900 3900 4250 3900
Wire Wire Line
	4900 5600 4250 5600
Wire Bus Line
	2050 3650 2450 3650
Wire Bus Line
	2450 2250 2300 2250
Wire Bus Line
	2450 2250 2450 2450
Wire Bus Line
	2450 2450 2450 2650
Wire Bus Line
	2450 2650 2450 2850
Wire Bus Line
	2450 2850 2450 2950
Wire Bus Line
	2450 2950 2450 3150
Wire Bus Line
	2450 3150 2450 3350
Wire Bus Line
	2450 3350 2450 3450
Wire Bus Line
	2450 3650 2450 3850
Wire Bus Line
	2450 4150 2450 4350
Wire Bus Line
	2450 4350 2450 4450
Wire Bus Line
	2450 4450 2450 4650
Wire Bus Line
	2450 4650 2450 4850
Wire Bus Line
	2450 4850 2450 4950
Wire Bus Line
	2450 4950 2450 5150
Wire Bus Line
	2450 5150 2450 5350
Wire Bus Line
	2450 5350 2450 5550
Wire Bus Line
	2450 5550 2450 7450
Wire Bus Line
	2450 7450 5000 7450
Wire Bus Line
	5000 600  5000 800 
Wire Bus Line
	5000 800  5000 900 
Wire Bus Line
	5000 900  5000 1100
Wire Bus Line
	5000 1100 5000 1300
Wire Bus Line
	5000 1300 5000 1400
Wire Bus Line
	5000 1400 5000 1600
Wire Bus Line
	5000 1600 5000 1800
Wire Bus Line
	5000 2000 5000 2200
Wire Bus Line
	5000 2200 5000 2300
Wire Bus Line
	5000 2300 5000 2500
Wire Bus Line
	5000 2500 5000 2700
Wire Bus Line
	5000 2700 5000 2900
Wire Bus Line
	5000 2900 5000 3100
Wire Bus Line
	5000 3100 5000 3300
Wire Bus Line
	5000 3300 5000 3500
Wire Bus Line
	5000 3500 5000 3700
Wire Bus Line
	5000 3700 5000 3800
Wire Bus Line
	5000 3900 5000 4100
Wire Bus Line
	5000 3900 5400 3900
Wire Bus Line
	5000 4100 5000 4300
Wire Bus Line
	5000 4300 5000 4500
Wire Bus Line
	5000 4500 5000 4600
Wire Bus Line
	5000 4600 5000 4800
Wire Bus Line
	5000 4800 5000 4900
Wire Bus Line
	5000 4900 5000 5100
Wire Bus Line
	5000 5300 5000 5500
Wire Bus Line
	5000 5800 5000 5900
Wire Bus Line
	5000 5900 5000 6100
Wire Bus Line
	5000 6100 5000 6300
Wire Bus Line
	5000 6300 5000 6500
Wire Bus Line
	5000 6500 5000 6700
Wire Bus Line
	5000 6700 5000 6900
Wire Bus Line
	5000 6900 5000 7100
Wire Bus Line
	5000 7100 5000 7300
Wire Bus Line
	5000 7300 5000 7450
Wire Bus Line
	5000 7450 5300 7450
Wire Bus Line
	5400 600  5000 600 
Wire Bus Line
	5400 2000 5000 2000
Wire Bus Line
	5400 5300 5000 5300
Text Label 2600 3850 0    50   ~ 0
GFXB-ADDR21
Text Label 2600 3950 0    50   ~ 0
GFXB-ADDR18
Text Label 3150 2450 2    50   ~ 0
GFXA-DATA11
Text Label 3150 2550 2    50   ~ 0
GFXA-DATA10
Text Label 3150 2650 2    50   ~ 0
GFXA-DATA9
Text Label 3150 2750 2    50   ~ 0
GFXA-DATA8
Text Label 3150 2850 2    50   ~ 0
GFXA-DATA7
Text Label 3150 2950 2    50   ~ 0
GFXA-DATA6
Text Label 3150 3050 2    50   ~ 0
GFXA-DATA5
Text Label 3150 3150 2    50   ~ 0
GFXA-DATA4
Text Label 3150 3250 2    50   ~ 0
GFXA-DATA3
Text Label 3150 3350 2    50   ~ 0
GFXA-DATA2
Text Label 3150 3450 2    50   ~ 0
GFXA-DATA1
Text Label 3150 3550 2    50   ~ 0
GFXA-DATA0
Text Label 3150 4050 2    50   ~ 0
GFXB-DATA15
Text Label 3150 4150 2    50   ~ 0
GFXB-DATA14
Text Label 3150 4250 2    50   ~ 0
GFXB-DATA13
Text Label 3150 4350 2    50   ~ 0
GFXB-DATA12
Text Label 3150 4450 2    50   ~ 0
GFXB-DATA11
Text Label 3150 4550 2    50   ~ 0
GFXB-DATA10
Text Label 3150 4650 2    50   ~ 0
GFXB-DATA9
Text Label 3150 4750 2    50   ~ 0
GFXB-DATA8
Text Label 3150 4850 2    50   ~ 0
GFXB-DATA7
Text Label 3150 4950 2    50   ~ 0
GFXB-DATA6
Text Label 3150 5050 2    50   ~ 0
GFXB-DATA5
Text Label 3150 5150 2    50   ~ 0
GFXB-DATA4
Text Label 3150 5250 2    50   ~ 0
GFXB-DATA3
Text Label 3150 5350 2    50   ~ 0
GFXB-DATA2
Text Label 3150 5450 2    50   ~ 0
GFXB-DATA1
Text Label 3150 5550 2    50   ~ 0
GFXB-DATA0
Text Label 4300 800  0    50   ~ 0
GFXA-ADDR13
Text Label 4300 900  0    50   ~ 0
GFXA-ADDR12
Text Label 4300 1000 0    50   ~ 0
GFXA-ADDR11
Text Label 4300 1100 0    50   ~ 0
GFXA-ADDR10
Text Label 4300 1200 0    50   ~ 0
GFXA-ADDR9
Text Label 4300 1300 0    50   ~ 0
GFXA-ADDR8
Text Label 4300 1400 0    50   ~ 0
GFXA-ADDR7
Text Label 4300 1500 0    50   ~ 0
GFXA-ADDR6
Text Label 4300 1600 0    50   ~ 0
GFXA-ADDR5
Text Label 4300 1700 0    50   ~ 0
GFXA-ADDR4
Text Label 4300 1800 0    50   ~ 0
GFXA-ADDR3
Text Label 4300 1900 0    50   ~ 0
GFXA-ADDR2
Text Label 4300 2200 0    50   ~ 0
GFXB-ADDR23
Text Label 4300 2300 0    50   ~ 0
GFXB-ADDR20
Text Label 4300 2400 0    50   ~ 0
GFXB-ADDR17
Text Label 4300 2600 0    50   ~ 0
GFXB-ADDR15
Text Label 4300 2700 0    50   ~ 0
GFXB-ADDR14
Text Label 4300 2800 0    50   ~ 0
GFXB-ADDR13
Text Label 4300 2900 0    50   ~ 0
GFXB-ADDR12
Text Label 4300 3000 0    50   ~ 0
GFXB-ADDR11
Text Label 4300 3100 0    50   ~ 0
GFXB-ADDR10
Text Label 4300 3200 0    50   ~ 0
GFXB-ADDR9
Text Label 4300 3300 0    50   ~ 0
GFXB-ADDR8
Text Label 4300 3400 0    50   ~ 0
GFXB-ADDR7
Text Label 4300 3500 0    50   ~ 0
GFXB-ADDR6
Text Label 4300 3600 0    50   ~ 0
GFXB-ADDR5
Text Label 4300 3700 0    50   ~ 0
GFXB-ADDR4
Text Label 4300 3800 0    50   ~ 0
GFXB-ADDR3
Text Label 4300 3900 0    50   ~ 0
GFXB-ADDR2
Text Label 4300 4100 0    50   ~ 0
GFXA-DATA27
Text Label 4300 4200 0    50   ~ 0
GFXA-DATA26
Text Label 4300 4300 0    50   ~ 0
GFXA-DATA25
Text Label 4300 4400 0    50   ~ 0
GFXA-DATA24
Text Label 4300 4500 0    50   ~ 0
GFXA-DATA23
Text Label 4300 4600 0    50   ~ 0
GFXA-DATA22
Text Label 4300 4700 0    50   ~ 0
GFXA-DATA21
Text Label 4300 4800 0    50   ~ 0
GFXA-DATA20
Text Label 4300 4900 0    50   ~ 0
GFXA-DATA19
Text Label 4300 5000 0    50   ~ 0
GFXA-DATA18
Text Label 4300 5100 0    50   ~ 0
GFXA-DATA17
Text Label 4300 5200 0    50   ~ 0
GFXA-DATA16
Text Label 4300 5500 0    50   ~ 0
GFXB-ADDR22
Text Label 4300 5700 0    50   ~ 0
GFXB-DATA31
Text Label 4300 5800 0    50   ~ 0
GFXB-DATA30
Text Label 4300 5900 0    50   ~ 0
GFXB-DATA29
Text Label 4300 6000 0    50   ~ 0
GFXB-DATA28
Text Label 4300 6100 0    50   ~ 0
GFXB-DATA27
Text Label 4300 6200 0    50   ~ 0
GFXB-DATA26
Text Label 4300 6300 0    50   ~ 0
GFXB-DATA25
Text Label 4300 6400 0    50   ~ 0
GFXB-DATA24
Text Label 4300 6500 0    50   ~ 0
GFXB-DATA23
Text Label 4300 6600 0    50   ~ 0
GFXB-DATA22
Text Label 4300 6700 0    50   ~ 0
GFXB-DATA21
Text Label 4300 6800 0    50   ~ 0
GFXB-DATA20
Text Label 4300 6900 0    50   ~ 0
GFXB-DATA19
Text Label 4300 7000 0    50   ~ 0
GFXB-DATA18
Text Label 4300 7100 0    50   ~ 0
GFXB-DATA17
Text Label 4300 7200 0    50   ~ 0
GFXB-DATA16
Text Label 4850 2500 2    50   ~ 0
GFXB-ADDR16
Text Label 4850 5600 2    50   ~ 0
GFXB-ADDR19
Text HLabel 2050 3650 0    50   BiDi ~ 0
GFXB-ADDR[2..23]
Text HLabel 2300 2250 0    50   Input ~ 0
GFXA-DATA[0..31]
Text HLabel 5300 7450 2    50   Input ~ 0
GFXB-DATA[0..31]
Text HLabel 5400 600  2    50   BiDi ~ 0
GFXA-ADDR[2..23]
Text HLabel 5400 2000 2    50   BiDi ~ 0
GFXB-ADDR[2..23]
Text HLabel 5400 3900 2    50   Input ~ 0
GFXA-DATA[0..31]
Text HLabel 5400 5300 2    50   BiDi ~ 0
GFXB-ADDR[2..23]
$Comp
L power:GND #PWR?
U 1 1 644BBCFE
P 3050 3650
AR Path="/6492EFBF/644BBCFE" Ref="#PWR?"  Part="1" 
AR Path="/644BBCFE" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/644BBCFE" Ref="#PWR?"  Part="1" 
AR Path="/644BB5C1/644BBCFE" Ref="#PWR0262"  Part="1" 
F 0 "#PWR0262" H 3050 3400 50  0001 C CNN
F 1 "GND" H 3055 3477 50  0001 C CNN
F 2 "" H 3050 3650 50  0001 C CNN
F 3 "" H 3050 3650 50  0001 C CNN
	1    3050 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 644BBCCD
P 4400 2000
AR Path="/6492EFBF/644BBCCD" Ref="#PWR?"  Part="1" 
AR Path="/644BBCCD" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/644BBCCD" Ref="#PWR?"  Part="1" 
AR Path="/644BB5C1/644BBCCD" Ref="#PWR0261"  Part="1" 
F 0 "#PWR0261" H 4400 1750 50  0001 C CNN
F 1 "GND" H 4405 1827 50  0001 C CNN
F 2 "" H 4400 2000 50  0001 C CNN
F 3 "" H 4400 2000 50  0001 C CNN
	1    4400 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 644BBD53
P 4400 5300
AR Path="/6492EFBF/644BBD53" Ref="#PWR?"  Part="1" 
AR Path="/644BBD53" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/644BBD53" Ref="#PWR?"  Part="1" 
AR Path="/644BB5C1/644BBD53" Ref="#PWR0263"  Part="1" 
F 0 "#PWR0263" H 4400 5050 50  0001 C CNN
F 1 "GND" H 4405 5127 50  0001 C CNN
F 2 "" H 4400 5300 50  0001 C CNN
F 3 "" H 4400 5300 50  0001 C CNN
	1    4400 5300
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:96PIN_Sep CN4
U 1 1 644BB5E3
P 3750 4000
F 0 "CN4" V 377 3975 50  0000 C CNN
F 1 "96PIN_Sep" H 423 3930 50  0001 R CNN
F 2 "" H 2550 4300 50  0001 C CNN
F 3 "" H 2550 4300 50  0001 C CNN
	1    3750 4000
	0    1    1    0   
$EndComp
$EndSCHEMATC
