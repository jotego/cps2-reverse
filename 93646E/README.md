#  93646E Reverse Engineering

![CPS2 E Board](pcb-images/93646E.jpg)

## PDF Render

[Click here to explore the schematics online](https://petitl.fr/cps2/93646E/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/93646E.pdf).

[![93646E](https://petitl.fr/cps2/93646E/93646E.svg)](https://petitl.fr/cps2/93646E/)

## Notes
* We can see that contrary to the B-board, the ENABLE is not inverted, therefore if this board is plugged in, the ROMs from the B-board will never be active in the QSound bus.
* The EEPROM uses the same bus as the optional EEPROM in the B-Board but another pin for chip select. I assume it contains an identification or some sort but I didnt check. That's why it is called `IDENT`.

## Authors & Licence
See main README.
