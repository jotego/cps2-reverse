# DL-1927 (CGA) Reverse Engineering

![DL-1927](./DL-1927.png)

## PDF Render

[Click here to explore the schematics online](https://petitl.fr/cps2/DL-1927/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-1927.pdf).

[![DL-1927](https://petitl.fr/cps2/DL-1927/DL-1927.svg)](https://petitl.fr/cps2/DL-1927/)

## Function

This chip is a large 21bits data selector that provides the address buses for the two graphics rom banks.

The buses are provided by A-01 (A-board) or the SPA (B-board) using the shared graphics bus. For the SPA shared bus, current address is stored in latches to be able to exploit the bus for data extraction.

Consult the schematics notes to have more details.

## Schematics notes

* The references of all the gates are all of the following format `UxRy`. `x` and `y` point to the coordinates in the gate array.
* The name of the gate is the name of the equivalent Fujitsu CG10 cell. One exception is the `B12` which looks and behaves like a 1-bit bus driver (`B11`) without the input transistors (maybe it's a CG24 only cell?).

## Pinout

| Pin | Type | Name |
| ----- | ----- | ----- |
| 1 | GND |  |
| 2 | 3 State | GFX-B23 |
| 3 | 3 State | GFX-B22 |
| 4 | Input | BUS23 |
| 5 | Input | BUS22 |
| 6 | Input | BUS21 |
| 7 | Input | BUS20 |
| 8 | Input | BUS19 |
| 9 | 3 State | GFX-B21 |
| 10 | 3 State | GFX-B20 |
| 11 | GND |  |
| 12 | 3 State | GFX-B19 |
| 13 | 3 State | GFX-B18 |
| 14 | 3 State | GFX-B17 |
| 15 | NC |  |
| 16 | VCC |  |
| 17 | Input | BUS18 |
| 18 | Input | BUS17 |
| 19 | 3 State | GFX-B16 |
| 20 | 3 State | GFX-B15 |
| 21 | GND |  |
| 22 | 3 State | GFX-B14 |
| 23 | 3 State | GFX-B13 |
| 24 | Input | BUS16 |
| 25 | Input | BUS15 |
| 26 | Input | BUS14 |
| 27 | Input | BUS13 |
| 28 | Input | BUS12 |
| 29 | 3 State | GFX-B12 |
| 30 | 3 State | GFX-B11 |
| 31 | GND |  |
| 32 | 3 State | GFX-B10 |
| 33 | 3 State | GFX-B9 |
| 34 | 3 State | GFX-B8 |
| 35 | NC |  |
| 36 | Input | BUS11 |
| 37 | Input | BUS10 |
| 38 | Input | BUS9 |
| 39 | 3 State | GFX-B7 |
| 40 | 3 State | GFX-B6 |
| 41 | GND |  |
| 42 | 3 State | GFX-B5 |
| 43 | 3 State | GFX-B4 |
| 44 | Input | BUS8 |
| 45 | Input | BUS7 |
| 46 | VCC |  |
| 47 | Input | BUS6 |
| 48 | Input | BUS5 |
| 49 | 3 State | GFX-B3 |
| 50 | 3 State | GFX-B2 |
| 51 | GND |  |
| 52 | 3 State | GFX-A23 |
| 53 | 3 State | GFX-A22 |
| 54 | 3 State | GFX-A21 |
| 55 | NC |  |
| 56 | Input | BUS4 |
| 57 | Input | BUS3 |
| 58 | 3 State | GFX-A20 |
| 59 | 3 State | GFX-A19 |
| 60 | VCC |  |
| 61 | GND |  |
| 62 | 3 State | GFX-A18 |
| 63 | Input | BUS2 |
| 64 | 3 State | GFX-A17 |
| 65 | Input | A01-A23 |
| 66 | Input | A01-A22 |
| 67 | Input | A01-A21 |
| 68 | Input | A01-A20 |
| 69 | 3 State | GFX-A16 |
| 70 | 3 State | GFX-A15 |
| 71 | GND |  |
| 72 | 3 State | GFX-A14 |
| 73 | 3 State | GFX-A13 |
| 74 | 3 State | GFX-A12 |
| 75 | NC |  |
| 76 | VCC |  |
| 77 | Input | A01-A19 |
| 78 | Input | A01-A18 |
| 79 | 3 State | GFX-A11 |
| 80 | 3 State | GFX-A10 |
| 81 | GND |  |
| 82 | 3 State | GFX-A9 |
| 83 | 3 State | GFX-A8 |
| 84 | Input | A01-A17 |
| 85 | Input | A01-A16 |
| 86 | Input | A01-A15 |
| 87 | Input | A01-A14 |
| 88 | Input | A01-A13 |
| 89 | 3 State | GFX-A7 |
| 90 | 3 State | GFX-A6 |
| 91 | GND |  |
| 92 | 3 State | GFX-A5 |
| 93 | 3 State | GFX-A4 |
| 94 | Input | A01-A12 |
| 95 | Input | A01-A11 |
| 96 | Input | A01-A10 |
| 97 | Input | A01-A9 |
| 98 | Input | A01-A8 |
| 99 | 3 State | GFX-A3 |
| 100 | 3 State | GFX-A2 |
| 101 | GND |  |
| 102 | 3 State | EXT-A01 |
| 103 | 3 State | EXT-BUS |
| 104 | Input | A01-A7 |
| 105 | Input | A01-A6 |
| 106 | VCC |  |
| 107 | Input | A01-A5 |
| 108 | Input | A01-A4 |
| 109 | Input | /ENABLE-OUT |
| 110 | Input | SELECT |
| 111 | GND |  |
| 112 | Input | BUS-W |
| 113 | NC |  |
| 114 | Input | ENABLE-SELECT |
| 115 | Input | /ENABLE-EXT |
| 116 | Input | A01-A3 |
| 117 | Input | A01-A2 |
| 118 | NC |  |
| 119 | Input | ENABLE |
| 120 | VCC |  |

## Authors & Licence & Thanks
See main [README](../../README.md) and the [DL README](../README.md).
