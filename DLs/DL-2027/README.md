# DL-2027 (CGD) Reverse Engineering

![DL-2027](./DL-2027.jpg)

## PDF Render

[Click here to explore the schematics online](https://petitl.fr/cps2/DL-2027/).

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-2027.pdf).

[![DL-2027](https://petitl.fr/cps2/DL-2027/DL-2027.svg)](https://petitl.fr/cps2/DL-2027/)

## Function

This chip is a large 32bits data selector that selects between the two main graphics rom channels. Its behaviour is pretty straightforeward.

## Schematics notes

* The references of all the gates are all of the following format `UxRy`. `x` and `y` point to the coordinates in the gate array.
* The name of the gate is the name of the equivalent Fujitsu CG10 cell. One exception is the `B12` which looks and behaves like a 1-bit bus driver (`B11`) without the input transistors (maybe it's a CG24 only cell?).

## Pinout

| Pin | Type | Name |
| ----- | ----- | ----- |
| 1 | GND | GND |
| 2 | Input | IN-B31 |
| 3 | Input | IN-B30 |
| 4 | Input | IN-B29 |
| 5 | Input | IN-B28 |
| 6 | Input | IN-B27 |
| 7 | Input | IN-B26 |
| 8 | Input | IN-B25 |
| 9 | Input | IN-B24 |
| 10 | Input | IN-B23 |
| 11 | GND |  |
| 12 | Input | IN-B22 |
| 13 | Input | IN-B21 |
| 14 | Input | IN-B20 |
| 15 | Input | IN-B19 |
| 16 | VCC |  |
| 17 | Input | IN-B18 |
| 18 | Input | IN-B17 |
| 19 | Input | IN-B16 |
| 20 | Input | IN-B15 |
| 21 | GND |  |
| 22 | Input | IN-B14 |
| 23 | Input | IN-B13 |
| 24 | Input | IN-B12 |
| 25 | Input | IN-B11 |
| 26 | Input | IN-B10 |
| 27 | Input | IN-B9 |
| 28 | Input | IN-B8 |
| 29 | Input | IN-B7 |
| 30 | Input | IN-B6 |
| 31 | GND |  |
| 32 | Input | IN-B5 |
| 33 | Input | IN-B4 |
| 34 | Input | IN-B3 |
| 35 | Input | IN-B2 |
| 36 | Input | IN-B1 |
| 37 | Input | IN-B0 |
| 38 | 3 State | OUT31 |
| 39 | 3 State | OUT30 |
| 40 | 3 State | OUT29 |
| 41 | GND |  |
| 42 | 3 State | OUT28 |
| 43 | 3 State | OUT27 |
| 44 | 3 State | OUT26 |
| 45 | 3 State | OUT25 |
| 46 | VCC |  |
| 47 | 3 State | OUT24 |
| 48 | 3 State | OUT23 |
| 49 | 3 State | OUT22 |
| 50 | 3 State | OUT21 |
| 51 | GND |  |
| 52 | 3 State | OUT20 |
| 53 | 3 State | OUT19 |
| 54 | 3 State | OUT18 |
| 55 | 3 State | OUT17 |
| 56 | 3 State | OUT16 |
| 57 | 3 State | OUT15 |
| 58 | 3 State | OUT14 |
| 59 | 3 State | OUT13 |
| 60 | VCC |  |
| 61 | GND |  |
| 62 | 3 State | OUT12 |
| 63 | 3 State | OUT11 |
| 64 | 3 State | OUT10 |
| 65 | 3 State | OUT9 |
| 66 | 3 State | OUT8 |
| 67 | 3 State | OUT7 |
| 68 | 3 State | OUT6 |
| 69 | 3 State | OUT5 |
| 70 | 3 State | OUT4 |
| 71 | GND |  |
| 72 | 3 State | OUT3 |
| 73 | 3 State | OUT2 |
| 74 | 3 State | OUT1 |
| 75 | 3 State | OUT0 |
| 76 | VCC |  |
| 77 | Input | IN-A31 |
| 78 | Input | IN-A30 |
| 79 | Input | IN-A29 |
| 80 | Input | IN-A28 |
| 81 | GND |  |
| 82 | Input | IN-A27 |
| 83 | Input | IN-A26 |
| 84 | Input | IN-A25 |
| 85 | Input | IN-A24 |
| 86 | Input | IN-A23 |
| 87 | Input | IN-A22 |
| 88 | Input | IN-A21 |
| 89 | Input | IN-A20 |
| 90 | Input | IN-A19 |
| 91 | GND |  |
| 92 | Input | IN-A18 |
| 93 | Input | IN-A17 |
| 94 | Input | IN-A16 |
| 95 | Input | IN-A15 |
| 96 | Input | IN-A14 |
| 97 | Input | IN-A13 |
| 98 | Input | IN-A12 |
| 99 | Input | IN-A11 |
| 100 | Input | IN-A10 |
| 101 | GND |  |
| 102 | Input | IN-A9 |
| 103 | Input | IN-A8 |
| 104 | Input | IN-A7 |
| 105 | Input | IN-A6 |
| 106 | VCC |  |
| 107 | Input | IN-A5 |
| 108 | Input | IN-A4 |
| 109 | Input | IN-A3 |
| 110 | Input | IN-A2 |
| 111 | GND |  |
| 112 | Input | IN-A1 |
| 113 | Input | IN-A0 |
| 114 | Input | /G3 |
| 115 | Input | ENABLE |
| 116 | Input | /G2 |
| 117 | Input | G1 |
| 118 | NC |  |
| 119 | Input | A/B |
| 120 | VCC |  |

## Authors & Licence & Thanks
See main [README](../../README.md) and the [DL README](../README.md).
