# DL-0921 (CPS-B-21) Video Signals Generation

## Base counters
The generation of the signal is based on three signals coming from the DL-0311 (CPS-A-01):
* LI: Line increment (impulse)
* FI: Frame increment (impulse)
* FLIP: True if the screen is rotated

The main clock is derived from the common 16MHz base clock into a 4MHz internal signal /CLK4M. Using this clock, the 
DL-0921 generates counters of lines and columns.

In the rest of the document, we assume that:
* LI is triggered after 512 main clock impulse
* FI is triggered after 262 LI impulses.

This needs to be confirmed with the DL-0311.

### Reset value
The default reset value for those counters are 0 for the column counter and 509 (-3) for the line counter. 

_Note_: it is possible to control the reset value of these counters using the offset `0x3e`. However, it requires to be
in debug mode using TI = 12. The line counter always starts with the MSB set (e.g. in negative values).

## Sync signals
* `/HBLANK`: 0 if column counter is `0..64` or `449..511` (total active columns: 384)
* `HSYNC`: 1 if column counter is `0..3` or `480..511` (front porch: 31, sync: 36, back porch: 61)
* `/VBLANK`: 0 if line counter is `0..16` or `241..261` (total active lines: 224)
* `VSYNC`: 1 if line counter is `252..254` (front porch: 11, sync: 3, back porch: 24)

## Graphics Timings
There are a lot of little signals that are derived from the counters that are used to stream the different layers
for tilemap, stars and sprites.

Example:
* GFX-CHNL selects the channel of graphics data
* XPD-LOAD writes the graphics rom data into a latch (one per channel)
* SERIAL-LOAD writes the loaded data in a shift register
* A 8Mhz pumps data out of the shift register bit by bit

Those signals are synced to always stream data without interruption.

| Channel | Latch A Load | Latch B Load | Latch A Out | Latch B Out | Serial Load | Serial Out |
| --- | --- | --- | --- | --- | --- | --- |
| 0 |  |  |  | 1 |  | A5 |
| 0 |  |  |  | 1 |  | A6 |
| 1 |  |  |  | 1 |  | A7 |
| 1 |  |  |  | 1 | CLK8M v | B0 |
| 0 |  |  |  | 1 |  | B1 |
| 0 |  |  |  | 1 |  | B2 |
| 1 |  |  |  | 1 |  | B3 |
| 1 |  |  | 1 |  |  | B4 |
| 0 |  |  | 1 |  |  | B5 |
| 0 | CLK8M ^ |  | 1 |  |  | B6 |
| 1 |  |  | 1 |  |  | B7 |
| 1 |  | CLK8M ^ | 1 |  | CLK8M v | A0 |
| 0 |  |  | 1 |  |  | A1 |
| 0 |  |  | 1 |  |  | A2 |
| 1 |  |  | 1 |  |  | A3 |
| 1 |  |  | 1 |  |  | A3 |