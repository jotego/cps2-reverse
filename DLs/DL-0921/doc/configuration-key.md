# DL-0921 (CPS-B-21) Configuration Key Layout

## Notes

* Bits are ordered in the order that is necessary to input in the security injection phase (bit 0 = first bit written)
* All addresses are aligned on 16 bits, so each address is coded on 5 bits and the first bit is ignored (e.g. if MAME
gives address `0x26 [0b00100110]` the value written in the key is `1 - 0 - 0 - 1 - 1`)
* All values are written from MSB to LSB (e.g. bit 0 = CPS-ID5, bit 5 = CPS-ID4)

## Key Data

| Bits | Key Data | Default bits |  |
| --- | --- | --- | --- |
| 0..5 | `CPS-ID-D[5..0]` | 0... | 0 |
| 6..10 | `MULT-X` | 00000 | 0 |
| 11..13 | `LAYER-D2` | 010 | 2 |
| 14..18 | `CPS-ID` | 11001 | 0x32 |
| 19..23 | `MULT-Y` | 00001 | 0x02 |
| 24..28 | `PAL-CTRL` | 11000 | 0x30 |
| 29..33 | `MULT-LSB` | 00010 | 0x04 |
| 34..38 | `PRIORITY3` | 10111 | 0x2e |
| 39..41 | `LAYER-D3` | 011 | 3 | 
| 42..46 | `MULT-MSB` | 00011 | 0x06 |
| 47..51 | `PRIORITY2` | 10110 | 0x2c |
| 52..56 | `MULT-TC` | 00100 | 0x8 |
| 57..61 | `PRIORITY1` | 10101 | 0x2a |
| 62..77 | `CHECK2-D[15..0]` | 0... | 0 |
| 78..80 | `LAYER-D5` | 101 | 5 |
| 81..85 | `CHECK1` | 00101 | 0x0a |
| 86..90 | `PRIORITY0` | 10100 | 0x28 |
| 91..106 | `CHECK1-D[15..0]` | 0... | 0 |
| 107..111 | `CHECK2` | 00110 | 0x0c |
| 112..114 | `LAYER-D4` | 100 | 4 | 
| 115..119 | `RASTER1` | 00111 | 0x0e |
| 120..124 | `LAYER-CTRL` | 10011 | 0x26 |
| 125..129 | `RASTER2` | 01000 | 0x10 |
| 130..134 | `RASTER3` | 01001 | 0x12 | 
| 135..137 | `LAYER-D1` | 001 | 1 |
| 138..143 | `CPS-ID[15..10]` | 0... | 0 |
