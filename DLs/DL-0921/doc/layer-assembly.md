# DL-0921 (CPS-B-21) Layer assembly

The layer assembly is one of the most important function of this chip. The role of this chip is to create the video
output by using the graphics rom data and convert it into a stream of palette addresses that is directly connected to
the video output.

The B-21 assumes that the graphics ROM is ready to provide data at a steady pace. As a general principle, you can assume
that the A-01 handles addresses and the B-21 handles the data.

## Graphics ROM
The graphics ROM contain exclusively all the raw colour data for all the graphical elements. Its data is transmitted via
four 8 bit buses (APD, BPD, CPD and DPD). Each value corresponds to a line of 8 pixels with 4 bits per pixel.

Exploring the graphics ROM data takes a bit of transformation for it to be usable. A quick and dirty implementation
is [available here](https://gitlab.com/loic.petit/cps2-reverse/-/snippets/2040959) to better grasp how the data is
streamed and assembled.

## The layers
There are 6 supported layers in the CPS:
* Sprites (16x16)
* Scroll1 (8x8)
* Scroll2 (16x16)
* Scroll3 (32x32)
* Stars1
* Stars2

Each layer is able to provide a stream of palette addresses in 9 bits. The 4 least significant bits are always the
colour. To see how the data is able to be ready on time, check [this section](video-signals.md) which describe how
the video signals are synchronized to perform this stream.

### Sprites
The sprites are only partly handled in the B-21 chip. Because their management is tightly linked to the object ram,
the B-21 decodes the rom data and transmit it to the A-01 using the FAD-FBD bus. Then, once the data has been
processed, it comes back using the ASD-BSD bus which will be multiplexed for assembly.

On the CPS2, the sprites are NOT handled by the B-21 __at all__. The object ram is handled by the custom 68k on the 
B-board and it is in charge of managing everything. It is still unclear how it injects inside the assembly on this
system as the 68k only has the capability of hijacking the graphics ram.

## Layer assembly
In order to assemble all layers, the B-21 prepares six sets of layers data, all those sets are able to send their
content to the LUT bus.  

In order to manage this process, there is a register in charge of configuring how the assembly of the different layer 
is performed: `LAYER-CTRL`. Its bits are the following:

| Bit | Function |
| --- | --- |
| 0 | N/A |  
| 1 | Enable scroll1 |
| 2 | Enable scroll2 |
| 3 | Enable scroll3 |
| 4 | Enable stars1 |
| 5 | Enable stars2 |
| 6-7 | Selects Layer 4 |
| 8-9 | Selects Layer 3 |
| 10-11 | Selects Layer 2 |
| 12-13 | Selects Layer 1 |
| 13-15 | N/A |

The order of the layers are: Layer 1 is in front of Layer 2 and so on. Each layer selection is encoded in two bits:

| Select | Layer |
| --- | --- |
| 0 | Sprites |
| 1 | Scroll1 |
| 2 | Scroll2 |
| 3 | Scroll3 |

By definition, stars layers are always assigned to Layer 5 and Layer 6.

Then, it looks at all its layer and selects the colour of the first layer that has not a transparent colour. The
transparent colour is always 0xF.

## Tile Priority over Sprites
There is one exception to the transparency rule that was stated previously. It is possible to configure four priority
banks. These priority data will state if the colour of the tilemap right below a sprite should actually be on top
of the sprite or not.

There are 4 levels of priority. The CDC bus coming from the A-01 provides the current priority of the tile. The rule for
selecting the current layer is actually:
* The current colour is not 0xF 
* And either
    * The current layer is NOT a sprite layer
    * Or the current colour and the current level of the layer below have NOT priority 

The priorities can be set using the `PRIORITY[0..3]` bus addresses as 16 bits. Example: if bit 12 of `PRIORITY1` is set,
it means that the colour 0xC has priority at level 1.