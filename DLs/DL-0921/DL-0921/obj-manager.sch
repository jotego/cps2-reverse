EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 50 69
Title ""
Date "2020-11-26"
Rev ""
Comp ""
Comment1 "Licence: CC-BY"
Comment2 "Author: Loïc *WydD* Petit"
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 7700 4600
Connection ~ 7700 4150
Connection ~ 7700 3700
Connection ~ 7700 2350
Connection ~ 7600 1800
Connection ~ 7500 2250
Connection ~ 7400 2700
Connection ~ 7700 2800
Connection ~ 7300 3150
Connection ~ 7700 3250
Wire Wire Line
	4350 4600 4350 4150
Wire Wire Line
	4350 4600 4450 4600
Wire Wire Line
	5800 1800 7600 1800
Wire Wire Line
	5800 1900 7500 1900
Wire Wire Line
	5800 2000 7400 2000
Wire Wire Line
	5800 2100 7300 2100
Wire Wire Line
	7100 3500 7700 3500
Wire Wire Line
	7100 3700 7700 3700
Wire Wire Line
	7300 2100 7300 3150
Wire Wire Line
	7300 3150 7300 4950
Wire Wire Line
	7300 3150 7800 3150
Wire Wire Line
	7300 4950 7800 4950
Wire Wire Line
	7400 2000 7400 2700
Wire Wire Line
	7400 2700 7400 4500
Wire Wire Line
	7400 2700 7800 2700
Wire Wire Line
	7400 4500 7800 4500
Wire Wire Line
	7500 1900 7500 2250
Wire Wire Line
	7500 2250 7500 4050
Wire Wire Line
	7500 2250 7800 2250
Wire Wire Line
	7500 4050 7800 4050
Wire Wire Line
	7600 1800 7600 3600
Wire Wire Line
	7600 1800 7800 1800
Wire Wire Line
	7600 3600 7800 3600
Wire Wire Line
	7700 1900 7700 2350
Wire Wire Line
	7700 2350 7700 2800
Wire Wire Line
	7700 2800 7700 3250
Wire Wire Line
	7700 3250 7700 3500
Wire Wire Line
	7700 3250 7800 3250
Wire Wire Line
	7700 3700 7700 4150
Wire Wire Line
	7700 4150 7700 4600
Wire Wire Line
	7700 4600 7700 5050
Wire Wire Line
	7700 5050 7800 5050
Wire Wire Line
	7800 1900 7700 1900
Wire Wire Line
	7800 2350 7700 2350
Wire Wire Line
	7800 2800 7700 2800
Wire Wire Line
	7800 3700 7700 3700
Wire Wire Line
	7800 4150 7700 4150
Wire Wire Line
	7800 4600 7700 4600
Text Notes 4950 4000 0    50   ~ 0
On CPS2 = Wired to HIGH (FAD/FBD is not wired anyway)\nOn CPS1 = Wired to FRAME@A-01
$Comp
L A5C:OR02 UCR301
U 1 1 695ADEF0
P 8050 1850
F 0 "UCR301" H 8075 2117 50  0000 C CNN
F 1 "OR02" H 8075 2026 50  0000 C CNN
F 2 "" H 8100 1850 50  0001 C CNN
F 3 "" H 8100 1850 50  0001 C CNN
	1    8050 1850
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR303
U 1 1 695AFB4B
P 8050 2300
F 0 "UCR303" H 8075 2567 50  0000 C CNN
F 1 "OR02" H 8075 2476 50  0000 C CNN
F 2 "" H 8100 2300 50  0001 C CNN
F 3 "" H 8100 2300 50  0001 C CNN
	1    8050 2300
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR305
U 1 1 695B025A
P 8050 2750
F 0 "UCR305" H 8075 3017 50  0000 C CNN
F 1 "OR02" H 8075 2926 50  0000 C CNN
F 2 "" H 8100 2750 50  0001 C CNN
F 3 "" H 8100 2750 50  0001 C CNN
	1    8050 2750
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR308
U 1 1 695B088D
P 8050 3200
F 0 "UCR308" H 8075 3467 50  0000 C CNN
F 1 "OR02" H 8075 3376 50  0000 C CNN
F 2 "" H 8100 3200 50  0001 C CNN
F 3 "" H 8100 3200 50  0001 C CNN
	1    8050 3200
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR311
U 1 1 698BD6E7
P 8050 3650
F 0 "UCR311" H 8075 3917 50  0000 C CNN
F 1 "OR02" H 8075 3826 50  0000 C CNN
F 2 "" H 8100 3650 50  0001 C CNN
F 3 "" H 8100 3650 50  0001 C CNN
	1    8050 3650
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR313
U 1 1 698BD6ED
P 8050 4100
F 0 "UCR313" H 8075 4367 50  0000 C CNN
F 1 "OR02" H 8075 4276 50  0000 C CNN
F 2 "" H 8100 4100 50  0001 C CNN
F 3 "" H 8100 4100 50  0001 C CNN
	1    8050 4100
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR315
U 1 1 698BD6F3
P 8050 4550
F 0 "UCR315" H 8075 4817 50  0000 C CNN
F 1 "OR02" H 8075 4726 50  0000 C CNN
F 2 "" H 8100 4550 50  0001 C CNN
F 3 "" H 8100 4550 50  0001 C CNN
	1    8050 4550
	1    0    0    -1  
$EndComp
$Comp
L A5C:OR02 UCR318
U 1 1 698BD6F9
P 8050 5000
F 0 "UCR318" H 8075 5267 50  0000 C CNN
F 1 "OR02" H 8075 5176 50  0000 C CNN
F 2 "" H 8100 5000 50  0001 C CNN
F 3 "" H 8100 5000 50  0001 C CNN
	1    8050 5000
	1    0    0    -1  
$EndComp
$Comp
L A5C:DBUF UCT310
U 1 1 6B9D0B16
P 6750 3700
F 0 "UCT310" H 6750 4115 50  0000 C CNN
F 1 "DBUF" H 6750 4024 50  0000 C CNN
F 2 "" H 6625 3925 50  0001 C CNN
F 3 "" H 6625 3925 50  0001 C CNN
	1    6750 3700
	1    0    0    -1  
$EndComp
$Sheet
S 4450 4350 1350 1200
U 5FC1AD64
F0 "sd-bus" 50
F1 "sd-bus.sch" 50
F2 "OBJEO" I L 4450 5000 50 
F3 "CLK8M" I L 4450 5150 50 
F4 "~RESETI" I L 4450 5250 50 
F5 "BSD[0..8]" I L 4450 4700 50 
F6 "~A~B" I L 4450 4600 50 
F7 "ASD[0..8]" I L 4450 4500 50 
F8 "~REWEN" O R 5800 4850 50 
F9 "XSD[0..8]" O R 5800 4500 50 
F10 "~DEBUG" I L 4450 5400 50 
F11 "TD[0..11]" T R 5800 5400 50 
$EndSheet
$Sheet
S 4450 1650 1350 1700
U 5FC1AD94
F0 "xpd-bus4" 50
F1 "xpd-bus4.sch" 50
F2 "XSEL" I L 4450 2900 50 
F3 "SERIAL-LOAD" I L 4450 2600 50 
F4 "CLK8M" I L 4450 3100 50 
F5 "BPD[0..7]" I L 4450 1900 50 
F6 "DPD[0..7]" I L 4450 2100 50 
F7 "APD[0..7]" I L 4450 1800 50 
F8 "CPD[0..7]" I L 4450 2000 50 
F9 "GFX-CHNL" I L 4450 2400 50 
F10 "GFX-LOAD" I L 4450 2300 50 
F11 "SERIAL-CHNL" I L 4450 2700 50 
F12 "~RESET" I L 4450 3200 50 
F13 "B" O R 5800 1900 50 
F14 "D" O R 5800 2100 50 
F15 "A" O R 5800 1800 50 
F16 "C" O R 5800 2000 50 
$EndSheet
Wire Wire Line
	4350 4150 7700 4150
Text HLabel 2950 5150 0    50   Input ~ 0
CLK8M
Text HLabel 2950 2900 0    50   Input ~ 0
XSEL
Text HLabel 2950 1800 0    50   Input ~ 0
APD[0..7]
Text HLabel 2950 1900 0    50   Input ~ 0
BPD[0..7]
Text HLabel 2950 2000 0    50   Input ~ 0
CPD[0..7]
Text HLabel 2950 2100 0    50   Input ~ 0
DPD[0..7]
Text HLabel 2950 2400 0    50   Input ~ 0
GFX-CHNL
Text HLabel 2950 2600 0    50   Input ~ 0
SERIAL-LOAD
Text HLabel 2950 2700 0    50   Input ~ 0
SERIAL-CHNL
Text HLabel 2950 3200 0    50   Input ~ 0
~RESET
Text HLabel 2950 2300 0    50   Input ~ 0
LOAD-XPD4
Text HLabel 2950 4500 0    50   Input ~ 0
ASD[0..8]
Wire Bus Line
	2950 1800 4450 1800
Wire Bus Line
	2950 1900 4450 1900
Wire Bus Line
	2950 2000 4450 2000
Wire Bus Line
	2950 2100 4450 2100
Wire Wire Line
	4450 2300 2950 2300
Wire Wire Line
	4450 2400 2950 2400
Wire Wire Line
	4450 2600 2950 2600
Wire Wire Line
	4450 2700 2950 2700
Wire Wire Line
	4450 2900 2950 2900
Wire Wire Line
	4450 3200 4150 3200
Wire Wire Line
	4250 3100 4250 5150
Wire Wire Line
	4250 5150 4450 5150
Wire Wire Line
	4250 3100 4450 3100
Wire Wire Line
	4250 5150 2950 5150
Connection ~ 4250 5150
Wire Bus Line
	4450 4500 2950 4500
Wire Bus Line
	4450 4700 2950 4700
Text HLabel 2950 4700 0    50   Input ~ 0
BSD[0..8]
Wire Wire Line
	4450 5000 2950 5000
Text HLabel 2950 5000 0    50   Input ~ 0
OBJEO
Wire Wire Line
	4450 5250 4150 5250
Wire Wire Line
	4150 5250 4150 3200
Connection ~ 4150 3200
Wire Wire Line
	4150 3200 2950 3200
Text HLabel 6400 3700 0    50   Input ~ 0
FRAME
Text Label 8450 1850 0    50   ~ 0
FAD0
Text Label 8450 2300 0    50   ~ 0
FAD1
Text Label 8450 2750 0    50   ~ 0
FAD2
Text Label 8450 3200 0    50   ~ 0
FAD3
Text Label 8450 3650 0    50   ~ 0
FBD0
Text Label 8450 4100 0    50   ~ 0
FBD1
Text Label 8450 4550 0    50   ~ 0
FBD2
Text Label 8450 5000 0    50   ~ 0
FBD3
Wire Wire Line
	8350 1850 8750 1850
Wire Wire Line
	8350 2300 8750 2300
Wire Wire Line
	8350 2750 8750 2750
Wire Wire Line
	8350 3200 8750 3200
Wire Wire Line
	8350 3650 8750 3650
Wire Wire Line
	8350 4100 8750 4100
Wire Wire Line
	8350 4550 8750 4550
Wire Wire Line
	8350 5000 8750 5000
Entry Wire Line
	8750 1850 8850 1950
Entry Wire Line
	8750 2300 8850 2400
Entry Wire Line
	8750 2750 8850 2850
Entry Wire Line
	8750 3200 8850 3300
Entry Wire Line
	8750 3650 8850 3750
Entry Wire Line
	8750 4100 8850 4200
Entry Wire Line
	8750 4550 8850 4650
Entry Wire Line
	8750 5000 8850 5100
Wire Bus Line
	8850 3400 8950 3400
Wire Bus Line
	8850 5200 8950 5200
Text HLabel 3750 5300 0    50   Input ~ 0
~DEBUG[2..2]
Wire Bus Line
	3750 5300 3850 5300
Entry Wire Line
	3850 5300 3950 5400
Wire Wire Line
	3950 5400 4450 5400
Text Label 4000 5400 0    50   ~ 0
~DEBUG2
Text HLabel 6350 4850 2    50   Output ~ 0
~REWEN
Text HLabel 6350 5400 2    50   3State ~ 0
TD[0..11]
Wire Bus Line
	6350 4500 5800 4500
Text HLabel 6350 4500 2    50   Output ~ 0
SPRITES[0..8]
Wire Wire Line
	5800 4850 6350 4850
Wire Bus Line
	5800 5400 6350 5400
Text HLabel 8950 5200 2    50   Output ~ 0
FBD[0..3]
Text HLabel 8950 3400 2    50   Output ~ 0
FAD[0..3]
Wire Bus Line
	8850 1950 8850 3400
Wire Bus Line
	8850 3750 8850 5200
Text Notes 1250 6750 0    100  ~ 0
The SPRITES generation is split in two.\nXPD-BUS4 streams the data to FAD/FBD to be stored in OBJRAM\nIt is handled by the A-01.\nIt comes back to ASD/BSD to be muxed to the SPRITES bus
$EndSCHEMATC
