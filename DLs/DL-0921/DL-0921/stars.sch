EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 69
Title ""
Date "2020-11-26"
Rev ""
Comp ""
Comment1 "Licence: CC-BY"
Comment2 "Author: Loïc *WydD* Petit"
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 2950 5050
Connection ~ 2950 3750
Connection ~ 2950 3550
Connection ~ 4450 4650
Connection ~ 4550 4800
Connection ~ 2950 2850
Connection ~ 4450 3650
Connection ~ 6200 2950
Connection ~ 6200 3150
Connection ~ 4550 3150
Connection ~ 6100 5200
Connection ~ 6400 4900
Connection ~ 6300 2300
Connection ~ 6200 4800
Connection ~ 6400 3800
Wire Wire Line
	2950 2850 2400 2850
Wire Wire Line
	2950 2850 2950 3550
Wire Wire Line
	2950 2850 3050 2850
Wire Wire Line
	2950 3750 2950 3550
Wire Wire Line
	2950 3750 2950 5050
Wire Wire Line
	2950 5050 2950 5950
Wire Wire Line
	3050 5050 2950 5050
Wire Wire Line
	3250 3550 2950 3550
Wire Wire Line
	4450 2450 4450 3650
Wire Wire Line
	4450 2450 5150 2450
Wire Wire Line
	4450 3650 4650 3650
Wire Wire Line
	4450 4650 4450 5850
Wire Wire Line
	4450 4650 5150 4650
Wire Wire Line
	4450 5850 4650 5850
Wire Wire Line
	4550 2600 4550 3150
Wire Wire Line
	4550 4800 4550 5400
Wire Wire Line
	4550 4800 5150 4800
Wire Wire Line
	5100 1300 5200 1300
Wire Wire Line
	5100 1400 5200 1400
Wire Wire Line
	5150 2600 4550 2600
Wire Wire Line
	6000 1400 6300 1400
Wire Wire Line
	6000 2300 6100 2300
Wire Wire Line
	6000 4500 6100 4500
Wire Wire Line
	6200 5900 6200 5400
Wire Wire Line
	6100 2300 6100 3600
Wire Wire Line
	6100 3600 6500 3600
Wire Wire Line
	6100 4500 6100 5200
Wire Wire Line
	6100 5200 6100 5800
Wire Wire Line
	6100 5800 6500 5800
Wire Wire Line
	6200 2500 6200 2950
Wire Wire Line
	6200 2500 6500 2500
Wire Wire Line
	6200 2950 6200 3150
Wire Wire Line
	6200 3150 6200 3700
Wire Wire Line
	6200 4800 6200 5400
Wire Wire Line
	6200 4800 6500 4800
Wire Wire Line
	6300 2300 6300 1400
Wire Wire Line
	6300 2300 6300 4500
Wire Wire Line
	6300 2300 6500 2300
Wire Wire Line
	6300 4500 6500 4500
Wire Wire Line
	6400 2600 6400 3800
Wire Wire Line
	6400 2600 6500 2600
Wire Wire Line
	6400 3800 6500 3800
Wire Wire Line
	6400 4900 6400 6000
Wire Wire Line
	6400 4900 6500 4900
Wire Wire Line
	6400 6000 6500 6000
Wire Wire Line
	7300 5650 7750 5650
Wire Wire Line
	7750 2950 6200 2950
Wire Wire Line
	7750 3450 7300 3450
Wire Wire Line
	7750 5200 6100 5200
Wire Bus Line
	4150 2450 4250 2450
Wire Bus Line
	4250 2450 4250 3450
Wire Bus Line
	4250 4650 4150 4650
Wire Bus Line
	4250 5650 4250 4650
Wire Bus Line
	5700 3450 6500 3450
Wire Bus Line
	5700 3550 5900 3550
Wire Bus Line
	5700 5650 6500 5650
Wire Bus Line
	5700 5750 5900 5750
Wire Bus Line
	5900 3050 7750 3050
Wire Bus Line
	5900 3550 5900 3050
Wire Bus Line
	5900 5300 7750 5300
Wire Bus Line
	5900 5750 5900 5300
Wire Bus Line
	7400 2400 7750 2400
Wire Bus Line
	7750 2300 7400 2300
Wire Bus Line
	7750 4500 7400 4500
Wire Bus Line
	7750 4600 7400 4600
Text Notes 1300 3350 1    200  ~ 0
STARS 2
Text Notes 1300 6250 1    200  ~ 0
STARS 1
Text Notes 4750 900  0    200  ~ 0
STARS LAYERS
Text Label 7750 3700 2    50   ~ 0
~LAYERS-E4
Text Label 7750 5900 2    50   ~ 0
~LAYERS-E3
$Comp
L A5C:NBUF03 UBU210
U 1 1 5FD38C53
P 3550 3550
F 0 "UBU210" H 3550 3866 50  0000 C CNN
F 1 "NBUF03" H 3550 3775 50  0000 C CNN
F 2 "" H 3550 3350 50  0001 C CNN
F 3 "" H 3550 3350 50  0001 C CNN
	1    3550 3550
	1    0    0    -1  
$EndComp
$Sheet
S 5150 4350 850  700 
U 5FBD0B25
F0 "counter-xs-2" 50
F1 "counter-xs-2.sch" 50
F2 "~RESETI" I L 5150 4950 50 
F3 "ZERO" O R 6000 4500 50 
F4 "XS[0..4]" I L 5150 4500 50 
F5 "LOAD" I L 5150 4650 50 
F6 "~CLK8M" I L 5150 4800 50 
$EndSheet
$Sheet
S 5200 1150 800  700 
U 5FBD0B4A
F0 "counter4" 50
F1 "counter4.sch" 50
F2 "ZERO" O R 6000 1400 50 
F3 "~CLK4M" I L 5200 1300 50 
F4 "FI" I L 5200 1400 50 
F5 "CLK8M" I L 5200 1600 50 
F6 "~RESETI" I L 5200 1700 50 
$EndSheet
$Sheet
S 7750 4350 1050 1800
U 5FBD0B3A
F0 "row5-assemble" 50
F1 "row5-assemble.sch" 50
F2 "CNT0-Q[0..3]" I L 7750 4500 50 
F3 "CNT1-Q[0..3]" I L 7750 4600 50 
F4 "PD[5..8]" I L 7750 5300 50 
F5 "ZERO-XS2" I L 7750 5200 50 
F6 "CLK8M" I L 7750 6000 50 
F7 "ZERO-PD5" I L 7750 5650 50 
F8 "ROW5-D[0..6]" O R 8800 4900 50 
F9 "~ROW5-E" O R 8800 5000 50 
F10 "~ENABLE" I L 7750 5900 50 
F11 "~DEBUG" I R 8800 4650 50 
F12 "TD[0..11]" T R 8800 4750 50 
$EndSheet
$Sheet
S 6500 4350 900  700 
U 5FBD0B58
F0 "row5-counter" 50
F1 "row5-counter.sch" 50
F2 "~RESETI" I L 6500 4900 50 
F3 "~CLK8M" I L 6500 4800 50 
F4 "CNT0-Q[0..3]" O R 7400 4500 50 
F5 "CNT1-Q[0..3]" O R 7400 4600 50 
F6 "INC" I L 6500 4500 50 
F7 "~DEBUG" I R 7400 4800 50 
$EndSheet
$Sheet
S 7750 2150 1050 1800
U 5FBD0B6A
F0 "row6-assemble" 50
F1 "row6-assemble.sch" 50
F2 "CNT0-Q[0..3]" I L 7750 2300 50 
F3 "CNT1-Q[0..3]" I L 7750 2400 50 
F4 "PD[5..8]" I L 7750 3050 50 
F5 "ZERO-XS1" I L 7750 2950 50 
F6 "CLK8M" I L 7750 3800 50 
F7 "ZERO-PD6" I L 7750 3450 50 
F8 "ROW6-D[0..6]" O R 8800 2700 50 
F9 "~ROW6-E" O R 8800 2800 50 
F10 "~ENABLE" I L 7750 3700 50 
F11 "~DEBUG" I R 8800 2450 50 
F12 "TD[0..11]" T R 8800 2550 50 
$EndSheet
$Sheet
S 6500 2150 900  600 
U 5FBD0B6B
F0 "row6-counter" 50
F1 "row6-counter.sch" 50
F2 "~RESETI" I L 6500 2600 50 
F3 "INC" I L 6500 2300 50 
F4 "~CLK8M" I L 6500 2500 50 
F5 "CNT0-Q[0..3]" O R 7400 2300 50 
F6 "CNT1-Q[0..3]" O R 7400 2400 50 
F7 "~DEBUG" I R 7400 2550 50 
$EndSheet
$Sheet
S 3050 4350 1100 850 
U 5FBD0AE4
F0 "xpd-bus5" 50
F1 "xpd-bus5.sch" 50
F2 "XS[0..4]" I L 3050 4500 50 
F3 "APD[0..7]" I L 3050 4650 50 
F4 "FLIP" I L 3050 4800 50 
F5 "XS-OUT[0..4]" O R 4150 4500 50 
F6 "CLK8M" I L 3050 5050 50 
F7 "LOAD" I L 3050 4950 50 
F8 "PD[0..8]" O R 4150 4650 50 
$EndSheet
$Sheet
S 4650 5500 1050 600 
U 5FBD0B26
F0 "xpd-bus5-sync" 50
F1 "xpd-bus5-sync.sch" 50
F2 "PD[0..8]" I L 4650 5650 50 
F3 "~PD-OUT[0..4]" O R 5700 5650 50 
F4 "PD-OUT[5..8]" O R 5700 5750 50 
F5 "CLK8M" I L 4650 5950 50 
F6 "~LOAD" I L 4650 5850 50 
$EndSheet
$Sheet
S 3050 2150 1100 850 
U 5FBD0ADE
F0 "xpd-bus6" 50
F1 "xpd-bus6.sch" 50
F2 "XS[0..4]" I L 3050 2300 50 
F3 "FLIP" I L 3050 2600 50 
F4 "XS-OUT[0..4]" O R 4150 2300 50 
F5 "APD[0..7]" I L 3050 2450 50 
F6 "CLK8M" I L 3050 2850 50 
F7 "LOAD" I L 3050 2750 50 
F8 "PD[0..8]" O R 4150 2450 50 
$EndSheet
$Sheet
S 4650 3300 1050 600 
U 5FBD0B14
F0 "xpd-bus6-sync" 50
F1 "xpd-bus6-sync.sch" 50
F2 "PD[0..8]" I L 4650 3450 50 
F3 "~PD-OUT[0..4]" O R 5700 3450 50 
F4 "PD-OUT[5..8]" O R 5700 3550 50 
F5 "CLK8M" I L 4650 3750 50 
F6 "~LOAD" I L 4650 3650 50 
$EndSheet
$Sheet
S 6500 5500 800  600 
U 5FBD0B2C
F0 "xpd5-sync" 50
F1 "xpd5-sync.sch" 50
F2 "LOAD" I L 6500 5800 50 
F3 "~RESET" I L 6500 6000 50 
F4 "~CLK8M" I L 6500 5900 50 
F5 "~PD[0..4]" I L 6500 5650 50 
F6 "ZERO" O R 7300 5650 50 
$EndSheet
$Sheet
S 6500 3300 800  600 
U 5FBD0B6C
F0 "xpd6-sync" 50
F1 "xpd6-sync.sch" 50
F2 "LOAD" I L 6500 3600 50 
F3 "~RESET" I L 6500 3800 50 
F4 "~CLK8M" I L 6500 3700 50 
F5 "~PD[0..4]" I L 6500 3450 50 
F6 "ZERO" O R 7300 3450 50 
$EndSheet
Wire Wire Line
	4550 3150 6200 3150
Wire Wire Line
	6500 3700 6200 3700
Connection ~ 6200 3700
Connection ~ 6200 5400
Wire Wire Line
	6200 5900 6500 5900
Wire Wire Line
	4550 5400 6200 5400
Wire Wire Line
	7650 3800 7750 3800
Wire Wire Line
	7650 6000 7650 6250
Wire Wire Line
	7650 6000 7750 6000
Wire Wire Line
	5050 4950 5150 4950
Wire Wire Line
	5050 4150 6400 4150
Wire Wire Line
	6400 3800 6400 4150
Wire Wire Line
	4550 6250 4550 5950
Wire Wire Line
	4550 6250 7650 6250
Wire Wire Line
	4550 5950 4650 5950
Wire Wire Line
	6200 3700 6200 4800
Connection ~ 4450 5850
Wire Wire Line
	4450 3650 4450 4650
Connection ~ 4350 3750
Wire Wire Line
	4350 3750 4650 3750
Wire Wire Line
	7650 3800 7650 4050
Wire Wire Line
	4350 3750 4350 4050
Wire Wire Line
	4350 4050 7650 4050
Connection ~ 6400 4150
Wire Wire Line
	6400 4150 6400 4900
Wire Wire Line
	5050 4150 5050 4950
Wire Wire Line
	6400 2600 6400 2000
Wire Wire Line
	6400 2000 5050 2000
Wire Wire Line
	5050 2000 5050 2750
Wire Wire Line
	5050 2750 5150 2750
Connection ~ 6400 2600
Connection ~ 5050 2000
Text HLabel 2400 2000 0    50   Input ~ 0
~RESET
Wire Wire Line
	5050 2000 5050 1700
Wire Wire Line
	5050 1700 5200 1700
Wire Wire Line
	4350 3750 4350 1600
Wire Wire Line
	4350 1600 5200 1600
Wire Wire Line
	4550 3150 4550 3550
Wire Bus Line
	4150 2300 5150 2300
Wire Bus Line
	4250 3450 4650 3450
Wire Wire Line
	3850 3550 4550 3550
Connection ~ 4550 3550
Wire Wire Line
	4550 3550 4550 4800
Wire Wire Line
	2950 3750 4350 3750
Wire Bus Line
	4250 5650 4650 5650
Wire Wire Line
	2950 5950 4550 5950
Connection ~ 4550 5950
Wire Bus Line
	4150 4500 5150 4500
Text HLabel 2400 2850 0    50   Input ~ 0
CLK8M
Wire Wire Line
	2400 2000 5050 2000
Wire Wire Line
	2400 2750 3050 2750
Wire Wire Line
	3050 4800 2850 4800
Wire Wire Line
	2850 4800 2850 2600
Wire Wire Line
	2850 2600 3050 2600
Wire Bus Line
	3050 4650 2750 4650
Wire Bus Line
	2750 4650 2750 2450
Wire Bus Line
	3050 2300 2650 2300
Wire Bus Line
	2650 2300 2650 4500
Wire Bus Line
	2650 4500 3050 4500
Text HLabel 2400 2750 0    50   Input ~ 0
STARS-LOAD2
Wire Wire Line
	2400 4950 3050 4950
Text HLabel 2400 4950 0    50   Input ~ 0
STARS-LOAD1
Text HLabel 2400 5850 0    50   Input ~ 0
STARS-SERIAL
Wire Wire Line
	2400 5850 4450 5850
Text HLabel 5100 1400 0    50   Input ~ 0
FI
Text HLabel 5100 1300 0    50   Input ~ 0
~CLK4M
Wire Bus Line
	8800 4750 8900 4750
Wire Bus Line
	8900 4750 8900 4100
Wire Bus Line
	8900 2550 8800 2550
Wire Bus Line
	8800 2700 9450 2700
Wire Bus Line
	8800 4900 9450 4900
Wire Bus Line
	8900 4100 9450 4100
Text HLabel 9450 4100 2    50   3State ~ 0
TD[0..11]
Text HLabel 9450 2700 2    50   Output ~ 0
STARS2[0..6]
Text HLabel 9450 2800 2    50   Output ~ 0
~STARS2-E
Text HLabel 9450 4900 2    50   Output ~ 0
STARS1[0..6]
Text HLabel 9450 5000 2    50   Output ~ 0
~STARS1-E
Wire Bus Line
	2650 2300 2400 2300
Connection ~ 2650 2300
Wire Bus Line
	2400 2450 2750 2450
Connection ~ 2750 2450
Wire Bus Line
	2750 2450 3050 2450
Wire Wire Line
	2850 2600 2400 2600
Connection ~ 2850 2600
Text HLabel 2400 2300 0    50   Input ~ 0
XS[0..4]
Text HLabel 2400 2450 0    50   Input ~ 0
APD[0..7]
Text HLabel 2400 2600 0    50   Input ~ 0
FLIP
Text HLabel 9950 5700 0    50   Input ~ 0
~LAYERS-E[3..4]
Wire Bus Line
	9950 5700 10100 5700
Entry Wire Line
	10100 5950 10200 6050
Entry Wire Line
	10100 5850 10200 5950
Text Label 10250 5950 0    50   ~ 0
~LAYERS-E3
Wire Wire Line
	10200 5950 10800 5950
Text Label 10250 6050 0    50   ~ 0
~LAYERS-E4
Wire Wire Line
	10200 6050 10800 6050
$Comp
L A5C:AND02 UBK?
U 1 1 601F9096
P 9300 4650
AR Path="/601F9096" Ref="UBK?"  Part="1" 
AR Path="/621F1DB4/601F9096" Ref="UBK?"  Part="1" 
AR Path="/5FBCE4A9/5FBD0B3A/601F9096" Ref="UBK?"  Part="1" 
AR Path="/5FBCE4A9/601F9096" Ref="UBK428"  Part="1" 
F 0 "UBK428" H 9325 4916 50  0000 C CNN
F 1 "AND02" H 9325 4825 50  0000 C CNN
F 2 "" H 9300 4650 50  0001 C CNN
F 3 "" H 9300 4650 50  0001 C CNN
	1    9300 4650
	-1   0    0    -1  
$EndComp
Text Label 10100 4700 2    50   ~ 0
~DEBUG6
Wire Wire Line
	10150 4700 9550 4700
Text Label 10100 4600 2    50   ~ 0
~DEBUG13
Wire Wire Line
	10150 4600 9650 4600
Wire Wire Line
	9000 4650 8800 4650
Text Label 10100 2500 2    50   ~ 0
~DEBUG7
Text Label 10100 2400 2    50   ~ 0
~DEBUG14
$Comp
L A5C:AND02 UBK?
U 1 1 6022FDD5
P 9300 2450
AR Path="/6022FDD5" Ref="UBK?"  Part="1" 
AR Path="/5FAFC3AE/6022FDD5" Ref="UBK?"  Part="1" 
AR Path="/5FBCE4A9/5FBD0B6A/6022FDD5" Ref="UBK?"  Part="1" 
AR Path="/5FBCE4A9/6022FDD5" Ref="UBK411"  Part="1" 
F 0 "UBK411" H 9325 2716 50  0000 C CNN
F 1 "AND02" H 9325 2625 50  0000 C CNN
F 2 "" H 9300 2450 50  0001 C CNN
F 3 "" H 9300 2450 50  0001 C CNN
	1    9300 2450
	-1   0    0    -1  
$EndComp
Connection ~ 8900 4100
Wire Bus Line
	8900 4100 8900 2550
Wire Wire Line
	8800 2450 9000 2450
Wire Wire Line
	10150 2400 9650 2400
Wire Wire Line
	9450 2800 8800 2800
Wire Wire Line
	9450 5000 8800 5000
$Sheet
S 5150 2150 850  700 
U 5FBD0B40
F0 "counter-xs-1" 50
F1 "counter-xs-1.sch" 50
F2 "XS[0..4]" I L 5150 2300 50 
F3 "~CLK8M" I L 5150 2600 50 
F4 "ZERO" O R 6000 2300 50 
F5 "~RESETI" I L 5150 2750 50 
F6 "LOAD" I L 5150 2450 50 
$EndSheet
Wire Wire Line
	7400 4800 7650 4800
Wire Wire Line
	7650 4800 7650 4200
Wire Wire Line
	7650 4200 9650 4200
Wire Wire Line
	9650 4200 9650 4600
Connection ~ 9650 4600
Wire Wire Line
	9650 4600 9550 4600
Wire Wire Line
	9650 2000 7550 2000
Wire Wire Line
	7550 2000 7550 2550
Wire Wire Line
	9650 2400 9550 2400
Wire Wire Line
	7400 2550 7550 2550
Wire Wire Line
	9550 2500 10150 2500
Connection ~ 9650 2400
Wire Wire Line
	9650 2000 9650 2400
Text HLabel 10350 2150 2    50   Input ~ 0
~DEBUG[6..14]
Wire Bus Line
	10350 2150 10250 2150
Entry Wire Line
	10150 2500 10250 2400
Entry Wire Line
	10150 2400 10250 2300
Entry Wire Line
	10150 4700 10250 4600
Entry Wire Line
	10150 4600 10250 4500
Wire Bus Line
	10100 5700 10100 5950
Wire Bus Line
	10250 2150 10250 4600
$EndSCHEMATC
