EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 20
Title ""
Date "2020-04-12"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1827"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 7550 5850
Connection ~ 3450 1000
Connection ~ 4950 5850
NoConn ~ 3200 6600
Entry Wire Line
	3450 1400 3550 1500
Entry Wire Line
	3450 1950 3550 2050
Entry Wire Line
	3450 2500 3550 2600
Entry Wire Line
	3450 3050 3550 3150
Entry Wire Line
	3450 3600 3550 3700
Entry Wire Line
	3450 4150 3550 4250
Entry Wire Line
	3450 4700 3550 4800
Entry Wire Line
	3450 5250 3550 5350
Entry Wire Line
	4850 1500 4950 1600
Entry Wire Line
	4850 2050 4950 2150
Entry Wire Line
	4850 2600 4950 2700
Entry Wire Line
	4850 3150 4950 3250
Entry Wire Line
	4850 3700 4950 3800
Entry Wire Line
	4850 4250 4950 4350
Entry Wire Line
	4850 4800 4950 4900
Entry Wire Line
	4850 5350 4950 5450
Entry Wire Line
	4950 6250 4850 6350
Entry Wire Line
	6050 1400 6150 1500
Entry Wire Line
	6050 1950 6150 2050
Entry Wire Line
	6050 2500 6150 2600
Entry Wire Line
	6050 3050 6150 3150
Entry Wire Line
	7450 1500 7550 1600
Entry Wire Line
	7450 2050 7550 2150
Entry Wire Line
	7450 2600 7550 2700
Entry Wire Line
	7450 3150 7550 3250
Wire Wire Line
	2350 6450 2700 6450
Wire Wire Line
	2350 6650 2700 6650
Wire Wire Line
	2350 6950 2950 6950
Wire Wire Line
	2950 6950 2950 6850
Wire Wire Line
	3200 6450 3750 6450
Wire Wire Line
	3400 6000 3200 6000
Wire Wire Line
	3400 6250 3400 6000
Wire Wire Line
	3550 1500 3900 1500
Wire Wire Line
	3550 2050 3900 2050
Wire Wire Line
	3550 2600 3900 2600
Wire Wire Line
	3550 3150 3900 3150
Wire Wire Line
	3550 3700 3900 3700
Wire Wire Line
	3550 4250 3900 4250
Wire Wire Line
	3550 4800 3900 4800
Wire Wire Line
	3550 5350 3900 5350
Wire Wire Line
	3750 6250 3400 6250
Wire Wire Line
	4500 1500 4850 1500
Wire Wire Line
	4500 2050 4850 2050
Wire Wire Line
	4500 2600 4850 2600
Wire Wire Line
	4500 3150 4850 3150
Wire Wire Line
	4500 3700 4850 3700
Wire Wire Line
	4500 4250 4850 4250
Wire Wire Line
	4500 4800 4850 4800
Wire Wire Line
	4500 5350 4850 5350
Wire Wire Line
	4850 6350 4350 6350
Wire Wire Line
	6150 1500 6500 1500
Wire Wire Line
	6150 2050 6500 2050
Wire Wire Line
	6150 2600 6500 2600
Wire Wire Line
	6150 3150 6500 3150
Wire Wire Line
	7100 1500 7450 1500
Wire Wire Line
	7100 2050 7450 2050
Wire Wire Line
	7100 2600 7450 2600
Wire Wire Line
	7100 3150 7450 3150
Wire Bus Line
	3450 1000 2300 1000
Wire Bus Line
	3450 1000 3450 1400
Wire Bus Line
	3450 1400 3450 2500
Wire Bus Line
	3450 2500 3450 3600
Wire Bus Line
	3450 3600 3450 4700
Wire Bus Line
	3450 4700 3450 5250
Wire Bus Line
	4950 1600 4950 2700
Wire Bus Line
	4950 2700 4950 3800
Wire Bus Line
	4950 3800 4950 4900
Wire Bus Line
	4950 4900 4950 5850
Wire Bus Line
	4950 5850 4950 6250
Wire Bus Line
	4950 5850 7550 5850
Wire Bus Line
	6050 1000 3450 1000
Wire Bus Line
	6050 1000 6050 1950
Wire Bus Line
	6050 1950 6050 3050
Wire Bus Line
	7550 1600 7550 2700
Wire Bus Line
	7550 2700 7550 5850
Wire Bus Line
	7550 5850 8450 5850
Text Label 3800 1500 2    50   ~ 0
IN0
Text Label 3800 2050 2    50   ~ 0
IN1
Text Label 3800 2600 2    50   ~ 0
IN2
Text Label 3800 3150 2    50   ~ 0
IN3
Text Label 3800 3700 2    50   ~ 0
IN4
Text Label 3800 4250 2    50   ~ 0
IN5
Text Label 3800 4800 2    50   ~ 0
IN6
Text Label 3800 5350 2    50   ~ 0
IN7
Text Label 4550 1500 0    50   ~ 0
OUT0
Text Label 4550 2050 0    50   ~ 0
OUT1
Text Label 4550 2600 0    50   ~ 0
OUT2
Text Label 4550 3150 0    50   ~ 0
OUT3
Text Label 4550 3700 0    50   ~ 0
OUT4
Text Label 4550 4250 0    50   ~ 0
OUT5
Text Label 4550 4800 0    50   ~ 0
OUT6
Text Label 4550 5350 0    50   ~ 0
OUT7
Text Label 4650 6350 2    50   ~ 0
OUT12
Text Label 6400 1500 2    50   ~ 0
IN8
Text Label 6400 2050 2    50   ~ 0
IN9
Text Label 6400 2600 2    50   ~ 0
IN10
Text Label 6400 3150 2    50   ~ 0
IN11
Text Label 7150 1500 0    50   ~ 0
OUT8
Text Label 7150 2050 0    50   ~ 0
OUT9
Text Label 7150 2600 0    50   ~ 0
OUT10
Text Label 7150 3150 0    50   ~ 0
OUT11
Text HLabel 2300 1000 0    50   Input ~ 0
IN[0..11]
Text HLabel 2350 6450 0    50   Input ~ 0
BANK-SELECT
Text HLabel 2350 6650 0    50   Input ~ 0
BANK-CLK
Text HLabel 2350 6950 0    50   Input ~ 0
~BANK-RESET
Text HLabel 3200 6000 0    50   Input ~ 0
IN12
Text HLabel 8450 5850 2    50   Output ~ 0
OUT[0..12]
$Comp
L CG24-cells:X2N U34R71
U 1 1 5F7A95EC
P 4050 6350
F 0 "U34R71" H 4050 6675 50  0000 C CNN
F 1 "X2N" H 4050 6584 50  0000 C CNN
F 2 "" H 4050 6350 50  0001 C CNN
F 3 "" H 4050 6350 50  0001 C CNN
	1    4050 6350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U30R154
U 1 1 5F207A0A
P 4200 1500
F 0 "U30R154" H 4200 1817 50  0000 C CNN
F 1 "B1N" H 4200 1726 50  0000 C CNN
F 2 "" H 4200 1500 50  0001 C CNN
F 3 "" H 4200 1500 50  0001 C CNN
	1    4200 1500
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U36R182
U 1 1 5F207BE0
P 4200 2050
F 0 "U36R182" H 4200 2367 50  0000 C CNN
F 1 "B1N" H 4200 2276 50  0000 C CNN
F 2 "" H 4200 2050 50  0001 C CNN
F 3 "" H 4200 2050 50  0001 C CNN
	1    4200 2050
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U26R174
U 1 1 5F2086E9
P 4200 2600
F 0 "U26R174" H 4200 2917 50  0000 C CNN
F 1 "B1N" H 4200 2826 50  0000 C CNN
F 2 "" H 4200 2600 50  0001 C CNN
F 3 "" H 4200 2600 50  0001 C CNN
	1    4200 2600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U30R174
U 1 1 5F2086EF
P 4200 3150
F 0 "U30R174" H 4200 3467 50  0000 C CNN
F 1 "B1N" H 4200 3376 50  0000 C CNN
F 2 "" H 4200 3150 50  0001 C CNN
F 3 "" H 4200 3150 50  0001 C CNN
	1    4200 3150
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U28R174
U 1 1 5F209E29
P 4200 3700
F 0 "U28R174" H 4200 4017 50  0000 C CNN
F 1 "B1N" H 4200 3926 50  0000 C CNN
F 2 "" H 4200 3700 50  0001 C CNN
F 3 "" H 4200 3700 50  0001 C CNN
	1    4200 3700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U28R180
U 1 1 5F209E2F
P 4200 4250
F 0 "U28R180" H 4200 4567 50  0000 C CNN
F 1 "B1N" H 4200 4476 50  0000 C CNN
F 2 "" H 4200 4250 50  0001 C CNN
F 3 "" H 4200 4250 50  0001 C CNN
	1    4200 4250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U24R174
U 1 1 5F209E35
P 4200 4800
F 0 "U24R174" H 4200 5117 50  0000 C CNN
F 1 "B1N" H 4200 5026 50  0000 C CNN
F 2 "" H 4200 4800 50  0001 C CNN
F 3 "" H 4200 4800 50  0001 C CNN
	1    4200 4800
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U30R180
U 1 1 5F209E3B
P 4200 5350
F 0 "U30R180" H 4200 5667 50  0000 C CNN
F 1 "B1N" H 4200 5576 50  0000 C CNN
F 2 "" H 4200 5350 50  0001 C CNN
F 3 "" H 4200 5350 50  0001 C CNN
	1    4200 5350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U20R164
U 1 1 5F212A2F
P 6800 1500
F 0 "U20R164" H 6800 1817 50  0000 C CNN
F 1 "B1N" H 6800 1726 50  0000 C CNN
F 2 "" H 6800 1500 50  0001 C CNN
F 3 "" H 6800 1500 50  0001 C CNN
	1    6800 1500
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U21R174
U 1 1 5F212A35
P 6800 2050
F 0 "U21R174" H 6800 2367 50  0000 C CNN
F 1 "B1N" H 6800 2276 50  0000 C CNN
F 2 "" H 6800 2050 50  0001 C CNN
F 3 "" H 6800 2050 50  0001 C CNN
	1    6800 2050
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U18R174
U 1 1 5F212A3B
P 6800 2600
F 0 "U18R174" H 6800 2917 50  0000 C CNN
F 1 "B1N" H 6800 2826 50  0000 C CNN
F 2 "" H 6800 2600 50  0001 C CNN
F 3 "" H 6800 2600 50  0001 C CNN
	1    6800 2600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B1N U16R174
U 1 1 5F212A41
P 6800 3150
F 0 "U16R174" H 6800 3467 50  0000 C CNN
F 1 "B1N" H 6800 3376 50  0000 C CNN
F 2 "" H 6800 3150 50  0001 C CNN
F 3 "" H 6800 3150 50  0001 C CNN
	1    6800 3150
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:FDO U34R58
U 1 1 5E8B4427
P 2950 6550
F 0 "U34R58" H 2950 6836 50  0000 C CNN
F 1 "FDO" H 3100 6277 50  0000 C CNN
F 2 "" H 2950 6550 50  0001 C CNN
F 3 "" H 2950 6550 50  0001 C CNN
	1    2950 6550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
