EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 20
Title ""
Date "2020-04-12"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1827"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4300 2500
Connection ~ 4300 3200
Connection ~ 4300 3900
Connection ~ 4300 4600
Connection ~ 4300 5300
Connection ~ 4300 6000
Connection ~ 6450 4000
Connection ~ 4300 4250
Connection ~ 7350 4000
Entry Wire Line
	6350 1550 6450 1650
Entry Wire Line
	6350 2250 6450 2350
Entry Wire Line
	6350 2950 6450 3050
Entry Wire Line
	6350 3650 6450 3750
Entry Wire Line
	6350 4350 6450 4250
Entry Wire Line
	6350 5050 6450 4950
Entry Wire Line
	6350 5750 6450 5650
Entry Wire Line
	6350 6450 6450 6350
Entry Wire Line
	7450 4750 7350 4650
Entry Wire Line
	7450 4850 7350 4750
Entry Wire Line
	7450 4950 7350 4850
Entry Wire Line
	7450 5050 7350 4950
Entry Wire Line
	7450 5150 7350 5050
Entry Wire Line
	7450 5250 7350 5150
Entry Wire Line
	7450 5350 7350 5250
Entry Wire Line
	7450 5450 7350 5350
Entry Wire Line
	8450 4750 8550 4850
Entry Wire Line
	8450 4850 8550 4950
Entry Wire Line
	8450 4950 8550 5050
Entry Wire Line
	8450 5050 8550 5150
Entry Wire Line
	8450 5150 8550 5250
Entry Wire Line
	8450 5250 8550 5350
Entry Wire Line
	8450 5350 8550 5450
Entry Wire Line
	8450 5450 8550 5550
Wire Wire Line
	4300 1800 4300 2500
Wire Wire Line
	4300 1800 5450 1800
Wire Wire Line
	4300 2500 4300 3200
Wire Wire Line
	4300 2500 5450 2500
Wire Wire Line
	4300 3200 4300 3900
Wire Wire Line
	4300 3200 5450 3200
Wire Wire Line
	4300 3900 4300 4250
Wire Wire Line
	4300 3900 5450 3900
Wire Wire Line
	4300 4250 4300 4600
Wire Wire Line
	4300 4600 4300 5300
Wire Wire Line
	4300 4600 5450 4600
Wire Wire Line
	4300 5300 4300 6000
Wire Wire Line
	4300 5300 5450 5300
Wire Wire Line
	4300 6000 4300 6700
Wire Wire Line
	4300 6000 5450 6000
Wire Wire Line
	4300 6700 5450 6700
Wire Wire Line
	5750 1550 6350 1550
Wire Wire Line
	5750 2250 6350 2250
Wire Wire Line
	5750 2950 6350 2950
Wire Wire Line
	5750 3650 6350 3650
Wire Wire Line
	5750 4350 6350 4350
Wire Wire Line
	5750 5050 6350 5050
Wire Wire Line
	5750 5750 6350 5750
Wire Wire Line
	5750 6450 6350 6450
Wire Wire Line
	7450 4750 8450 4750
Wire Wire Line
	7450 4850 8450 4850
Wire Wire Line
	7450 4950 8450 4950
Wire Wire Line
	7450 5050 8450 5050
Wire Wire Line
	7450 5150 8450 5150
Wire Wire Line
	7450 5250 8450 5250
Wire Wire Line
	7450 5350 8450 5350
Wire Wire Line
	7450 5450 8450 5450
Wire Bus Line
	6450 1650 6450 3050
Wire Bus Line
	6450 3050 6450 4000
Wire Bus Line
	6450 4000 6450 4950
Wire Bus Line
	6450 4000 7350 4000
Wire Bus Line
	6450 4950 6450 6350
Wire Bus Line
	7350 4000 7350 4750
Wire Bus Line
	7350 4000 7550 4000
Wire Bus Line
	7350 4750 7350 4950
Wire Bus Line
	7350 4950 7350 5050
Wire Bus Line
	7350 5050 7350 5250
Wire Bus Line
	7350 5250 7350 5350
Wire Bus Line
	8550 4850 8550 5050
Wire Bus Line
	8550 5050 8550 5150
Wire Bus Line
	8550 5150 8550 5350
Wire Bus Line
	8550 5350 8550 5550
Wire Bus Line
	8550 5550 8550 5650
Wire Bus Line
	8550 5650 8800 5650
Text Label 5800 1550 0    50   ~ 0
CPU-ADDR16
Text Label 5800 2250 0    50   ~ 0
CPU-ADDR17
Text Label 5800 2950 0    50   ~ 0
CPU-ADDR18
Text Label 5800 3650 0    50   ~ 0
CPU-ADDR19
Text Label 5800 4350 0    50   ~ 0
CPU-ADDR20
Text Label 5800 5050 0    50   ~ 0
CPU-ADDR21
Text Label 5800 5750 0    50   ~ 0
CPU-ADDR22
Text Label 5800 6450 0    50   ~ 0
CPU-ADDR23
Text Label 7500 4750 0    50   ~ 0
CPU-ADDR16
Text Label 7500 4850 0    50   ~ 0
CPU-ADDR17
Text Label 7500 4950 0    50   ~ 0
CPU-ADDR18
Text Label 7500 5050 0    50   ~ 0
CPU-ADDR19
Text Label 7500 5150 0    50   ~ 0
CPU-ADDR20
Text Label 7500 5250 0    50   ~ 0
CPU-ADDR21
Text Label 7500 5350 0    50   ~ 0
CPU-ADDR22
Text Label 7500 5450 0    50   ~ 0
CPU-ADDR23
Text Label 8400 4750 2    50   ~ 0
DF0
Text Label 8400 4850 2    50   ~ 0
DF1
Text Label 8400 4950 2    50   ~ 0
DF2
Text Label 8400 5050 2    50   ~ 0
DF3
Text Label 8400 5150 2    50   ~ 0
DF4
Text Label 8400 5250 2    50   ~ 0
DF5
Text Label 8400 5350 2    50   ~ 0
DF6
Text Label 8400 5450 2    50   ~ 0
DF7
Text HLabel 3700 4250 0    50   Input ~ 0
ENABLE
Text HLabel 5150 1550 0    50   Input ~ 0
ADDR16
Text HLabel 5150 2250 0    50   Input ~ 0
ADDR17
Text HLabel 5150 2950 0    50   Input ~ 0
ADDR18
Text HLabel 5150 3650 0    50   Input ~ 0
ADDR19
Text HLabel 5150 4350 0    50   Input ~ 0
ADDR20
Text HLabel 5150 5050 0    50   Input ~ 0
ADDR21
Text HLabel 5150 5750 0    50   Input ~ 0
ADDR22
Text HLabel 5150 6450 0    50   Input ~ 0
ADDR23
Text HLabel 7550 4000 2    50   Output ~ 0
CPU-ADDR[16..23]
Text HLabel 8800 5650 2    50   Output ~ 0
DF[0..7]
$Comp
L CG24-cells:V1L U12R30
U 1 1 5EAE9EAF
P 4000 4250
AR Path="/5EAE33D1/5EAE9EAF" Ref="U12R30"  Part="1" 
AR Path="/5E981F8B/5EAE9EAF" Ref="U12R30"  Part="1" 
F 0 "U12R30" H 4000 3933 50  0000 C CNN
F 1 "V1L" H 4000 4024 50  0000 C CNN
F 2 "" H 4000 4050 50  0001 C CNN
F 3 "" H 4000 4050 50  0001 C CNN
	1    4000 4250
	1    0    0    1   
$EndComp
$Comp
L CG24-cells:B12 U12R22
U 1 1 5EAE9E62
P 5450 1550
AR Path="/5EAE33D1/5EAE9E62" Ref="U12R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E62" Ref="U12R22"  Part="1" 
F 0 "U12R22" H 5450 1915 50  0000 C CNN
F 1 "B12" H 5450 1824 50  0000 C CNN
F 2 "" H 5450 1550 50  0001 C CNN
F 3 "" H 5450 1550 50  0001 C CNN
	1    5450 1550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U14R22
U 1 1 5EAE9E68
P 5450 2250
AR Path="/5EAE33D1/5EAE9E68" Ref="U14R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E68" Ref="U14R22"  Part="1" 
F 0 "U14R22" H 5450 2615 50  0000 C CNN
F 1 "B12" H 5450 2524 50  0000 C CNN
F 2 "" H 5450 2250 50  0001 C CNN
F 3 "" H 5450 2250 50  0001 C CNN
	1    5450 2250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U16R22
U 1 1 5EAE9E6E
P 5450 2950
AR Path="/5EAE33D1/5EAE9E6E" Ref="U16R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E6E" Ref="U16R22"  Part="1" 
F 0 "U16R22" H 5450 3315 50  0000 C CNN
F 1 "B12" H 5450 3224 50  0000 C CNN
F 2 "" H 5450 2950 50  0001 C CNN
F 3 "" H 5450 2950 50  0001 C CNN
	1    5450 2950
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U18R22
U 1 1 5EAE9E7A
P 5450 3650
AR Path="/5EAE33D1/5EAE9E7A" Ref="U18R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E7A" Ref="U18R22"  Part="1" 
F 0 "U18R22" H 5450 4015 50  0000 C CNN
F 1 "B12" H 5450 3924 50  0000 C CNN
F 2 "" H 5450 3650 50  0001 C CNN
F 3 "" H 5450 3650 50  0001 C CNN
	1    5450 3650
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U16R24
U 1 1 5EAE9E74
P 5450 4350
AR Path="/5EAE33D1/5EAE9E74" Ref="U16R24"  Part="1" 
AR Path="/5E981F8B/5EAE9E74" Ref="U16R24"  Part="1" 
F 0 "U16R24" H 5450 4715 50  0000 C CNN
F 1 "B12" H 5450 4624 50  0000 C CNN
F 2 "" H 5450 4350 50  0001 C CNN
F 3 "" H 5450 4350 50  0001 C CNN
	1    5450 4350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U20R22
U 1 1 5EAE9E86
P 5450 5050
AR Path="/5EAE33D1/5EAE9E86" Ref="U20R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E86" Ref="U20R22"  Part="1" 
F 0 "U20R22" H 5450 5415 50  0000 C CNN
F 1 "B12" H 5450 5324 50  0000 C CNN
F 2 "" H 5450 5050 50  0001 C CNN
F 3 "" H 5450 5050 50  0001 C CNN
	1    5450 5050
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U18R24
U 1 1 5EAE9E80
P 5450 5750
AR Path="/5EAE33D1/5EAE9E80" Ref="U18R24"  Part="1" 
AR Path="/5E981F8B/5EAE9E80" Ref="U18R24"  Part="1" 
F 0 "U18R24" H 5450 6115 50  0000 C CNN
F 1 "B12" H 5450 6024 50  0000 C CNN
F 2 "" H 5450 5750 50  0001 C CNN
F 3 "" H 5450 5750 50  0001 C CNN
	1    5450 5750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:B12 U24R22
U 1 1 5EAE9E8C
P 5450 6450
AR Path="/5EAE33D1/5EAE9E8C" Ref="U24R22"  Part="1" 
AR Path="/5E981F8B/5EAE9E8C" Ref="U24R22"  Part="1" 
F 0 "U24R22" H 5450 6815 50  0000 C CNN
F 1 "B12" H 5450 6724 50  0000 C CNN
F 2 "" H 5450 6450 50  0001 C CNN
F 3 "" H 5450 6450 50  0001 C CNN
	1    5450 6450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
