EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 20
Title "DL-1727"
Date "2020-04-26"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1727"
$EndDescr
Connection ~ 3200 2250
Connection ~ 6850 2250
Wire Wire Line
	2500 1450 3550 1450
Wire Wire Line
	3200 1800 3550 1800
Wire Wire Line
	3200 2250 3200 1800
Wire Wire Line
	6850 1800 7200 1800
Wire Wire Line
	6850 2250 6850 1800
Wire Wire Line
	3550 1350 3200 1350
Wire Wire Line
	3550 1900 2500 1900
Wire Wire Line
	3550 2250 3200 2250
Wire Wire Line
	3550 2350 2500 2350
Wire Wire Line
	3550 2700 3200 2700
Wire Wire Line
	3550 2800 2500 2800
Wire Wire Line
	3550 3150 3200 3150
Wire Wire Line
	3550 3250 2500 3250
Wire Wire Line
	3550 3600 3200 3600
Wire Wire Line
	3550 3700 2500 3700
Wire Wire Line
	3550 4050 3200 4050
Wire Wire Line
	3550 4150 2500 4150
Wire Wire Line
	3550 4500 3200 4500
Wire Wire Line
	3550 4600 2500 4600
Wire Wire Line
	3550 4950 3200 4950
Wire Wire Line
	3550 5050 2500 5050
Wire Wire Line
	3550 5400 3200 5400
Wire Wire Line
	3550 5500 2500 5500
Wire Wire Line
	3550 5850 3200 5850
Wire Wire Line
	3550 5950 2500 5950
Wire Wire Line
	3550 6300 3200 6300
Wire Wire Line
	3550 6400 2500 6400
Wire Wire Line
	7200 1350 6850 1350
Wire Wire Line
	7200 1450 6150 1450
Wire Wire Line
	7200 1900 6150 1900
Wire Wire Line
	7200 2250 6850 2250
Wire Wire Line
	7200 2350 6150 2350
Wire Wire Line
	7200 2700 6850 2700
Wire Wire Line
	7200 2800 6150 2800
Wire Wire Line
	7200 3150 6850 3150
Wire Wire Line
	7200 3250 6150 3250
Wire Wire Line
	7200 3600 6850 3600
Wire Wire Line
	7200 3700 6150 3700
Wire Wire Line
	7200 4050 6850 4050
Wire Wire Line
	7200 4150 6150 4150
Wire Wire Line
	7200 4500 6850 4500
Wire Wire Line
	7200 4600 6150 4600
Wire Wire Line
	7200 4950 6850 4950
Wire Wire Line
	7200 5050 6150 5050
Wire Wire Line
	7200 5400 6850 5400
Wire Wire Line
	7200 5500 6150 5500
Wire Wire Line
	7200 5850 6850 5850
Wire Wire Line
	7200 5950 6150 5950
Wire Wire Line
	4100 1400 4800 1400
Wire Wire Line
	4100 2300 4800 2300
Wire Wire Line
	4100 2750 4800 2750
Wire Wire Line
	4100 3200 4800 3200
Wire Wire Line
	4100 3650 4800 3650
Wire Wire Line
	4100 4100 4800 4100
Wire Wire Line
	4100 4550 4800 4550
Wire Wire Line
	4100 5000 4800 5000
Wire Wire Line
	4100 5450 4800 5450
Wire Wire Line
	4100 5900 4800 5900
Wire Wire Line
	4100 6350 4800 6350
Wire Wire Line
	7750 1400 8450 1400
Wire Wire Line
	7750 1850 8450 1850
Wire Wire Line
	7750 2300 8450 2300
Wire Wire Line
	7750 2750 8450 2750
Wire Wire Line
	7750 3200 8450 3200
Wire Wire Line
	7750 3650 8450 3650
Wire Wire Line
	7750 4100 8450 4100
Wire Wire Line
	7750 4550 8450 4550
Wire Wire Line
	7750 5000 8450 5000
Wire Wire Line
	7750 5450 8450 5450
Wire Wire Line
	7750 5900 8450 5900
Wire Wire Line
	4800 1850 4100 1850
$Comp
L CG24-cells:R2P U6R173
U 1 1 5EDF826F
P 3800 1400
F 0 "U6R173" H 3825 1667 50  0000 C CNN
F 1 "R2P" H 3825 1576 50  0000 C CNN
F 2 "" H 3850 1400 50  0001 C CNN
F 3 "" H 3850 1400 50  0001 C CNN
	1    3800 1400
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U8R163
U 1 1 5EE02FBE
P 3800 1850
F 0 "U8R163" H 3825 2117 50  0000 C CNN
F 1 "R2P" H 3825 2026 50  0000 C CNN
F 2 "" H 3850 1850 50  0001 C CNN
F 3 "" H 3850 1850 50  0001 C CNN
	1    3800 1850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U10R163
U 1 1 5E9D28F4
P 3800 2300
F 0 "U10R163" H 3825 2567 50  0000 C CNN
F 1 "R2P" H 3825 2476 50  0000 C CNN
F 2 "" H 3850 2300 50  0001 C CNN
F 3 "" H 3850 2300 50  0001 C CNN
	1    3800 2300
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U12R173
U 1 1 5E9E50E3
P 3800 2750
F 0 "U12R173" H 3825 3017 50  0000 C CNN
F 1 "R2P" H 3825 2926 50  0000 C CNN
F 2 "" H 3850 2750 50  0001 C CNN
F 3 "" H 3850 2750 50  0001 C CNN
	1    3800 2750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U16R173
U 1 1 5EA22E96
P 3800 3200
F 0 "U16R173" H 3825 3467 50  0000 C CNN
F 1 "R2P" H 3825 3376 50  0000 C CNN
F 2 "" H 3850 3200 50  0001 C CNN
F 3 "" H 3850 3200 50  0001 C CNN
	1    3800 3200
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U8R173
U 1 1 5EA34DFC
P 3800 3650
F 0 "U8R173" H 3825 3917 50  0000 C CNN
F 1 "R2P" H 3825 3826 50  0000 C CNN
F 2 "" H 3850 3650 50  0001 C CNN
F 3 "" H 3850 3650 50  0001 C CNN
	1    3800 3650
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U8R178
U 1 1 5EAADE01
P 3800 4100
F 0 "U8R178" H 3825 4367 50  0000 C CNN
F 1 "R2P" H 3825 4276 50  0000 C CNN
F 2 "" H 3850 4100 50  0001 C CNN
F 3 "" H 3850 4100 50  0001 C CNN
	1    3800 4100
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U10R173
U 1 1 5EAC1F44
P 3800 4550
F 0 "U10R173" H 3825 4817 50  0000 C CNN
F 1 "R2P" H 3825 4726 50  0000 C CNN
F 2 "" H 3850 4550 50  0001 C CNN
F 3 "" H 3850 4550 50  0001 C CNN
	1    3800 4550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U10R180
U 1 1 5EAA40AB
P 3800 5000
F 0 "U10R180" H 3825 5267 50  0000 C CNN
F 1 "R2P" H 3825 5176 50  0000 C CNN
F 2 "" H 3850 5000 50  0001 C CNN
F 3 "" H 3850 5000 50  0001 C CNN
	1    3800 5000
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U11R180
U 1 1 5EAD6B89
P 3800 5450
F 0 "U11R180" H 3825 5717 50  0000 C CNN
F 1 "R2P" H 3825 5626 50  0000 C CNN
F 2 "" H 3850 5450 50  0001 C CNN
F 3 "" H 3850 5450 50  0001 C CNN
	1    3800 5450
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U28R178
U 1 1 5EA9AC57
P 3800 5900
F 0 "U28R178" H 3825 6167 50  0000 C CNN
F 1 "R2P" H 3825 6076 50  0000 C CNN
F 2 "" H 3850 5900 50  0001 C CNN
F 3 "" H 3850 5900 50  0001 C CNN
	1    3800 5900
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U36R179
U 1 1 5EAEC8AC
P 3800 6350
F 0 "U36R179" H 3825 6617 50  0000 C CNN
F 1 "R2P" H 3825 6526 50  0000 C CNN
F 2 "" H 3850 6350 50  0001 C CNN
F 3 "" H 3850 6350 50  0001 C CNN
	1    3800 6350
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U38R179
U 1 1 5EA9206E
P 7450 1400
F 0 "U38R179" H 7475 1667 50  0000 C CNN
F 1 "R2P" H 7475 1576 50  0000 C CNN
F 2 "" H 7500 1400 50  0001 C CNN
F 3 "" H 7500 1400 50  0001 C CNN
	1    7450 1400
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U36R173
U 1 1 5EB035EB
P 7450 1850
F 0 "U36R173" H 7475 2117 50  0000 C CNN
F 1 "R2P" H 7475 2026 50  0000 C CNN
F 2 "" H 7500 1850 50  0001 C CNN
F 3 "" H 7500 1850 50  0001 C CNN
	1    7450 1850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U38R173
U 1 1 5EA89A58
P 7450 2300
F 0 "U38R173" H 7475 2567 50  0000 C CNN
F 1 "R2P" H 7475 2476 50  0000 C CNN
F 2 "" H 7500 2300 50  0001 C CNN
F 3 "" H 7500 2300 50  0001 C CNN
	1    7450 2300
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U38R153
U 1 1 5EB1B1C1
P 7450 2750
F 0 "U38R153" H 7475 3017 50  0000 C CNN
F 1 "R2P" H 7475 2926 50  0000 C CNN
F 2 "" H 7500 2750 50  0001 C CNN
F 3 "" H 7500 2750 50  0001 C CNN
	1    7450 2750
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U40R153
U 1 1 5EA62DC4
P 7450 3200
F 0 "U40R153" H 7475 3467 50  0000 C CNN
F 1 "R2P" H 7475 3376 50  0000 C CNN
F 2 "" H 7500 3200 50  0001 C CNN
F 3 "" H 7500 3200 50  0001 C CNN
	1    7450 3200
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U38R101
U 1 1 5EA797E6
P 7450 3650
F 0 "U38R101" H 7475 3917 50  0000 C CNN
F 1 "R2P" H 7475 3826 50  0000 C CNN
F 2 "" H 7500 3650 50  0001 C CNN
F 3 "" H 7500 3650 50  0001 C CNN
	1    7450 3650
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U40R101
U 1 1 5EA5BE7E
P 7450 4100
F 0 "U40R101" H 7475 4367 50  0000 C CNN
F 1 "R2P" H 7475 4276 50  0000 C CNN
F 2 "" H 7500 4100 50  0001 C CNN
F 3 "" H 7500 4100 50  0001 C CNN
	1    7450 4100
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U38R91
U 1 1 5EA6A0D2
P 7450 4550
F 0 "U38R91" H 7475 4817 50  0000 C CNN
F 1 "R2P" H 7475 4726 50  0000 C CNN
F 2 "" H 7500 4550 50  0001 C CNN
F 3 "" H 7500 4550 50  0001 C CNN
	1    7450 4550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U40R91
U 1 1 5EA47E78
P 7450 5000
F 0 "U40R91" H 7475 5267 50  0000 C CNN
F 1 "R2P" H 7475 5176 50  0000 C CNN
F 2 "" H 7500 5000 50  0001 C CNN
F 3 "" H 7500 5000 50  0001 C CNN
	1    7450 5000
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U42R101
U 1 1 5EA4E27A
P 7450 5450
F 0 "U42R101" H 7475 5717 50  0000 C CNN
F 1 "R2P" H 7475 5626 50  0000 C CNN
F 2 "" H 7500 5450 50  0001 C CNN
F 3 "" H 7500 5450 50  0001 C CNN
	1    7450 5450
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:R2P U43R101
U 1 1 5EA41CA6
P 7450 5900
F 0 "U43R101" H 7475 6167 50  0000 C CNN
F 1 "R2P" H 7475 6076 50  0000 C CNN
F 2 "" H 7500 5900 50  0001 C CNN
F 3 "" H 7500 5900 50  0001 C CNN
	1    7450 5900
	1    0    0    -1  
$EndComp
Connection ~ 3200 1800
Wire Wire Line
	3200 1350 3200 1800
Connection ~ 3200 2700
Wire Wire Line
	3200 2700 3200 2250
Wire Wire Line
	3200 2700 3200 3150
Connection ~ 3200 3150
Wire Wire Line
	3200 3150 3200 3600
Connection ~ 3200 3600
Wire Wire Line
	3200 3600 3200 4050
Connection ~ 3200 4050
Wire Wire Line
	3200 4050 3200 4500
Connection ~ 3200 4500
Wire Wire Line
	3200 4500 3200 4950
Connection ~ 3200 4950
Wire Wire Line
	3200 4950 3200 5400
Connection ~ 3200 5400
Wire Wire Line
	3200 5400 3200 5850
Connection ~ 3200 5850
Wire Wire Line
	3200 5850 3200 6300
Wire Wire Line
	3200 1350 3200 850 
Wire Wire Line
	3200 850  2950 850 
Connection ~ 3200 1350
Connection ~ 6850 1350
Connection ~ 6850 1800
Wire Wire Line
	6850 850  6850 1350
Wire Wire Line
	6850 1350 6850 1800
Connection ~ 6850 2700
Wire Wire Line
	6850 2700 6850 2250
Wire Wire Line
	6850 2700 6850 3150
Connection ~ 6850 3150
Wire Wire Line
	6850 3150 6850 3600
Connection ~ 6850 3600
Wire Wire Line
	6850 3600 6850 4050
Connection ~ 6850 4050
Wire Wire Line
	6850 4050 6850 4500
Connection ~ 6850 4500
Wire Wire Line
	6850 4500 6850 4950
Connection ~ 6850 4950
Wire Wire Line
	6850 4950 6850 5400
Connection ~ 6850 5400
Wire Wire Line
	6850 5400 6850 5850
Text Label 6250 5950 0    50   ~ 0
IN1
Text Label 6250 5500 0    50   ~ 0
IN2
Text Label 6250 5050 0    50   ~ 0
IN3
Text Label 6250 4600 0    50   ~ 0
IN4
Text Label 6250 4150 0    50   ~ 0
IN5
Text Label 6250 3700 0    50   ~ 0
IN6
Text Label 6250 3250 0    50   ~ 0
IN7
Text Label 6250 2800 0    50   ~ 0
IN8
Text Label 6250 2350 0    50   ~ 0
IN9
Text Label 6250 1900 0    50   ~ 0
IN10
Text Label 6250 1450 0    50   ~ 0
IN11
Text Label 2600 6400 0    50   ~ 0
IN12
Text Label 2600 5950 0    50   ~ 0
IN13
Text Label 2600 5500 0    50   ~ 0
IN14
Text Label 2600 5050 0    50   ~ 0
IN15
Text Label 2600 4600 0    50   ~ 0
IN16
Text Label 2600 4150 0    50   ~ 0
IN17
Text Label 2600 3700 0    50   ~ 0
IN18
Text Label 2600 3250 0    50   ~ 0
IN19
Text Label 2600 2800 0    50   ~ 0
IN20
Text Label 2600 2350 0    50   ~ 0
IN21
Text Label 2600 1900 0    50   ~ 0
IN22
Text Label 2600 1450 0    50   ~ 0
IN23
Entry Wire Line
	2400 1350 2500 1450
Entry Wire Line
	2400 1800 2500 1900
Entry Wire Line
	2400 2250 2500 2350
Entry Wire Line
	2400 2700 2500 2800
Entry Wire Line
	2400 3150 2500 3250
Entry Wire Line
	2400 3600 2500 3700
Entry Wire Line
	2400 4050 2500 4150
Entry Wire Line
	2400 4500 2500 4600
Entry Wire Line
	2400 4950 2500 5050
Entry Wire Line
	2400 5400 2500 5500
Entry Wire Line
	2400 5850 2500 5950
Entry Wire Line
	2400 6300 2500 6400
Entry Wire Line
	6050 1350 6150 1450
Entry Wire Line
	6050 1800 6150 1900
Entry Wire Line
	6050 2250 6150 2350
Entry Wire Line
	6050 2700 6150 2800
Entry Wire Line
	6050 3150 6150 3250
Entry Wire Line
	6050 3600 6150 3700
Entry Wire Line
	6050 4050 6150 4150
Entry Wire Line
	6050 4500 6150 4600
Entry Wire Line
	6050 4950 6150 5050
Entry Wire Line
	6050 5400 6150 5500
Entry Wire Line
	6050 5850 6150 5950
Entry Wire Line
	4900 6000 4800 5900
Entry Wire Line
	4900 5550 4800 5450
Entry Wire Line
	4900 5100 4800 5000
Entry Wire Line
	4900 4650 4800 4550
Entry Wire Line
	4900 4200 4800 4100
Entry Wire Line
	4900 3750 4800 3650
Entry Wire Line
	4900 3300 4800 3200
Entry Wire Line
	4900 2850 4800 2750
Entry Wire Line
	4900 2400 4800 2300
Entry Wire Line
	4900 1950 4800 1850
Entry Wire Line
	4900 1500 4800 1400
Entry Wire Line
	4900 6250 4800 6350
Entry Wire Line
	8550 6000 8450 5900
Entry Wire Line
	8550 5550 8450 5450
Entry Wire Line
	8550 5100 8450 5000
Entry Wire Line
	8550 4650 8450 4550
Entry Wire Line
	8550 4200 8450 4100
Entry Wire Line
	8550 3750 8450 3650
Entry Wire Line
	8550 3300 8450 3200
Entry Wire Line
	8550 2850 8450 2750
Entry Wire Line
	8550 2400 8450 2300
Entry Wire Line
	8550 1950 8450 1850
Entry Wire Line
	8550 1500 8450 1400
Text Label 8300 5900 2    50   ~ 0
OUT1
Text Label 8300 5450 2    50   ~ 0
OUT2
Text Label 8300 5000 2    50   ~ 0
OUT3
Text Label 8300 4550 2    50   ~ 0
OUT4
Text Label 8300 4100 2    50   ~ 0
OUT5
Text Label 8300 3650 2    50   ~ 0
OUT6
Text Label 8300 3200 2    50   ~ 0
OUT7
Text Label 8300 2750 2    50   ~ 0
OUT8
Text Label 8300 2300 2    50   ~ 0
OUT9
Text Label 8300 1850 2    50   ~ 0
OUT10
Text Label 8300 1400 2    50   ~ 0
OUT11
Text Label 4650 6350 2    50   ~ 0
OUT12
Text Label 4650 5900 2    50   ~ 0
OUT13
Text Label 4650 5450 2    50   ~ 0
OUT14
Text Label 4650 5000 2    50   ~ 0
OUT15
Text Label 4650 4550 2    50   ~ 0
OUT16
Text Label 4650 4100 2    50   ~ 0
OUT17
Text Label 4650 3650 2    50   ~ 0
OUT18
Text Label 4650 3200 2    50   ~ 0
OUT19
Text Label 4650 2750 2    50   ~ 0
OUT20
Text Label 4650 2300 2    50   ~ 0
OUT21
Text Label 4650 1850 2    50   ~ 0
OUT22
Text Label 4650 1400 2    50   ~ 0
OUT23
Wire Bus Line
	6050 1000 2400 1000
Wire Bus Line
	4900 6250 8550 6250
Connection ~ 8550 6250
Wire Bus Line
	8550 6250 9050 6250
Text HLabel 9050 6250 2    50   Output ~ 0
OUT[1..23]
Text HLabel 1850 1000 0    50   Input ~ 0
IN[1..23]
Wire Bus Line
	2400 1000 1850 1000
Connection ~ 2400 1000
Text HLabel 2950 850  0    50   Input ~ 0
~ENABLE-HI
Wire Wire Line
	6850 850  6600 850 
Text HLabel 6600 850  0    50   Input ~ 0
~ENABLE-LO
Wire Bus Line
	6050 1000 6050 5850
Wire Bus Line
	2400 1000 2400 6300
Wire Bus Line
	4900 1500 4900 6250
Wire Bus Line
	8550 1500 8550 6250
$EndSCHEMATC
