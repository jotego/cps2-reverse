EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 20
Title "DL-1727"
Date "2020-04-26"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1727"
$EndDescr
Connection ~ 6100 4000
Connection ~ 6100 3450
Connection ~ 6200 5700
Connection ~ 6200 4950
Connection ~ 6100 5500
Connection ~ 6100 4750
Wire Wire Line
	6100 2700 6100 3450
Wire Wire Line
	6100 2700 6350 2700
Wire Wire Line
	6100 3450 6100 4000
Wire Wire Line
	6100 3450 6350 3450
Wire Wire Line
	6100 4000 6100 4750
Wire Wire Line
	6100 4750 6100 5500
Wire Wire Line
	6100 4750 6350 4750
Wire Wire Line
	6100 5500 6100 5700
Wire Wire Line
	6100 5500 6350 5500
Wire Wire Line
	6200 2500 6200 3250
Wire Wire Line
	6200 3250 6350 3250
Wire Wire Line
	6200 4200 6350 4200
Wire Wire Line
	6200 4950 6200 4200
Wire Wire Line
	6200 4950 6350 4950
Wire Wire Line
	6200 5700 6200 4950
Wire Wire Line
	6200 5700 6350 5700
Wire Wire Line
	6350 2500 6200 2500
Wire Wire Line
	6350 4000 6100 4000
$Comp
L CG24-cells:V1N U4R113
U 1 1 67685D4D
P 4800 3700
F 0 "U4R113" H 4800 4017 50  0000 C CNN
F 1 "V1N" H 4800 3926 50  0000 C CNN
F 2 "" H 4800 3500 50  0001 C CNN
F 3 "" H 4800 3500 50  0001 C CNN
	1    4800 3700
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U32R164
U 1 1 62243633
P 4800 6250
F 0 "U32R164" H 4800 6567 50  0000 C CNN
F 1 "V1N" H 4800 6476 50  0000 C CNN
F 2 "" H 4800 6050 50  0001 C CNN
F 3 "" H 4800 6050 50  0001 C CNN
	1    4800 6250
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U9R102
U 1 1 6025C2FF
P 7450 3300
F 0 "U9R102" H 7450 3617 50  0000 C CNN
F 1 "V1N" H 7450 3526 50  0000 C CNN
F 2 "" H 7450 3100 50  0001 C CNN
F 3 "" H 7450 3100 50  0001 C CNN
	1    7450 3300
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U6R102
U 1 1 640D1F20
P 7450 2550
F 0 "U6R102" H 7450 2867 50  0000 C CNN
F 1 "V1N" H 7450 2776 50  0000 C CNN
F 2 "" H 7450 2350 50  0001 C CNN
F 3 "" H 7450 2350 50  0001 C CNN
	1    7450 2550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U24R61
U 1 1 61F17D56
P 7450 4050
F 0 "U24R61" H 7450 4367 50  0000 C CNN
F 1 "V1N" H 7450 4276 50  0000 C CNN
F 2 "" H 7450 3850 50  0001 C CNN
F 3 "" H 7450 3850 50  0001 C CNN
	1    7450 4050
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U39R61
U 1 1 61E74854
P 7450 4800
F 0 "U39R61" H 7450 5117 50  0000 C CNN
F 1 "V1N" H 7450 5026 50  0000 C CNN
F 2 "" H 7450 4600 50  0001 C CNN
F 3 "" H 7450 4600 50  0001 C CNN
	1    7450 4800
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:V1N U33R44
U 1 1 61FB78F5
P 7450 5550
F 0 "U33R44" H 7450 5867 50  0000 C CNN
F 1 "V1N" H 7450 5776 50  0000 C CNN
F 2 "" H 7450 5350 50  0001 C CNN
F 3 "" H 7450 5350 50  0001 C CNN
	1    7450 5550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:D24 U6R112
U 1 1 675DB32B
P 6750 3300
F 0 "U6R112" H 6750 3667 50  0000 C CNN
F 1 "D24" H 6750 3576 50  0000 C CNN
F 2 "" H 6850 3300 50  0001 C CNN
F 3 "" H 6850 3300 50  0001 C CNN
	1    6750 3300
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:D24 U4R122
U 1 1 67587941
P 6750 2550
F 0 "U4R122" H 6750 2917 50  0000 C CNN
F 1 "D24" H 6750 2826 50  0000 C CNN
F 2 "" H 6850 2550 50  0001 C CNN
F 3 "" H 6850 2550 50  0001 C CNN
	1    6750 2550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:D24 U32R71
U 1 1 614819C6
P 6750 4050
F 0 "U32R71" H 6750 4417 50  0000 C CNN
F 1 "D24" H 6750 4326 50  0000 C CNN
F 2 "" H 6850 4050 50  0001 C CNN
F 3 "" H 6850 4050 50  0001 C CNN
	1    6750 4050
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:D24 U38R81
U 1 1 61482208
P 6750 4800
F 0 "U38R81" H 6750 5167 50  0000 C CNN
F 1 "D24" H 6750 5076 50  0000 C CNN
F 2 "" H 6850 4800 50  0001 C CNN
F 3 "" H 6850 4800 50  0001 C CNN
	1    6750 4800
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:D24 U34R71
U 1 1 614828C8
P 6750 5550
F 0 "U34R71" H 6750 5917 50  0000 C CNN
F 1 "D24" H 6750 5826 50  0000 C CNN
F 2 "" H 6850 5550 50  0001 C CNN
F 3 "" H 6850 5550 50  0001 C CNN
	1    6750 5550
	1    0    0    -1  
$EndComp
Text Notes 7100 2450 0    50   ~ 0
0
Text Notes 7100 3250 0    50   ~ 0
1
Text Notes 7100 4000 0    50   ~ 0
2
Text Notes 7100 4750 0    50   ~ 0
3
Text Notes 7100 5500 0    50   ~ 0
4
Connection ~ 4400 5700
Wire Wire Line
	4400 5700 4400 6250
Wire Wire Line
	6200 5700 6200 6250
Connection ~ 6200 3250
Entry Wire Line
	5200 2300 5300 2400
Entry Wire Line
	5200 3050 5300 3150
Entry Wire Line
	5200 3800 5300 3900
Entry Wire Line
	5200 4550 5300 4650
Entry Wire Line
	5200 5300 5300 5400
Wire Wire Line
	6200 3700 6200 3250
Wire Wire Line
	4400 3700 4400 5700
Wire Bus Line
	5200 2050 4850 2050
Text Label 5750 2600 0    50   ~ 0
A0
Text Label 5750 3350 0    50   ~ 0
A1
Text Label 5750 4100 0    50   ~ 0
A2
Text Label 5750 4850 0    50   ~ 0
A3
Text Label 5750 5600 0    50   ~ 0
A4
Text HLabel 4850 2050 0    50   Input ~ 0
B[0..4]
Entry Wire Line
	5600 5500 5700 5600
Wire Wire Line
	5300 2400 6350 2400
Wire Wire Line
	5300 3150 6350 3150
Wire Wire Line
	5300 3900 6350 3900
Wire Wire Line
	5300 4650 6350 4650
Wire Wire Line
	5300 5400 6350 5400
Wire Wire Line
	5700 5600 6350 5600
Text Label 5350 5400 0    50   ~ 0
B4
Entry Wire Line
	5600 4750 5700 4850
Wire Wire Line
	5700 4850 6350 4850
Text Label 5350 4650 0    50   ~ 0
B3
Entry Wire Line
	5600 4000 5700 4100
Wire Wire Line
	5700 4100 6350 4100
Text Label 5350 3900 0    50   ~ 0
B2
Entry Wire Line
	5600 3250 5700 3350
Wire Wire Line
	5700 3350 6350 3350
Text Label 5350 3150 0    50   ~ 0
B1
Entry Wire Line
	5600 2500 5700 2600
Wire Wire Line
	5700 2600 6350 2600
Text Label 5350 2400 0    50   ~ 0
B0
Wire Bus Line
	5600 1800 4850 1800
Text HLabel 4850 1800 0    50   Input ~ 0
A[0..4]
Wire Wire Line
	5100 3700 6200 3700
Wire Wire Line
	4400 6250 4000 6250
Wire Wire Line
	4400 5700 6100 5700
Wire Wire Line
	4500 3700 4400 3700
Wire Wire Line
	4500 6250 4400 6250
Connection ~ 4400 6250
Wire Wire Line
	5100 6250 6200 6250
Text HLabel 4000 6250 0    50   Input ~ 0
~A~B
Wire Wire Line
	7750 2550 8100 2550
Text Label 7850 2550 0    50   ~ 0
Q0
Text Label 7850 3300 0    50   ~ 0
Q1
Text Label 7850 4050 0    50   ~ 0
Q2
Text Label 7850 4800 0    50   ~ 0
Q3
Text Label 7850 5550 0    50   ~ 0
Q4
Wire Wire Line
	7750 3300 8100 3300
Wire Wire Line
	7750 4050 8100 4050
Wire Wire Line
	7750 4800 8100 4800
Wire Wire Line
	7750 5550 8100 5550
Entry Wire Line
	8100 2550 8200 2650
Entry Wire Line
	8100 3300 8200 3400
Entry Wire Line
	8100 4050 8200 4150
Entry Wire Line
	8100 4800 8200 4900
Entry Wire Line
	8100 5550 8200 5650
Wire Bus Line
	8200 5900 8600 5900
Text HLabel 8600 5900 2    50   Output ~ 0
Q[0..4]
Wire Bus Line
	5200 2050 5200 5300
Wire Bus Line
	8200 2650 8200 5900
Wire Bus Line
	5600 1800 5600 5500
$EndSCHEMATC
