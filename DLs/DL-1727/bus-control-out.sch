EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 20
Title "DL-1727"
Date "2020-04-26"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: DL-1727"
$EndDescr
Connection ~ 4450 6900
Wire Wire Line
	4250 6150 4600 6150
Wire Wire Line
	4250 6800 4600 6800
Wire Wire Line
	4250 7500 4600 7500
Wire Wire Line
	4450 6250 4450 6900
Wire Wire Line
	4450 6250 4600 6250
Wire Wire Line
	4450 6900 4600 6900
Wire Wire Line
	4450 7600 4450 6900
Wire Wire Line
	4600 7600 4450 7600
Wire Wire Line
	5500 3800 6650 3800
Wire Wire Line
	5500 3150 6650 3150
Wire Wire Line
	5500 1850 6650 1850
Wire Wire Line
	5500 1200 6650 1200
Wire Wire Line
	5500 5100 6650 5100
Wire Wire Line
	5500 5750 6650 5750
Wire Wire Line
	5500 4450 6650 4450
Wire Wire Line
	6650 5750 7150 5750
$Comp
L CG24-cells:N2P U14R178
U 1 1 6F59FF44
P 4850 6200
F 0 "U14R178" H 4875 6467 50  0000 C CNN
F 1 "N2P" H 4875 6376 50  0000 C CNN
F 2 "" H 4850 6200 50  0001 C CNN
F 3 "" H 4850 6200 50  0001 C CNN
	1    4850 6200
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:N2P U12R179
U 1 1 6F6CC475
P 4850 7550
F 0 "U12R179" H 4875 7817 50  0000 C CNN
F 1 "N2P" H 4875 7726 50  0000 C CNN
F 2 "" H 4850 7550 50  0001 C CNN
F 3 "" H 4850 7550 50  0001 C CNN
	1    4850 7550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:N2P U18R177
U 1 1 6BD3C551
P 4850 6850
F 0 "U18R177" H 4875 7117 50  0000 C CNN
F 1 "N2P" H 4875 7026 50  0000 C CNN
F 2 "" H 4850 6850 50  0001 C CNN
F 3 "" H 4850 6850 50  0001 C CNN
	1    4850 6850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O1B U30R21
U 1 1 5E9C5966
P 5450 6200
F 0 "U30R21" H 5450 6517 50  0000 C CNN
F 1 "O1B" H 5450 6426 50  0000 C CNN
F 2 "" H 5450 6200 50  0001 C CNN
F 3 "" H 5450 6200 50  0001 C CNN
	1    5450 6200
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O1B U8R21
U 1 1 5E9C5967
P 5450 7550
F 0 "U8R21" H 5450 7867 50  0000 C CNN
F 1 "O1B" H 5450 7776 50  0000 C CNN
F 2 "" H 5450 7550 50  0001 C CNN
F 3 "" H 5450 7550 50  0001 C CNN
	1    5450 7550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O1B U26R21
U 1 1 5E9C5968
P 5450 6850
F 0 "U26R21" H 5450 7167 50  0000 C CNN
F 1 "O1B" H 5450 7076 50  0000 C CNN
F 2 "" H 5450 6850 50  0001 C CNN
F 3 "" H 5450 6850 50  0001 C CNN
	1    5450 6850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:K1B U10R122
U 1 1 6A58D9D8
P 7450 5750
F 0 "U10R122" H 7450 6067 50  0000 C CNN
F 1 "K1B" H 7450 5976 50  0000 C CNN
F 2 "" H 7450 5750 50  0001 C CNN
F 3 "" H 7450 5750 50  0001 C CNN
	1    7450 5750
	-1   0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U32R181
U 1 1 5E9C5969
P 5500 3550
F 0 "U32R181" H 5500 3867 50  0000 C CNN
F 1 "O4R" H 5500 3776 50  0000 C CNN
F 2 "" H 5500 3550 50  0001 C CNN
F 3 "" H 5500 3550 50  0001 C CNN
	1    5500 3550
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U30R181
U 1 1 5E9C5993
P 5500 2900
F 0 "U30R181" H 5500 3217 50  0000 C CNN
F 1 "O4R" H 5500 3126 50  0000 C CNN
F 2 "" H 5500 2900 50  0001 C CNN
F 3 "" H 5500 2900 50  0001 C CNN
	1    5500 2900
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U6R181
U 1 1 5E9C596E
P 5500 1600
F 0 "U6R181" H 5500 1917 50  0000 C CNN
F 1 "O4R" H 5500 1826 50  0000 C CNN
F 2 "" H 5500 1600 50  0001 C CNN
F 3 "" H 5500 1600 50  0001 C CNN
	1    5500 1600
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U4R179
U 1 1 5E9C599D
P 5500 950
F 0 "U4R179" H 5500 1267 50  0000 C CNN
F 1 "O4R" H 5500 1176 50  0000 C CNN
F 2 "" H 5500 950 50  0001 C CNN
F 3 "" H 5500 950 50  0001 C CNN
	1    5500 950 
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U12R23
U 1 1 5E9C5975
P 5500 5500
F 0 "U12R23" H 5500 5817 50  0000 C CNN
F 1 "O4R" H 5500 5726 50  0000 C CNN
F 2 "" H 5500 5500 50  0001 C CNN
F 3 "" H 5500 5500 50  0001 C CNN
	1    5500 5500
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U24R21
U 1 1 5E9C59A2
P 5500 4850
F 0 "U24R21" H 5500 5167 50  0000 C CNN
F 1 "O4R" H 5500 5076 50  0000 C CNN
F 2 "" H 5500 4850 50  0001 C CNN
F 3 "" H 5500 4850 50  0001 C CNN
	1    5500 4850
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U28R21
U 1 1 5E9C59A6
P 5500 4200
F 0 "U28R21" H 5500 4517 50  0000 C CNN
F 1 "O4R" H 5500 4426 50  0000 C CNN
F 2 "" H 5500 4200 50  0001 C CNN
F 3 "" H 5500 4200 50  0001 C CNN
	1    5500 4200
	1    0    0    -1  
$EndComp
$Comp
L CG24-cells:O4R U6R?
U 1 1 603443E3
P 5500 2250
AR Path="/5EBD13AD/603443E3" Ref="U6R?"  Part="1" 
AR Path="/5EDE5EB8/603443E3" Ref="U6R179"  Part="1" 
F 0 "U6R179" H 5500 2567 50  0000 C CNN
F 1 "O4R" H 5500 2476 50  0000 C CNN
F 2 "" H 5500 2250 50  0001 C CNN
F 3 "" H 5500 2250 50  0001 C CNN
	1    5500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2500 6650 2500
Wire Wire Line
	6650 1200 6650 1850
Connection ~ 6650 1850
Wire Wire Line
	6650 1850 6650 2500
Connection ~ 6650 2500
Wire Wire Line
	6650 2500 6650 3150
Connection ~ 6650 3150
Wire Wire Line
	6650 3150 6650 3800
Connection ~ 6650 3800
Wire Wire Line
	6650 3800 6650 4450
Connection ~ 6650 4450
Wire Wire Line
	6650 4450 6650 5100
Connection ~ 6650 5100
Wire Wire Line
	6650 5100 6650 5750
Connection ~ 6650 5750
Wire Wire Line
	4400 5900 4450 5900
Wire Wire Line
	4450 5900 4450 6250
Text HLabel 5750 6200 2    50   Output ~ 0
_CLK2M
Text HLabel 5750 6850 2    50   Output ~ 0
_CLK16M
Text HLabel 5750 7550 2    50   Output ~ 0
_~RESETI
Text HLabel 4250 6150 0    50   Input ~ 0
CLK2M
Text HLabel 4250 6800 0    50   Input ~ 0
CLK16M
Text HLabel 4250 7500 0    50   Input ~ 0
~RESETI
Text HLabel 7750 5750 2    50   Input ~ 0
~ENABLE
Text HLabel 5800 4850 2    50   Output ~ 0
_REGA
Text HLabel 4850 4850 0    50   Input ~ 0
REGA
Wire Wire Line
	5200 4850 4850 4850
Text HLabel 4850 5500 0    50   Input ~ 0
REG8
Wire Wire Line
	5200 5500 4850 5500
Text HLabel 5800 5500 2    50   Output ~ 0
_REG8
Text HLabel 4850 4200 0    50   Input ~ 0
~OBJRAM-BANK-W
Wire Wire Line
	5200 4200 4850 4200
Text HLabel 5800 4200 2    50   Output ~ 0
_~OBJRAM-BANK-W
Text HLabel 5800 3550 2    50   Output ~ 0
_~EXPANSION-E
Text HLabel 4850 3550 0    50   Input ~ 0
~EXPANSION-E
Wire Wire Line
	5200 3550 4850 3550
Text HLabel 5800 2250 2    50   Output ~ 0
_~REGB-R
Text HLabel 5800 2900 2    50   Output ~ 0
_CBUS-DATA-DIR
Wire Wire Line
	5200 2900 4850 2900
Wire Wire Line
	5200 2250 4850 2250
Wire Wire Line
	5200 1600 4850 1600
Wire Wire Line
	5200 950  4850 950 
Text HLabel 4850 950  0    50   Input ~ 0
CBUS-ADDR-DIR
Text HLabel 4850 1600 0    50   Input ~ 0
~CBUS-ADDR-E
Text HLabel 4850 2250 0    50   Input ~ 0
~REGB-R
Text HLabel 4850 2900 0    50   Input ~ 0
CBUS-DATA-DIR
Text HLabel 5800 1600 2    50   Output ~ 0
_~CBUS-ADDR-E
Text HLabel 5800 950  2    50   Output ~ 0
_CBUS-ADDR-DIR
Text HLabel 4400 5900 0    50   Input ~ 0
ENABLE-CLK
$EndSCHEMATC
